<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }

        function menus(){
            $crud = $this->crud_function('','');  
            $crud->set_relation('menus_id','menus','nombre',array('menus_id'=>NULL));                 
            $output = $crud->render();
            $this->loadView($output);
        }

        function cookies(){
            $this->loadView(array(
                'view'=>'panel',
                'crud'=>'user',
                'output'=>$this->load->view('cookies',array(),TRUE)
            ));
        }

        function ajustes(){
            $crud = $this->crud_function('','');
            $crud->unset_add()->unset_delete()->unset_read()->unset_export()->unset_print();
            $crud->field_type('keywords','tags');
            $crud->columns('id');
            $crud->field_type('analytics','string')
                 ->field_type('cookies','string')
                 ->field_type('idiomas','tags')
                 ->set_field_upload('favicon','img')
                 ->set_field_upload('logo','img')
                 ->set_field_upload('logo_light','img')
                 ->set_field_upload('fondo','img')
                 ->field_type('mapa','tags')
                 ->display_as('idiomas','Idiomas (Separados por coma[,])')
                 ->display_as('mapa','Mapa (Formato[Lat, Lon])');
            $crud->callback_before_update(function($post,$primary){
                $this->db = get_instance()->db;
                $ajustes = $this->db->get('ajustes')->row();
                if($ajustes->precio_minimo!=$post['precio_minimo']){
                    get_instance()->ajustarPrecios($post);
                }
                if($ajustes->precio_maximo!=$post['precio_maximo']){
                    get_instance()->ajustarPrecios($post);
                }
                if($ajustes->capacidad_minima!=$post['capacidad_minima']){
                    get_instance()->ajustarPrecios($post);
                }
                if($ajustes->capacidad_maxima!=$post['capacidad_maxima']){
                    get_instance()->ajustarPrecios($post);
                }
            });
            $crud = $crud->render();
            $this->loadView($crud);
        } 

        function ajustarPrecios($post){
            $espacios = $this->db->get_where('venues_espacios');
            foreach($espacios->result() as $e){
                list($min,$max) = explode('-',$e->precio);
                $min = $min<$post['precio_minimo']?$post['precio_minimo']:$min;
                $max = $max>$post['precio_maximo']?$post['precio_maximo']:$max;
                $this->db->update('venues_espacios',array('precio'=>$min.'-'.$max),array('id'=>$e->id));

                list($min,$max) = explode('-',$e->capacidad);
                $min = $min<$post['capacidad_minima']?$post['capacidad_minima']:$min;
                $max = $max>$post['capacidad_maxima']?$post['capacidad_maxima']:$max;
                $this->db->update('venues_espacios',array('capacidad'=>$min.'-'.$max),array('id'=>$e->id));
            }
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}');
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{email}');                                 
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->set_subject('Usuario');
            $crud->field_type('status','true_false',array('0'=>'Inactivo','1'=>'Activo'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password');            
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            }            
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');            
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;

            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);                
                return $post;
            });
            $crud->add_action('Propiedades','',base_url('entradas/admin/venues_user').'/');
            $crud->add_action('Usar usuario','',base_url('seguridad/usar/').'/');
            $crud->columns('foto','nombre','apellido_paterno','email','status');
            $output = $crud->render();
            $output->title = 'Usuarios';
            $this->loadView($output);
        }

        function usar($id){
            //session_destroy();
            $this->user->login_short($id);
            redirect('panel');
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);
            /*$crud->fields('nombre','apellido','password','centro','curso','nro_grupo','foto');*/
            $crud->field_type('password','password')
                 ->field_type('admin','hidden')
                 ->field_type('status','hidden');
            $crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'300px','height'=>'300px'));
            $crud->fields('nombre','apellido_paterno','email','password');
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $crud->unset_back_to_list()->unset_list()->unset_add()->unset_delete()->unset_export()->unset_print()->unset_read();
            $output = $crud->render();            
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
