<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Backend extends Panel{
        function __construct() {
            parent::__construct();                     
        }

        public function loadView($param = array('view'=>'main'))
        {
            if($this->router->fetch_class()!=='registro' && empty($_SESSION['user']))
            {               
                header("Location:".base_url('registro/index/add'));
            }
            else{
                if($this->router->fetch_class()!=='registro' && !$this->user->hasAccess()){
                    throw  new Exception('<b>ERROR: 403<b/> No posees servicios suficientes para eralizar esta acción','403');
                }
                else{
                    if(!empty($param->output)){
                        $panel = 'panelUsuario';
                        $param->view = empty($param->view)?$panel:$param->view;
                        $param->crud = empty($param->crud)?'user':$param->crud;
                        $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    }
                    if(is_string($param)){
                        $param = array('view'=>$param);
                    }
                    $template = 'template';
                    $param = (object)$param;
                    $param->favicon = $this->db->get_where('ajustes')->row()->favicon;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                    $this->load->view($template,$param);
                }                
            }
        }
        
        function venues(){           
            $crud = $this->crud_function('','');    
            $crud->set_theme('mis-venues'); 
            $crud->where('user_id',$this->user->id);
            $crud->columns('foto','nombre','precio','personas','vistas','pausado','link');
            $crud->order_by('id','DESC');
            $crud->field_type('requiere_factura','hidden');  
            $crud->unset_back_to_list();          
            $crud->callback_column('foto',function($val,$row){
            	$this->db->join('venues_espacios','venues_espacios.id = venues_fotos.venues_espacios_id');
            	return @$this->db->get_where('venues_fotos',array('venues_id'=>$row->id))->row()->foto;
            });
            $crud->callback_column('precio',function($val,$row){            	
            	return @$this->db->get_where('venues_espacios',array('venues_id'=>$row->id))->row()->precio;
            });
            $crud->callback_column('personas',function($val,$row){            	
            	return @$this->db->get_where('venues_espacios',array('venues_id'=>$row->id))->row()->capacidad;
            });
            $crud->callback_column('link',function($val,$row){              
                return base_url('venue/'.toUrl($row->id.'-'.$row->nombre));
            });
            $crud->callback_before_update(function($post,$primary){
                if(empty($post['requiere_factura'])){
                    $post['requiere_factura'] = 0;
                }else{
                    //Enviar correo
                    $data = array();
                    $data['cliente'] = get_instance()->user->nombre.' '.get_instance()->user->apellido_paterno;
                    $data['importe'] = $this->elements->lastTransaccion($primary);
                    $data['importe'] = $data['importe']->num_rows()>0?$data['importe']->row()->importe:0;
                    $data['importe'] = '$'.number_format($data['importe'],0,'.',',').'MXN';
                    $data['fecha'] = date("d/m/Y");                
                    $data['razon_social'] = $post['razon_social'];
                    $data['rfc'] = $post['rfc'];
                    get_instance()->enviarcorreo((object)$data,21,get_instance()->ajustes->email_facturacion);                    
                }
                $amenidades = implode(',',$post['amenidades']);
                get_instance()->db->order_by('id','ASC');
                $espacio = get_instance()->db->get_where('venues_espacios',array('venues_id'=>$primary))->row()->id;
                get_instance()->db->update('venues_espacios',array('amenidades'=>$amenidades),array('id'=>$espacio));
                $post['amenidades'] = '';
                return $post;
            });
            $crud->field_type('pausado','true_false',array('1'=>'Pausado','0'=>'Activo'))
                 ->field_type('bloqueado','true_false',array('0'=>'NO','1'=>'SI'));            
            $crud = $crud->render('','application/modules/paginas/views/cruds/');            
            $this->loadView($crud);
        }

        function venues_espacios($x = '',$y = '',$z = ''){           
            $this->norequireds = array('amenidades');
            $crud = $this->crud_function('','');         
            $crud->set_theme('generic');     
            if(is_numeric($x)){
                $crud->where('venues_id',$x);
            }else{
                $crud->where('venues_id',-1);
            }
            if($crud->getParameters(FALSE)=='edit'){
                redirect('editar-espacios.html?key='.base64_encode($z));
            }
            if($crud->getParameters(FALSE)=='add'){
                redirect('add-espacios.html?key='.base64_encode($x));
            }
            $crud->callback_before_update(function($post){
                $post['amenidades'] = implode(',',$post['amenidades']);                
                return $post;
            });
            $crud->unset_back_to_list();
            $crud->columns('nombre','capacidad','precio')
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            $crud = $crud->render('','application/modules/paginas/views/cruds/');
        }
        function venues_fotos($x = '',$y = '',$z = ''){           
            $crud = $this->crud_function('',''); 
            if(is_numeric($x)){
                $crud->where('venues_espacios_id',$x);
            }else{
                $crud->where('venues_espacios_id',-1);
            }
            if($crud->getParameters(FALSE)=='add'){
                redirect('add-fotos.html?key='.base64_encode($x));
            }
            if($crud->getParameters(FALSE)=='edit'){
                redirect('editar-fotos.html?key='.base64_encode($z));
            }
            $crud->unset_back_to_list();
            $crud->set_lang_string('insert_success_message','Datos guardados con éxito <script>history.back();</script>');
            $crud->columns('foto','pie_foto')
                 ->field_type('foto','image',array('path'=>'img/venues','width'=>'1920','height'=>'949'))
                 ->unset_read()
                 ->unset_print()
                 ->unset_export();
            $crud = $crud->render();
        }


        function mensajes($key = ''){           
            $crud = $this->crud_function('','');    
            $crud->set_theme('mis-mensajes'); 
            $crud->where('user_id',$this->user->id);
            $crud->order_by('id','DESC');  
            $crud->columns('nombre','correo','mensaje');          
            $crud->where('venues_id',$key);
            $crud = $crud->render('','application/modules/paginas/views/cruds/');
            $this->loadView($crud);
        }

        function favoritos($x = ''){           
            if($x=='ajax_list' || $x=='ajax_list_info'){
                $this->as['favoritos'] = 'view_favoritos';
            }
            $crud = $this->crud_function('','');   
            if($crud->getParameters()=='list'){
                $crud->set_primary_key('id');
            }     
            $crud->set_theme('favoritos'); 
            if(!empty($_POST['tipos_venue_id'])){
                $crud->where('tipos_venue_id',$_POST['tipos_venue_id']);
            }
            if(!empty($_POST['tipos_venue_id2'])){
                $crud->where('tipos_venue_id',$_POST['tipos_venue_id2']);
            }
            if(!empty($_POST['lugar'])){
                $crud->where("(
                    venues.nombre like '%".$_POST['lugar']."%' OR
                    venues.estado like '%".$_POST['lugar']."%' OR
                    venues.ciudad like '%".$_POST['lugar']."%' OR
                    venues.colonia like '%".$_POST['lugar']."%'
                )",'ESCAPE',FALSE);
            }
            
            if(isset($_POST['precio_min']) && isset($_POST['precio_max'])){
                if(is_numeric($_POST['precio_min']) && is_numeric($_POST['precio_max'])){
                    $crud->where('(precio_minimo BETWEEN '.$_POST['precio_min'].' AND '.$_POST['precio_max'].' OR precio_maximo BETWEEN '.$_POST['precio_min'].' AND '.$_POST['precio_max'].')','ESCAPE',FALSE);
                }
            }  
            if(isset($_POST['capacidad_min']) && isset($_POST['capacidad_max'])){
                if(is_numeric($_POST['capacidad_min']) && is_numeric($_POST['capacidad_max'])){
                    $crud->where('(capacidad_minimo BETWEEN '.$_POST['capacidad_min'].' AND '.$_POST['capacidad_max'].' OR capacidad_maximo BETWEEN '.$_POST['capacidad_min'].' AND '.$_POST['capacidad_max'].')','ESCAPE',FALSE);
                }
            }    
            if(!empty($_POST['nombre']) && empty($_POST['d_estado']) && empty($_POST['d_ciudad']) && empty($_POST['d_colonia'])){
                $n = $_POST['nombre'];
                $crud->where('(view_favoritos.direccion like \'%'.$n.'%\' OR view_favoritos.nombre like \'%'.$n.'%\' OR d_ciudad like \'%'.$n.'%\' OR d_estado like \'%'.$n.'%\' OR d_colonia like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }
            
            if(!empty($_POST['d_estado'])){
                $n = $_POST['d_estado'];
                $crud->where('(d_estado like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }
            
            if(!empty($_POST['d_ciudad'])){
                $n = $_POST['d_ciudad'];
                $crud->where('(d_ciudad like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }
            
            if(!empty($_POST['d_colonia'])){
                $n = $_POST['d_colonia'];
                $crud->where('(d_colonia like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }       

            $crud->where('pausado = 0','ESCAPE',TRUE);
            $crud->where('bloqueado = 0','ESCAPE',TRUE);


            $crud->where('user_id',$this->user->id);              
            $crud->callbacK_before_insert(function($post){
                $this->db = get_instance()->db;
                $this->user = get_instance()->user;
                if($this->db->get_where('favoritos',array('user_id'=>$this->user->id,'venues_id'=>$post['venues_id']))->num_rows()>0){
                    $this->db->delete('favoritos',array('user_id'=>$this->user->id,'venues_id'=>$post['venues_id']));
                    echo '<textarea>{"success":false}</textarea>';
                    die();                    
                }
                $post['user_id'] = get_instance()->user->id;
                return $post;
            });
            $crud = $crud->render('','application/modules/paginas/views/cruds/');
            $this->loadView($crud);
        }

        function iniciarpausar(){
        	if(!empty($_POST['id']) && isset($_POST['pausado'])){
        		$pausado = $this->db->get_where('venues',array('id'=>$_POST['id'],'user_id'=>$this->user->id));
        		if($pausado->num_rows()>0){
        			$this->db->update('venues',array('pausado'=>$_POST['pausado']),array('id'=>$_POST['id']));
                    $accion = $_POST['pausado']==1?'pausada':'reactivada';
        			echo $this->success('<center>La publicación ha sido '.$accion.' <script>$(".filtering_form").submit();</script></center>');
        		}else{
        			echo $this->error('Cambio no autorizado');
        		}
        	}
        }

        function cancel($id){
            if(!empty($id) && is_numeric($id)){
                $pausado = $this->db->get_where('venues',array('id'=>$id,'user_id'=>$this->user->id));
                if($pausado->num_rows()>0){                    
                    //Validar forma de pago
                    if($pausado->row()->forma_pago==1){ //Conekta
                        $this->db->update('venues',array('status'=>0),array('id'=>$id));
                        redirect('venue-plan/'.$id.'?msj=success');
                    }else{
                        //Paypal, Eliminar membresia
                        $alias = $pausado->row()->token;
                        if($this->elements->cancelPaypalSubscription){
                            $this->db->update('venues',array('status'=>0),array('id'=>$id));
                            redirect('venue-plan/'.$id.'?msj=success');
                        }else{
                            redirect('venue-plan/'.$id.'?msj=fail');
                        }
                    }
                }else{
                    redirect('venue-plan/'.$id.'?msj=fail');
                }
            }
        }

        function change(){
            $this->form_validation->set_rules('venues_id','Venue','required')
                                  ->set_rules('membresias_id','Membresia','required');
            if($this->form_validation->run()){
                $venue = $this->db->get_where('venues',array('membresias_id'=>$_POST['membresias_id']))->row();
                $this->db->update('venues',array('membresias_id'=>$_POST['membresias_id'],'status'=>1),array('id'=>$_POST['venues_id']));
                $this->db->order_by('fecha','DESC');
                $last = $this->elements->transacciones(array('venues_id'=>$_POST['venues_id'],'estado_pago'=>2));    
                $membresia = $this->db->get_where('membresias',array('id'=>$_POST['membresias_id']))->row();
                $datos = (object)array(
                    'vencimiento'=>'',
                    'importe'=>'',
                    'membresia1'=>'',
                    'membresia2'=>''
                );
                if($last->num_rows()>0){
                    $datos = $last->row();  
                    $datos->membresia1 = $this->db->get_where('membresias',array('id'=>$venue->membresias_id))->row()->nombre;
                    $datos->membresia2 =  $membresia->nombre;                 
                    $datos->vencimiento = date("d/m/Y",strtotime($datos->vencimiento));
                    $datos->importe = moneda($membresia->precio,'$').'MXN';                    
                }else{
                    $datos->vencimiento = date("d/m/Y");
                    $datos->membresia1 = $this->db->get_where('venues',array('id'=>$venue->membresias_id))->row()->nombre;
                    $datos->membresia2 =  $membresia->nombre;                 
                    $datos->importe = moneda($membresia->precio,'$').'MXN';
                }

                $this->enviarcorreo($datos,22,$this->user->email);

                echo $this->success('Membresia cambiada con éxito  Hemos enviado los detalles del cambio de membresía a su correo electrónico: '.$this->user->email);
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }

        //funciones de testing, borrar luego
        function conekta(){
            $this->load->view('conekta');
        }

        function paypal(){
            $this->load->view('paypal');
        }   

        function view_misplanes(){
            $crud = $this->crud_function('','');    
            $crud->set_theme('membresias'); 
            $crud->where('user_id',$this->user->id)
                 ->set_primary_key('id')
                 ->unset_edit()->unset_delete()
                 ->unset_columns('nro_transaccion',$crud->encodeFieldName('user_id'),'fecha')
                 ->add_action('Ver más','',base_url('venue-plan').'/');
                 /*->add_action('<i class="fas fa-times cancel" title="cancelar membresia"></i>','',base_url('entradas/backend/cancel').'/')
                 ->add_action('<i class="fas fa-sync change" title="Cambiar membresia"></i>','',base_url('entradas/backend/change').'/');*/
            $crud->field_type('estado_pago','dropdown',array('-2'=>'Reembolsado','-1'=>'Pago rechazado','1'=>'En validación','2'=>'Procesado'));
            $crud->callback_column('estado_pago',function($val,$row){
                $valor = '';
                switch($val){
                    case '-2':
                        $valor = 'Reembolsado';
                    break;
                    case '-1':
                        $valor =  '<a href="javascript:void(0)" onclick="retry('.$row->nro_transaccion.')">Rechazado</a>';
                    break;
                    case '1':
                        $valor =  'En validación';
                    break;
                    case '2':
                        $valor =  'Procesado';
                    break;
                }

                // si esta desactivado se cambia su estado
                $desactivado = $this->db->get_where('venues',array('id'=>$row->id))->row()->status;
                if(!$desactivado){
                    $valor =  'Cancelado';
                }
                return $valor;
            });
            $crud->callback_column('Importe',function($val,$row){                
                return '$ '.number_format($val,2,'.',',').' MXN';
            });
            $crud->order_by('id','DESC');
            $crud = $crud->render('','application/modules/paginas/views/cruds/');
        }

        function venuesPlan($id){
            $this->loadView(array('view'=>'venuesPlan','id'=>$id));
        }

        function retryPay(){  
            $this->form_validation->set_rules('transaccion','Transacción','required|numeric')
                                  ->set_rules('token','Token','required');
            if($this->form_validation->run()){
                echo $this->elements->retryPay($_POST['transaccion'],$this->user->id,$_POST['token'])->msj;
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }

        function actualizar_forma_pago(){
            $this->form_validation->set_rules('venues_id','Venue','required');
            $this->form_validation->set_rules('forma','Forma de pago','required');
            if($this->form_validation->run()){
                $venue = $this->elements->venues(array('venues.id'=>$_POST['venues_id']),FALSE);
                if($venue->num_rows()>0){
                    $venue = $venue->row();
                    //Quitar membresia paypal
                    if($venue->forma_pago==2){
                        $this->elements->cancelPaypal($venue->token);
                    }
                    switch($_POST['forma']){
                        case '1'://Conekta
                            if($venue->forma_pago==2){//Si es paypal se cancela la membresia
                                $this->elements->cancelPaypalSubscription($venue->token);
                            }
                            $this->db->update('venues',array('forma_pago'=>1,'token'=>$_POST['token']),array('id'=>$_POST['venues_id']));
                            $this->elements->updateCard($this->user->id,$_POST['token']);
                        break;
                        case '2': //Paypal
                            $this->db->update('venues',array('forma_pago'=>2,'token'=>'Paypal Connect'),array('id'=>$_POST['venues_id']));
                            $this->load->view('paypal',$this->elements->getPayPalData($_POST['venues_id']));
                        break;
                    }
                    echo $this->success('Cambiado correctamente');
                }else{
                    echo $this->error('Venue no encontrado');
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }

    }
?>
