<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();                     
        }

        function deleteIMGWhatNotExistsInBd(){
            $files = scandir('img/venues');
            foreach($files as $n=>$v){
                if($v!='.' && $v!='..' && (strpos($v,'.jpg') || strpos($v,'.jpeg') || strpos($v,'.JPG') || strpos($v,'.png')) && $this->db->get_where('venues_fotos',array('foto'=>$v))->num_rows()==0){
                    unlink('img/venues/'.$v);
                }
            }
        }

        function tipos_venue(){           
            $crud = $this->crud_function('','');
            $crud->set_subject('Tipos de venues');
            $crud = $crud->render();
            $crud->title = 'Tipos de venues';
            $this->loadView($crud);
        }

        function soporte(){           
            $crud = $this->crud_function('','');
            $crud->set_subject('Ticket de soporte');
            $crud = $crud->render();
            $crud->title = 'Tickets de soporte';
            $this->loadView($crud);
        }

        function amenidades(){           
            $crud = $this->crud_function('',''); 

            $crud->field_type('icono','image',array('path'=>'img/amenidades','width'=>'100px','height'=>'100px','minSize'=>'100,100'));
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function venues($x = '',$y = ''){           
            $crud = $this->crud_function('','');  
            $crud->order_by('id','DESC');
            $crud->field_type('mapa','map',array('width'=>'100%','height'=>'250px'))
                 ->field_type('foto','image',array('path'=>'img/venues','width'=>'1920px','height'=>'949px'))
                 ->field_type('forma_pago','dropdown',array('1'=>'Conekta','2'=>'Paypal'))
                 ->field_type('pausado','true_false',array('1'=>'SI','0'=>'NO'))
                 ->field_type('bloqueado','true_false',array('0'=>'NO','1'=>'SI'))
                 ->field_type('requiere_factura','true_false',array('0'=>'NO','1'=>'SI'))                 
                 ->field_type('status','true_false',array('0'=>'Membresia cancelada','1'=>'Membresia activa'))
                 ->field_type('fecha_aprobacion','invisible')
                 ->display_as('estado','Estado')
                 ->display_as('ciudad','Delegación o municipio')
                 ->display_as('id','#Venue')
                 ->display_as('destacado','Popular')
                 ->set_relation('user_id','user','{email}')
                 ->set_relation('estado','estados','nombre') 
                 ->set_relation('ciudad','ciudades','nombre')
                 ->set_relation('colonia','colonias','nombre')                 
                 ->set_relation_dependency('ciudad','estado','estados_id')
                 ->set_relation_dependency('colonia','ciudad','ciudades_id')
                 ->columns('id','nombre','user_id','forma_pago','membresias_id','pausado','bloqueado','link','requiere_factura','ultimo_pago')
                 ->unset_searchs('ultimo_pago');
            $crud->set_no_using_ajax('colonia');
            $crud->callback_column('link',function($val,$row){
                return '<a href="'.base_url('venue/'.toURL($row->id.'-'.$row->nombre)).'" target="_new">VER</a>';
            });
            $crud->callback_column('ultimo_pago',function($val,$row){
                $this->db->order_by('id','DESC');
                $ultimo_pago = $this->db->get_where('transacciones',array('venues_id'=>$row->id));                
                if($ultimo_pago->num_rows()>0){
                    $ult = $ultimo_pago->row();
                    $estado = $ult->estado_pago==2?'Cobrado':'Pendiente';
                    return '<a href="'.base_url('entradas/admin/transacciones/edit/'.$ultimo_pago->row()->id).'">'.$estado.'</a>';
                }
                return 'N/A';
            });
            if($crud->getParameters()=='add'){
                $crud->field_type('user_id','hidden',$this->user->id);
                $crud->field_type('pausado','hidden',0);
                $crud->field_type('bloqueado','hidden',0);
            }
            $crud->unset_back_to_list();
            $crud->set_lang_string('insert_success_message','Venue cargado con éxito, redireccionando a datos adicionales <script>document.location.href="'.base_url().'entradas/admin/venues/edit/{id}";</script>');
            $crud->unset_fields('foto','codigo_descuento','token','forma_pago','membresias_id','vistas','codigo_promocion','fecha_vencimiento');
            $crud->disable_ftp();  

            $crud->callback_before_update(function($post,$primary){
                $amenidades = implode(',',$post['amenidades']);
                get_instance()->db->order_by('id','ASC');
                $espacio = get_instance()->db->get_where('venues_espacios',array('venues_id'=>$primary))->row()->id;
                get_instance()->db->update('venues_espacios',array('amenidades'=>$amenidades),array('id'=>$espacio));
                $post['amenidades'] = '';

                //Es primera autorización
                $venue = get_instance()->db->get_where('venues',array('id'=>$primary))->row();                
                if(empty($venue->fecha_aprobacion) && $post['bloqueado'] == 0){
                    get_instance()->db->update('venues',array('fecha_aprobacion'=>date("Y-m-d H:i:s")),array('id'=>$primary));
                }
                return $post;
            });

            $crud->callback_before_delete(function($primary){                
                foreach(get_instance()->db->get_where('venues_espacios',array('venues_id'=>$primary))->result() as $v){
                    get_instance()->db->delete('venues_fotos',array('venues_espacios_id'=>$v->id));
                }
                get_instance()->db->delete('venues_espacios',array('venues_id'=>$primary));
                get_instance()->deleteIMGWhatNotExistsInBd();
            });

            if(is_numeric($y)){
                get_instance()->y = $y;
            }

            $crud->callback_field('amenidades',function($val,$row){
              $val = !empty(get_instance()->y)?$this->elements->venues(array('venues.id'=>get_instance()->y),FALSE)->row()->amenidades:array();
              $amenidades = $this->db->get_where('amenidades');
              $data = '<select id="field-amenidades" multiple name="amenidades[]" class="form-control chosen-multiple-select">';
              foreach($amenidades->result() as $a){
                $selected = in_array($a->id,$val)?'selected':'';
                $data.= '<option value="'.$a->id.'" '.$selected.'>'.$a->nombre.'</option>';
              }
              $data.= '</select>';
              return $data;
            });

            $output = $crud->render();
            if($crud->getParameters()=='edit'){
                $output->espacios = $this->espacios($y);
                $output->js_files = array_merge($output->js_files,$output->espacios->js_files);
                $output->espacios = $output->espacios->output;
                $output->output = $this->load->view('editar-venues',array('output'=>$output->output,'espacios'=>$output->espacios),TRUE);
            }
            $this->loadView($output);
        }

        function espacios($x = '',$y = '',$z = ''){
            $this->as['venues'] = 'venues_espacios';
            $this->as['espacios'] = 'venues_espacios';
            $crud = $this->crud_function('','');
            $crud = $crud->where('venues_id',$x)
                         ->field_type('venues_id','hidden',$x);
            $crud->set_url('entradas/admin/espacios/'.$x.'/');             
            $crud->field_type('amenidades','hidden');
            $crud->callback_field('capacidad',function($val){
                    return '<input type="text" class="capacidadtag form-control" readonly value="'.$val.'"><input type="hidden" name="capacidad" id="field-capacidad" value="'.$val.'" class="capacidad">';})
                 ->callback_field('precio',function($val){            
                      $precio = explode('-',$val);
                      $precio = '$'.@number_format($precio[0],0,'.',',').'-$'.@number_format($precio[1],0,'.',',');
                      return '<input type="text" class="preciotag form-control" readonly value="'.$precio.'"><input type="hidden" name="precio" id="field-precio" value="'.$val.'" class="precio">';
                  });
            if($this->router->fetch_method()!='espacios'){
                $crud = $crud->render(1);
                return $crud;
            }else{                
                $output = $crud->render();
                if($crud->getParameters()=='edit'){
                    $output->fotos = $this->venues_fotos($z);
                    $output->css_files[] = base_url('assets/grocery_crud/css/jquery_plugins/chosen/chosen.css');
                    $output->js_files = array_merge($output->js_files,$output->fotos->js_files);
                    $output->js_files[] = base_url('assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js');
                    $output->js_files[] = base_url('assets/grocery_crud/js/jquery_plugins/config/jquery.chosen.config.js');
                    $output->fotos = $output->fotos->output;
                    $output->output = $this->load->view('editar-fotos',array('output'=>$output->output,'fotos'=>$output->fotos,'venue'=>$x),TRUE);
                }
                $this->loadView($output);
            }
        }

        function venues_fotos($x = '',$y = ''){
            $this->as['espacios'] = 'venues_fotos';
            $crud = $this->crud_function('','');
            $crud = $crud->where('venues_espacios_id',$x)
                         ->field_type('venues_espacios_id','hidden',$x);
            $crud->field_type('foto','image',array('path'=>'img/venues','width'=>'2100','height'=>'1406'));
            $crud->set_url('entradas/admin/venues_fotos/'.$x.'/');
            if($this->router->fetch_method()!='venues_fotos'){
                $crud = $crud->render(1);
                return $crud;
            }else{  
                $crud = $crud->render();
                $this->loadView($crud);
            }
        }

        function codigos_promocionales(){           
            $crud = $this->crud_function('','');               
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function tips(){           
            $crud = $this->crud_function('','');    
            $crud->field_type('paso','dropdown',array(
                '1'=>'Paso 1',
                '2'=>'Paso 2',
                '3'=>'Paso 3',
                '4'=>'Paso 4',
                '5'=>'Paso 5',
                '6'=>'Paso 6'
            ));        
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function membresias(){           
            $crud = $this->crud_function('',''); 
            $crud->display_as('accion','Texto en el botón');
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function categoria_venue(){           
            $crud = $this->crud_function('','');
            $crud->set_subject('Categoria de venues');                     
            $crud = $crud->render();
            $crud->title = 'Categorias de venues';
            $this->loadView($crud);
        }

        function transacciones(){           
            $crud = $this->crud_function('','');    
            $crud->order_by('id','DESC');     
            $crud->display_as('venues_id','Venue')
                 ->display_as('s9d5f52ea','Cliente')
                 ->display_as('j9d5f52ea.nombre','Cliente')
                 ->display_as('j83a76099.nombre','Venue');
            $crud->set_relation('venues_id','venues','{nombre}');   
            $crud->set_relation('j83a76099.user_id','user','{nombre} {apellido_paterno}');
            $crud->field_type('estado_pago','dropdown',array('-2'=>'Reembolsado','-1'=>'Pago rechazado','1'=>'En validación','2'=>'Procesado'));
            $crud->columns('id','j9d5f52ea.nombre','importe','j83a76099.nombre','fecha','estado_pago');
            $crud->callback_column('j9d5f52ea.nombre',function($val,$row){
                return $row->s9d5f52ea;
            });
            $crud->callback_column('j83a76099.nombre',function($val,$row){
                return $row->s83a76099;
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function venues_user($user = ''){
            $this->as['venues_user'] = 'venues';
            $crud = $this->crud_function('','');  
            $crud->fields('user_id','tipos_venue_id','pausado','bloqueado','destacado','categoria_venue_id')
                 ->where('user_id',$user)
                 ->field_type('user_id','hidden',$user)
                 ->columns('nombre','forma_pago','membresias_id','pausado','bloqueado','link');
            $crud->callback_column('link',function($val,$row){
                return '<a href="'.base_url('venue/'.toURL($row->id.'-'.$row->nombre)).'" target="_new">VER</a>';
            });

            $crud->set_subject('Venue');
            
            $crud->unset_back_to_list()
                 ->unset_add()
                 ->unset_print()
                 ->unset_export()
                 ->unset_delete();            
            $crud->disable_ftp();            
            $output = $crud->render();            
            $output->title = 'Venue por usuario';
            $this->loadView($output);        
        }

        function estados(){           
            $crud = $this->crud_function('','');
            $crud->order_by('nombre','ASC');
            $crud->group_by('nombre');
            $crud->columns('id','nombre');
            $crud->callback_column('nombre',function($val,$row){
                return '<a href="'.base_url('entradas/admin/ciudades/'.$row->id).'">'.$val.'</a>';
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function ciudades($x = ''){           
            $crud = $this->crud_function('','');
            $crud->order_by('nombre','ASC');
            $crud->group_by('nombre');
            $crud->columns('id','nombre')
                 ->field_type('estados_id','hidden',$x)
                 ->where('estados_id',$x);
            $crud->callback_column('nombre',function($val,$row){
                return '<a href="'.base_url('entradas/admin/colonias/'.$row->id).'">'.$val.'</a>';
            });
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function colonias($x = ''){           
            $crud = $this->crud_function('','');  
            $crud->order_by('nombre','ASC');
            $crud->group_by('nombre');
            $crud->columns('id','nombre')
                 ->field_type('ciudades_id','hidden',$x)
                 ->where('ciudades_id',$x);
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
