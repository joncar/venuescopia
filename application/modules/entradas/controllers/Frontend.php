<?php 
    require_once APPPATH.'controllers/Main.php';        
    class Frontend extends Main{
        protected $tipos_pago = array('1','2');            
        function __construct() {
            parent::__construct();       
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }

        function venues($x = '',$name = ''){            
            $crud = new $this->ajax_grocery_crud();
            $crud->set_theme('bootstrap2')
                 ->set_table('venues')
                 ->set_subject('venues');
            $crud->field_type('foto','image',array('path'=>'img/venues','width'=>'1920px','height'=>'949px'));
            $crud->set_rules('nombre','Nombre del venue','required|callback_validate_espacios');
            $crud->set_lang_string('insert_success_message','Sus datos han sido almacenados con éxito <script>ready();</script>');
            $crud->unset_back_to_list()
                 ->unset_list()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print();
            $crud->set_rules('correo_contacto','Correo de contacto','required|valid_email');
            $crud->set_rules('telefono_contacto','Teléfono','required');
            $crud->set_rules('descripcion','Descripción','required')
                 ->set_rules('tipos_venue_id','Tipo de venue','required')
                 ->set_rules('direccion','Dirección','required')
                 ->set_rules('membresias_id','Membresia','required');
            $crud->callback_before_insert(array($this,'binsert'));
            $crud->callback_after_insert(array($this,'ainsert'));
            $crud->set_rules('forma_pago','Forma de pago','required|callback_validate_token');
            $crud->required_fields_array();
            $crud = $crud->render();
        }

        function uploadPhotos(){
            $files = array();
            /*foreach($_FILES['fotos']['name'] as $n=>$v){
                $file = array();
                $file['name'] = array($v);
                $file['type'] = array($_FILES['fotos']['type'][$n]);
                $file['tmp_name'] = array($_FILES['fotos']['tmp_name'][$n]);
                $file['error'] = array($_FILES['fotos']['error'][$n]);
                $file['size'] = array($_FILES['fotos']['size'][$n]);
                $files[] = upload_file($file,'img/venues/');
            }*/
            $file = array();
            $file['name'] = array($_FILES['fotos']['name']);
            $file['type'] = array($_FILES['fotos']['type']);
            $file['tmp_name'] = array($_FILES['fotos']['tmp_name']);
            $file['error'] = array($_FILES['fotos']['error']);
            $file['size'] = array($_FILES['fotos']['size']);
            $files[] = upload_file($file,'img/venues/');
            echo json_encode($files);
        }

        function crop(){                          
            $name = 'slim';
            $images = Slim::getImages($name);            
            $path = 'img/venues/';
            $slash = substr($path,strlen($path)-1);
            if($slash!='/'){
                $path.= '/';
            }
            if ($images === false) {
                return Slim::outputJSON(array(
                    'status' => SlimStatus::FAILURE,
                    'message' => 'No data posted'
                ));
            }
            $image = array_shift($images);

            if (!isset($image)) {
                return Slim::outputJSON(array(
                    'status' => SlimStatus::FAILURE,
                    'message' => 'No images found'
                ));
                
            }            
            if (!isset($image['output']['data']) && !isset($image['input']['data'])) {                
                return Slim::outputJSON(array(
                    'status' => SlimStatus::FAILURE,
                    'message' => 'No image data'
                ));                
            }  

            if (isset($image['output']['data'])) {                
                $name = $image['output']['name'];                
                $data = $image['output']['data']; 
                if(!empty($image['meta']->before)){
                    $before = $image['meta']->before;
                    $before = explode('/',$before);
                    $before = $before[count($before)-1];
                }                            
                $output = Slim::saveFile($data, $name, $path,false);
            }            
            
            /*if (isset($image['input']['data'])) {
                $name = $image['input']['name'];
                $data = $image['input']['data'];
                $input = Slim::saveFile($data, $name);
            }*/
            
            $response = array(
                'status' => SlimStatus::SUCCESS
            );
            if (isset($output) && isset($input)) {
                $response['output'] = array(
                    'file' => $output['name'],
                    'path' => $output['path'],
                    'field'=>$name
                );
                $response['input'] = array(
                    'file' => $input['name'],
                    'path' => $input['path'],
                    'field'=>$name
                );
            }
            else {
                $response['file'] = isset($output) ? $output['name'] : $input['name'];
                $response['path'] = isset($output) ? $output['path'] : $input['path'];
                $response['field'] = $name;
            }
            echo json_encode($response);            
            die();
        }

        function validate_token(){

            if(in_array($_POST['forma_pago'],$this->tipos_pago)){
                switch($_POST['forma_pago']){
                    case '1': //Conekta
                        if(empty($_POST['token'])){
                            $this->form_validation->set_message('validate_token','Ocurrio un error al obtener token de conekta');
                            return false;
                        }
                    break;
                }
            }            
            return true;
        }

        function venues_list($x = '',$y = ''){                        

            $crud = new $this->ajax_grocery_crud();            
            if(!empty($_POST['tipos_venue_id'])){
                $crud->where('view_venues.tipos_venue_id',$_POST['tipos_venue_id']);
            }
            if(!empty($_POST['tipos_venue_id2'])){
                $crud->where('view_venues.tipos_venue_id',$_POST['tipos_venue_id2']);
            }
            if(!empty($_POST['lugar'])){
                $crud->where("(
                    venues.nombre like '%".$_POST['lugar']."%' OR
                    venues.estado like '%".$_POST['lugar']."%' OR
                    venues.ciudad like '%".$_POST['lugar']."%' OR
                    venues.colonia like '%".$_POST['lugar']."%'
                )",'ESCAPE',FALSE);
            }            
            if(isset($_POST['precio_min']) && isset($_POST['precio_max'])){
                if(is_numeric($_POST['precio_min']) && is_numeric($_POST['precio_max'])){
                    $crud->where('(precio_minimo BETWEEN '.$_POST['precio_min'].' AND '.$_POST['precio_max'].' OR precio_maximo BETWEEN '.$_POST['precio_min'].' AND '.$_POST['precio_max'].')','ESCAPE',FALSE);
                }
            }  
            if(isset($_POST['capacidad_min']) && isset($_POST['capacidad_max'])){
                if(is_numeric($_POST['capacidad_min']) && is_numeric($_POST['capacidad_max'])){
                    $crud->where('(capacidad_minimo BETWEEN '.$_POST['capacidad_min'].' AND '.$_POST['capacidad_max'].' OR capacidad_maximo BETWEEN '.$_POST['capacidad_min'].' AND '.$_POST['capacidad_max'].')','ESCAPE',FALSE);
                }
            }           
            if(!empty($_POST['nombre']) && empty($_POST['d_estado']) && empty($_POST['d_ciudad']) && empty($_POST['d_colonia'])){
                $n = $_POST['nombre'];
                $crud->where('(view_venues.tipoVenue = \''.$n.'\' OR view_venues.direccion like \'%'.$n.'%\' OR view_venues.nombre like \'%'.$n.'%\' OR d_ciudad like \'%'.$n.'%\' OR d_estado like \'%'.$n.'%\' OR d_colonia like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }
            
            if(!empty($_POST['d_estado'])){
                $n = $_POST['d_estado'];
                $crud->where('(d_estado like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }
            
            if(!empty($_POST['d_ciudad'])){
                $n = $_POST['d_ciudad'];
                $crud->where('(d_ciudad like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }
            
            if(!empty($_POST['d_colonia'])){
                $n = $_POST['d_colonia'];
                $crud->where('(d_colonia like \'%'.$n.'%\')','ESCAPE',FALSE);   
            }            
            

            if(!empty($x)){
                switch($x){
                    case 'populares':
                    $crud->set_table('view_populares');
                    $crud->where('view_populares.pausado = 0','ESCAPE',TRUE);
                    $crud->where('view_populares.bloqueado = 0','ESCAPE',TRUE);
                    break;
                    case 'categorias':
                    $crud->set_table('view_venues');
                    $categoria = $this->db->get_where('categoria_venue',array('nombre'=>$y));
                    if($categoria->num_rows()>0){
                        $crud->where('view_venues.categoria_venue_id',$categoria->row()->id);
                    }
                    $crud->where('view_venues.pausado = 0','ESCAPE',TRUE);
                    $crud->where('view_venues.bloqueado = 0','ESCAPE',TRUE);
                    break;
                    default:
                    $crud->set_table('view_venues');
                    $crud->where('view_venues.pausado = 0','ESCAPE',TRUE);
                    $crud->where('view_venues.bloqueado = 0','ESCAPE',TRUE);
                    break;
                }
                $_GET['tipo'] = $x;
            }else{
                $crud->set_table('view_venues');
            }

            if(!empty($_POST['ordenar'])){
                list($name,$value) = explode('-',$_POST['ordenar']);
                $crud->order_by('precio_minimo '.$value.', precio_maximo '.$value.'','ESCAPE');
            }


            $crud->set_theme('listado')
                 ->set_subject('venues')
                 ->set_primary_key('id');
            $crud->field_type('foto','image',array('path'=>'img/venues','width'=>'1920px','height'=>'949px'));
            $crud->set_rules('nombre','Nombre del venue','required|callback_validate_espacios');            
            $crud->unset_back_to_list()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_read()
                 ->unset_print();                        
            $crud = $crud->render('','application/modules/paginas/views/cruds/');
        }

        function validate_espacios(){            
            $response = true;
            $espacios = $_POST['espacios'];
            foreach($espacios as $n=>$e){
                $errorMessage = '';                
                $espacio = $n==0?'Venue principal':'espacio '.($n);
                if(empty($e['nombre'])){
                    $response = false;   
                    $errorMessage.= '<p>En el '.$espacio.' falta asignar el nombre</p>';                    
                }

                if(empty($e['descripcion'])){                    
                    $response = false;   
                    $errorMessage.= '<p>En el '.$espacio.' falta asignar la descripción</p>';   
                }

                if(empty($e['precio'])){
                    $response = false;   
                    $errorMessage.= '<p>En el '.$espacio.' falta asignar el precio</p>';       
                }

                if(empty($e['capacidad'])){
                    $response = false;   
                    $errorMessage.= '<p>En el '.$espacio.' falta asignar la capacidad</p>';         
                }                                   
                $this->form_validation->set_message('validate_espacios',$errorMessage);                
                if(!empty($e['fotos']) && count($e['fotos'])==0){
                    $response = false;
                    $this->form_validation->set_message('validate_espacios','Los espacios deben contener al menos una foto');
                }

                if(empty($_SESSION['user'])){
                    $response = false;
                    $this->form_validation->set_message('validate_espacios','Debes iniciar sesión para crear este venue');
                }
                if(!empty($_POST['codigo_promocion']) && $this->db->get_where('codigos_promocionales',array('codigo'=>$_POST['codigo_promocion']))->num_rows()==0){
                    $response = false;
                    $this->form_validation->set_message('validate_espacios','Código promocional no existe. Verifica que este bien escrito antes de continuar');
                }
                if(!empty($_POST['membresias_id']) && !empty($_POST['codigo_promocion']) && $this->db->get_where('codigos_promocionales',array('codigo'=>$_POST['codigo_promocion']))->num_rows()>0){
                    $promo = $this->db->get_where('codigos_promocionales',array('codigo'=>$_POST['codigo_promocion']))->row();
                    $membresia = $this->db->get_where('membresias',array('id'=>$_POST['membresias_id']));
                    if($membresia->num_rows()>0 && ($promo->primer_pago_gratis && $membresia->row()->dias_renovacion!=30)){
                        $response = false;
                        $this->form_validation->set_message('validate_espacios','Este código no es aplicable para esta membresia.');
                    }

                }
                if(!$response)return false;
            }
            return true;
            
        }

        function redimensionarFoto($origin,$jpgQuality=100)
        {
            $newWidth = $this->min_width_image;
            $newHeight = $this->min_height_image;
            $path = 'img/venues/';          
            $original = $origin;
            $destine =   'custom_'.$origin;
            $destino = $path.$destine;   
            $origin = $path.$original;         
            $datos=getimagesize($origin);
            if($datos[0]!=$newWidth || $datos[1]!=$newHeight)
            {                                
                if($datos[2]==1){
                    $img=imagecreatefromgif($origin);
                }
                if($datos[2]==2){
                    $img=imagecreatefromjpeg($origin);
                }
                if($datos[2]==3){
                    $img=imagecreatefrompng($origin);
                }
                /*if(rad2deg(atan($datos[0]/$datos[1]))>rad2deg(atan($newWidth/$newHeight)))
                {
                    $anchura=$newWidth;
                    $altura=round(($datos[1]*$newWidth)/$datos[0]);
                }else{
                    $altura=$newHeight;
                    $anchura=round(($datos[0]*$newHeight)/$datos[1]);
                }*/                
                $altura = $newHeight;
                $anchura = $newWidth;
                $newImage = imagecreatetruecolor($anchura,$altura);                
                imagecopyresampled($newImage, $img, 0, 0, 0, 0, $anchura, $altura, $datos[0], $datos[1]);                
                if($datos[2]==1){
                    imagegif($newImage,$destino);
                }
                if($datos[2]==2){
                    imagejpeg($newImage,$destino,$jpgQuality);
                }
                if($datos[2]==3){
                    imagepng($newImage,$destino);
                }
                imagedestroy($newImage);                
                return $destine;
            }
            return $original;
        }

        function binsert($post){
            $post['user_id'] = $this->user->id;
            $post['fecha_publicacion'] = date("Y-m-d H:i");
            $post['pausado'] = 0;
            $post['bloqueado'] = 1;
            $post['descripcion'] = nl2br($post['descripcion']);
            return $post;
        }

        function ainsert($post,$primary){
            //Almacenar espacios
            $espacios = $_POST['espacios'];
            foreach($espacios as $n=>$e){
                $fotos = !empty($e['fotos'])?$e['fotos']:array();
                $pies = !empty($e['pies_fotos'])?$e['pies_fotos']:array();
                unset($e['fotos']);
                unset($e['pies_fotos']);
                $e['venues_id'] = $primary;
                $e['amenidades'] = $n==0?implode(',',$e['amenidades']):'';
                $e['descripcion'] = nl2br($e['descripcion']);
                $this->db->insert('venues_espacios',$e);
                $id = $this->db->insert_id();
                //Almacenamos fotos
                foreach($fotos as $n=>$f){
                    $pie = !empty($pies[$n])?$pies[$n]:'';
                    if(is_json($f)){
                        $f = json_decode($f);
                        $f = $f->file;                        
                    }
                    $f = $this->redimensionarFoto($f);
                    $this->db->insert('venues_fotos',array(
                        'venues_espacios_id'=>$id,
                        'foto'=>$f,
                        'pie_foto'=>$pie
                    ));
                }
            }
            if($post['forma_pago']==1){
                //Cargar pago
                $this->cargarConekta($post,$primary);
            }

            unset($_SESSION['publicacion']);
        }   

        

        function venue($id){
            $id = explode('-',$id,2);
            if(is_numeric($id[0])){      
                $id = $id[0];
                $filtrar = !empty($_SESSION['user']) && $this->user->admin==1?FALSE:TRUE;
                $venue = $this->db->get_where('venues',array('id'=>$id));
                if($filtrar && $venue->num_rows()>0 && $venue->row()->bloqueado==1){
                    if(empty($_SESSION['user']) || $_SESSION['user']!=$venue->row()->user_id){
                        $this->loadView('venueBloqueado');
                    }else{
                        $this->loadView('venueBloqueado');

                    }                    
                }
                else{
                    $venue = $this->elements->venues(array('venues.id'=>$id),$filtrar);
                    if($venue->num_rows()>0){
                        $venue = $venue->row();
                        $this->db->update('venues',array('vistas'=>$venue->vistas+1),array('id'=>$venue->id));
                        $this->loadView(array(
                            'view'=>'detail',
                            'venue'=>$venue,
                            'title'=>$venue->nombre,
                            'foto'=>$venue->foto,
                            'descr'=>$venue->descripcion,
                            'url'=>$venue->link
                        ));                
                    }
                }
            }else{
                $this->loadView('venueBloqueado');
            }
        }

        function mensajes($x = ''){            
            $crud = new $this->ajax_grocery_crud();
            $crud->set_theme('bootstrap2')
                 ->set_table('mensajes')
                 ->set_subject('Mensajes');            
            $crud->required_fields_array();
            $crud->unset_back_to_list()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
            $crud->set_lang_string('insert_success_message','Tu mensaje ha sido enviado exitosamente');
            $crud->callback_after_insert(function($post,$primary){
                $venue = $this->db->get_where('venues',array('id'=>$post['venues_id']))->row();                
                unset($post['venues_id']);
                $d = '';
                foreach($post as $n=>$v){
                    $d.= '<p><b>'.$n.': </b>'.$v.'</p>';
                }
                $post['datos'] = $d;
                $post['venueNombre'] = $venue->nombre;
                get_instance()->enviarcorreo((object)$post,19,$venue->correo_contacto);
            });
            $crud = $crud->render();
        }

        function pagarPaypal($id){
            if($this->user->log){
                $post = $this->db->get_where('venues',array('venues.id'=>$id,'user_id'=>$this->user->id));
                if($post->num_rows()>0){
                    $post = (array)$post->row();
                    $membresia = $this->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();
                    $promomsj = '';
                    $descuento = 0;
                    if(!empty($post['codigo_promocion'])){
                        $promomsj.= 'Código de descuento aplicado <b>'.$post['codigo_promocion'].'</b>';
                        $promocion = $this->db->get_where('codigos_promocionales',array('codigo'=>$post['codigo_promocion'],'status'=>1))->row();                        
                        if(!empty($promocion->descuento_neto)){
                            $descuento+= $promocion->descuento_neto;
                            $promomsj.= '<br/>Descuento neto de <b>$'.number_format($promocion->descuento_neto,0,'.',',').'MXN</b>';
                        }
                        if(!empty($promocion->descuento_porcentaje)){
                            $descuento+= (($promocion->descuento_porcentaje * $membresia->precio)/100);
                            $promomsj.= '<br/>Descuento de <b>'.$promocion->descuento_porcentaje.'%</b>';
                        }
                        //$membresia->precio-= $descuento;
                        $descuento = $membresia->precio-$descuento;
                    }

                    if(!empty($promocion->primer_pago_gratis) && $membresia->dias_renovacion==30){
                        $descuento = -1;
                    }

                    switch($membresia->dias_renovacion){
                        case '30':
                            $interval = 'M';
                            $frec = 1;
                        break;
                        case '90':
                            $interval = 'M';
                            $frec = 3;
                        break;
                        case '365':
                            $interval = 'Y';
                            $frec = 1;
                        break;
                    }

                    $this->load->view('paypal',array('id'=>$id,'post'=>$post,'interval'=>$interval,'frec'=>$frec,'descuento'=>$descuento,'precio'=>$membresia->precio));


                }else{
                    throw new exception('Pagina no autorizada');
                }
            }else{
                throw new exception('Pagina no autorizada');
            }
        }

        protected function cargarConekta($post,$primary){            
            //Validamos informaciń de pago            
            $venue = $this->db->get_where('venues',array('venues.id'=>$primary))->row();
            $cliente = $this->db->get_where('user',array('user.id'=>$venue->user_id))->row();
            $membresia = $this->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();
            $promomsj = '';
            if(!empty($post['codigo_promocion'])){
                $promomsj.= 'Código de descuento aplicado <b>'.$post['codigo_promocion'].'</b>';
                $promocion = $this->db->get_where('codigos_promocionales',array('codigo'=>$post['codigo_promocion'],'status'=>1))->row();
                $descuento = 0;
                if(!empty($promocion->descuento_neto)){
                    $descuento+= $promocion->descuento_neto;
                    $promomsj.= '<br/>Descuento neto de <b>$'.number_format($promocion->descuento_neto,0,'.',',').'MXN</b>';
                }
                if(!empty($promocion->descuento_porcentaje)){
                    $descuento+= (($promocion->descuento_porcentaje * $membresia->precio)/100);
                    $promomsj.= '<br/>Descuento de <b>'.$promocion->descuento_porcentaje.'%</b>';
                }
                $membresia->precio-= $descuento;
                $membresia->precio = $membresia->precio>=0?$membresia->precio:0;
            }
            $membresia->precio = str_replace(',','.',$membresia->precio);
            $primerGratis = true;
            if(empty($promocion->primer_pago_gratis) && $membresia->dias_renovacion==30 && $membresia->precio>0){                
                $primerGratis = false;
            }
            $imp = $primerGratis?'0':$membresia->precio;
            $this->db->insert('transacciones',array(
                'venues_id'=>$primary,
                'estado_pago'=>1,
                'fecha'=>date("Y-m-d H:i"),
                'vencimiento'=>date("Y-m-d",strtotime(date("Y-m-d").' +'.$membresia->dias_renovacion.' days')),
                'importe'=>$imp,
                'descripcion'=>'Publicación y suscripción del venue',
                'forma_pago'=>1,
                'user_id'=>$this->user->id
            ));
            $pago = $this->db->insert_id();

            require_once(APPPATH.'libraries/conekta/Conekta.php');
            \Conekta\Conekta::setLocale('es');
            try {
                //Creamos cliente
                $this->load->config('conekta');
                \Conekta\Conekta::setApiKey($this->config->item('conekta_private'));                
                if(empty($this->user->token_conekta)){
                    $customer = \Conekta\Customer::create(
                      array(
                        'name'  => $this->user->nombre,
                        'email' => $this->user->email,                
                        'payment_sources' => array(array(
                            'token_id' => $post['token'],
                            'type' => "card"
                        ))
                      )
                    );                
                    $this->db->update('user',array('token_conekta'=>$customer->id),array('id'=>$this->user->id));
                    $this->user->login_short($this->user->id);
                }else{
                    $customer = $this->user->token_conekta;
                    $customer = \Conekta\Customer::find($customer);                                
                    if(count($customer->payment_sources)>0){
                        $customer->payment_sources[0]->delete();
                    }                                
                    $customer->createPaymentSource(array('token_id' => $post['token'],'type' => "card"));
                }
                $primerGratis = true;
                //Crear primer cargo
                if(empty($promocion->primer_pago_gratis) && $membresia->dias_renovacion>=30 && $membresia->precio>0){
                    $charge = \Conekta\Order::create(array(
                      'currency' => 'MXN',
                      'customer_info' => array(
                        'customer_id' => $customer->id
                      ),
                      'line_items' => array(
                        array(
                          'name' => 'Publicación y suscripción del venue',
                          'unit_price' => $membresia->precio*100,
                          'quantity' => 1
                        )
                      ),
                      'charges' => array(
                        array(
                          'payment_method' => array(
                            'type' => 'default'
                          )
                        )
                      )
                    ));
                    $primerGratis = false;
                }

                $this->db->update('venues',array('conekta_plan_id'=>"Venue-id-".$primary,'bloqueado'=>1,'pausado'=>0),array('id'=>$primary));
                $this->db->update('transacciones',array('estado_pago'=>2),array('id'=>$pago));                
                $transaccion = $this->db->get_where('transacciones',array('id'=>$pago))->row();
                $post = array();
                $post['cliente'] = $cliente->nombre.' '.$cliente->apellido_paterno;
                $post['importe'] = $primerGratis?'$0 MXN':'$'.$transaccion->importe.'MXN';
                $post['fecha'] = date("d/m/Y");   
                $post['espacios'] = $this->db->get_where('venues_espacios',array('venues_id'=>$venue->id))->num_rows();
                $post['espacios'] = $post['espacios']>0?$post['espacios']-1:0;
                $post['espacios'] = (string)$post['espacios'];
                $post['renovacion'] = date("d/m/Y",strtotime('+'.$membresia->dias_renovacion.' days'));
                $post['publicacion'] = 'desde '.date("d/m/Y",strtotime('+1 days')).' hasta '.date("d/m/Y",strtotime('+'.$membresia->dias_renovacion.' days'));
                if($venue->requiere_factura==1){ 
                    $post['razon_social'] = $venue->razon_social;
                    $post['rfc'] = $venue->rfc;
                    $post['email_facturacion'] = $venue->email_facturacion;                                        
                }else{
                    $post['razon_social'] = '';
                    $post['rfc'] = '';
                    $post['email_facturacion'] = '';                    
                }
                $post['medio_pago'] = 'CONEKTA';
                $post['requiere_factura'] = $venue->requiere_factura==1?'SI':'NO';
                $post['nombre'] = $venue->nombre;
                $post['ciudad'] = $venue->ciudad;
                $post['colonia'] = $venue->colonia;
                $post['direccion'] = $venue->direccion;
                $post['link'] = base_url('entradas/admin/venues/edit/'.$venue->id);
                $post['link'] = '<a href="'.$post['link'].'">'.$post['link'].'</a>';
                $post['membresia'] = $membresia->nombre;
                $post['promo'] = $promomsj;
                get_instance()->enviarcorreo((object)$post,21,$this->ajustes->email_facturacion);
                get_instance()->enviarcorreo((object)$post,13,$this->ajustes->email_nuevo_venue);              
                get_instance()->enviarcorreo((object)$post,18,$this->user->email);              
            }catch (\Conekta\Handler $error) {
              //Normal object methods     
              if($error->code == 'conekta.errors.resource_not_found.entity'){                
                $this->db->update('user',array('token_conekta'=>''),array('id'=>$_SESSION['id']));
                $this->user->login_short($_SESSION['user']);
                return $this->cargarConekta($post,$primary);
              }else{
                  $this->db->update('transacciones',array('estado_pago'=>-1),array('id'=>$pago));
                  get_instance()->enviarcorreo((object)array('mensajeconekta'=>$error->getMessage()),17,$this->user->email);                            
                  $this->reverse($error->getMessage(),$primary);
                  return false;
              }
            }        
        }

        function reverse($msj,$primary){
          @ob_end_clean();
          echo '<textarea>'.json_encode(array('success'=>false,'error_message'=>$msj)).'</textarea>';            
          $this->db->delete('venues_espacios',array('venues_id'=>$primary));
          $this->db->delete('venues',array('venues.id'=>$primary));
          die();
        }

        function procesarPagoPaypal($id = ''){
            correo('joncar.c@gmail.com','TEST '.$id,print_r($_POST,TRUE));
            $venue = $this->db->get_where('venues',array('venues.id'=>$id));   
            //$_POST = array('payment_status'=>'Completed','verify_sign'=>'123','subscr_id'=>'I-121212');
            //$_POST = array ('txn_type' => 'subscr_signup','subscr_id' => 'I-5C3B01BG6398','last_name' => 'Pruebas','residence_country' => 'MX','item_name' => 'VEN#17 - 1ROGRATIS','mc_currency' => 'MXN','business' => 'venuescopia-facilitator@gmail.com','recurring' => '1','verify_sign' => 'ANPsreXgeICgiL6VVahL10wqChREA2cvXFjuVTfaZBPWJRoPWenmM8Tn','payer_status' => 'verified','test_ipn' => '1','payer_email' => 'test@venuescopia.com','first_name' => 'Andrs Test','receiver_email' => 'venuescopia-facilitator@gmail.com','payer_id' => 'DC382BMLLLDVJ','reattempt' => '1','subscr_date' => '13:08:32 Oct 09, 2019 PDT','charset' => 'windows-1252','notify_version' => '3.9','period1' => '1 M','mc_amount1' => '0.00','period3' => '1 M','mc_amount3' => '899.00','ipn_track_id' => '66c3417780a5');
            if($venue->num_rows()>0 && !empty($_POST)){
                $post = $_POST;
                $primary = $id;
                $venue = $venue->row();
                $user = $this->db->get_where('user',array('id'=>$venue->user_id))->row();
                $membresia = $this->db->get_where('membresias',array('id'=>$venue->membresias_id))->row();
                $dias_renovacion = $membresia->dias_renovacion;
                $descuento = $membresia->precio;
                $promomsj = '';
                if(!empty($venue->codigo_promocion)){
                    $promomsj.= 'Código de descuento aplicado <b>'.$venue->codigo_promocion.'</b>';
                    $promocion = $this->db->get_where('codigos_promocionales',array('codigo'=>$venue->codigo_promocion,'status'=>1))->row();
                    $descuento = 0;
                    if(!empty($promocion->descuento_neto)){
                        $descuento+= $promocion->descuento_neto;
                        $promomsj.= '<br/>Descuento neto de <b>$'.number_format($promocion->descuento_neto,0,'.',',').'MXN</b>';
                    }
                    if(!empty($promocion->descuento_porcentaje)){
                        $descuento+= (($promocion->descuento_porcentaje * $membresia->precio)/100);
                        $promomsj.= '<br/>Descuento de <b>'.$promocion->descuento_porcentaje.'%</b>';
                    }
                    $membresia->precio-= $descuento;
                }
                $membresia->precio = str_replace(',','.',$membresia->precio);
                echo $descuento;
                $primerGratis = true;
                if(empty($promocion->primer_pago_gratis) && $membresia->dias_renovacion==30 && $membresia->precio>0){
                    $estado = $post['payment_status']=='Completed'?2:-1;
                    $estado = $post['payment_status']=='Refunded'?-2:$estado;
                    $primerGratis = false;
                }else{
                    $estado = 2;
                }
                if($venue->bloqueado==1){
                    $this->db->update('venues',array('bloqueado'=>1,'pausado'=>0,'token'=>$post['subscr_id']),array('id'=>$id));
                }else{
                    $this->db->update('venues',array('token'=>$post['subscr_id']),array('id'=>$id));
                }
                $membresia->precio = str_replace(',','.',$membresia->precio);
                $imp = $primerGratis?'0':$membresia->precio;
                $this->db->insert('transacciones',array(
                    'venues_id'=>$primary,
                    'estado_pago'=>$estado,
                    'fecha'=>date("Y-m-d H:i"),
                    'vencimiento'=>date("Y-m-d",strtotime(date("Y-m-d").' +'.$dias_renovacion.' days')),
                    'importe'=>$imp,
                    'descripcion'=>'Publicación y suscripción del venue',
                    'forma_pago'=>2,
                    'user_id'=>$venue->user_id,
                ));
                if($estado==2){
                    $post = array();
                    $post['cliente'] = $user->nombre.' '.$user->apellido_paterno;
                    $post['importe'] = $primerGratis?'$0 MXN':'$'.number_format($membresia->precio,2).'MXN';
                    $post['fecha'] = date("d/m/Y"); 
                    $post['espacios'] = $this->db->get_where('venues_espacios',array('venues_id'=>$venue->id))->num_rows();
                    $post['espacios'] = $post['espacios']>0?$post['espacios']-1:0;
                    $post['espacios'] = (string)$post['espacios'];
                    $post['renovacion'] = date("d/m/Y",strtotime('+'.$membresia->dias_renovacion.' days'));
                    $post['publicacion'] = 'desde '.date("d/m/Y",strtotime('+1 days')).' hasta '.date("d/m/Y",strtotime('+'.$membresia->dias_renovacion.' days'));

                    if($venue->requiere_factura==1){ 
                        $post['razon_social'] = $venue->razon_social;
                        $post['rfc'] = $venue->rfc;
                        $post['email_facturacion'] = $venue->email_facturacion;                    
                    }else{
                        $post['razon_social'] = '';
                        $post['rfc'] = '';
                        $post['email_facturacion'] = '';
                    }
                    $post['medio_pago'] = 'PAYPAL';
                    $post['requiere_factura'] = $venue->requiere_factura==1?'SI':'NO';
                    $post['nombre'] = $venue->nombre;
                    $post['ciudad'] = $venue->ciudad;
                    $post['colonia'] = $venue->colonia;
                    $post['direccion'] = $venue->direccion;
                    $post['link'] = base_url('entradas/admin/venues/edit/'.$venue->id);
                    $post['link'] = '<a href="'.$post['link'].'">'.$post['link'].'</a>';
                    $post['promo'] = $promomsj;
                    $post['membresia'] = $membresia->nombre;
                    get_instance()->enviarcorreo((object)$post,21,$this->ajustes->email_facturacion);                    
                    if(get_instance()->db->get_where('transacciones',array('venues_id'=>$venue->id,'estado_pago'=>2))->num_rows()==0){
                        get_instance()->enviarcorreo((object)$post,13,$this->ajustes->email_nuevo_venue);              
                    }
                    get_instance()->enviarcorreo((object)$post,18,$user->email);    
                }
            }
        }

        function soporte(){                       
            $crud = new ajax_grocery_crud();
            $crud->set_table('soporte')
                 ->set_theme('bootstrap2')
                 ->set_subject('Soporte');
            $crud->required_fields_array();
            $crud->unset_back_to_list();
            $crud->set_subject('TIcket de soporte');
            $crud->set_lang_string('insert_success_message','<script>document.location.href="'.base_url('contacto-envio.html').'"</script>');
            $crud->callback_after_insert(function($post,$primary){
                $post['extras'] = '';
                foreach($post as $n=>$v){
                    $post['extras'].= '<p class="MsoNormal"><span style="font-family: arial, helvetica, sans-serif;" data-mce-style="font-family: arial, helvetica, sans-serif;"><strong><span style="color: #808080;" data-mce-style="color: #808080;">'.$n.':</span></strong> '.$v.'</span></p>';                            
                }
                $post['asunto'] = 'Duda solicitada desde el apartado de FAQ';
                $post['message'] = $post['mensaje'];
                $email = get_instance()->ajustes->email_contacto;
                get_instance()->enviarcorreo((object)$post,1,$email);
            });
            $crud = $crud->render();
            $crud->title = 'Tickets de soporte';
            $this->loadView($crud);
        }

        function lista($tipo = '',$value = ''){
            $page = $this->load->view('es/venues',array(
                'tipo'=>$tipo,
                'value'=>$value
            ),TRUE,'paginas');
            $title = empty($value)?$tipo:$value;
            $title = ucfirst($title);
            $this->loadView(array(
                'view'=>'read',
                'page'=>$page,
                'module'=>'paginas',
                'title'=>'Lista de propiedades '.$title
            ));
        }

        function conektaConnet(){
            $body = @file_get_contents('php://input');            
            http_response_code(200); // Return 200 OK
            correo("joncar.c@gmail.com","Pago confirmado",$body);

        }

        function solicitud_telefono($venue){
            if(is_numeric($venue)){
                $venue = $this->db->get_where('venues',array('id'=>$venue));
                if($venue->num_rows()>0){
                    $venue = $venue->row();
                    $this->db->update('venues',array('solicitud_telefono'=>($venue->solicitud_telefono+1)),array('id'=>$venue->id));
                }
            }
        }

        function venues_search($x = ''){                        
            if(!empty($_POST['q'])){
                $q = $_POST['q'];                
                $this->db->limit(5);
                $this->db->where("nombre like '".$q."%'",NULL,FALSE);
                //$this->db->like('nombre',$q);                
                $this->db->order_by('prioridad','ASC');
                $venues = $this->db->get('view_buscador');
                $result = array();
                foreach($venues->result() as $r){
                    $result[] = array('text'=>$r->nombre,'link'=>$r->url);
                }
                echo json_encode($result);
            }
        }

        function venues_search_favoritos($x = ''){
            if(!empty($_POST['q'])){
                $q = $_POST['q'];                
                $this->db->limit(5);
                $this->db->where("nombre like '".$q."%'",NULL,FALSE);
                //$this->db->like('nombre',$q);                
                $this->db->order_by('prioridad','ASC');
                $venues = $this->db->get('view_buscador');
                $result = array();
                foreach($venues->result() as $r){
                    $r->url = str_replace('venues.html','favoritos.html',$r->url);
                    $result[] = array('text'=>$r->nombre,'link'=>$r->url);
                }
                echo json_encode($result);
            }
        }
        
         function estado_search($x = ''){                        
            if(!empty($_POST['q'])){
                $q = $_POST['q'];                
                $this->db->limit(5);
                $this->db->where("d_estado like '".$q."%'",NULL,FALSE);
                if(!empty($_POST['d_colonia'])){
                    $this->db->where('d_colonia',$_POST['d_colonia']);
                }
                if(!empty($_POST['d_ciudad'])){
                    $this->db->where('d_ciudad',$_POST['d_ciudad']);
                }
                $this->db->group_by('view_venues.d_estado');
                $venues = $this->db->get('view_venues');                
                $result = array();
                foreach($venues->result() as $r){
                    $result[] = array('text'=>$r->d_estado,'link'=>'javascript:selEstado(\''.$r->d_estado.'\')');
                }
                echo json_encode($result);
            }
        }
        
        function ciudad_search($x = ''){                        
            if(!empty($_POST['q']) || !empty($_POST['d_estado'])){
                $q = $_POST['q'];                
                $this->db->limit(5);
                $this->db->where("d_ciudad like '".$q."%'",NULL,FALSE);
                if(!empty($_POST['d_estado'])){
                    $this->db->where('d_estado',$_POST['d_estado']);
                }
                if(!empty($_POST['d_colonia'])){
                    $this->db->where('d_colonia',$_POST['d_colonia']);
                }
                $this->db->group_by('view_venues.d_ciudad');
                $venues = $this->db->get('view_venues');                
                $result = array();
                foreach($venues->result() as $r){
                    $result[] = array('text'=>$r->d_ciudad,'link'=>'javascript:selCiudad(\''.$r->d_ciudad.'\')');
                }
                echo json_encode($result);
            }
        }
        
        function colonia_search($x = ''){                        
            if(!empty($_POST['q']) || !empty($_POST['d_ciudad'])){
                $q = $_POST['q'];                
                $this->db->limit(5);
                $this->db->where("d_colonia like '".$q."%'",NULL,FALSE);
                if(!empty($_POST['d_estado'])){
                    $this->db->where('d_estado',$_POST['d_estado']);
                }
                if(!empty($_POST['d_ciudad'])){
                    $this->db->where('d_ciudad',$_POST['d_ciudad']);
                }
                $this->db->group_by('view_venues.d_colonia');
                $venues = $this->db->get('view_venues');                
                $result = array();
                foreach($venues->result() as $r){
                    $result[] = array('text'=>$r->d_colonia,'link'=>'javascript:selColonia(\''.$r->d_colonia.'\')');
                }
                echo json_encode($result);
            }
        }

        function validarCupon(){
            $response = array('success'=>false,'msj'=>'Cupón no encontrado o no disponible para esta cuenta');
            $this->form_validation->set_rules('cupon','CUPON','required');
            if($this->form_validation->run()){
                $cupon = $_POST['cupon'];
                $cupon = base64_decode($cupon);
                $cupon = $this->db->get_where('codigos_promocionales',array('codigo'=>$cupon,'status'=>1));
                if($cupon->num_rows()>0){
                    $cupon = $cupon->row();
                    $response = array('success'=>true,'type1'=>$cupon->primer_pago_gratis,'type2'=>$cupon->descuento_porcentaje,'type3'=>$cupon->descuento_neto);
                    echo json_encode($response);
                }else{
                    echo json_encode($response);
                }
            }else{
                echo json_encode($response);
            }
        }

        function cachearPublicacion(){
            if(!empty($_GET)){
                if(!isset($_GET['clear'])){
                    $_SESSION['publicacion'] = $_GET;
                }else{
                    unset($_SESSION['publicacion']);
                }
            }
            
        }
        
        function getDataEstado(){
            if(!empty($_POST['q'])){
                $q = $_POST['q'];
                $this->db->limit(5);
                $this->db->where("TRIM(nombre) like '".$q."%'",NULL,FALSE);
                $this->db->group_by('estados.id');
                $venues = $this->db->get('estados');                
                $result = array();
                foreach($venues->result() as $r){
                    $result[] = array('text'=>$r->nombre,'link'=>'javascript:selEstado(\''.$r->nombre.'\',\''.$r->id.'\')');
                }
                echo json_encode($result);
            }
        }
        
        function getDataCiudad(){
            if(!empty($_POST['q']) || !empty($_POST['estado'])){                
                $this->db->limit(5);
                if(!empty($_POST['q'])){
                    $q = $_POST['q'];                
                    $this->db->where("TRIM(nombre) like '".$q."%'",NULL,FALSE);
                }
                if(!empty($_POST['estado'])){
                    $this->db->where('estados_id',$_POST['estado']);
                }
                $this->db->group_by('ciudades.id');
                $venues = $this->db->get('ciudades');                
                $result = array();
                foreach($venues->result() as $r){
                    $result[] = array('text'=>$r->nombre,'link'=>'javascript:selCiudad(\''.$r->nombre.'\',\''.$r->id.'\')');
                }
                echo json_encode($result);
            }
        }
        
        function getDataColonia(){
            if(!empty($_POST['q']) || !empty($_POST['ciudad'])){                
                $this->db->limit(5);
                if(!empty($_POST['q'])){
                    $q = $_POST['q'];                
                    $this->db->where("TRIM(nombre) like '".$q."%'",NULL,FALSE);
                }
                if(!empty($_POST['ciudad'])){
                    $this->db->where('ciudades_id',$_POST['ciudad']);
                }
                $this->db->group_by('colonias.nombre');
                $venues = $this->db->get('colonias');
                $result = array();
                foreach($venues->result() as $r){
                    $result[] = array('text'=>$r->nombre,'link'=>'javascript:selColonia(\''.$r->nombre.'\',\''.$r->id.'\')');
                }
                echo json_encode($result);
            }
        }
        
        function getSelectRegion($name,$table,$relationName,$relation){
            $this->db->where($relationName,$relation);
            $this->db->order_by('nombre','ASC');
            echo form_dropdown_from_query($name,$table,'id','nombre','','id="'.$name.'"',TRUE,'','Seleccione una '.$name);
        }
        
    }
?>
