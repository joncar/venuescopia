<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Cron extends Main{
        protected $tipos_pago = array('1','2');
        function __construct() {
            parent::__construct();
        }

        function cobranza(){
            //correo('joncar.c@gmail.com','Cobranza','Cobranza Crontab');
        	$venues_vencidos = $this->db->get_where('transacciones',array('vencimiento'=>date("Y-m-d")));
        	foreach($venues_vencidos->result() as $v){
        		$this->db->order_by('id','DESC');
        		$ultimo_cobro = $this->db->get_where('transacciones',array('venues_id'=>$v->venues_id,'estado_pago'=>2));
        		if($ultimo_cobro->num_rows()>0){
        			if(strtotime($ultimo_cobro->row()->vencimiento)<=time() && $ultimo_cobro->row()->forma_pago==1){
        				//Bloquear venue y notificar que se debe cobrar
                        $this->db->order_by('id','DESC');
                        $ultimo_cobro = $this->db->get_where('transacciones',array('venues_id'=>$v->venues_id,'forma_pago'=>1));
        				if($ultimo_cobro->num_rows()==0 || ($ultimo_cobro->num_rows() > 0 && $ultimo_cobro->row()->estado_pago!=-1 && strtotime($ultimo_cobro->row()->vencimiento)<=time())){
                            $this->cobrar($v->venues_id,$v->importe);
                        }
        			}elseif(strtotime($ultimo_cobro->row()->vencimiento)<=time() && $ultimo_cobro->row()->forma_pago==2){
                        //Si el ultimo cobro fue por paypal se bloquea hasta que se notifique por IPN
                        $this->db->update('venues',array('bloqueado'=>1),array('id'=>$v->venues_id));
                    }
        		}else{
                    $this->db->order_by('vencimiento','DESC');
                    $ultimo_cobro = $this->db->get_where('transacciones',array('venues_id'=>$v->venues_id,'estado_pago'=>-1,'forma_pago'=>1));
        			//Bloquear venue y notificar que se debe cobrar
                    if($ultimo_cobro->num_rows()==0){
        			     $this->cobrar($v->venues_id,$v->importe);
                    }
        		}
        	}
        }

        function cobrar($venue,$importe){
        	//Se bloquea el venue hasta recibir el pago
        	$this->db->update('venues',array('bloqueado'=>1),array('id'=>$venue));
        	$venue = $this->elements->venues(array('venues.id'=>$venue),FALSE)->row();
        	if($venue->status==1){ //Se verifica que la membresia del venue no este cancelada
                $user = $this->db->get_where('user',array('id'=>$venue->user_id))->row();
            	$membresia = $this->db->get_where('membresias',array('id'=>$venue->membresias_id))->row();
            	$yaaperturo = $this->db->get_where('transacciones',array('user_id'=>$user->id,'fecha'=>date("Y-m-d"),'estado_pago'=>1));
            	if($yaaperturo->num_rows()==0){
    	        	$this->db->insert('transacciones',array(
    	        		'venues_id'=>$venue->id,
    	        		'fecha'=>date("Y-m-d"),
    	        		'vencimiento'=>date("Y-m-d",strtotime(date("Y-m-d").' +'.$membresia->dias_renovacion.' days')),
    	        		'estado_pago'=>1,
    	        		'importe'=>$importe,
    	        		'descripcion'=>'Renovación de venue '.$venue->nombre.' '.date("m-Y"),
    	        		'forma_pago'=>'1',
    	        		'user_id'=>$user->id        		
    	        	));
    	        	$transaccion = $this->db->insert_id();
            	}else{
            		$transaccion = $yaaperturo->row()->id;
            	}
            	if(empty($user->token_conekta)){
            		//Se notifica que no se pudo hacer el cobro        		
            		$post = (object)array('nombre'=>$venue->nombre);
            		$this->enviarcorreo($post,20,$user->email);
            	}else{                
            		$response = $this->elements->retryPay($transaccion,$user->id);        		
            	}        	
            }
        	
        }
    }
?>
