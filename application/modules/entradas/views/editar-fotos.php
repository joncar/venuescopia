<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css">
<style>
  .irs--flat .irs-bar {
    top: 25px;
    height: 12px;
    background-color: #000000;
}

.irs-with-grid .irs-grid {display: none;}
.irs--flat .irs-bar { top: 25px; height: 12px; background-color: #000000; }
.irs--flat .irs-handle.state_hover > i:first-child, .irs--flat .irs-handle > i:first-child {background-color: transparent;}
.irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color: #000000;}
.irs--flat .irs-from::before, .irs--flat .irs-to::before, .irs--flat .irs-single::before {border-top-color: #000000;}
.irs--flat.irs-with-grid { height: 60px; margin-bottom: 30px; }
.irs {font-family:'Avenir LT Std 35 Light'; margin-bottom: 30px;}
.irs--flat .irs-handle.state_hover > i:first-child, .irs--flat .irs-handle:hover > i:first-child {background-color: transparent;}
.irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {
color: white;
font-size: 10px;
line-height: 1;
text-shadow: none;
padding: 0px;
background-color: #000000;
border-radius: 0px;
padding: 2px 5px;
}
.irs--flat .irs-bar {top: 35px;}
.irs--flat .irs-line {top: 35px;}
.irs--flat .irs-line {border-radius: 0px;}
.irs--flat .irs-bar {
    top: 25px;
    height: 12px;
    background-color: #000000;
}
.irs-with-grid .irs-grid {display: none;}
.irs--flat .irs-bar { top: 25px; height: 12px; background-color: #000000; }
.irs--flat .irs-handle.state_hover > i:first-child, .irs--flat .irs-handle > i:first-child {background-color: transparent;}
.irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color: #000000;}
.irs--flat .irs-from::before, .irs--flat .irs-to::before, .irs--flat .irs-single::before {border-top-color: #000000;}
.irs--flat.irs-with-grid { height: 60px; margin-bottom: 30px; }
.irs {font-family:'Avenir LT Std 35 Light'; margin-bottom: 30px;}
.irs--flat .irs-handle.state_hover > i:first-child, .irs--flat .irs-handle:hover > i:first-child {background-color: transparent;}
.irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {
color: white;
font-size: 10px;
line-height: 1;
text-shadow: none;
padding: 0px;
background-color: #000000;
border-radius: 0px;
padding: 2px 5px;
}
.irs--flat .irs-bar {top: 35px;}
.irs--flat .irs-line {top: 35px;}
.irs--flat .irs-line {border-radius: 0px;}
.irs-with-grid .irs-grid {display: none;}
.irs--flat .irs-bar { top: 25px; height: 12px; background-color: #000000; }
.irs--flat .irs-handle.state_hover > i:first-child, .irs--flat .irs-handle > i:first-child {background-color: transparent;}
.irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {background-color: #000000;}
.irs--flat .irs-from::before, .irs--flat .irs-to::before, .irs--flat .irs-single::before {border-top-color: #000000;}
.irs--flat.irs-with-grid { height: 60px; margin-bottom: 30px; }
.irs {font-family:'Avenir LT Std 35 Light';}
.irs--flat .irs-handle.state_hover > i:first-child, .irs--flat .irs-handle:hover > i:first-child {background-color: transparent;}
.irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {
  color: #e1e4e9;
  font-size: 10px;
  line-height: 1;
  text-shadow: none;
  padding: 0px;
  background-color: transparent;
  border-radius: 0px;
  padding: 0px 5px;
  color:#fff;
  visibility: visible !important;
}
.irs--flat .irs-bar {top: 35px;}
.irs--flat .irs-line {top: 35px;}
.irs--flat .irs-line {border-radius: 0px;}


.irs-to, .irs--flat .irs-single {
    background-color: transparent;
    bottom: -70px;
    top: inherit;
}

.irs--flat .irs-from, .irs--flat  {
    background-color: transparent;
    bottom: -20px;
    top: inherit;
    color: #fff;
}

.irs--flat .irs-from::before, .irs--flat .irs-to::before, .irs--flat .irs-single::before {
  margin-bottom: 20px;
  margin-left: -10px;
  border-top: solid 1em #000;
  border-left: solid 1em transparent;
  border-right: solid 1em transparent;
  border-top: 0;
  border-bottom: solid 1em #000;
}

.irs--flat .irs-from::before{
  margin-bottom: 20px;
  margin-left: -10px;
  border-top: solid 1em #000;
  border-left: solid 1em transparent;
  border-right: solid 1em transparent;
  border-bottom: 0;
  border-top: solid 1em #000;
}

input, textarea {
    background: transparent;
    width: 100%;
    border: 1px solid rgba(0, 0, 0, 0.1);
    margin-bottom: 10px;
    padding: 15px 20px;
    box-shadow: none;
    font: 300 16px "Heebo", sans-serif;
        font-weight: 300;
        font-size: 16px;
        line-height: normal;
        font-family: "Heebo", sans-serif;
    -webkit-transition: ease-out 0.2s;
    transition: ease-out 0.2s;
}

input, textarea {
  background: white;
  width: 100%;
  border: 2px solid #000000;
  margin-bottom: 0px;
  padding: 12px 10px;
  padding-right: 10px;
  padding-left: 10px;
    box-shadow: none;
    font-weight: 300;
    font-size: 13px;
    -webkit-transition: ease-out 0.2s;
    transition: ease-out 0.2s;
    font-family: 'Avenir LT Std 35 Light';

    padding-left: 20px;
  padding-right: 34px;
}

.padding-movil-casilla2 {
    padding-left: 24px;
    padding-right: 80px;
    text-align: right;
}

.personas-range-slide {
    font-size: 12px;
    font-weight: 100;
    letter-spacing: 3px;
    color: 
    #000;
}

#ancho-casilla {
    font-size: 12px;
}

.irs--flat .irs-single::before {
  display: none;
}
</style>
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link" href="<?= base_url('entradas/admin/venues/edit/'.$venue) ?>" role="tab" aria-controls="home" aria-selected="true">Venue</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="#home" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">Espacios</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#profile" data-toggle="tab" role="tab" aria-controls="profile" aria-selected="false">Fotos</a>
  </li>
</ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <?= $fotos ?>
    </div>
  </div>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="<?= base_url() ?>js/rangeSlider.js?v1.2"></script>
<script>
  $(document).on('ready',function(){
    $(".preciotag").on('click',function(){precio($(this).parents('div'));});
    $(".capacidadtag").on('click',function(){capacidad($(this).parents('div'));});
  });
</script>

<!-- Filtros de precio -->
<div class="modal fade" id="filtro-capacidad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title" id="exampleModalLabel">Capacidad</h5>
        </div>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-lg-12 no-padding">
            <div class="input-range">
                  <?php
                    $hasta = 10000;
                    $this->db->select('MAX(capacidad_maximo) as total');
                    $_hasta = $this->db->get_where('view_venues');
                    if($_hasta->num_rows()>0){
                      $hasta = $_hasta->row()->total;
                    }
                  ?>

                  <div class="personas-range-slide" id="ancho-casilla">
                    <div style="position:relative; display:inline-block; width:40%;" id="ancho-casilla">
                      <input id="ancho-casilla" class="lowDollarAmountCapacidad padding-movil-casilla2" value="0" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;">
                      <div class="d-none d-lg-block" style="position:absolute;right: 3px;top: 14px;;">Personas</div>
                    </div>
                    <div class="margin-personas-movil" style="position:relative; display:inline-block; width:40%;" id="ancho-casilla">
                      <input id="ancho-casilla" class="highDollarAmountCapacidad padding-movil-casilla2" value="10,000+" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;">
                      <div class="d-none d-lg-block" style="position:absolute;right: 3px;top: 14px;;">Personas</div>
                    </div>
                  </div>

                  <input type="text" class="js-range-sliderCapacidad" name="my_range" value=""
                    data-type="double"
                    data-min="<?= $this->ajustes->capacidad_minima ?>"
                    data-max="<?= $this->ajustes->capacidad_maxima ?>"
                    data-from="0"
                    data-to="10000"
                    data-grid="true"
                    data-min-interval=""
                  />

              </div>
          </div>
        </div>

      </div>

      <div class="modal-footer col-12 text-responsive2">
        <button type="button" class="button button-md text-responsive2 montserrat font-weight-bold" id="btn-negro" data-dismiss="modal" style="margin-right: 5px;">Aplicar</button>
      </div>

    </div>
  </div>
</div>

<!-- Filtros de precio -->
<div class="modal fade" id="filtro-precio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title text-uppercase" id="exampleModalLabel">Precio</h5>
        </div>
      </div>

      <div class="modal-body">


        <style>
          #precio-modal-filtro {margin-top: -50px;}
        </style>

        <div class="row">
          <div class="col-12 col-lg-12 no-padding">
              <div class="input-range">
                  <?php
                    $hasta = 1000000;
                    $this->db->select('MAX(precio_maximo) as total');
                    $_hasta = $this->db->get_where('view_venues');
                    if($_hasta->num_rows()>0){
                      $hasta = $_hasta->row()->total;
                    }
                  ?>

                  <div>
                    <div id="precio-modal-filtro" style="position:relative; display:inline-block; width:40%;">
                      <div id="ancho-casilla">
                        <span style="position:absolute;left: 14px;top: 13px;">$</span>
                        <input class="lowDollarAmountPrecio padding-movil-casilla" data-type="MXN" value="0" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;" id="ancho-casilla">
                        <span class="d-none d-lg-block" style="position:absolute;right: 4px;top: 14px;">MXN</span>
                      </div>
                    </div>
                    <div style="position:relative; display:inline-block; width:40%; margin-left: 18%;" id="ancho-casilla">
                      <span style="position:absolute;left: 14px;top: 13px;">$</span>
                      <input class="highDollarAmountPrecio padding-movil-casilla" data-type="MXN" value="1,000,000+" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;" id="ancho-casilla">
                      <span class="d-none d-lg-block" style="position:absolute;right: 4px;top: 14px;">MXN</span>
                    </div>
                  </div>

                  <input type="text" class="js-range-slider" name="my_range" value=""
                    data-type="double"
                    data-min="<?= $this->ajustes->precio_minimo ?>"
                    data-max="<?= $this->ajustes->precio_maximo ?>"
                    data-from="0"
                    data-to="1000000"
                    data-grid="true"
                    data-min-interval=""
                  />
              </div>
          </div>
        </div>

      </div>

      <div class="modal-footer col-12 text-responsive2">
        <button type="button" class="button button-md text-responsive2 montserrat" id="btn-negro" data-dismiss="modal" style="letter-spacing: 3px;font-size: 12px; font-weight: 600;">Aplicar</button>
      </div>

    </div>
  </div>
</div>