<?php 
	/*****
		TEST PAYPAL
		<form id="form" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
		venuescopia-facilitator@gmail.com

		Prod PAYPAL
		<form id="form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
		andres@venuescopia.com
	****/
	$encoded = base64_encode($id);
	$encoded = str_replace('=','',$encoded);
?>
<html>
	<body>
		<form id="form" action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<input type="hidden" name="cmd" value="_xclick-subscriptions">
			<input type="hidden" name="item_name" value="<?= "VEN#".$encoded ?>">
			<input type="hidden" name="business" value="andres@venuescopia.com">
			<input type="hidden" name="currency_code" value="MXN">
			<input type="hidden" name="no_shipping" value="1">
			<?php if($descuento>0 || $descuento == -1): $descuento = $descuento == -1?0:$descuento; ?>
			<input type="hidden" name="a1" value="<?= round($descuento,0) ?>">
			<input type="hidden" name="p1" value="<?= $frec ?>"> 
			<input type="hidden" name="t1" value="<?= $interval ?>">
			<?php endif ?>
			<input type="hidden" name="a3" value="<?= round($precio,0) ?>">
			<input type="hidden" name="p3" value="<?= $frec ?>"> 
			<input type="hidden" name="t3" value="<?= $interval ?>">
			<input type="hidden" name="src" value="1">
			<input type="hidden" name="sra" value="1">
			<input type="hidden" name="sra" value="<?= $id ?>">
			<input type="hidden" name="return" value="<?= base_url() ?>pagook.html">
			<input type="hidden" name="return_cancel" value="<?= base_url() ?>pagoko.html">			
			<input type="hidden" name="notify_url" value="<?= base_url() ?>entradas/frontend/procesarPagoPaypal/<?= $id ?>">
			Redireccionando a paypal, por favor espere.
		</form>

		<script>
			document.getElementById('form').submit();
		</script>
	</body>
</html>
