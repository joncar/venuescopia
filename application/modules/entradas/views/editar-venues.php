<div>

  <!-- Nav tabs -->
  <ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link active" href="#home" data-toggle="tab" role="tab" aria-controls="home" aria-selected="true">Ficha</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#profile" data-toggle="tab" role="tab" aria-controls="profile" aria-selected="false">Espacios</a>
  </li>
</ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
      <?= $output ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">
      <?= $espacios ?>
    </div>
  </div>

</div>

<script>
$(document).on('change','#field-direccion',function(){
    if($(this).val()!=''){
        searchDireccion($(this).val(),true);                
    }
});
var input = document.getElementById('field-direccion'); 
var autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
autocomplete.addListener('place_changed', fillInAddress);
console.log(mapa.marker);
function fillInAddress() {
    var place = autocomplete.getPlace().geometry.location;        
    mapa.marker.setPosition(new google.maps.LatLng(place.lat(), place.lng()));
    mapa.map.panTo(new google.maps.LatLng(place.lat(), place.lng()));
    //searchDireccion($("#direccion").val());
    $("#field-mapa").val(marker.getPosition());
}

function searchDireccion(direccion,moveMark) {
    var geocoder = new google.maps.Geocoder();
    var address = direccion;
    geocoder.geocode({'address': address}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            mapa.map.setCenter(results[0].geometry.location);
            var lugares = results[0].address_components;            
            if(typeof moveMark!=='undefined'){
                mapa.marker.setPosition(results[0].geometry.location);
                $("#field-mapa").val(results[0].geometry.location);
            }
            //$('input[name="colonia"]').val(lugares[2].long_name);
            //$('input[name="ciudad"]').val(lugares[3].long_name);                
        } else {
            alert('No se ha podido encontrar la ubicación indicada: ' + status);
            window.history.back();
        }
    });
}

$(document).on('change','#field-direccion',function(){
    
});
</script>