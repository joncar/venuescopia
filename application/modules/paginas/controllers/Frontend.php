<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }       

        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){            
            $theme = $this->theme;
            $params = $this->uri->segments;
            //Existe?            
            if(file_exists(APPPATH.'modules/paginas/views/'.$this->theme.$url.'.php')){
                $page = $this->load->view($theme.$url,array(),TRUE);
                $traducciones = $this->db->get_where('traducciones',array('original'=>$url,'idioma'=>$_SESSION['lang']));
                if($traducciones->num_rows()>0){
                    $url = $traducciones->row()->traduccion;                    
                }
            }else{
                //Verificar si existe en otros idiomas
                $idiomas = $this->db->get('ajustes')->row()->idiomas;
                $idiomas = explode(',',$idiomas);
                foreach($idiomas as $i){
                    $i = trim($i);
                    if(file_exists(APPPATH.'modules/paginas/views/'.$i.'/'.$url.'.php')){                    
                        if($i!=$this->theme){//Verificar si hay que traducir
                            $traducciones = $this->db->get_where('traducciones',array('original'=>$url,'idioma'=>$_SESSION['lang']));
                            if($traducciones->num_rows()>0){
                                $url = $traducciones->row()->traduccion;
                                if(file_exists(APPPATH.'modules/paginas/views/'.$this->theme.$url.'.php')){
                                    header("Location:".base_url().$url.'.html');
                                    die();
                                }    
                            }
                        }
                        $page = $this->load->view($i.'/'.$url,array(),TRUE);
                        $_SESSION['lang'] = $i;
                        $this->theme = $i.'/';
                    }
                }
            }
            if(empty($page)){
                throw new exception('Pagina web no encontrada',404);
            }
            $this->load->model('querys');
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$page,
                    'link'=>$url,
                    'url'=>$url,
                    'title'=>ucfirst(str_replace('-',' ',$url))
                )
            );
        }

        function getShort(){
            if(!empty($_POST)){
                $this->load->view($_POST['url']);
            }
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                //$page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                /*$page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);*/
                $page = $this->load->view($url,array(),TRUE);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nombre','required');             
            //$this->form_validation->set_rules('politicas','Politicas de privacidad','required');             
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                    $datos = $_POST;
                    if(!empty($_POST['extras'])){
                        unset($datos['extras']);
                        $datos['extras'] = '';
                        foreach($_POST['extras'] as $n=>$v){
                            $datos['extras'].= '<p class="MsoNormal"><span style="font-family: arial, helvetica, sans-serif;" data-mce-style="font-family: arial, helvetica, sans-serif;"><strong><span style="color: #808080;" data-mce-style="color: #808080;">'.$n.':</span></strong> '.$v.'</span></p>';                            
                        }
                    }else{
                        $datos['extras'] = '';
                    }

                    if(empty($_POST['asunto'])){
                        $datos['asunto'] = '';
                    }
                    $email = $this->ajustes->email_contacto;
                    if(!empty($_POST['to'])){
                        unset($datos['to']);
                        $email = $_POST['to'];
                    }
                    $this->enviarcorreo((object)$datos,1,$email);
                    $_SESSION['msj'] = '<script>document.location.href="'.base_url('contacto-envio.html').'";</script>';
            }else{                
               $_SESSION['msj'] = $this->error('Por favor complete los datos solicitados  <script>$("#guardar").attr("disabled",false); </script>');
            }

            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }
        
        function subscribir(){
            $err = 'error';
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('politicas','Políticas','required');
            if($this->form_validation->run()){
                
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    $_SESSION['msj2'] = 'Subscripción exitosa';
                    $err = 'success';
                    $this->enviarcorreo((object)$_POST,10);
                }else{
                    $_SESSION['msj2'] = 'Ya el correo esta registrado';
                }
            }else{
                $_SESSION['msj2'] = $this->error($this->form_validation->error_string());
            }
            echo json_encode(array('result'=>$err,'msg'=>$_SESSION['msj2']));
            unset($_SESSION['msj2']);
            //redirect(base_url().'p/contacte#subscribir');
        }
        
        function unsubscribe($email){
            $email = base64_decode($email);
            $this->db->delete('subscritos',array('email'=>$email));
            echo 'Dado de baja correctamente';
        }

        function search(){
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3Asedaclinic.com+'.urlencode($_GET['q']).'&oq=site%3Asedaclinic.com+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }
                $result = $_SESSION[$_GET['q']];                
                preg_match_all('@<div id=\"ires\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = fragmentar($resultado,'<ol>','</ol>');
                $resultado = $resultado[0];

           
                $resultado = str_replace('http://www.sedaclinic.com/url?q=','',$resultado);
                $resultado = str_replace('/url?q=','',$resultado);
                $resultado = explode('<div class="g">',$resultado);                
                foreach($resultado as $n=>$r){
                    if(strpos($r,'/search?q=site:')){
                        unset($resultado[$n]);
                        continue;
                    }
                    $resultado[$n] = '<div class="g">'.$r;                    
                    $resultado[$n] = substr($r,0,strpos($r,'&'));
                    $pos = strpos($r,'">')+2;
                    $rr = substr($r,$pos);
                    $pos = strpos($rr,'">');
                    $rr = substr($rr,$pos);                    
                    $resultado[$n].= $rr;
                    $resultado[$n] = '<div class="g">'.$resultado[$n];
                    $resultado[$n] = utf8_encode($resultado[$n]);
                }
                
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }

        function sitemap(){
            $pages = array(
                base_url(),
                base_url().'anuncia.html',
                base_url().'publicar.html',
                base_url().'about.html',
                base_url().'faq.html',
                base_url().'contacto.html',
                base_url().'login.html',
                base_url().'terminos-condiciones.html',
                base_url().'aviso-privacidad.html',
                base_url().'venues.html',
                base_url().'venues/populares'
            );

            //Listados por categoria
            foreach($this->elements->categoria_venue()->result() as $c){
                $pages[] = base_url().'venues/categorias/'.toUrl($c->nombre);
            }

            //Listados por venue
            foreach($this->elements->venues()->result() as $b){
                $pages[] = $b->link;
            }

            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }
    }
