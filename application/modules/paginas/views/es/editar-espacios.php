<?php if(empty($_SESSION['user'])) redirect('panel'); ?>
<?php
if(!empty($_GET['key']) && is_numeric(base64_decode($_GET['key']))):
$this->load->library('grocery_crud');
$this->load->library('ajax_grocery_crud');
$venue = $this->db->get_where('venues_espacios',array('id'=>base64_decode($_GET['key'])));
//Espacios
$fotos = new ajax_grocery_crud();
$fotos->set_table('venues_fotos')
   ->set_subject('fotos')
   ->set_theme('generic')
   ->columns('foto','pie_foto')
   ->where('venues_espacios_id',base64_decode($_GET['key']));
$fotos->set_url('entradas/backend/venues_fotos/'.base64_decode($_GET['key']).'/');
$fotos = $fotos->render(1,'application/modules/paginas/views/cruds/');

//Espacios
$espacios = new ajax_grocery_crud();
$espacios->set_table('venues_espacios')
   ->set_subject('espacios')
   ->set_theme('mis-venues')   
   ->field_type('venues_id','hidden')
   ->field_type('descripcion','string')
   ->field_type('amenidades','invisible')
   ->display_as('capacidad','Capacidad/Personas [Mínimo - Máximo]')
   ->display_as('precio','Precios [$Mínimo - $Máximo] MXN')
   ->display_as('descripcion','Descripción')
   ->callback_field('capacidad',function($val){
      return '<input type="text" class="capacidadtag form-control" readonly value="'.$val.'"><input type="hidden" name="capacidad" id="field-capacidad" value="'.$val.'" class="capacidad">';})
   ->callback_field('precio',function($val){            
      $precio = explode('-',$val);
      $precio = '$'.@number_format($precio[0],0,'.',',').'-$'.@number_format($precio[1],0,'.',',');
      return '<input type="text" class="preciotag form-control" readonly value="'.$precio.'"><input type="hidden" name="precio" id="field-precio" value="'.$val.'" class="precio">';})
   ->set_primary_key_value(base64_decode($_GET['key']));
$espacios->callback_field('amenidades',function($val,$row){
  $val = explode(',',$val);
  $amenidades = $this->db->get_where('amenidades');
  $data = '<select id="field-amenidades" multiple name="amenidades[]" class="form-control chosen-multiple-select">';
  foreach($amenidades->result() as $a){
    $selected = in_array($a->id,$val)?'selected':'';
    $data.= '<option value="'.$a->id.'" '.$selected.'>'.$a->nombre.'</option>';
  }
  $data.= '</select>';
  return $data;
});
$espacios->set_url('entradas/backend/venues_espacios/'.base64_decode($_GET['key']).'/');
$espacios = $espacios->render(3,'application/modules/paginas/views/cruds/');

$css_files = array_merge($fotos->css_files,$espacios->css_files);
$js_files = array_merge($fotos->js_files,$espacios->js_files);
?>
<?php 
if(!empty($css_files)):
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
<?php endforeach; ?>
<?php endif; ?>
<!-- About section -->
<div class="section margin-registro-top text-general-p">
  <!-- Product Tab content -->
  <div class="section no-padding margin-top-30">
    <div class="container">
      <div class="product-tab">
        <ul class="nav margin-bottom-20 text-uppercase">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('mis-venues.html') ?>"><h5>Mis venues</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= $venue->num_rows()>0?base_url('editar-venue.html?key='.base64_encode($venue->row()->venues_id)):'#' ?>"><h5>Editar Venue</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#tab-venues"><h5>Editar Espacio</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url() ?>mis-fotos.html?key=<?= $_GET['key'] ?>"><h5>Fotos <span></span></h5></a>
          </li>
        </ul>
        <div class="tab-content">
          <!-- Description tab content -->
          <div class="tab-pane fade show active" id="tab-venues">
            <?php 
              //View en application/modules/paginas/views/cruds/mis-venues/list_template.php
              //View en application/modules/paginas/views/cruds/mis-venues/list.php             
              echo $espacios->output;
            ?>
          </div>
          <!-- Reviews tab content -->
          <div class="tab-pane fade" id="tab-contactos">
            <?php 
              //View en application/modules/paginas/views/cruds/mis-mensajes/list_template.php
              //View en application/modules/paginas/views/cruds/mis-mensajes/list.php             
              echo $fotos->output;
            ?>
          </div>
        </div>
      </div>
      </div><!-- end container -->
    </div>
    <!-- end Product Tab content -->
  </div>
  <!-- end About section -->
  <?php 
    if(!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>

    <script>

      function pauseVenue(id){        
          $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(0);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Pausar');
          $("#pausar-publicacion").modal('toggle');
      }

      function starVenue(id){
        $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(1);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Activar');
          $("#pausar-publicacion").modal('toggle');
      }

      function actualizar(form){
        insertar('entradas/backend/venues_espacios/update/<?= base64_decode($_GET['key']) ?>',form,'#report-success');
        return false;
      }
    </script>
    <?php endif ?>
<script>
  $(document).on('ready',function(){
    $(".chosen-select,.chosen-multiple-select").chosen({"search_contains": true, allow_single_deselect:true});  
  });
</script>

<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/tinymce.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/jquery.tinymce.js"></script>
<script>

  var css = "<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.css,";
      css+= "<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.css,";
      css+= "<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.theme.default.min.css,";
      css+= "<?= base_url() ?>theme/assets/plugins/magnific-popup/magnific-popup.min.css,";     
      css+= "https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css,";
      css+= "<?= base_url() ?>theme/assets/css/main.css,";
      css+= "<?= base_url() ?>theme/assets/css/responsive.css,";
      css+="<?= base_url() ?>theme/assets/font/avenir/style.css,";
      css+= "<?= base_url() ?>theme/assets/plugins/font-awesome/css/all.css,";
      css+= "<?= base_url() ?>theme/assets/plugins/themify/themify-icons.min.css,";
      css+= "https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900,900i,";
      css+= "<?= base_url('assets/grocery_crud/css/jquery_plugins/chosen/chosen.css') ?>";

  tinymce.init({
    selector: '#field-descripcion',
    theme:'modern',
    image_advtab: true, 
    menubar: false,
    statusbar:false,
    toolbar1: 'undo redo',
    plugins: "link paste visualblocks code spellchecker, searchreplace, table, image, imagetools, textcolor, filemanager, fullscreen",
    images_upload_base_path: '/img/uploads',
    relative_urls: true,
    convert_urls: false,
    cleanup_on_startup: false,
    trim_span_elements: false,
    verify_html: false,
    cleanup: false,
    paste_as_text: true,
    content_css:css
  });

  $(document).on('ready',function(){
    $(".preciotag").on('click',function(){precio($(this).parents('div'));});
    $(".capacidadtag").on('click',function(){capacidad($(this).parents('div'));});
  });
</script>