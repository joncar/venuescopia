<div  itemscope itemtype="https://schema.org/Place">
<!-- Home section -->
<div class="owl-carousel owl-nav-overlay owl-dots-overlay" data-owl-nav="true" data-owl-dots="true" data-owl-items="1" id="slide-venue">
  <?php foreach($detail->espacios->result() as $e): ?>
    <?php 
      foreach($e->fotos->result() as $f): 
        $f->foto = $f->foto == base_url('img/venues/')?base_url('img/2100x1406.png'):$f->foto;
    ?>
      <!-- Slider -->
        <div class="section-fullscreen bg-image" style="background-image: url('<?= $f->foto ?>');">
          <img src="<?= $f->foto ?>" itemprop="photo" itemscope itemtype="https://schema.org/Photograph" style="display: none">
          <div class="section-xl">
            <div class="container">

              <div class="row contenedor-pie-slider">
                <div class="col-12 col-sm-6 text-left d-none d-lg-block">
                  <span class="pie-foto-slider"><?= $f->pie_foto ?></span>
                </div>
                <div class="col-12 col-sm-6 text-right">
                  <a class="button button-md text-center" id="btn-blanco-header" data-toggle="modal" data-target="#share" title="Compartir" style="padding:  10px 20px; margin-right: 60px; cursor:pointer;">
                    <b>Share</b>
                  </a>
                  <div id="btn-slider-fav">
                    <button type="button" onclick="addFav('<?= $detail->id ?>')">
                          <div class="notification-icon prueba animate-swing">
                            <i id="" class="icon<?= $detail->id ?> far <?= $detail->fav?'fas':'' ?> fa-heart"></i>
                          </div>
                        </button>
                  </div>
                </div>
              </div>

            </div><!-- end container -->
          </div>
        </div>
    <?php endforeach ?>
  <?php endforeach ?>
</div><!-- end owl-carousel -->
<!-- end Home section -->

<div class="section">
  <div class="container">

      <div class="row">
          <div class="col-12 col-sm-8">

              <div class="section-title-interna3">
                  <h2 class="text-uppercase font-montserrat" itemprop="name"><b><?= $detail->nombre ?></b></h2>
                  <div class="texto-descripcion">
                    <div class="subtitulos-general"><?= $detail->direccion ?></div>
                  </div>
              </div>

              <div class="row personas-venue text-left font-montserrat">
                  <div class="col-12 col-lg-4 font-weight-bold" style="padding: 0px;"><h4><b><img src="<?= base_url() ?>theme/assets/images/iconos/personas.png" alt="Amenidades del Venue" style="width:15px;"> <?= $detail->capacidad ?> personas</b></h4></div>
                  <div class="col-12 col-lg-6 font-weight-bold" style="padding: 0px;"><h4><b><img src="<?= base_url() ?>theme/assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" style="width:15px;"> <?= $detail->precio ?> MXN</b></h4></div>
              </div>

              <div class="texto-descripcion margin-bottom-50">
                  <p class="text-justify" itemprop="description" itemtype="https://schema.org/Text"><?=  $detail->descripcion ?></p>
              </div>

              <div class="section-title-interna3">
                  <h2 class="text-uppercase font-montserrat"><b>Amenidades</b></h2>
              </div>

              <!-- Amenidades Desktop -->
              <div class="personas-venue text-left font-montserrat d-none d-sm-block">
                  <table class="table" id="table-amenidades">
                    <tbody>
                    <?php $x = 0; ?>
                    <?php                      
                      $am = $detail->amenidades;                      
                      foreach($am as $n=>$a): if(!empty($a)): $a = @$this->elements->amenidades(array('id'=>$a))->row();
                    ?>
                    <?php if($x==0): ?>
                      <tr>
                    <?php endif ?>
                        <td>
                            <img src="<?= @$a->icono ?>" alt="<?= @$a->nombre ?>" id="img-icon-venue" style="width:25px; height: 25px;">
                            <span style="font-size:10px;"><?= @$a->nombre ?></span>
                        </td>
                    <?php $x++; ?>
                    <?php if($x==3 || $n==count($am)): $x = 0; ?>
                      </tr>
                    <?php endif ?>
                    <?php endif ?>
                    <?php endforeach ?>
                    </tbody>
                  </table>
              </div>

              <!-- Amenidades Movil -->
              <div class="margin-bottom-30 margin-top-30 d-sm-none list-amenidades">
                  <ul style="padding: 0;">
                    <?php
                      $am = $detail->amenidades;
                      array_pop($am);
                      foreach($am as $n=>$a): $a = $this->elements->amenidades(array('id'=>$a))->row();
                    ?>
                    <li class="amenidades-movil"><img src="<?= @$a->icono ?>" alt="<?= @$a->nombre ?>" style="width:15px; height: 15px; margin-right: 10px;"> <?= @$a->nombre ?></li>
                    <?php endforeach ?>
                  </ul>
              </div>
          </div>

          <div class="col-12 col-sm-4" style="display: block; z-index: 2;" id="margin-form-movil">
              <div class="row sticky">
                <div class="btn-solicitar">
                  <!--<a class="button button-md text-center" id="btn-telefono" data-toggle="modal" data-target="#telefono" title="Publica tu Venue" style="padding:  10px 30px;"><b>Ver teléfono</b></a>-->
                  <button type="button" class="button button-md text-center" id="btn-blanco" onclick="verTelefono();" title="Teléfono"><b>Ver teléfono</b></button>
                </div>
                <!-- Contact Form -->
                <div class="contact-form">
                  <form method="post" id="contactform" onsubmit="return insertar('entradas/frontend/mensajes/insert',this,'.submit-result',function(data){$('.submit-result').removeClass('alert-info').html(data.success_message); $('#prueba-precios').modal('toggle')});">
                    <div class="form-row">
                      <div class="col-12 col-sm-12">
                        <input type="text" id="name" name="nombre" placeholder="Nombre completo*" required>
                      </div>
                      <div class="col-12 col-sm-12">
                        <input type="email" id="email" name="correo" placeholder="Correo electrónico*" required>
                      </div>
                      <div class="col-12 col-sm-12">
                        <textarea name="mensaje" id="mensaje" placeholder="¡Hola! Me interesa este venue. Gracias por contactarme." style="font-size: 13px;"></textarea>
                      </div>
                    </div>
                    <div class="btn-solicitar">                      
                      <input type="hidden" name="venues_id" value="<?= $detail->id ?>">
                      <!--
                      <button class="button button-lg btn-buscar-header btn-contacto-movil" type="submit" data-toggle="modal" data-target="#contacto-venue">Contactar</button>-->
                      <button type="submit" class="button button-md text-center" id="btn-negro" data-toggle="modal" data-target="#contacto-venue"><b>Contactar</b></button>
                    </div>
                  </form>
                </div>
                <!-- Contact Form -->
              </div>
          </div>
      </div>

      <?php if($detail->espacios->num_rows()>1): ?>
        <div class="col-12 col-lg-12 section-title-interna3"><h2 class="text-uppercase font-montserrat"><b>Espacios:</b></h2></div>

        <?php foreach($detail->espacios->result() as $n=>$e): if($n>0): ?>
        <div class="row margin-bottom-10">
            <div class="col-12" style="padding-left: 0px;">
              <?php $this->load->view('es/includes/venues/espacios.php',array('e'=>$e),FALSE,'paginas');?>
            </div>
        </div>
        <?php endif; endforeach ?>
      <?php endif ?>
      <div class="row section-title-interna3">
          <div class="col-12"><h2 class="text-uppercase font-montserrat"><b>Mapa</b></h2></div>
          <div class="col-12"><div class="subtitulos-general" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"><?= $detail->direccion.', '.$detail->ciudad ?></div></div>
          <div class="col-12"><div id="mapa" style="width:100%; height:300px"></div></div>
      </div>

  </div><!-- end container -->
</div><!-- end container -->

<div class="section">
    <div class="container">
        <?php $this->load->view('es/includes/venues/recomendaciones.php',array(),FALSE,'paginas');?>
    </div>
</div>

<!-- Telefono venue -->
<div class="modal fade" id="telefono" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body text-center">
          <span class="font-montserrat" style="font-size:14px; letter-spacing: 3px;"><b><?= $detail->nombre ?></b></span><br>
          <div class="telefono-modal margin-top-20" itemprop="telephone" itemscope itemtype="https://schema.org/Text"><i class="fas fa-phone"></i> <?= $detail->telefono_contacto ?></div>
      </div>
    </div>
  </div>
</div>

<!-- Share venue -->
<div class="modal fade" id="share" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body text-center">
          <div class="col-12">
              DESCUBRÍ ESTE LUGAR EN VENUESCOPIA:<br><br>
              <span class="font-montserrat"><b><?= $detail->nombre ?></b></span><br>
              <?= $detail->direccion ?>
          </div>

          <div class="col-12 margin-top-20">
              <ul class="list-horizontal-unstyled">
                <li><a href="mailto:?subject=<?= 'DESCUBRÍ ESTE LUGAR EN VENUESCOPIA: '.$detail->nombre.' '.$detail->link; ?>&body=<?= 'DESCUBRÍ ESTE LUGAR EN VENUESCOPIA: '.$detail->nombre.' '.$detail->link; ?>&attachment=<?= $detail->foto ?>"><i class="fas fa-envelope"></i></a></li>

                <li class="d-none d-xs-block"><a href="https://wa.me?text=<?= 'DESCUBRÍ ESTE LUGAR EN VENUESCOPIA: '.$detail->nombre.' '.base_url('venue/'.toUrl($detail->id.'-'.$detail->nombre)); ?>"><i class="fab fa-whatsapp"></i></a></li>
                <li class="d-xs-none"><a href="https://api.whatsapp.com/send?text=<?= 'DESCUBRÍ ESTE LUGAR EN VENUESCOPIA: '.$detail->nombre.' '.base_url('venue/'.toUrl($detail->id.'-'.$detail->nombre)); ?>" target="_new"><i class="fab fa-whatsapp"></i></a></li>


                <li><a href="https://www.facebook.com/sharer.php?u=<?= $detail->link ?>" title="Compartir en Facebook" target="_new"><i class="fab fa-facebook-square"></i></a></li>
                <li><a href="https://twitter.com/share?text=<?= 'DESCUBRÍ ESTE LUGAR EN VENUESCOPIA: '.$detail->nombre; ?>&url=<?= base_url('venue/'.toUrl($detail->id.'-'.$detail->nombre)) ?>" target="_new"><i class="fab fa-twitter"></i></a></li>
                <li class="copyLink">
                  <span class="copyLinktext" id="mycopyLink">Copiar al portapapeles</span>
                  <a href="javascript:void(0)" onclick="copy()" title="Copiar link"><i class="fas fa-link"></i></a>
                  <input type="text" id="link" value="<?= $detail->link ?>" style="position:fixed;left:-100vw; width:100px;">
                </li>
              </ul>
          </div>
      </div>
    </div>
  </div>
</div>

<!-- Nuevo modal de rango de precios -->
<div class="modal fade" id="prueba-precios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>
      <div class="modal-body text-center">

        <div class="submit-result"></div>

      </div>
    </div>
  </div>
</div>

<script>
  $("#slider-range").slider({
    range: true,
    min: 0,
    max: 3500,
    step: 50,
    slide: function( event, ui ) {
      $( "#min-price").html(ui.values[ 0 ]);

      suffix = '';
      if (ui.values[ 1 ] == $( "#max-price").data('max') ){
         suffix = ' +';
      }
      $( "#max-price").html(ui.values[ 1 ] + suffix);
    }
  })
</script>
<!-- Nuevo modal de rango de precios -->

<script>
  window.onload = function(){
  var c = document.getElementById('mapa');
  var opt = {
    center:new google.maps.LatLng<?= $detail->mapa ?>,
    zoom:16
  };
  var map = new google.maps.Map(c,opt);
  var marker = new google.maps.Marker({
    position:new google.maps.LatLng<?= $detail->mapa ?>,
    map:map
  });
  }

  function copy(){
    var copyText = document.getElementById("link");
    copyText.select();
    setTimeout(function(){    
      document.execCommand("copy");
      var tooltip = document.getElementById("mycopyLink");
      tooltip.innerHTML = "copiado";
    },600);
  }

  function verTelefono(){
      $("#telefono").modal('toggle');
      $.get('<?= base_url() ?>entradas/frontend/solicitud_telefono/<?= $detail->id ?>',{},function(){});
  }

  $(document).on('ready',function(){
      $("#prueba-precios").on('hide.bs.modal',function(){
        $(".submit-result").html('');
      });
  });
</script>
</div>