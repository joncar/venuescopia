<script>
  var noHidePreload = true;
</script>
<?php
$this->load->library('grocery_crud');
$this->load->library('ajax_grocery_crud');
//Venues
$crud = new ajax_grocery_crud();
$crud->set_table('venues')
   ->set_subject('venues')
   ->set_theme('listado');
if(empty($tipo)){
  $crud->set_url('entradas/frontend/venues_list/');
}else{
  $_GET['tipo'] = empty($value)?$tipo:$value;
  $value = empty($value)?'':$value.'/';
  $crud->set_url('entradas/frontend/venues_list/'.$tipo.'/'.$value);  
}
$crud = $crud->render('','application/modules/paginas/views/cruds/');

$css_files = $crud->css_files;
$js_files = $crud->js_files;
?>

 <!-- Products section -->
      <div class="container margin-registro-top">
        <div class="row contenedor-resultas-height">

         <?php 
            //VIEW EN application/modules/paginas/views/cruds/list_template.php
            //VIEW EN application/modules/paginas/views/cruds/list.php
            echo $crud->output
         ?>

        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Products section -->

    <?php 
    if(!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>

    <script>      
      $(document).on('ready',function(){
        $('select[name="order_by[0]"]').on('change',function(){
          var type = $(this).find('option:selected').data('type');
          $('input[name="order_by[1]"]').val(type);
          $(".filtering_form").submit();
        });
      });
    </script>

    <!-- Complete Search Index -->
<script>
  var completar, completarCiudad,completarEstado,completarColonia;
  function selEstado(nombre){
      $(".estado").val(nombre);      
      $("input[name='d_estado']").val(nombre);      
      $(".ciudad").val('');      
      $("input[name='d_ciudad']").val('');
      $(".colonia").val('');
      $("input[name='d_colonia']").val('');
      completarCiudad._search();
      $(".filtering_form").submit();
  }
  
  function selCiudad(nombre){
      $(".ciudad").val(nombre);      
      $("input[name='d_ciudad']").val(nombre);      
      $(".colonia").val('');
      completarColonia._search();
      $(".filtering_form").submit();
  }
  
  function selColonia(nombre){
      $(".colonia").val(nombre);
      $("input[name='d_colonia']").val(nombre);
      $(".filtering_form").submit();
  }
$(document).on('ready',function(){
  completar = new autocomplete($(".autocomplete input"),'entradas/frontend/venues_search/');
  completarEstado = new autocomplete($("input.estado"),'entradas/frontend/estado_search',0,document.getElementById('filtering_form'));
  completarCiudad = new autocomplete($("input.ciudad"),'entradas/frontend/ciudad_search',0,document.getElementById('filtering_form'));
  completarColonia = new autocomplete($("input.colonia"),'entradas/frontend/colonia_search',0,document.getElementById('filtering_form'));
 
 $("input.ciudad").change(function(){if($("input[name='ciudad']").val()!=$(this).val()){selCiudad($(this).val());}});
 $("input.colonia").change(function(){if($("input[name='colonia']").val()!=$(this).val()){selColonia($(this).val());}});
  $(document).on('refreshlist',function(){  
    $(".owl-carousel").each( function() {
        var $carousel = $(this);

        var $defaults = {
          rewind: true,
          navText: ["<i class='ti-angle-left'></i>","<i class='ti-angle-right'></i>"],
          autoHeight: false, 
          autoplayTimeout: 4000,
          autoplayHoverPause: false
        }

        var $options = {
          items: $carousel.data("owl-items"),
          margin: $carousel.data("owl-margin"),
          loop: $carousel.data("owl-loop"),
          center: $carousel.data("owl-center"),
          mouseDrag: $carousel.data("owl-mouseDrag"),
          touchDrag: $carousel.data("owl-touchDrag"),
          pullDrag: $carousel.data("owl-pullDrag"),
          freeDrag: $carousel.data("owl-freeDrag"),
          stagePadding: $carousel.data("owl-stagePadding"),
          autoWidth: $carousel.data("owl-autoWidth"),
          startPosition: $carousel.data("owl-startPosition"),
          URLhashListener: $carousel.data("owl-URLhashListener"),
          nav: $carousel.data("owl-nav"),
          rewind: $carousel.data("owl-rewind"),
          navElement: $carousel.data("owl-navElement"),
          slideBy: $carousel.data("owl-slideBy"),
          dots: $carousel.data("owl-dots"),
          dotsEach: $carousel.data("owl-dotsEach"),
          autoplay: $carousel.data("owl-autoplay"),
          autoplayTimeout: $carousel.data("owl-autoplayTimeout"),
          smartSpeed: $carousel.data("owl-smartSpeed"),
          fluidSpeed: $carousel.data("owl-fluidSpeed"),
          autoplaySpeed: $carousel.data("owl-autoplaySpeed"),
          navSpeed: $carousel.data("owl-navSpeed"),
          dotsSpeed: $carousel.data("owl-dotsSpeed"),
          dragEndSpeed: $carousel.data("owl-dragEndSpeed"),
          callback: $carousel.data("owl-callback"),
          video: $carousel.data("owl-video"),
          videoHeight: $carousel.data("owl-videoHeight"),
          videoWidth: $carousel.data("owl-videoWidth"),
          itemElement: $carousel.data("owl-itemElement"),
          stageElement: $carousel.data("owl-stageElement"),
          navContainer: $carousel.data("owl-navContainer"),
          dotsContainer: $carousel.data("owl-dotsContainer")
        }

        var $responsive = {
          responsive: {
            0 : {
              items: $carousel.data("owl-xs")
            },
            // breakpoint from 576px+
            576 : {
              items: $carousel.data("owl-sm")
            },
            // breakpoint from 768px+
            768 : {
              items: $carousel.data("owl-md")
            },
            // breakpoint from 992px+
            992 : {
              items: $carousel.data("owl-lg")
            },
            // breakpoint from 1200px+
            1200 : {
              items: $carousel.data("owl-xl")
            }
          }
        }

        $carousel.owlCarousel( $.extend( $defaults, $options, $responsive) );
      });
  });

  $("#filtro-capacidad,#filtro-precio").on('hidden.bs.modal',function(){
    $(".filtering_form").submit();
  });

  $('button[type="submit"]').on('click',function(e){
    document.location.href="<?= base_url() ?>venues.html?nombre="+encodeURI($('input[name="nombre"]').val());
  });

  $('input[name="nombre"]').on('keypress',function(e){
    if(e.which==13){
      //document.location.href="<?= base_url() ?>venues.html?nombre="+encodeURI($(this).val());
      completar.onSend(e);
    }
  });
});


function cleanFilters(){
  document.location.href="venues.html";
}



</script>