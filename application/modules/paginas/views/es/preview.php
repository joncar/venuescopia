 <?php
  if(!empty($_SESSION['publicacion'])):
  $detail = $_SESSION['publicacion'];
  $detail = json_encode($detail);
  $detail = json_decode($detail);
  if(!empty($detail->espacios) && count($detail->espacios)>0 && count($detail->espacios[0]->fotos)>0):
    
    $detail->precio = $detail->espacios[0]->precio;
    $detail->capacidad = $detail->espacios[0]->capacidad;
    $detail->amenidades = explode(',',$detail->espacios[0]->amenidades[0]);

    if(!empty($detail->capacidad)){
      $detail->capacidad = explode('-',$detail->capacidad);

      
      $detail->capacidad[0] = number_format($detail->capacidad[0],0,'.',',');
      $detail->capacidad[1] = $detail->capacidad[1]==$this->ajustes->capacidad_maxima?number_format($detail->capacidad[1],0,'.',',').'+':number_format($detail->capacidad[1],0,'.',',');
      $detail->capacidad = implode('-',$detail->capacidad);
    }
    if(!empty($detail->precio)){
      $detail->precio = explode('-',$detail->precio);
      
      

      $detail->precio[0] = number_format($detail->precio[0],0,'.',',');
      $detail->precio[1] = $detail->precio[1]==$this->ajustes->precio_maximo?number_format($detail->precio[1],0,'.',',').'+':number_format($detail->precio[1],0,'.',',');      
      $detail->precio = implode('-',$detail->precio).'MXN';
    }
 ?>

   <div class="owl-carousel owl-nav-overlay owl-dots-overlay" data-owl-nav="true" data-owl-dots="true" data-owl-items="1" id="slide-venue">
      <?php foreach($detail->espacios as $e): ?>
        <?php foreach($e->fotos as $n=>$f): $f = is_json($f)?json_decode($f)->file:$f; ?>
          <!-- Slider -->
            <div class="section-fullscreen bg-image" style="background-image: url('<?= base_url().'img/venues/'.$f ?>');">
              <div class="section-xl">
                <div class="container">

                  <div class="row contenedor-pie-slider">
                    <div class="col-12 col-sm-6 text-left d-none d-lg-block">
                      <span class="pie-foto-slider"><?= @$e->pies_fotos[$n] ?></span>
                    </div>
                    <div class="col-12 col-sm-6 text-right editando-preview" style="display: -webkit-inline-flex;">
                      <a class="button button-lg text-center" href="javascript:editar()" id="btn-blanco-preview" title="Publica tu Venue" style="padding:  10px 30px; margin-right: 20px;"><b>Seguir editando</b></a>
                      <a class="button button-lg text-center" href="javascript:pagar()" id="btn-blanco-preview" title="Publica tu Venue" style="padding:  10px 30px;"><b>Pagar para finalizar</b></a>
                    </div>
                  </div>

                  <!--
                  <div class="row share-header">
                      <a class="button button-lg text-center" href="javascript:editar()" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px; margin-right: 20px;"><b>Seguir editando</b></a>
                      <a class="button button-lg text-center" href="javascript:pagar()" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px;"><b>Pagar para finalizar</b></a>
                      <div class="col-12 col-sm-12 text-left margin-top-10"><?= @$e->pies_fotos[$n] ?></div>
                  </div>-->

                </div><!-- end container -->
              </div>
            </div>
        <?php endforeach ?>
      <?php endforeach ?>
    </div><!-- end owl-carousel -->
    <!-- end Home section -->


    <div class="section">
      <div class="container">

          <div class="row">
            <div class="col-12 col-sm-8">
                <div class="section-title-interna3">
                    <div class="row">
                      <div class="col-12">
                        <h2 class="text-uppercase font-montserrat"><b><?= $detail->nombre ?></b></h2>
                        <div class="subtitulos-general"><?= $detail->direccion ?></div>
                      </div>
                    </div>
                </div>

                <div class="row personas-venue text-left font-montserrat">
                    <div class="col-6 col-lg-4"><h4><b><img src="<?= base_url() ?>theme/assets/images/iconos/personas.png" alt="Amenidades del Venue" style="width:15px;"> <?= $detail->capacidad ?> personas</b></h4></div>
                    <div class="col-6 col-lg-6"><h4><b><img src="<?= base_url() ?>theme/assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" style="width:15px;"> $<?= $detail->precio ?></b></h4></div>
                </div>

                <div class="row texto-descripcion margin-bottom-30">
                  <div class="col-12">
                    <?= nl2br($detail->descripcion) ?>
                  </div>
                </div>

                <div class="section-title-interna3">
                  <div class="row">
                    <div class="col-12">
                      <h2 class="text-uppercase font-montserrat"><b>Amenidades</b></h2>
                    </div>
                  </div>
                </div>

                <!-- Amenidades Desktop -->
                <div class="row personas-venue text-left font-montserrat d-none d-sm-block">
                  <div class="col-12">
                      <table class="table" id="table-amenidades">
                        <tbody>


                        <?php $x = 0; ?>
                        <?php
                          $am = $detail->amenidades;
                          array_pop($am);
                          foreach($am as $n=>$a): $a = @$this->elements->amenidades(array('id'=>$a))->row();
                        ?>
                        <?php if($x==0): ?>
                          <tr>
                        <?php endif ?>
                            <td>
                                <img src="<?= @$a->icono ?>" alt="<?= @$a->nombre ?>" style="width:25px; height: 25px;">
                                <span style="font-size:10px;"><?= @$a->nombre ?></span>
                            </td>
                        <?php $x++; ?>
                        <?php if($x==3 || $n==count($am)): $x = 0; ?>
                          </tr>
                        <?php endif ?>
                        <?php endforeach ?>
                        </tbody>
                      </table>
                  </div>
                </div>


                <!-- Amenidades Movil -->
                <div class="row margin-bottom-30 d-sm-none list-amenidades margin-top-50">
                  <div class="col-12">
                    <ul>
                      <?php
                        $am = $detail->amenidades;
                        array_pop($am);
                        foreach($am as $n=>$a): $a = $this->elements->amenidades(array('id'=>$a))->row();
                      ?>
                        <li class="amenidades-movil"><img src="<?= @$a->icono ?>" alt="<?= @$a->nombre ?>" style="width:25px; height: 25px;"> <?= @$a->nombre ?></li>
                      <?php endforeach ?>
                    </ul>
                  </div>
                </div>

            </div>

            <div class="col-12 col-sm-4" id="margin-form-movil">
                <div class="row sticky">


                  <div class="btn-solicitar">
                    <a class="button button-md text-center" id="btn-blanco" data-toggle="modal" data-target="#telefono" title="Publica tu Venue"><b>Ver teléfono</b></a>
                  </div>


                  <!-- Contact Form -->
                  <div class="contact-form">
                    <form method="post" id="contactform" onsubmit="return false">
                      <div class="form-row">
                        <div class="col-12 col-sm-12">
                          <input type="text" id="name" name="nombre" placeholder="Nombre completo*" disabled>
                        </div>
                        <div class="col-12 col-sm-12">
                          <input type="email" id="email" name="correo" placeholder="Correo electrónico*" disabled>
                        </div>
                        <div class="col-12 col-sm-12">
                          <textarea name="mensaje" id="mensaje" placeholder="¡Hola! Me interesa este venue. Gracias por contactarme." disabled style="font-size: 13px;"></textarea>
                        </div>
                      </div>



                      <div class="btn-solicitar">
                        <button class="button button-md text-center" type="submit" id="btn-negro" data-toggle="modal" data-target="#contacto-venue"><b>Contactar</b></button>
                      </div>

                    </form>
                </div><!-- end inner row -->
              </div>
            </div>

          </div><!-- End Row -->
          <?php if(count($detail->espacios)>1): ?>
            <div class="col-12 col-lg-12 section-title-interna3"><h2 class="text-uppercase font-montserrat"><b>Espacios:</b></h2></div>

            <?php foreach($detail->espacios as $n=>$e): if($n>0): ?>
            <div class="row margin-bottom-10">
                <div class="col-12" style="padding-left: 0px;">
                  <?php $this->load->view('es/includes/venues/espacios-preview.php',array('e'=>$e,'detail'=>$detail),FALSE,'paginas');?>
                </div>
            </div>
            <?php endif; endforeach ?>
          <?php endif ?>

          <div class="section-title-interna3">
            <div class="row margin-bottom-30">
              <div class="col-12">
                <h2 class="text-uppercase font-montserrat"><b>Mapa</b></h2>
              </div>

              <div class="col-12">
                <div class="subtitulos-general"><?= $detail->direccion ?></div>
              </div>

              <div class="col-12">
                  <div id="mapa" style="width:100%; height:300px"></div>
              </div>
            </div>
          </div>

          <div class="row">
              <div class="col-12 col-sm-12">
                  <a class="button button-md margin-top-10 text-center btn-preview" id="btn-negro" href="javascript:editar()" title="Conoce Venuescopia"><b>Seguir editando</b></a>
                  <a class="button button-md margin-top-10 text-center btn-preview" id="btn-negro" href="javascript:pagar()" title="Conoce Venuescopia"><b>Pagar para finalizar</b></a>
              </div>
          </div>

      </div><!-- end container -->
    </div>

<!-- Telefono venue -->
<div class="modal fade" id="telefono" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>

      <div class="modal-body text-center">
          <div class="font-montserrat text-uppercase" style="font-size:14px; letter-spacing: 3px; font-family: 'Avenir LT Std 35 Light';"><?= $detail->nombre ?></div>
          <div class="telefono-modal"><i class="fas fa-phone"></i> <?= $detail->telefono_contacto ?></div>
      </div>

    </div>
  </div>
</div>

<script>
  window.onload = function(){
  var c = document.getElementById('mapa');
  var opt = {
    center:new google.maps.LatLng<?= empty($detail->mapa)?'(0,0)':$detail->mapa; ?>,
    zoom:16
  };
  var map = new google.maps.Map(c,opt);
  var marker = new google.maps.Marker({
    position:new google.maps.LatLng<?= empty($detail->mapa)?'(0,0)':$detail->mapa; ?>,
    map:map
  });
  }

  function pagar(){
    if(typeof(window.opener)!='undefined'){
      window.opener.showTask(6);
      window.opener.focus();
      window.close();
    }
  }

  function editar(){
    if(typeof(window.opener)!='undefined'){
      window.opener.showTask(5);
      window.opener.focus();
      window.close();
    }
  }
</script>
<?php else: ?>
  Aún te faltan datos, completalos para previsualizar tu anuncio
<?php endif ?>
<?php endif ?>