 <?php if(empty($_SESSION['user'])) redirect('panel'); ?>
 <!-- About section -->
    <div class="section margin-registro-top">
      <div class="container">
        <form id="actualizar" onsubmit="insertar('seguridad/perfil/update/<?= $this->user->id ?>',this,'#perfilResponse'); return false;">
          <div class="margin-bottom-30 col-12 col-md-8 offset-md-2">
            <div class="col-12">
              <h2 class="font-weight-norma text-uppercase titulos font-montserrat"><b>Mis Datos</b></h2>
                <label>Nombre:</label>
                <input type="text" value="<?= $this->user->nombre ?>" name="nombre" required="" class="margin-bottom-10">
                <label>Apellidos:</label>
                <input type="text" value="<?= $this->user->apellido_paterno ?>" name="apellido_paterno" required="" class="margin-bottom-10">
                <label>Correo:</label>
                <input type="email" value="<?= $this->user->email ?>" name="email" required="" class="margin-bottom-10">
                <label>Contraseña:</label>
                <input type="password" value="<?= $this->user->password ?>" name="password" required="" class="margin-bottom-30">              
                <div id="perfilResponse"></div>            
            </div>

            <div class="col-12 text-right">
              <div class="col-12 col-sm-12" style="padding: 0px;">
                <a href="<?= base_url() ?>" class="button button-md botones-cuenta margin-btn-guardar-movil" id="btn-negro" style="text-align: center;"><b>Regresar</b></a>
                <button type="submit" class="button button-md botones-cuenta" id="btn-negro" title="Guardar cambios"><b>Guardar cambios</b></button>
              </div>
            </div>  

            <div class="col-12 text-right">
                <button type="button" data-toggle="modal" data-target="#cancelarModal" class="btn-eliminar-cuenta">Eliminar cuenta</button>
            </div>  

          </div>
        </form>

        
      </div><!-- end container -->
    </div>
    <!-- end About section -->