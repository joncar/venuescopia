<?php if(empty($_SESSION['user'])) redirect('panel'); ?>
<?php
if(!empty($_GET['key']) && is_numeric(base64_decode($_GET['key']))):
$this->load->library('grocery_crud');
$this->load->library('ajax_grocery_crud');
//Espacios
$espacios = new ajax_grocery_crud();
$espacios->set_table('venues_espacios')
	 ->set_subject('espacios')
	 ->set_theme('generic')
	 ->columns('nombre','capacidad','precio')
	 ->where('venues_id',base64_decode($_GET['key']));
$espacios->set_url('entradas/backend/venues_espacios/'.base64_decode($_GET['key']).'/');
$espacios = $espacios->render(1,'application/modules/paginas/views/cruds/');

//Venues
$crud = new ajax_grocery_crud();
$crud->set_table('venues')
	 ->set_subject('venues')
	 ->set_theme('mis-venues')
	 ->unset_back_to_list()	 	 
	 ->field_type('foto','invisible')
	 ->field_type('requiere_factura','true_false',array('0'=>'NO','1'=>'SI'))
	 ->field_type('membresias_id','hidden')	 
	 ->field_type('codigo_descuento','invisible')
	 ->field_type('fecha_vencimiento','invisible')
	 ->field_type('categoria_venue_id','invisible')
	 ->field_type('destacado','invisible')
	 ->field_type('user_id','invisible')
	 ->field_type('vistas','invisible')
	 ->field_type('pausado','invisible')
	 ->field_type('bloqueado','invisible')
	 ->field_type('forma_pago','hidden')
	 ->field_type('status','invisible')
	 ->field_type('novedades','invisible')
	 ->field_type('token','hidden')
	 ->field_type('codigo_promocion','invisible')
	 ->field_type('fecha_publicacion','invisible')
	 ->field_type('conekta_plan_id','invisible')
	 ->field_type('tipos_venue_id','hidden')
	 ->field_type('requiere_factura','checkbox')
	 ->field_type('solicitud_telefono','invisible')
	 ->field_type('fecha_aprobacion','invisible')
	 ->display_as('descripcion','Descripción del venue')
	 ->display_as('telefono_contacto','Teléfono de contacto')
	 ->display_as('correo_contacto','Correo de contacto')
	 ->display_as('direccion','Dirección del venue')
	 ->display_as('email_facturacion','Correo de facturación')
	 ->display_as('rfc','RFC')
	 ->display_as('razon_social','Razón social')
	 ->callback_field('estado',function($val){
	 	$hestado = !empty($val)?$this->db->get_where('estados',array('id'=>$val))->row()->nombre:'';
	 	return '<div class="estados listadoDropableSearch" style="margin-bottom: 0px;">			        
			        <input type="text" id="estado" name="hestado" class="estado form-control" placeholder="Estado" value="'.$hestado.'">
			        <input id="field-estado" type="hidden" name="estado" class="hciudad" value="'.$val.'">
			        <ul></ul>
			    </div>';
	 })
	 ->callback_field('ciudad',function($val){
	 	$hestado = !empty($val)?$this->db->get_where('ciudades',array('id'=>$val))->row()->nombre:'';
	 	return '<div class="estados listadoDropableSearch" style="margin-bottom: 0px;">
			        <label>En caso de que tu venue no esté en la CDMX:</label>			        
			        <input type="text" id="ciudad" name="hciudad" class="ciudad form-control" placeholder="Ciudad / Municipio" value="'.$hestado.'">
			        <input id="field-ciudad" type="hidden" name="ciudad" class="hciudad" value="'.$val.'">
			        <ul></ul>
			    </div>';
	 })
	 ->callback_field('colonia',function($val){
	 	$hestado = !empty($val)?$this->db->get_where('colonias',array('id'=>$val))->row()->nombre:'';
	 	return '<div class="estados listadoDropableSearch" style="margin-bottom: 0px;">
			        <input type="text" id="colonia" name="hcolonia" class="colonia form-control" placeholder="Colonia" value="'.$hestado.'" style="margin-bottom: 0px;">
			        <input id="field-colonia" type="hidden" name="colonia" class="hcolonia" value="'.$val.'">
			        <ul></ul>
			    </div>';
	 })
	 ->field_type('descripcion','editor',array('type'=>'textarea'))	 
	 ->callback_field('requiere_factura',function($val,$row){
	 	$checked = $val==1?'checked':'';
	 	return '<label id="btn-blanco" type="button" class="'.($val==1?'active':'').' button button-md margin-top-10 text-center" style="font-size: 10px;">
					<input type="checkbox" id="field-requiere_factura" name="requiere_factura" value="1" style="display: none;width: 10px;" '.$checked.'> Requiero factura
				</label>';
	 })
	 ->callback_field('mapa',function($val){
	 	$valor = $val;
	 	$val = str_replace(array('(',')'),array('',''),$val);
	 	list($lat,$lng) = explode(', ',$val);
	 	return '<input type="hidden" name="mapa" value="'.$valor.'" id="ubicacion"><div id="mapa" data-lat="'.$lat.'" data-lng="'.$lng.'" style="width:100%; height:250px;"></div>';
	 })
	 ->callback_field('amenidades',function($val,$row){
	  $val = $this->elements->venues(array('venues.id'=>base64_decode($_GET['key'])),FALSE)->row()->amenidades;
	  $amenidades = $this->db->get_where('amenidades');
	  $data = '<select id="field-amenidades" multiple name="amenidades[]" class="form-control chosen-multiple-select">';
	  foreach($amenidades->result() as $a){
	    $selected = in_array($a->id,$val)?'selected':'';
	    $data.= '<option value="'.$a->id.'" '.$selected.'>'.$a->nombre.'</option>';
	  }
	  $data.= '</select>';
	  return $data;
	})
	 ->set_primary_key_value(base64_decode($_GET['key']));
$crud->set_url('entradas/backend/venues/');
$crud = $crud->render(3,'application/modules/paginas/views/cruds/');

$css_files = array_merge($crud->css_files,$espacios->css_files);
$js_files = array_merge($crud->js_files,$espacios->js_files);
?>

<style>
	input[type="radio"]{
		display: inline-block !important;
		margin-right: 3px;
	}
</style>
<!-- About section -->
<div class="section margin-registro-top text-general-p">
	<!-- Product Tab content -->
	<div class="section no-padding margin-top-30">
		<div class="container">
			<div class="product-tab">
				<ul class="nav margin-bottom-20 text-uppercase">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#tab-venues"><h5>Editar Venue</h5></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#tab-contactos"><h5>Espacios <span></span></h5></a>
					</li>
				</ul>
				<div class="tab-content">
					<!-- Description tab content -->
					<div class="tab-pane fade  show active" id="tab-venues">
						<?php 
							//View en application/modules/paginas/views/cruds/mis-venues/list_template.php
							//View en application/modules/paginas/views/cruds/mis-venues/list.php							
							echo $crud->output;
						?>
					</div>
					<div class="tab-pane fade" id="tab-contactos">
						<?php 
							//View en application/modules/paginas/views/cruds/mis-venues/list_template.php
							//View en application/modules/paginas/views/cruds/mis-venues/list.php							
							echo $espacios->output;
						?>
					</div>
				</div>
			</div>
			</div><!-- end container -->
		</div>
		<!-- end Product Tab content -->
	</div>
	<!-- end About section -->
	

    <script>
		var autocompleteGraph = autocomplete;
    	function pauseVenue(id){    	  
          $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(0);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Pausar');
          $("#pausar-publicacion").modal('toggle');
    	}

    	function starVenue(id){
    	  $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(1);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Activar');
          $("#pausar-publicacion").modal('toggle');
    	}

    	function actualizar(form){
    		insertar('entradas/backend/venues/update/<?= base64_decode($_GET['key']) ?>',form,'#report-success');
    		return false;
    	}

    	function fillInAddress(){
		    var place = autocomplete.getPlace().geometry.location;
		    marker.setPosition(new google.maps.LatLng(place.lat(),place.lng()));
		    map.panTo(new google.maps.LatLng(place.lat(),place.lng()));
		    $("#ubicacion").val(marker.getPosition());
		}

		function searchDireccion(direccion){
	        var geocoder = new google.maps.Geocoder();
	        var address = direccion;
	        geocoder.geocode({'address': address}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    for(var i in results){
                        CrearMarcas(results[i]);
                    }
                    if(scope.lugares!==undefined){
                        scope.lugares(results);
                    }
                } else {
                  alert('No se ha podido encontrar la ubicación indicada: ' + status);
                  window.history.back();
                }
	        });
		}

    	$(document).on('ready',function(){
    		$('#field-telefono_contacto').mask('+00 0 00 0000-0000',{placeholder: "+__ _ __ ____-____"});
    		$("#field-requiere_factura").on('click',function(){    			
    			if($(this).prop('checked')){
    				$("#rfc_field_box,#razon_social_field_box").show();
    				$(this).parent().addClass('active');
    			}else{
    				$("#rfc_field_box,#razon_social_field_box").hide();
    				$(this).parent().removeClass('active');
    			}
    		});
    		if($("#field-requiere_factura").prop('checked')){
				$("#rfc_field_box,#razon_social_field_box").show();
			}else{
				$("#rfc_field_box,#razon_social_field_box").hide();
			}
			var mapa = $("#mapa");
			var c = document.getElementById('mapa');
    		var opt = {
    			center:new google.maps.LatLng(mapa.data('lat'),mapa.data('lng')),
    			zoom:16
    		};
    		map = new google.maps.Map(c,opt);
    		marker = new google.maps.Marker({
    			position:new google.maps.LatLng(mapa.data('lat'),mapa.data('lng')),
    			map:map
    		});

    		google.maps.event.addListener(marker,'dragend',function(){
		        $('#ubicacion').val(marker.getPosition());
		    });

    		var input = document.getElementById('field-direccion');
  			autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode','establishment']});
  			autocomplete.addListener('place_changed', fillInAddress);
    	});
    </script>
    <?php endif ?>

    <?php 
    if(!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>

<script>
  $(document).on('ready',function(){
    $(".chosen-select,.chosen-multiple-select").chosen({"search_contains": true, allow_single_deselect:true});  
  });
</script>

<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/tinymce.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/jquery.tinymce.js"></script>
<script>	
	var css = "<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.css,";
	    css+= "<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.css,";
	    css+= "<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.theme.default.min.css,";
	    css+= "<?= base_url() ?>theme/assets/plugins/magnific-popup/magnific-popup.min.css,";	    
	    css+= "https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css,";
	    css+= "<?= base_url() ?>theme/assets/css/main.css,";
	    css+= "<?= base_url() ?>theme/assets/css/responsive.css,";
	    css+="<?= base_url() ?>theme/assets/font/avenir/style.css,";
	    css+= "<?= base_url() ?>theme/assets/plugins/font-awesome/css/all.css,";
	    css+= "<?= base_url() ?>theme/assets/plugins/themify/themify-icons.min.css,";
	    css+= "https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900,900i,";
	    css+= "<?= base_url('assets/grocery_crud/css/jquery_plugins/chosen/chosen.css') ?>";
	tinymce.init({
		selector: '#field-descripcion',
		theme:'modern',
		image_advtab: true, 
		menubar: false,
		statusbar:false,
		toolbar1: 'undo redo',
		plugins: "link paste visualblocks code spellchecker, searchreplace, table, image, imagetools, textcolor, filemanager, fullscreen",
		images_upload_base_path: '/img/uploads',
		relative_urls: true,
		convert_urls: false,
		cleanup_on_startup: false,
		trim_span_elements: false,
		verify_html: false,
		cleanup: false,
		paste_as_text: true,
		content_css:css
	});
</script>

<script>
	var completar, completarCiudad,completarEstado,completarColonia;
	$(document).ready(function () {        
        completarEstado = new autocompleteGraph($("input.estado"),'entradas/frontend/getDataEstado',1,document.getElementById('formVenue'));
        completarCiudad = new autocompleteGraph($("input.ciudad"),'entradas/frontend/getDataCiudad',1,document.getElementById('formVenue'));
        completarColonia = new autocompleteGraph($("input.colonia"),'entradas/frontend/getDataColonia',1,document.getElementById('formVenue'));        
    });
    
    function selEstado(nombre,id){
        $("input.estado").val(nombre);
        $("input.hestado").val(id);
        completarCiudad._search();
        $("input.ciudad").val('');
        $("input.hciudad").val('');
        $("input.colonia").val('');
        $("input.hcolonia").val('');
        cachearDatos();
    }
    
    function selCiudad(nombre,id){
        $("input.ciudad").val(nombre);
        $("input.hciudad").val(id);
        completarColonia._search();
        $("input.colonia").val('');
        $("input.hcolonia").val('');
        cachearDatos();
    }
    
    function selColonia(nombre,id){
        $("input.colonia").val(nombre);
        $("input.hcolonia").val(id);
        cachearDatos();
    }
</script>