<!--
<div class="row section-title-interna3">
  <div class="col-12"><h2 class="text-uppercase font-montserrat"><b><?= $e->nombre ?></b></h2></div>
</div>-->

<div class="row contenedor-espacios-venues">
    <!-- Slider -->
    <div class="col-12 col-sm-4 product-single margin-bottom-50" id="product-single">
      <div class="product-single-img">
        <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
          <div class="owl-stage-outer owl-height" style="height: 158px;">
            <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
              <?php foreach($e->fotos->result() as $n=>$f): ?>
                <div class="owl-item <?= $n==0?'active':'' ?>"><div data-hash="first"><img src="<?= $f->foto ?>" alt="<?= $e->nombre ?>"></div></div>
              <?php endforeach ?>
            </div>
          </div>

          <?php if($e->fotos->num_rows()>1): ?>
            <div class="owl-nav">
              <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
              <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
            </div>
          <?php endif ?>
        </div>
      </div>
      <!-- end product-img -->

      <div class="favoritos-home2" style="text-align: right;">
          <button type="button" onclick="addFav('<?= $detail->id ?>')">
            <div class="notification-icon prueba animate-swing">
              <i id="" class="icon<?= $detail->id ?> far <?= $detail->fav?'fas':'' ?> fa-heart"></i>
            </div>
          </button>
      </div>

      <div class="titulo-venue-home" style="margin-top: 15px;">
        <?php
            $e->direccion = $detail->direccion;
            $e->link = $detail->link;
            $this->load->view('es/includes/venues/precios-venues',array('d'=>$e),FALSE,'paginas');
         ?>
      </div>
    </div>
    <!-- Slider -->

    <div class="col-12 col-sm-8"><p class="text-justify"><?= $e->descripcion ?></p></div>
</div>
