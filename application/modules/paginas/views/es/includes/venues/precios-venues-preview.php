<?php
if(!empty($d->capacidad)){
	$d->capacidad = explode('-',$d->capacidad);
	$d->capacidad[0] = number_format($d->capacidad[0],0,'.',',');
	$d->capacidad[1] = number_format($d->capacidad[1],0,'.',',');
	$d->capacidad = implode('-',$d->capacidad);
}
if(!empty($d->precio)){
	$d->precio = explode('-',$d->precio);
	$d->precio[0] = '$'.number_format($d->precio[0],0,'.',',');
	$d->precio[1] = '$'.number_format($d->precio[1],0,'.',',');
	$d->precio = implode('-',$d->precio).'MXN';
}
?>
<a href="<?= $d->link ?>">

  <h5 class="text-uppercase font-montserrat"><b><?= $d->nombre ?></b></h5>
  <div class="text-general-p text-resultas-movil text-responsive"><?= $d->direccion ?></div>


  <div class="row text-general-p font-montserrat font-weight-bold">
    <div class="col-6 col-sm-6" id="iconos-precio-venues" style="padding-left: 0px; padding-right: 0px;">
      <img src="<?= base_url() ?>theme/assets/images/iconos/personas.png" alt="Capacidad del Venue"> <?= $d->capacidad ?>
    </div>

    <div class="col-6 col-sm-6" id="iconos-precio-venues" style="padding-left: 0px; padding-right: 0px;">
      <img src="<?= base_url() ?>theme/assets/images/iconos/presupuesto.png" alt="Precio del Venue"> <?= $d->precio ?>
    </div>
  </div>

</a>
