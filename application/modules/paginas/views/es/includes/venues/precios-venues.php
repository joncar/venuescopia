<div  itemscope itemtype="https://schema.org/Place">
<a href="<?= $d->link ?>" itemprop="url">
  <h5 class="text-uppercase font-montserrat"><b itemprop="name"><?= $d->nombre ?></b></h5>
  <div class="text-general-p text-resultas-movil" itemprop="address" itemtype="https://schema.org/PostalAddress"><?= $d->direccion ?></div>

  <div class="row text-general-p font-montserrat font-weight-bold precios-venues">
    <div class="col-5 col-sm-5 col-lg-5" id="iconos-precio-venues2" style="padding-left: 0px; padding-right: 0px; margin-bottom: 0px;">
      <img src="<?= base_url() ?>theme/assets/images/iconos/personas.png" alt="Capacidad del Venue"> <?= $d->capacidad ?>
    </div>

    <div class="col-6 col-sm-7 col-lg-7" id="iconos-precio-venues" style="padding-left: 0px; padding-right: 0px;">
      <img src="<?= base_url() ?>theme/assets/images/iconos/presupuesto.png" alt="Precio del Venue"> <?= $d->precio ?>
    </div>
  </div>

</a>
</div>