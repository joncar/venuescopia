<!-- Destacadas -->
<div class="row section-title section-home">
    <div class="col-12" style="margin-bottom: 0px;">
      <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Recomendaciones</b></h2>
      <div class="subtitulos-general text-responsive">¡Más venues para ti!</div>
    </div>
</div><!-- end section-title -->

<div class="row margin-bottom-30">
  <div class="col-12">

      <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true" >
        <?php foreach($this->elements->venues(array('venues.id !='=>$detail->id))->result() as $v): ?>
          <div class="border-radius bg-white">
            <a href="<?= $v->link ?>" title="Ver Venue">
              <div class="mx-auto d-block imagen-venues-home"><img src="<?= $v->foto ?>" alt="<?= $v->nombre ?>" alt="Venues Recomendados"></div>
            </a>
            <div class="favoritos-home">
              <div class="wrap">
                <button type="button" onclick="addFav('<?= $v->id ?>')">
                  <div class="notification-icon prueba animate-swing"> 
                    <i id="icon<?= $v->id ?>" class="icon<?= $v->id ?> far <?= $v->fav?'fas':'' ?> fa-heart"></i>
                  </div>
                </button>
              </div>
            </div>
            <div class="titulo-venue-home" id="inicio-carrousel"><?php $this->load->view('es/includes/venues/precios-venues',array('d'=>$v),FALSE,'paginas');?></div>
          </div>
        <?php endforeach ?>
      </div>

  </div>
</div>
<!-- Destacadas -->
