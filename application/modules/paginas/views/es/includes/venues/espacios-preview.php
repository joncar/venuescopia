  


  <div class="row margin-bottom-30">

    <!-- Product box -->
    <div class="col-12 col-lg-4 product-single margin-bottom-20" id="product-single">
      <div class="product-single-img">
        <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
          <div class="owl-stage-outer owl-height" style="height: 158px;">
            <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
              
              <?php foreach($e->fotos as $n=>$f): ?>
                <div class="owl-item <?= $n==0?'active':'' ?>">
                  <div data-hash="first"><img src="<?= base_url().'img/venues/'.$f; ?>" alt="<?= $e->nombre ?>"></div>
                </div>
              <?php endforeach ?>
            </div>


          </div>

          <div class="owl-nav">
            <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
            <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
          </div>
        </div>
      </div>
      <!-- end product-img -->

      <!--<div class="favoritos-home2" style="text-align: right;">
          <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
      </div>-->

      <div class="titulo-venue-home" style="margin-top: 15px;>
        <?php 
            $e->direccion = $detail->direccion;           
            $e->link = 'javascript:void(0)';
            $this->load->view('es/includes/venues/precios-venues-preview',array('d'=>$e),FALSE,'paginas');
         ?>
      </div>
    </div>


    <div class="col-12 col-lg-8 margin-bottom-50">
        <p>
          <?= nl2br($e->descripcion) ?>
        </p>
    </div>
  </div>
