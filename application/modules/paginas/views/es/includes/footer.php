<!-- Desktop  -->
<footer class="d-none d-lg-block">
  <div class="footer bg-dark-lighter text-uppercase">
    <div class="container" style="padding-left: 0px;padding-right: 0px;">
      <div class="row" style="display: flex; justify-content: center; align-items: center;">
        <div class="col-12 col-sm-3 col-md-3 col-lg-3 logo-footer" style="padding-left: 0px;padding-right: 0px;">
          <a href="<?= base_url() ?>">
            <img src="<?= base_url() ?>theme/assets/images/icon-venuescopia-footer.png" alt="Logo Venuescopia">
          </a>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-2 text-center" style="padding-left: 0px;padding-right: 0px;" id="margin-footer-01">
          <ul class="list-icon ">
            <li><a href="<?= base_url() ?>publicar.html">Anuncia tu Venue</a></li>
            <li><a href="<?= base_url() ?>about.html">About</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-2 text-center" style="padding-left: 0px;padding-right: 0px;" id="margin-footer-02">
          <ul class="list-icon ">
            <li><a href="<?= base_url() ?>faq.html">FAQ</a></li>
            <li><a href="<?= base_url() ?>contacto.html">Contacto</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-2 text-center" style="padding-left: 0px;padding-right: 0px; z-index: 99999999; position: relative;" id="margin-footer-03">
          <ul class="list-icon ">
            <li><a href="<?= base_url() ?>terminos-condiciones.html">Términos y Condiciones</a></li>
            <li><a href="<?= base_url() ?>aviso-privacidad.html">Aviso de Privacidad</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-3 col-md-3 col-lg-3 text-right" id="margin-footer-redes">
          <ul class="list-horizontal-unstyled">
            <li><a href="https://www.facebook.com/venuescopia/" target="_new"><i class="fab fa-facebook-square" style="font-size:22px;"></i></a></li>
            <li><a href="https://instagram.com/venuescopia?igshid=11h7ubsvl7316" target="_new"><i class="fab fa-instagram" style="font-size:22px;"></i></a></li>
          </ul>
        </div>
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end footer -->

  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 text-responsive" style="font-size: 10px;">© <?= date("Y") ?> by Venuescopia</div>
      </div><!-- end row -->
    </div><!-- end container -->
  </div>
</footer>


<!-- Movil -->
<footer class="d-block d-lg-none">
  <div class="footer bg-dark-lighter text-uppercase" id="footer-movil">
    <div class="container">




      <div class="row align-items-center">
        <div class="col-6 logo-footer" style="padding-bottom: 10px;">
          <a href="<?= base_url() ?>">
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia-blanco.png" alt="Logo Venuescopia" class="mx-auto">
          </a>
        </div>
        <div class="col-6 margin-redes-movil font-movil-footer" style="padding-bottom: 10px;">
          <div class="iconos-footer-movil">
            <a href="https://www.facebook.com/venuescopia/"><i class="fab fa-facebook-square" style="font-size:22px;"></i></a>
            <a href="https://instagram.com/venuescopia?igshid=11h7ubsvl7316"><i class="fab fa-instagram" style="font-size:22px;"></i></a>
          </div>
          <!--
          <a href="https://www.venuescopia.com/terminos-condiciones.html" style="text-decoration: underline;">Términos y Condiciones</a><br>
          <a href="https://www.venuescopia.com/aviso-privacidad.html" style="text-decoration: underline;">Aviso de Privacidad</a>-->
        </div>
      </div>





      <!--
      <div class="row">
        <div class="col-12 col-md-6 col-lg-3 logo-footer" style="padding-left: 0px;padding-right: 0px;">
          <a href="<?= base_url() ?>">
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia-blanco.png" alt="Logo Venuescopia" class="mx-auto d-block">
          </a>
        </div>
        <div class="col-12 col-md-6 col-lg-3 text-center margin-bottom-30 margin-redes-movil">
          <ul class="list-horizontal-unstyled">
            <li><a href="https://www.facebook.com/venuescopia/"><i class="fab fa-facebook-square" style="font-size:22px;"></i></a></li>
            <li><a href="https://instagram.com/venuescopia?igshid=11h7ubsvl7316"><i class="fab fa-instagram" style="font-size:22px;"></i></a></li>
          </ul>
        </div>
      </div>

      <div class="row margin-bottom-20 font-movil-footer">
        <div class="col-12 text-center" style="text-decoration: underline; margin-bottom: 0px;">
          <a href="https://www.venuescopia.com/terminos-condiciones.html">Términos y Condiciones</a><br>
          <a href="https://www.venuescopia.com/aviso-privacidad.html">Aviso de Privacidad</a>
        </div>
      </div>-->





    </div><!-- end container -->
  </div><!-- end footer -->

  <div class="footer-bottom">
    <div class="container">
        <div class="col-12 text-responsive" style="font-size: 10px;">© <?= date("Y") ?> by Venuescopia</div>
    </div><!-- end container -->
  </div>
</footer>
