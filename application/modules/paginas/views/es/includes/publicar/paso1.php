<?php
if (!empty($_SESSION['publicacion'])) {
    $cache = $_SESSION['publicacion'];
}
?>
<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col12 col-sm-12" style="margin-bottom: 0px;">
        <h2 class="text-uppercase titulos-general2 font-montserrat"><b>1. Registra tu Venue</b></h2>
    </div>
</div>
<div class="row margin-bottom-20">
    <div class="col12 col-sm-12">
        <p style="font-size:13px;">
            ¡Bienvenido a Venuescopia!<br>
            En los próximos minutos, te guiaremos a través de unos simples pasos para que logres registrar con éxito tu venue.
        </p>
    </div>
</div>
<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col12 col-sm-12">
        <h2 class="text-uppercase titulos-general2 font-montserrat"><b>¿Cuál es el nombre de tu Venue?</b></h2>
    </div>
</div>
<div class="row margin-bottom-20">
    <div class="col12 col-sm-12">
        <input type="text" id="nombre" name="nombre" placeholder="Nombre" class="margin-bottom-20" value="<?= @$cache['nombre'] ?>">
        <textarea rows="10" name="descripcion" id="descripcion" placeholder="Descripción de tu venue. (Recuerda que no puedes agregar url’s de otro sitio ajeno a venuescopia.com)" maxlength="1500" style="font-size: 13px;"><?= @$cache['descripcion'] ?></textarea>
    </div>
</div>
<!-- Botones -->
<div class="margin-bottom-50">
    <div class="col-12 col-sm-12" id="btn-continuar-anuncia">
        <button type="button" class="button button-md text-center" id="btn-negro" onclick="showTask(2)" title="Continuar" data-step="1"><b>Continuar</b></button>		
    </div>
</div>
<!-- Botones -->
