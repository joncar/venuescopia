<?php
if (!empty($_SESSION['publicacion'])) {
    $cache = $_SESSION['publicacion'];
}
?>
<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col12 col-sm-12" style="margin-bottom: 0px;">
        <h2 class="text-uppercase titulos-general2 font-montserrat"><b>3. Agrega detalles de tu Venue</b></h2>
    </div>
</div>
<div id="espaciosDatos" class="margin-bottom-20">
    <div class="row" data-ident='0'>
        <div class="col12 col-sm-12 margin-bottom-20">
            <div class="no-padding father" style="margin-bottom: 0px;">
                <div class="contact-form no-padding">
                    <div class="form-row">
                        <div class="col-12 col-sm-12 margin-bottom-20">
                            <label class="espaciostag">Venue Principal</label>
                            <input class="nombreEspacio" type="text" name="espacios[0][nombre]" placeholder="Nombre del espacio" class="nombreEspacio" readonly="" value="<?= @$cache['espacios'][0]['nombre'] ?>">
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="button text-center" type="button" onclick="capacidad($(this).parents('.father'))" id="btn-blanco-small" style="width: 100%; cursor: pointer;"><b class="d-none d-sm-block">Capacidad</b> <b class="d-sm-none capacidadtag"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][0]['capacidad']) ? $cache['espacios'][0]['capacidad'] : 'Capacidad' ?></b></div>
                            <!-- Preview - esto se muestra una vez que escogiste opciones del modal -->
                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                <img src="<?= base_url() ?>theme/assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:6%;"> <span class="capacidadtag"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][0]['capacidad']) ? $cache['espacios'][0]['capacidad'] : 'Capacidad' ?></span>
                            </div>
                        </div>
                        <div class="col-6 col-sm-4">
                            <div class="button text-center" type="button" onclick="precio($(this).parents('.father'))" id="btn-blanco-small" style="width: 100%; cursor: pointer;"><b class="d-none d-sm-block">Precio</b> <b class="d-sm-none preciotag"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][0]['precio']) ? $cache['espacios'][0]['precio'] : 'Precio' ?></b></div>
                            <!-- Preview -->
                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                <img src="<?= base_url() ?>theme/assets/images/iconos/presupuesto.png" alt="Amenidades del espacio" class="iconos-venue" style="width:6%;"> <span class="preciotag"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][0]['precio']) ? $cache['espacios'][0]['precio'] : 'Precio' ?></span>
                            </div>
                        </div>

                        <div class="col-12 col-sm-4 amenidadesContent">
                            <div class="button text-center" type="button" onclick="amenidades($(this).parents('.father'))" id="btn-blanco-small" style="width: 100%; cursor: pointer;"><b class="d-none d-sm-block">Amenidades</b> <b class="d-sm-none amenidadestag">Amenidades</b></div>
                            <!-- Preview -->
                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                <a class="margin-bottom-20 text-center amenidades-registro" onclick="amenidades($(this).parents('.father'))" type="button" style="width: 100%;"><span class="amenidadestag"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][0]['amenidades']) ? count(explode(',',$cache['espacios'][0]['amenidades'][0]))-1 : '0' ?> amenidades</span> <i class="fas fa-eye"></i></a>
                            </div>
                        </div>
                        
                        <input type="hidden" class="precio" name="espacios[0][precio]" value="<?= @$cache['espacios'][0]['precio'] ?>">
                        <input type="hidden" class="capacidad" name="espacios[0][capacidad]" value="<?= @$cache['espacios'][0]['capacidad'] ?>">
                        <input type="hidden" class="amenidades" name="espacios[0][amenidades][]" value="<?= !empty($cache['espacios'][0]['amenidades']) && is_array($cache['espacios'][0]['amenidades']) ? implode(',', @$cache['espacios'][0]['amenidades']) : 0 ?>">
                        <div class="col-12 col-sm-12  margin-bottom-20">
                            <textarea rows="10" name="espacios[0][descripcion]" placeholder="Descripción del espacio"  maxlength="800" style="display: none;"><?= @$cache['espacios'][0]['descripcion'] ?></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end row -->
</div>

<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
        <label>
            ¡Atención!<br>
            <span style="font-size: 13px;">¿Tu Venue cuenta con varios espacios que puedan ser rentados al mismo tiempo para diferentes eventos?
        </label>
    </div>
</div>

<div class="row membresias margin-bottom-10">
    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
        <button type="button" style="width:90px;" class="button button-md margin-top-10 text-center <?= !empty($cache) && !empty($cache['espacios']) && count($cache['espacios']) > 1 ? 'active' : '' ?>" data-toggle="collapse" data-target="#hasEspacios" onclick="$('#noHasEspacios').collapse('hide');
                $('.membresias button').removeClass('active');
                $(this).addClass('active');" type="checkbox" checked autocomplete="off" id="btn-blanco">SI</button>
        <button type="button" style="width:90px;" class="button button-md margin-top-10 text-center" data-toggle="collapse" data-target="#noHasEspacios" onclick="noespacios()" type="checkbox" checked autocomplete="off" id="btn-blanco">NO</button>
    </div>
    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
        <div class="collapse multi-collapse <?= !empty($cache) && !empty($cache['espacios']) && count($cache['espacios']) > 1 ? 'in show' : '' ?>" id="hasEspacios">
            <div class="card card-body">
                <label style="font-size: 13px;">Además del espacio principal ¿Con cuántos espacios para renta adicionales cuenta tu venue?</label>
                <select class="custom-select" id="numero_espacios">
                    <option value="">Selecciona la cantidad</option>
                    <?php for ($i = 1;$i <= 8;$i++): ?>
                        <option value="<?= $i ?>" <?= !empty($cache) && !empty($cache['espacios']) && count($cache['espacios'])-1 == $i ? 'selected' : '' ?>><?= $i ?></option>
                    <?php endfor ?>
                </select>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
        <div class="collapse multi-collapse" id="noHasEspacios"></div>
    </div>
</div>

<div id="espaciosDatosExtras" class="margin-bottom-20">

<?php
if (!empty($cache) && !empty($cache['espacios']) && count($cache['espacios']) > 1):
    foreach ($cache['espacios'] as $n =>$v):
        if ($n > 0):
            ?>
                <div class="row margin-bottom-20" data-ident='<?= $n ?>'>
                    <div class="col12 col-sm-12 margin-bottom-20">
                        <div class="no-padding father" style="margin-bottom: 0px;">
                            <div class="contact-form no-padding">
                                <div class="form-row">
                                    <div class="col-12 col-sm-12 margin-bottom-20">
                                        <label class="espaciostag">Espacio <?= $n ?></label>
                                        <input class="nombreEspacio" type="text" name="espacios[<?= $n ?>][nombre]" placeholder="Nombre del espacio" class="nombreEspacio" value="<?= @$cache['espacios'][$n]['nombre'] ?>">
                                    </div>
                                    <div class="col-6 col-sm-4" style="margin-bottom: 0px;">
                                        <div class="button text-center" type="button" onclick="capacidad($(this).parents('.father'))" id="btn-blanco-small" style="width: 100%; cursor: pointer;"><b class="d-none d-sm-block">Capacidad</b> <b class="d-sm-none"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][$n]['capacidad']) ? $cache['espacios'][$n]['capacidad'] : 'Capacidad' ?></b></div>
                                        <!-- Preview - esto se muestra una vez que escogiste opciones del modal -->
                                        <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                            <img src="<?= base_url() ?>theme/assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:6%;"> <span class="capacidadtag"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][$n]['capacidad']) ? $cache['espacios'][$n]['capacidad'] : 'Capacidad' ?></span>
                                        </div>
                                    </div>
                                    <div class="col-6 col-sm-4" style="margin-bottom: 0px;">
                                        <div class="button text-center" type="button" onclick="precio($(this).parents('.father'))" id="btn-blanco-small" style="width: 100%; cursor: pointer;"><b class="d-none d-sm-block">Precio</b> <b class="d-sm-none"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][$n]['precio']) ? $cache['espacios'][$n]['precio'] : 'Precio' ?></b></div>
                                        <!-- Preview -->
                                        <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                            <img src="<?= base_url() ?>theme/assets/images/iconos/presupuesto.png" alt="Amenidades del espacio" class="iconos-venue" style="width:6%;"> <span class="preciotag"><?= !empty($cache) && !empty($cache['espacios']) && !empty($cache['espacios'][$n]['precio']) ? $cache['espacios'][$n]['precio'] : 'Precio' ?></span>
                                        </div>
                                    </div>
                                    <input type="hidden" class="precio" name="espacios[<?= $n ?>][precio]" value="<?= @$cache['espacios'][$n]['precio'] ?>">
                                    <input type="hidden" class="capacidad" name="espacios[<?= $n ?>][capacidad]" value="<?= @$cache['espacios'][$n]['capacidad'] ?>">	

                                    <div class="col-12 col-sm-12  margin-bottom-20 margin-top-20">
                                        <textarea rows="10" name="espacios[<?= $n ?>][descripcion]" placeholder="Descripción del espacio"  maxlength="800"><?= @$cache['espacios'][$n]['descripcion'] ?></textarea>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end row -->
        <?php endif ?>
    <?php endforeach ?>
<?php endif ?>

</div>
<!-- Botones -->
<div class="row margin-bottom-50">
    <div class="col-12 col-sm-12">
        <button type="button" class="button button-md margin-top-10 text-center" id="btn-negro" onclick="showTask(2)" title="Conoce Venuescopia"><b>Regresar</b></button>
        <button type="button" class="button button-md margin-top-10 text-center" id="btn-negro" onclick="showTask(4)" title="Conoce Venuescopia"><b>Continuar</b></button>
    </div>
</div>
<!-- Botones -->
