<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
        <h2 class="text-uppercase titulos-general2 font-montserrat"><b>6. Pagar para finalizar</b></h2>
    </div>
</div>
<div class="row margin-texto-pago" style="margin-bottom: 20px;">
    <div class="col-12 col-sm-12">
        <p style="font-size:13px;">
            Elige el plan de tu preferencia<br>
            Una vez efectuado tu pago y en no más de 24 horas quedará lista la publicación de tu venue.<br><br>
            Recuerda que el pago es con renovación automática, podrás cancelar en el momento que desees. Precios con IVA incluido.
        </p>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".js-monthly").click(function () {
            $("#selection-one").prop("checked", false);
            $("#selection-two").prop("checked", true);
            $('.js-monthly').addClass('selected')
            $('.js-yearly').removeClass('selected')
        });
        $(".js-yearly").click(function () {
            $("#selection-one").prop("checked", true);
            $("#selection-two").prop("checked", false);
            $('.js-monthly').removeClass('selected')
            $('.js-yearly').addClass('selected')
        });
    });
</script>

<div class="row margin-bottom-20">

    <?php foreach ($this->elements->membresias()->result() as
            $m): ?>
        <div class="col-12 col-sm-6 precios-paso6" style="cursor:pointer;" onclick="activar(this)">
            <div class="prices-box bg-white beneficios-anunciar anuncia-precios" style="font-size: 10px; font-family:'Avenir LT Std 35 Light'; letter-spacing: 1.5px;">
                <div data-precio="<?= $m->precio ?>" data-dias class="c-selection-card js-yearly">
                    <input id="membresias_id-<?= $m->id ?>" name='membresias_id' class='c-selection-card__selector' type='radio' value='<?= $m->id ?>' data-val="<?= $m->precio ?>" data-dias="<?= $m->dias_renovacion ?>" />
                    <label for='membresias_id-<?= $m->id ?>'>
                        <div class="control__indicator"></div>
                    </label>
                    <div class="c-selection-card__body">
                        <h3 class="font-montserrat">Costo por Venue</h3>
                        <div class="price" style="margin: 10px 0px;">
                            <h2 class="font-montserrat text-center">
                                <span style="letter-spacing: 1px; font-weight: 700; font-family: 'Montserrat', sans-serif;" id="texto-precios-pasos">
                                    <?= number_format($m->precio, 0, '.', ',') ?>
                                </span>
                                <span style="font-size: 10px; letter-spacing: 1.5px;">/<?= $m->nombre ?></span>
                            </h2>
                        </div>
                        <p id="precios"><?= $m->descripcion ?></p>
                        <div class="ahorro-anuncia"><?= $m->accion ?></div>
                    </div>
                </div>
            </div>
        </div>
<?php endforeach ?>

</div>

<div class="row membresias">
    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
        <p style="font-size:13px;">Elige la opción de pago que deseas.</p>
    </div>

    <div class="col-12 col-sm-12" style="margin-bottom: 0px;padding: 0px; display: inline-flex;" id="btns-pagos-anuncia">
        <div class="col-6 col-sm-6" style="margin-bottom: 0px;"><button class="TDC button button-md margin-top-10 text-center" type="button" id="btn-blanco" onclick="pagarCon('1')" style="width: 100%; font-size: 9px; height: 60px; padding:0px 1px;">Tarjeta de crédito o débito</button></div>
        <div class="col-6 col-sm-6" style="margin-bottom: 0px;"><button class="PAYPAL button button-md margin-top-10 text-center" type="button" id="btn-blanco" onclick="pagarCon('2')" style="width: 100%; font-size: 9px; height: 60px; padding:0px 1px;">Paypal</button></div>
    </div>

    <div class="col-12 col-sm-12" style="margin-bottom: 30px;">
        <div class="collapse multi-collapse" id="conekta">
            <div class="card card-body">

                <div class="col-12 col-lg-12">
                    <input type="text" id="name" value="" name="tarjeta_nombre" placeholder="Nombre en la tarjeta">
                </div>

                <div class="row">
                    <div class="col-12 col-lg-7">
                        <input type="text" id="name" value="" name="tarjeta_numero" maxlength="16" placeholder="Número de la Tarjeta">
                    </div>

                    <div class="col-12 col-lg-3">
                        <input type="text" id="name" value="" name="tarjeta_fecha" placeholder="MM/AA">
                    </div>
                    <div class="col-12 col-lg-2">
                        <input type="text" id="name" value="" name="tarjeta_cvc" maxlength="4" placeholder="CVC">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-12 col-lg-12 collapse multi-collapse" id="paypal">
        <div class="card card-body" style="font-size:13px; letter-spacing: 0.05em; line-height:2;">Tipo de pago con PAYPAL seleccionado.</div>
    </div>
    <div class="col-12 col-lg-12" style="margin-bottom: 20px;">

        <div class="contact-form margin-codigo-movil">
            <p style="font-size:13px;">¿Tienes un código de descuento? Ingrésalo aquí</p>
            <div class="input-group">
                <input type="text" id="codigoPromocion" name="codigo_promocion" class="form-control" placeholder="XXXXXX" aria-describedby="button-addon2">
                <div class="input-group-append"><button onclick="ValidarCupon()" class="btn" type="button" id="btn-negro" style="font-family: 'Avenir LT Std 35 Light'; font-size: 10px; border-radius: 0px;">Validar cupón</button></div>
            </div>
        </div>

    </div>
    <input type="hidden" name="token" value="">
    <input type="hidden" name="forma_pago" value="1">
</div>

<div class="row text-right" style="padding: 40px 0px; font-size: 14px; font-family: 'Avenir LT Std 35 Light'; letter-spacing: 0.05em;">
    <div class="col-12" style="margin-bottom: 0px;">
        <span>SUBTOTAL:</span><span id="subtotalTag"> $0</span><br>
        <span>DESCUENTO:</span><span id="descuentoTag"> $0</span><br>
        <hr>
        <span style="font-family: 'Avenir LT Std 85 Heavy';"><b>TOTAL: <span id="totalTag">$0</b></span></span>
    </div>
</div>

<div class="row">
    <div class="col-12 btn-factura">
        <label id="btn-blanco" type="button" class="button button-md margin-top-10 text-center" style="font-size: 10px;">
            <input type="checkbox" name="requiere_factura" value="1" style="display: none;width: 10px;"> Requiero factura
        </label>
        <div class="collapse multi-collapse" id="requiere_factura">
            <div class="card card-body">
                <div class="col-12 col-lg-12">
                    <input type="text" id="name" value="" name="rfc" placeholder="RFC">
                </div>
                <div class="col-12 col-lg-12">
                    <input type="text" id="name" value="" name="razon_social" placeholder="Razón social">
                </div>
                <div class="col-12 col-lg-12" style="margin-bottom: 0px;">
                    <input type="email" id="name" value="" name="email_facturacion" placeholder="Correo de facturación">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row margin-bottom-20">
    <div class="col-12"> 
        <div class="resultado"></div>
    </div>
</div>

<div class="row margin-bottom-50">
    <div class="col-12 col-sm-12">
        <button type="button" class="button button-md margin-top-10 text-center" id="btn-negro" onclick="showTask(5)" title="Conoce Venuescopia"><b>Regresar</b></button>
        <?php if (empty($_SESSION['user'])): ?>
            <button type="button" class="botonLuegoDeConectar button button-md margin-top-10 text-center" id="btn-negro" onclick="conectar()"><b>Crear y pagar</b></button>
        <?php else: ?>
            <button type="submit" class="button button-md margin-top-10 text-center" id="btn-negro"><b>Crear y pagar</b></button>
<?php endif ?>
        <a id="btn-negro" class="ready button button-md margin-top-10 text-center" href="<?= base_url() ?>mis-venues.html" style="display: none; vertical-align: bottom"><b>Ir a mis venues</b></a>
    </div>
</div>
