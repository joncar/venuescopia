<div class="row section-title-interna" style="padding-left: 0px;">
	<div class="col-12 col-sm-12" style="margin-bottom: 0px;">
		<h2 class="text-uppercase titulos-general2 font-montserrat"><b>5. Visualiza tu anuncio</b></h2>
	</div>
</div>
<div class="row margin-visualizar-movil">
	<div class="col-12 col-sm-12">
		<p style="font-size:13px;">Antes del último paso, revisa tu anuncio de pies a cabeza y visualiza cómo se verá publicado.</p>
		<a class="button button-md margin-top-10 text-center" id="btn-blanco" onclick="preview()" href="javascript:void(0)" title="Conoce Venuescopia" style="margin-bottom: 20px;"><b>Visualizar</b></a>
	</div>
</div>
<div class="row margin-bottom-20">
	<div class="col-12 col-sm-12">
		<a class="button button-md margin-top-10 text-center" id="btn-negro" onclick="showTask(4)" href="javascript:void(0)" title="Conoce Venuescopia"><b>Regresar</b></a>
		<a class="button button-md margin-top-10 text-center" id="btn-negro" onclick="showTask(6)" href="javascript:void(0)" title="Conoce Venuescopia"><b>Continuar</b></a>
	</div>
</div>
