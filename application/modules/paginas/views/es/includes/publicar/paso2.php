<?php
if (!empty($_SESSION['publicacion'])) {
    $cache = $_SESSION['publicacion'];
}
?>
<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col12 col-sm-12" style="margin-bottom: 0px;">
        <h2 class="text-uppercase titulos-general2 font-montserrat"><b>2. Platícanos un poco más sobre tu Venue</b></h2>
    </div>
</div>
<div class="row margin-bottom-20">
    <div class="col12 col-sm-12">
        <label>Tipo de Venue</label>
        <?= form_dropdown_from_query('tipos_venue_id', 'tipos_venue', 'id', 'nombre', @$cache['tipos_venue_id']); ?>
    </div>
</div>
<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col12 col-sm-12">
        <h2 class="text-uppercase titulos-general2 font-montserrat" style="margin-bottom: 0px;"><b>Datos de contacto</b></h2>
        <label style="font-size: 12px;margin-bottom: 0px; padding-bottom: 20px;">A través de estos datos te contactarán los usuarios que requieran más información de tu venue.</label>
    </div>
</div>
<div class="row margin-bottom-20">
    <div class="col-12 col-lg-6">
        <input type="text" id="telefono_contacto" name="telefono_contacto" placeholder="Teléfono" value="<?= @$cache['telefono_contacto'] ?>">
    </div>
    <div class="col-12 col-lg-6">
        <input type="text" id="correo_contacto" name="correo_contacto" placeholder="Correo" value="<?= empty($cache) ? @$this->user->email : @$cache['correo_contacto'] ?>">
    </div>
</div>
<div class="row section-title-interna" style="padding-left: 0px;">
    <div class="col12 col-sm-12">
        <h2 class="text-uppercase titulos-general2 font-montserrat"><b>Enséñanos la ubicación de tu venue</b></h2>
    </div>
</div>
<div class="row margin-bottom-10">
    <div class="col-12 col-lg-12 estados listadoDropableSearch" style="margin-bottom: 10px;">
        <input type="text" id="estado" name="hestado" class="estado form-control" placeholder="Estado" value="<?= @$cache['hestado'] ?>">
        <input type="hidden" name="estado" class="hestado" value="<?= @$cache['estado'] ?>">
        <ul>
            <?php                 
                get_instance()->db->order_by('nombre','ASC');                
                foreach(get_instance()->db->get('estados')->result() as $v): 
            ?>
            <li><a href="javascript:selEstado('<?= $v->nombre ?>','<?= $v->id ?>')"><?= $v->nombre ?></a></li>
            <?php endforeach ?>
        </ul>
    </div>
    <div class="col-12 col-lg-12 estados listadoDropableSearch" style="margin-bottom: 10px;">
        <!--<span style="font-size: 12px;display: block;margin-top: -10px;">En caso de que tu venue no esté en la ciudad:</span>-->
        <input type="text" id="ciudad" name="hciudad" class="ciudad form-control" placeholder="Ciudad / Municipio" value="<?= @$cache['hciudad'] ?>" style="margin-bottom: 0px;">
        <input type="hidden" name="ciudad" class="hciudad" value="<?= @$cache['ciudad'] ?>">
        <ul></ul>
        <label style="font-size: 12px;margin-bottom: 0px;">Si tu venue no se encuentra en una ciudad indica el municipio.</label>
    </div>
    <div class="col-12 col-lg-12 estados listadoDropableSearch" style="margin-bottom: 10px;">
        <input type="text" id="colonia" name="hcolonia" class="colonia form-control" placeholder="Colonia" value="<?= @$cache['hcolonia'] ?>" style="margin-bottom: 0px;">
        <input type="hidden" name="colonia" class="hcolonia" value="<?= @$cache['colonia'] ?>">
        <ul></ul>
    </div>
</div>
<div class="row margin-bottom-20">
    <div class="col12 col-sm-12 contact-form">
        <div class="form-row">
            <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="text" id="direccion" name="direccion" placeholder="Ingresa la dirección" value="<?= @$cache['direccion'] ?>">
            </div>
            <div class="col-12 col-sm-12">
                <input type="hidden" name="mapa" id="ubicacion" value="<?= empty($cache) || empty($cache['mapa'])?'(0,0)':$cache['mapa'] ?>">
                <div id="mapa" style="width:100%; height:200px;"></div>
            </div>
        </div>
    </div>
</div><!-- end row -->
<!-- Botones -->
<div class="row margin-bottom-50">
    <div class="col12 col-sm-12">
        <button type="button" class="button button-md margin-top-10 text-center" id="btn-negro" onclick="showTask(1)" title="Conoce Venuescopia"><b>Regresar</b></button>
        <button type="button" class="button button-md margin-top-10 text-center" id="btn-negro" onclick="showTask(3)" title="Conoce Venuescopia"><b>Continuar</b></button>
    </div>
</div>
<!-- Botones -->
