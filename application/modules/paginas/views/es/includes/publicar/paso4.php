<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/slim.cropper/slim.min.css" />
<link rel="stylesheet" href="<?= base_url() ?>theme/assets/css/dropzone.css">
<style>
    #cropModal{
        left:-1000vh;
    }
</style>
<div id="fotoContent"></div>

<div class="margin-bottom-50">
    <div class="col-12 col-sm-12">
        <a class="button button-md margin-top-10 text-center submit" id="btn-negro" onclick="showTask(3)" title="Conoce Venuescopia"><b>Regresar</b></a>
        <a class="button button-md margin-top-10 text-center submit" id="btn-negro" onclick="showTask(5)" title="Conoce Venuescopia"><b>Continuar</b></a>
    </div>
</div>

<div class="row margin-bottom-10" id="fotoElement">
    <div class="titulo text-general-p" style="margin-bottom:20px;">
        <h2 class="text-uppercase titulos-general2 font-montserrat" style="letter-spacing: 5px;font-size: 18px;">
            <span class="fotostag">Venue principal</span>
        </h2>
        <div class="otherAlert">Selecciona todas las fotos de tu venue principal.<br>Recuerda que la primera imagen será la fotografía de portada.</div>
    </div>

    <div class="panel panel-default" style="width: 100%;">
        <div class="panel-body text-center">
            <div id="formDropZone">
                <div id='dZUpload1' class='dropzone borde-dropzone' style='cursor: pointer;'>
                    <div class='dz-default dz-message text-center'>
                        <i class="fas fa-cloud-upload-alt" style="font-size: 2em; color: #E5E5E5;"></i><br>
                        Arrastra tus archivos aquí<br>
                        o selecciona
                    </div>
                </div>
            </div>                
        </div>
    </div>
</div>



<div id="tpl" style="display: none;">
  <div class="dz-preview dz-file-preview">
      <div class="dz-image">
        <img data-dz-thumbnail="">
      </div>
      <div class="dz-details" style="display: none">
        <div class="dz-size"><span data-dz-size=""></span></div>
        <div class="dz-filename"><span data-dz-name=""></span></div>
      </div>
      <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
      <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
      <div class="dz-success-mark">
        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
          <title>Check</title>
          <desc>Created with Sketch.</desc>
          <defs></defs>
          <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
              <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
          </g>
        </svg>
      </div>
      <div class="dz-error-mark">
        <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
            <title>error</title>
            <desc>Created with Sketch.</desc>
            <defs></defs>
            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                    <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                </g>
            </g>
        </svg>
      </div>
      <input type="text" class="pie_foto" name="espacios[i00][pies_fotos][]" placeholder="Pie de foto" class="form-control" style="display: none;">      
      <input type="hidden" class="input_foto" name="espacios[i00][fotos][]" class="form-control">
      <div class="dz-buttons" style="display: none">
          <a href="javascript:void()" class="cropImage" style="display: none"><i class="fa fa-crop"></i></a>
          <a href="javascript:void()" data-dz-remove><i class="fa fa-trash"></i></a>
      </div>
    </div>
</div>

<!-- Modal -->
<div id="cropModal" class="modal fade">
    <div class="modal-dialog" style="width: 65%;">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title">Editar foto</h4>
        </div>
        <div class="modal-body">
            <div id="slim-foto"></div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>js/dropzone.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/exif-js/2.3.0/exif.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/slim.croppic.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/slim.cropper.js"></script>

<script>    
    var templateFotoElement = $("#fotoElement").clone();
    var tpl = document.querySelector('#tpl').innerHTML;
    var minWidth = <?= $this->min_width_image ?>;
    var minHeight = <?= $this->min_height_image ?>;
    $("#tpl").remove();
    $("#fotoElement").remove();    
    var FotoElement = function(){
        this.fotos = [];
        this.pies = [];
        this.nombre = '';                
        this.tpl = tpl;
        this.id = 0;
        this.dropzone = undefined;
        this.url = URL+'entradas/frontend/uploadPhotos';
        this.realPath = URL+'img/venues/';
        this.maxFotos = 4;
        this.mensajeMaxFiles = 'Ha exedido el máximo de fotos permitidas para este espacio';
        this.render = function(){
            this.templateFotoElement = templateFotoElement.clone();
            this.templateFotoElement.find('.fotostag').html(this.nombre);
            this.templateFotoElement.find('.dropzone').attr('id','dropzone'+this.id);                        
            this.tpl = this.tpl.replace(/i00/g,this.id);
            $("#fotoContent").append(this.templateFotoElement);        
            this.setDropZone();            
        }

        this.setNombre = function(nombre){
            this.nombre = nombre;
            this.templateFotoElement.find('.fotostag').html(this.nombre);
        }

        this.setDropZone = function(){
            var $this = this;
            var myAwesomeDropzone = {
                url: this.url,
                acceptedFiles: "image/*",
                addRemoveLinks: false,
                paramName: "fotos",
                maxFilesize: 5, // MB
                maxFiles:$this.maxFotos,
                dictRemoveFile: "<i class='fa fa-trash'></i>",
                uploadMultiple:false,
                params: {espacio:this.id},
                previewTemplate:this.tpl,
                dictCancelUpload:'<i class="fa fa-times"></i>',  
                thumbnailWidth:null,
                thumbnailHeight:180,
                resizeWidth:800,                 
                dictMaxFilesExceeded:$this.mensajeMaxFiles,                
                success: function (file, response) {
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");             
                    response = JSON.parse(imgName);                      
                    for(var i in response){                    
                        var enc = $this.fotos.find(x => x.nameServer==response[i]);                        
                        if(!enc){
                            var foto = file.previewTemplate;
                            foto.nameServer = response[i];
                            foto.pie_foto = '';                            
                            $this.fotos.push(foto);
                            $(file.previewElement).find('img').attr('src',$this.realPath+foto.nameServer);
                        }
                    }   
                    $(file.previewElement.querySelectorAll(".cropImage")).show();
                    $(file.previewElement.querySelectorAll(".pie_foto")).show();
                    $(file.previewElement.querySelectorAll(".dz-buttons")).show();
                    l.updateValues();   
                },accept:function(file,done){
                    if($this.fotos.length >= $this.maxFotos){
                        done($this.mensajeMaxFiles);
                    }else{
                        done();
                    }
                },error:function(file,error,code){
                    $(file.previewElement).addClass('dz-error');
                    $(file.previewElement.querySelectorAll("[data-dz-errormessage]")).html(error);
                    $(file.previewElement.querySelectorAll(".cropImage")).hide();
                    $(file.previewElement.querySelectorAll(".pie_foto")).hide();
                    $(file.previewElement.querySelectorAll(".dz-buttons")).show();
                }
            }
            
            this.dropzone = new Dropzone('#dropzone'+this.id, myAwesomeDropzone); 
            var l = this;
            this.dropzone.on("complete", function(file,response) {});
            this.dropzone.on('removedfile',function(file,response,args){
                var enc = $this.fotos.findIndex(x => x.nameServer==file.name);
                console.log(enc);
                if(enc){
                    l.fotos.splice(enc,1);
                }
                l.updateValues();
            });

            this.updateValues = function(){                
                for(var i in this.fotos){
                    console.log($(this.fotos[i]));
                    $(this.fotos[i]).find('.pie_foto').val(this.fotos[i].pie_foto);
                    $(this.fotos[i]).find('.input_foto').val(this.fotos[i].nameServer);
                    $(this.fotos[i]).find('.cropImage').attr('href','javascript:crop(\''+this.fotos[i].nameServer+'\',\''+this.id+'\')');
                }
                cachearDatos();
            }

            this.redraw = function(){
                var $this = this;
                var fotos = this.fotos;    
                var pies = this.pies;
                this.fotos = [];            
                for(var i in fotos){
                    if(fotos[i]!=''){
                        var mockFile = {name: fotos[i]}; 
                        this.dropzone.options.addedfile.call(this.dropzone, mockFile); 
                        this.dropzone.options.thumbnail.call(this.dropzone, mockFile, this.realPath+fotos[i]);                         
                        var foto = this.dropzone.element.dropzone.previewsContainer.dropzone.previewsContainer.lastChild;
                        foto.nameServer = fotos[i];
                        foto.pie_foto = typeof(pies[i]) != 'undefined'?pies[i]:'';
                        $(foto).find('img').attr('src',URL+'img/venues/'+foto.nameServer+'?v='+Math.random());
                        $(foto).find('.dz-progress').hide();
                        $(foto).find('.dz-buttons').show();
                        $(foto).find('.cropImage').show();
                        $(foto).find(".pie_foto").show();
                        this.fotos.push(foto);                        
                    }
                }                
                this.updateValues();
            }

            this.remove = function(){
                this.templateFotoElement.remove();
            }
        }
        
    }

    function crop(imagen,id){
        var cropper = undefined;
        imagen = URL+'img/venues/'+imagen;
        $("#cropModal").modal('toggle');
        $("#cropModal").on('shown.bs.modal',function(){
            $("#slim-foto").html('<img src="'+imagen+'">');
            cropper = new Slim(
                document.getElementById('slim-foto'),{
                    forceSize: {
                        width: minWidth,
                        height: minHeight                        
                    },
                    push:true,
                    service: URL+'entradas/frontend/crop',
                    download:true,
                    edit:true,
                    didUpload:function(error, data, response){                        
                        var file = response.file;
                        var fotos = espacios[id].fotos.fotos;
                        for(var i in fotos){
                            if(fotos[i].nameServer==file){
                                file = URL+'img/venues/'+file;
                                $(fotos[i]).find('img').attr('src',file+'?v='+Math.random());
                            }
                        }
                        setTimeout(function(){$("#cropModal").modal('hide');},600);
                    },
                    didInit:function(){
                        $(".slim-btn-edit").trigger('click');                        
                    },
                    didCancel:function(){
                        setTimeout(function(){$("#cropModal").modal('hide');},600);
                    }
                }
            );
        }); 

        $("#cropModal").on('hidden.bs.modal',function(){
            cropper.destroy();
        });
    }

</script>