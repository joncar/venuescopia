<!-- Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" style="z-index: 9999999999999999999; overflow: overlay; height: 500px; display: block; overflow-x: overlay; overflow-y: scroll;">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-12 col-sm-12">
            <ul class="nav justify-content-center margin-bottom-30">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tab-login"><h6 class="heading-uppercase">Login</h6></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab-register"><h6 class="heading-uppercase">Regístrate</h6></a>
              </li>
            </ul>
            <div class="tab-content">
              <!-- Login Tab content -->
              <div class="tab-pane fade show active" id="tab-login">
                <form action="main/loginShort" onsubmit="return sendForm(this,'#loginResponse'); return false;">
                    <h2 class="text-uppercase titulos-general-modal font-montserrat text-center margin-bottom-20"><b>BIENVENIDO A CASA</b></h2>


                      <input type="text" placeholder="Usuario (correo o contraseña)" name="email" required class="margin-bottom-20">
                      <input type="password" placeholder="Contraseña" name="pass" required class="margin-bottom-20">


                    <div id="loginResponse"></div>

                    <div class="col-12 text-center" style="margin-bottom: 0px; padding: 0px;">
                      <button type="submit" class="button button-lg text-center margin-bottom-10" id="btn-negro"><b>Login</b></button>
                    </div>

                    <div class="col-12 text-center">
                        <a data-toggle="modal" data-target="#recuperar" title="Recuperar mi contraseña" style="text-decoration: underline; padding: 20px 0px;">OLVIDÉ MI CONTRASEÑA</a>
                    </div>
                  </form>
              </div>
              <!-- Register Tab content -->
              <div class="tab-pane fade" id="tab-register">
                <h2 class="text-uppercase titulos-general-modal font-montserrat text-center margin-bottom-20">
                  <b>¿Eres nuevo?</b><br>
                  <b>BIENVENIDO A BORDO</b>
                </h2>

                <form onsubmit="insertar('registro/index/insert/',this,'#respuestaregistromodal'); return false">
                  <input type="text" placeholder="Nombre" name="nombre" required class="margin-bottom-30">
                  <input type="text" placeholder="Apellidos" name="apellido_paterno" required class="margin-bottom-30">
                  <input type="text" placeholder="Correo electrónico" name="email" required class="margin-bottom-30">
                  <input type="password" placeholder="Contraseña" name="password" required class="margin-bottom-30">
                  <input type="password" placeholder="Confirmar contraseña" name="password2" required class="margin-bottom-20">
                  <input type="hidden" name="ajax" value="1">

                  <div class="col-12 margin-bottom-20 margin-top-20 text-general-p">
                    <p class="text-left">
                        <input type="checkbox" id="box-4" name="politicas" value="1" style="margin-top:10px">
                        <label for="box-4" style="margin-top: -10px;">
                            Al registrarte aceptas nuestros:<br>
                            <a href="<?= base_url() ?>terminos-condiciones.html" target="_new" style="text-decoration:underline" title="Ver Términos y Condiciones">Términos y Condiciones</a> y
                            <a href="<?= base_url() ?>aviso-privacidad.html" target="_new" style="text-decoration:underline" title="Ver Aviso de Privacidad">Aviso de Privacidad</a>
                        </label>
                    </p>
                  </div>

                  <div id="respuestaregistromodal"></div>

                  <div class="col-12 text-center">
                    <button type="submit" class="button button-lg text-center" id="btn-negro"><b>Crear cuenta</b></button>
                  </div>

                </form>

              </div>
            </div>
          </div>
        </div><!-- end row -->

      </div>
    </div>
  </div>
</div>

<!-- Recuperar contraseña -->
<div class="modal fade" id="recuperar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>
      <div class="modal-body">
        <form action="registro/forget" onsubmit="return sendForm(this,'#recoverResponse')">
          <div class="margin-bottom-30">
            <p class="text-center">Ingresa tu correo electrónico y recibirás instrucciones para recuperar tu contraseña.</p>
          </div>
          <input type="text" placeholder="Correo electrónico*" name="email" required="" class="margin-bottom-10">
          <div id="recoverResponse"></div>
          <div class="col-12 text-center">
            <button class="button button-md text-center montserrat font-weight-bold" type="submit" id="btn-negro" title="Conoce Venuescopia">Recuperar contraseña</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- FAQ -->
<div class="modal fade" id="faq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="post" class="margin-bottom-10" onsubmit="insertar('entradas/frontend/soporte/insert',this,'#faq .submit-result'); return false;">
          <div class="modal-header">
            <div class="col-12">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
                <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
            </div>
          </div>
          <div class="modal-body text-center">

            <div class="contact-form">
              <!--<p class="text-center margin-bottom-20">¿Cómo podemos ayudarte?</p>-->

              <h2 class="text-uppercase titulos-general-modal font-montserrat margin-bottom-20"><b>¿Cómo podemos ayudarte?</b></h2>

                <div class="form-row">
                  <div class="col-12 col-sm-12 margin-bottom-20">
                    <input type="text" id="name" name="nombre" placeholder="Nombre completo*" required style="border: #0000000 solid 2px;">
                  </div>
                  <div class="col-12 col-sm-12 margin-bottom-20">
                    <input type="email" id="email" name="email" placeholder="Correo electrónico*" required style="border: #0000000 solid 2px;">
                  </div>
                </div>
              <textarea name="mensaje" id="message" placeholder="Mensaje*" style="border: #0000000 solid 2px; height: 150px; resize: none; font-size: 13px;"></textarea>
              <input type="hidden" name="fecha" value="<?= date("Y-m-d H:i") ?>">

              <div class="col-12 text-center" style="padding-left: 0px;padding-right: 0px;">
                <button type="submit" class="button button-md text-center montserrat font-weight-bold" id="btn-negro" href="contacto-envio.php" title="Enviar mis comentarios" style="width: 100%;"><b>Enviar</b></button>
              </div>

              <!-- Submit result -->
              <div class="submit-result">
                <span id="success">Thank you! Your Message has been sent.</span>
                <span id="error">Something went wrong, Please try again!</span>
              </div>
          </form>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Eliminar favoritos -->
<div class="modal fade" id="eliminar-favoritos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">¿Eliminar de la lista de tus favoritos?</p>
      </div>

      <div class="modal-footer text-center">
        <div class="row margin-bottom-10">
          <div class="col-12 col-sm-6">
            <button type="button" class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
          </div>
          <div class="col-12 col-sm-6">
            <button type="button" class="button button-md text-center montserrat font-weight-bold" id="btn-negro" onclick="document.onDelete();">Aceptar</button>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>


<!-- Gracias por tu Pago -->
<div class="modal fade" id="pago-completo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            <span class="font-weight-bold">Gracias por tu pago y confiar en Venuescopia</span><br><br>
            Con esto obtienes los siguientes beneficios:<br>
            <ul>
              <li>Anuncio privado para tu Venue.</li>
              <li>Descripción y listado de amenidades del Venue.</li>
              <li>Subir hasta 10 imágenes por anuncio.</li>
              <li>Upload en el anuncio des espacios para renta separados dentro del mismo Venue.</li>
              <li>Ubicación y mapa live en el anuncio.</li>
              <li>Contacto de interesados directamente desde el sitio.</li>
              <li>Registro de visitas al anuncio desde tu Venue.</li>
              <li>Editable y visible 24/7 a todo el mundo.</li>
              <li>Tráfico de personas interesadas en ranta de espacios.</li>
              <li>Mayor oportunidad de capitalizar tu negocio.</li>
            </ul><br><br>
            Esperemos que disfrues tu estancia en nuestra plataforma, cualquier duda o comentario te invitamos a ir a la sección de contacto.<br><br>
            Gracias.<br>
            Att. El Equipo de Venuescopia
          </p>
      </div>

      <div class="modal-footer text-center">
        <a href="<?= base_url() ?>mis-venues.html" class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Continuar</a>
      </div>

    </div>
  </div>
</div>
<!-- Gracias por tu Pago -->

<!-- Pausar Publicación -->
<div class="modal fade" id="pausar-publicacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

      <form action="entradas/backend/iniciarpausar/" onsubmit="return iniciarPausarSubmit(this)">
          <div class="modal-header">
            <div class="col-12">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
                <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
            </div>
          </div>
          <div class="modal-body">              
              <p class="text-center texthelp" style="display: none">
                ¿Deseas <span class="accionP">Activar</span> esta publicación?<br>
                Puedes <span class="accion">Activarla</span> y <span class="contraccion">pausarla</span> cada vez que lo prefieras.
                <input type="hidden" name="pausado" value="0">
                <input type="hidden" name="id" value="0">
              </p>
              <div id="venues" style="margin-bottom: 30px;"></div>
          </div>

          <div class="modal-footer text-center">
            <!--<button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cerrar</button>-->
            <button type="submit" class="button button-md text-center montserrat font-weight-bold margin-bottom-membresias" id="btn-negro">Aceptar</button>
            <button data-dismiss="modal" class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Cerrar</button>
          </div>
     </form>

    </div>
  </div>
</div>


<!-- Eliminar Publicación -->
<div class="modal fade" id="eliminar-publicacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Eliminar este Venue?<br>
            Puedes eliminar este Venue y reactivarlo en cuanto lo prefieras.<br>
            No hay reembolso en caso de que quieras cancelar tu suscripción. - Descripción de la eliminación
          </p>
      </div>

      <div class="modal-footer text-center">        
        <button type="submit" class="button button-md text-center montserrat font-weight-bold margin-bottom-membresias" id="btn-negro">Aceptar</button>
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cerrar</button>
      </div>

    </div>
  </div>
</div>

<!-- Editar Publicación -->
<div class="modal fade" id="editar-publicacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>
      <div class="modal-body">

          <p class="text-center">
            Editar Venue
          </p>

          <form method="post" id="contactform">
            <div class="form-row">
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="text" id="name" name="name" placeholder="Nombre*" required style="border: #0000000 solid 2px;">
              </div>
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="email" id="email" name="email" placeholder="Precio*" required style="border: #0000000 solid 2px;">
              </div>
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="email" id="email" name="email" placeholder="Personas*" required style="border: #0000000 solid 2px;">
              </div>
              <div class="col-12 col-sm-12 margin-bottom-20">
                  <label>Cambiar forma de Pago</label>
                  <select class="custom-select">
                    <option value="1">Anual</option>
                    <option value="1">Mensual</option>
                    <option value="1">Trimestral</option>
                  </select>
              </div>
            </div>
          </form>

      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<!-- Ver Solicitud -->
<div class="modal fade" id="ver-solicitud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            <b>Nombre:</b> Nombre del Solicitante<br>
            <b>Rango de Precio:</b> No. <br>
            <b>Rango de Personas:</b> No.
          </p>
      </div>
      <div class="modal-footer">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Update credit card -->
<div class="modal fade bd-example-modal-lg" id="update-card" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form method="post" action="entradas/backend/retryPay" id="contactform" onsubmit="retryPay(this); return false;">
          <div class="modal-header">
            <div class="col-12">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            </div>
          </div>
          <div class="modal-body">

              <div class="col-12 col-sm-12">
                  <div class="contact-form">

                      <div class="form-row">
                        <div class="col-12 col-sm-12 margin-bottom-30">
                          <input type="text" id="name" name="tarjeta_numero" placeholder="Número de tarjeta" maxlength="16" required="">
                        </div>

                        <div class="col-12 col-sm-12 margin-bottom-30">
                          <input type="text" id="name" name="tarjeta_nombre" placeholder="Nombre de la tarjeta" required="">
                        </div>
                      </div>

                  </div>
              </div>

              <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
                  <div class="contact-form">
                    <div class="form-row">
                      <div class="col-12 col-sm-12 margin-bottom-30">
                        <input type="text" id="name" name="tarjeta_fecha" placeholder="MM/YY" required="">
                      </div>

                      <div class="col-12 col-sm-12">
                        <input type="text" id="name" name="tarjeta_cvc" placeholder="CVC" maxlength="3" required="">
                      </div>
                    </div>
                  </div>
              </div>

              <div class="col-12 col-sm-12">
                <div class="responseUdpateCard"></div>
              </div>

              <input type="hidden" name="transaccion" value="">
              <input type="hidden" name="token" value="">

          </div>

          <div class="modal-footer text-center">
            <button type="button" class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
            <button type="submit" class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
          </div>
      </form>
    </div>
  </div>
</div>

<!-- Cancelar suscripción -->
<div class="modal fade" id="iniciarpausarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Seguro deseas dejar de anunciar tu venue?<br>
            Tu anuncio dejará de estar visible en el sitio.
          </p>

          <div class="respuesta"></div>
      </div>

      <div class="modal-footer text-center">
        <form action="entradas/backend/iniciarpausar" onsubmit="sendForm(this,'#iniciarpausarModal .respuesta'); return false;">
          <input type="hidden" name="id" value="0">
          <input type="hidden" name="pausado" value="1">
          <button type="button" class="button button-md text-center montserrat font-weight-bold margin-bottom-membresias" data-dismiss="modal" id="btn-negro">Cancelar</button>
          <button type="submit" class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
        </form>
      </div>

    </div>
  </div>
</div>

<!-- Cancelar suscripción -->
<div class="modal fade" id="iniciarpausarModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Seguro deseas volver anunciar tu suscripción?<br>
            El venue volverá de estar visible en los listados
          </p>

          <div class="respuesta"></div>
      </div>

      <div class="modal-footer text-center">
        <form action="entradas/backend/iniciarpausar" onsubmit="sendForm(this,'#iniciarpausarModal2 .respuesta'); return false;">
          <input type="hidden" name="id" value="0">
          <input type="hidden" name="pausado" value="0">
          <button type="button" class="button button-md text-center montserrat font-weight-bold margin-bottom-membresias" data-dismiss="modal" id="btn-negro">Cancelar</button>
          <button type="submit" class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
        </form>
      </div>

    </div>
  </div>
</div>

<!-- Dejar de publicar -->
<div class="modal fade" id="cancelar-suscripcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Seguro que deseas cancelar tu membresía?<br>
            La cancelación comienza al término del plazo contratado actual.
          </p>
      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold margin-bottom-membresias" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold cancelarSubscripcionBtn" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<!-- Cancelar suscripción -->
<div class="modal fade" id="venues-individuales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">

        <div class="row margin-bottom-20">
          <img src="<?= base_url() ?>theme/assets/images/venues-interna/venue-interna-05.jpg" alt="Propiedades Venuescopia"><br>
          <span class="font-montserrat text-uppercase"><b>Hotel ST. Regis - Centro de convenciones</b></span>
        </div>

        <div class="row margin-bottom-20">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent purus risus, condimentum ut magna id, fringilla mattis nisi. Praesent non orci risus. Pellentesque habitant
            morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
        </div>

      </div>

      <div class="modal-footer">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cerrar</button>
      </div>

    </div>
  </div>
</div>

<!-- Filtros de precio -->
<div class="modal fade" id="filtro-capacidad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title" id="exampleModalLabel">Capacidad</h5>
        </div>
      </div>

      <div class="modal-body">
        <div class="row">
          <div class="col-12 col-lg-12 no-padding">
            <div class="input-range">
                  <?php
                    $hasta = 10000;
                    $this->db->select('MAX(capacidad_maximo) as total');
                    $_hasta = $this->db->get_where('view_venues');
                    if($_hasta->num_rows()>0){
                      $hasta = $_hasta->row()->total;
                    }
                  ?>

                  <div class="personas-range-slide" id="ancho-casilla">
                    <div style="position:relative; display:inline-block; width:40%;" id="ancho-casilla">
                      <input id="ancho-casilla" class="lowDollarAmountCapacidad padding-movil-casilla2" value="0" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;">
                      <div class="d-none d-lg-block" style="position:absolute;right: 3px;top: 12px;">Personas</div>
                    </div>
                    <div class="margin-personas-movil" style="position:relative; display:inline-block; width:40%;" id="ancho-casilla">
                      <input id="ancho-casilla" class="highDollarAmountCapacidad padding-movil-casilla2" value="10,000+" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;">
                      <div class="d-none d-lg-block" style="position:absolute;right: 3px;top: 12px;">Personas</div>
                    </div>
                  </div>

                  <input type="text" class="js-range-sliderCapacidad" name="my_range" value=""
                    data-type="double"
                    data-min="<?= $this->ajustes->capacidad_minima ?>"
                    data-max="<?= $this->ajustes->capacidad_maxima ?>"
                    data-from="0"
                    data-to="10000"
                    data-grid="true"
                    data-min-interval=""
                  />

              </div>
          </div>
        </div>

      </div>

      <div class="modal-footer col-12 text-responsive2">
        <button type="button" class="button button-md text-responsive2 montserrat font-weight-bold" id="btn-negro" data-dismiss="modal" style="margin-right: 5px;">Aplicar</button>
      </div>

    </div>
  </div>
</div>

<!-- Filtros de precio -->
<div class="modal fade" id="filtro-precio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title text-uppercase" id="exampleModalLabel">Precio</h5>
        </div>
      </div>

      <div class="modal-body">


        <style>
          #precio-modal-filtro {margin-top: -50px;}
        </style>

        <div class="row">
          <div class="col-12 col-lg-12 no-padding">
              <div class="input-range">
                  <?php
                    $hasta = 1000000;
                    $this->db->select('MAX(precio_maximo) as total');
                    $_hasta = $this->db->get_where('view_venues');
                    if($_hasta->num_rows()>0){
                      $hasta = $_hasta->row()->total;
                    }
                  ?>

                  <div>
                    <div id="precio-modal-filtro" style="position:relative; display:inline-block; width:40%;">
                      <div id="ancho-casilla">
                        <span style="position:absolute;left: 14px;top: 13px;">$</span>
                        <input class="lowDollarAmountPrecio padding-movil-casilla" data-type="MXN" value="0" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;" id="ancho-casilla">
                        <span class="d-none d-lg-block" style="position:absolute;right: 4px;top: 14px;">MXN</span>
                      </div>
                    </div>
                    <div style="position:relative; display:inline-block; width:40%; margin-left: 18%;" id="ancho-casilla">
                      <span style="position:absolute;left: 14px;top: 13px;">$</span>
                      <input class="highDollarAmountPrecio padding-movil-casilla" data-type="MXN" value="1,000,000+" style="width: 46%;margin-left: 2%; margin-right: 2%;font-weight: 100; letter-spacing: 3px; color: #000; width:100%;" id="ancho-casilla">
                      <span class="d-none d-lg-block" style="position:absolute;right: 4px;top: 14px;">MXN</span>
                    </div>
                  </div>

                  <input type="text" class="js-range-slider" name="my_range" value=""
                    data-type="double"
                    data-min="<?= $this->ajustes->precio_minimo ?>"
                    data-max="<?= $this->ajustes->precio_maximo ?>"
                    data-from="0"
                    data-to="1000000"
                    data-grid="true"
                    data-min-interval=""
                  />
              </div>
          </div>
        </div>

      </div>

      <div class="modal-footer col-12 text-responsive2">
        <button type="button" class="button button-md text-responsive2 montserrat" id="btn-negro" data-dismiss="modal" style="letter-spacing: 3px;font-size: 12px; font-weight: 600;">Aplicar</button>
      </div>

    </div>
  </div>
</div>

<!-- Amenidades Publicación -->
<div class="modal fade" id="amenidades" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="amenidades-modal">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>

      <div class="modal-body">
          <div class="row margin-bottom-20">Agrega todas las amenidades del Venue</div>

          <div class="row">
            <ul class="amenidadesModal">
              <?php                
                if (!empty($_SESSION['publicacion'])) {
                    $cache = $_SESSION['publicacion'];
                }
                $am = $this->elements->amenidades();
                foreach($am->result() as $n=>$a):
                  $selected = '';
                  if(!empty($cache) && !empty($cache['espacios'][0]['amenidades']) && !empty($cache['espacios'][0]['amenidades'][0])){
                    $amenidades = explode(',',$cache['espacios'][0]['amenidades'][0]);
                    if(in_array($a->nombre,$amenidades)){
                      $selected = 'checked="true"';
                    }
                  }                  
              ?>
                <li>
                  <input type="checkbox" id="box-<?= $a->id ?>" name="amenidades[]" value="<?= $a->id ?>">
                  <label for="box-<?= $a->id ?>" style="font-size:10px;"><?= $a->nombre ?></label>
                </li>
              <?php endforeach ?>
            </ul>

          </div><!-- Row -->
      </div>

      <div class="modal-footer text-responsive">
        <button class="button button-md montserrat font-weight-bold" id="btn-negro" data-dismiss="modal">Agregar amenidades</button>
      </div>

    </div>
  </div>
</div>

<!-- Cancelar suscripción -->
<div class="modal fade" id="cancelarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Seguro deseas cancelar tu cuenta?<br>
            Te vamos a extrañar en Venuescopia
          </p>

          <div class="respuesta"></div>
      </div>

      <div class="modal-footer text-center">
        <form action="registro/cancelarCuenta" onsubmit="sendForm(this,'#cancelarModal .respuesta'); return false;">
          <input type="hidden" name="id" value="0">
          <input type="hidden" name="pausado" value="1">
          <button type="button" class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
          <button type="submit" class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
        </form>
      </div>

    </div>
  </div>
</div>


<!-- Funciones Slider Range Precio -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!-- Funciones Slider Range Precio -->
