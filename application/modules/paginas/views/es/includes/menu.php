<header>
  <!-- Menu Escritorio -->
  <nav class="navbar navbar-absolute navbar-fixed d-none d-lg-block">
      <div class="container">

          <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url() ?>theme/assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia"></a>

          <!--<ul class="nav dropdown-dark" id="nav-main">-->
            <ul class="nav">
            <li class="nav-item" id="bold-menu"><a class="nav-link font-montserrat" href="<?= base_url() ?>">HOME</a></li>
            <li class="nav-item"><a class="nav-link" id="btn-top-login" href="<?= base_url() ?>anuncia.html" style="font-family: 'Avenir LT Std 35 Light'; margin-top:-1px;">ANUNCIA TU VENUE</a></li>
            <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>about.html" style="font-family: 'Avenir LT Std 35 Light';">ABOUT</a></li>
            <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>faq.html" style="font-family: 'Avenir LT Std 35 Light';">FAQ</a></li>
            <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>contacto.html" style="font-family: 'Avenir LT Std 35 Light';">CONTACTO</a></li>

            <?php if(empty($_SESSION['user'])): ?>
              <?php if(empty($url) || $url!='publicar'): ?>
              <li class="nav-item"><a class="nav-link" id="btn-top-login2" href="<?= base_url() ?>login.html" style="font-family: 'Avenir LT Std 35 Light'; margin-top:-1px;">LOGIN / SIGN UP</a></li>
              <?php else: ?>
              <li class="nav-item"><a class="nav-link" id="btn-top-login2" href="#login" data-toggle="modal" style="font-family: 'Avenir LT Std 35 Light'; margin-top:-1px;">LOGIN / SIGN UP</a></li>
              <?php endif ?>
            <?php else: ?>
            <li class="nav-item nav-dropdown">
              <a class="nav-link" href="#" style="font-family: 'Avenir LT Std 35 Light';">Hola <?= $_SESSION['nombre'] ?></a>
              <ul class="dropdown-menu" style="margin-top: -15px;">
                <li><a href="<?= base_url() ?>cuenta.html" style="padding:10px;" class="font-weight-normal">Cuenta</a></li>

                <?php if($this->db->get_where('venues',array('user_id'=>$this->user->id))->num_rows()>0): ?>
                <li><a href="<?= base_url() ?>membresias.html" style="padding:10px;" class="font-weight-normal">Membresías</a></li>
                <li><a href="<?= base_url() ?>mis-venues.html" style="padding:10px;" class="font-weight-normal">Mis Venues</a></li>
                <?php endif ?>
                <li><a href="<?= base_url() ?>favoritos.html" style="padding:10px;" class="font-weight-normal">Favoritos</a></li>
                <li><a href="<?= base_url('main/unlog') ?>" style="padding:10px;" class="font-weight-normal">Salir <i class="fas fa-sign-in-alt"></i></a></li>
              </ul>
            </li>
            <?php endif ?>
          </ul>

          <ul class="list-horizontal-unstyled margin-nav" id="redes-top">
            <?php if(!empty($_SESSION['user'])): ?>
            <li class="nav-item"><a class="nav-link redes-menu" href="<?= base_url() ?>favoritos.html" title="Ver Favoritos"><i class="far fa-heart"></i></a></li>
            <?php endif ?>
            <li class="nav-item"><a class="nav-link redes-menu" href="https://www.facebook.com/venuescopia/" target="blank" title="Ir a Facebook"><i class="fab fa-facebook-square"></i></a></li>
            <li class="nav-item"><a class="nav-link redes-menu" href="https://instagram.com/venuescopia?igshid=11h7ubsvl7316" target="blank" title="Ir a Instagram"><i class="fab fa-instagram"></i></a></li>
          </ul>

      </div>
  </nav>

  <!-- Menu Movil -->
  <nav class="navbar navbar-absolute navbar-fixed d-block d-lg-none" id="menu-movil">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url() ?>">
        <img src="<?= base_url() ?>theme/assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia">
      </a>
      <ul class="nav dropdown-dark">
        <?php if(!empty($_SESSION['user'])): ?>
          <div class="font-weight-bold usuario-movil-top">Hola <?= $this->user->nombre ?></div>
        <?php endif ?>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>"><b>Inicio</b></a></li>
        <li class="nav-item"><a class="nav-link" id="btn-top-login" href="<?= base_url() ?>anuncia.html">ANUNCIA TU VENUE</a></li>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>about.html">ABOUT</a></li>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>faq.html">FAQ</a></li>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>contacto.html">CONTACTO</a></li>

        <?php if(empty($_SESSION['user'])): ?>
        <li class="nav-item"><a class="nav-link" id="btn-top-login2" href="<?= base_url() ?>login.html">LOGIN / SIGN UP</a></li>
        <?php else: ?>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>cuenta.html" class="font-weight-normal">Cuenta</a></li>
        <?php if($this->db->get_where('venues',array('user_id'=>$this->user->id))->num_rows()>0): ?>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>membresias.html" class="font-weight-normal">Membresías</a></li>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>mis-venues.html" class="font-weight-normal">Mis Venues</a></li>
        <?php endif ?>
        <li class="nav-item"><a class="nav-link" href="<?= base_url() ?>favoritos.html" class="font-weight-normal">Favoritos</a></li>
        <li class="nav-item"><a class="nav-link" href="<?= base_url('main/unlog') ?>" class="font-weight-normal">Salir <i class="fas fa-sign-in-alt"></i></a></li>

        <?php endif ?>
        <li class="nav-item redes-movil-general">
          <?php if(!empty($_SESSION['user'])): ?>
            <a href="<?= base_url() ?>favoritos.html" class="redes-menu-movil" title="Ver Favoritos"><i class="far fa-heart"></i></a>
          <?php endif ?>
          <a href="https://www.facebook.com/venuescopia/" target="blank" class="redes-menu-movil" title="Ir a Facebook"><i class="fab fa-facebook-square"></i></a>
          <a href="https://www.instagram.com/venuescopia/?igshid=11h7ubsvl7316" target="blank" class="redes-menu-movil" title="Ir a Instragram"><i class="fab fa-instagram"></i></a>
        </li>
      </ul>

      <button class="nav-toggle-btn">
          <span class="lines"></span>
      </button>
    </div>
  </nav>

</header>

<div class="scrolltotop">
  <a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="fas fa-chevron-up"></i></a>
</div>
