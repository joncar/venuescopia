<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/appear.min.js"></script> 
<script src="<?= base_url() ?>theme/assets/plugins/easing.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/imagesloaded.pkgd.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/isotope.pkgd.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/jarallax/jarallax.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/jarallax/jarallax-video.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/jarallax/jarallax-element.min.js"></script>
<script src="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyA0s642F0yJ7s6dAjT0u0f4HD7xMs2TetU&libraries=drawing,geometry,places"></script> 
<script src="<?= base_url() ?>theme/assets/plugins/gmaps.min.js"></script>
<script src="<?= base_url() ?>theme/assets/js/functions.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="<?= base_url() ?>js/rangeSlider.js?v1.2"></script>



<script>
  var log = <?= $this->user->log?'true':'false'; ?>;
  function addFav(id){
    if(log){
      var datos = new FormData();
      datos.append('venues_id',id);
      insertar('entradas/backend/favoritos/insert',datos,'',function(data){
        
        if(data.success){
          $(".icon"+id).addClass('fas');
        }else{
          $(".icon"+id).removeClass('fas');
        }
      });
    }else{
      window.onlogin = function(){
        log = true;
        $("#login").modal('toggle');
        addFav(id);
        $.post('<?= base_url() ?>paginas/frontend/getShort',{url:'es/includes/menu'},function(data){
          $("header").replaceWith(data);
          loadEventNav();
        });
      }
      $("#login").modal('toggle');
    }
  }

  function conectar(){
      if(!window.onlogin){
        window.onlogin = function(){
          $(".botonLuegoDeConectar").attr('type','submit');
          $(".botonLuegoDeConectar").unbind('click');
          $(".botonLuegoDeConectar").attr('onclick',null);
          $(".botonLuegoDeConectar").trigger('click');          
          $("#login").modal('toggle');
          $.post('<?= base_url() ?>paginas/frontend/getShort',{url:'es/includes/menu'},function(data){
            $("header").replaceWith(data);
             loadEventNav();
          });
        };
      }
      $("#login").modal('toggle');
    }
</script>

<!-- Estilos Selects -->
    <script>
        
    function setSelect($this){        
        if(!$this.attr('multiple')){
        var numberOfOptions = $this.children('option').length;
          $this.addClass('select-hidden');
          $this.wrap('<div class="select"></div>');
          $this.after('<div class="select-styled"></div>');

          var $styledSelect = $this.next('div.select-styled');
          $styledSelect.text($this.children('option').eq(0).text());

          var $list = $('<ul />', {
              'class': 'select-options'
          }).insertAfter($styledSelect);

          for (var i = 0; i < numberOfOptions; i++) {
            if($this.children('option').eq(i).val()!=''){
              $('<li />', {
                  text: $this.children('option').eq(i).text(),
                  rel: $this.children('option').eq(i).val()
              }).appendTo($list);
            }
          }

          var $listItems = $list.children('li');

          $styledSelect.click(function(e) {
              e.stopPropagation();
              $('div.select-styled.active').not(this).each(function(){
                  $(this).removeClass('active').next('ul.select-options').hide();
              });
              $(this).toggleClass('active').next('ul.select-options').toggle();

          });

          $listItems.click(function(e) {
              e.stopPropagation();
              $styledSelect.text($(this).text()).removeClass('active');
              $this.val($(this).attr('rel'));
              $this.trigger('change');
              if($('.filtering_form').length>0){
                $('.filtering_form').submit();
              }
              $list.hide();
          });

          $(document).click(function() {
              $styledSelect.removeClass('active');
              $list.hide();
          });
          
          $styledSelect.text($this.find('option:selected').text()).removeClass('active');
        }
    }
    $('select').each(function(){
        setSelect($(this),this);
    });
    </script>    
    <script src="https://code.jquery.com/jquery-migrate-1.0.0.js"></script>
<script src="<?= base_url('assets/grocery_crud/js/jquery_plugins/jquery.chosen.min.js') ?>"></script>

<!-- Complete Search Index -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.js"></script>
