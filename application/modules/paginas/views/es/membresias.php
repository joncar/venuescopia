<?php if(empty($_SESSION['user'])) redirect('panel'); ?>
<?php
$this->load->library('grocery_crud');
$this->load->library('ajax_grocery_crud');
//Venues
$crud = new ajax_grocery_crud();
$crud->set_table('view_misplanes')
	 ->set_subject('Membresias')
	 ->set_theme('membresias')
	 ->set_primary_key('id')
	 ->unset_columns('nro_transaccion',$crud->encodeFieldName('user_id'),'fecha')
	 ->unset_add();
$crud->set_url('entradas/backend/view_misplanes/');
$crud = $crud->render(1,'application/modules/paginas/views/cruds/');
$css_files = $crud->css_files;
$js_files = $crud->js_files;
?>
<?php 
if(!empty($css_files)):
foreach($css_files as $file): ?>
<!--<link type="text/css" rel="stylesheet" href="<?= $file ?>" />-->
<?php endforeach; ?>
<?php endif; ?>
<!-- About section -->
<div class="section margin-registro-top text-general-p">
<!-- Product Tab content -->
<div class="section no-padding margin-top-30">
	<div class="container">
		<div class="product-tab">			
			<div class="tab-content">
				<!-- Description tab content -->
        <?php if(!empty($_GET['msj'])): ?>
          <div class="col-12">
              <?php if($_GET['msj']=='success'): ?>
                <div class="alert alert-success">Acción realizada con éxito</div>
              <?php else: ?>
                <div class="alert alert-danger">Hubo un error al realizar su solicitud</div>
              <?php endif ?>
          </div>
        <?php endif ?>
				<div class="tab-pane fade show active" id="tab-venues">
					<?php 
						//View en application/modules/paginas/views/cruds/mis-venues/list_template.php
						//View en application/modules/paginas/views/cruds/mis-venues/list.php							
						echo $crud->output;
					?>
				</div>
			</div>
		</div>
		</div><!-- end container -->
	</div>
	<!-- end Product Tab content -->
</div>
<!-- end About section -->
<?php 
if(!empty($js_files)): ?>
<?php foreach($js_files as $file): ?>
<script src="<?= $file ?>"></script>
<?php endforeach; ?>                
<?php endif; ?>
<script src="<?= base_url('js/conekta.js') ?>"></script>
<script>
	function retry(id){
		$("#update-card input[name='transaccion']").val(id);
		$("#update-card").modal('toggle');
	}

	function retryPay(form){
      $(".responseUdpateCard").removeClass('alert alert-danger alert-info alert-success').html('');
      var fecha = $("#update-card input[name='tarjeta_fecha']").val();
      fecha = fecha.split('/');
      if(fecha.length==2){
        var params = {
          "card": {
            "number": $("#update-card input[name='tarjeta_numero']").val(),
            "name": $("#update-card input[name='tarjeta_nombre']").val(),
            "exp_year": fecha[1],
            "exp_month": fecha[0],
            "cvc": $("#update-card input[name='tarjeta_cvc']").val()
          }
        };
      }else{
        params = {};
      }
      conektaLib.tokenizar(params,function(response){
        $("#update-card input[name='token']").val(response.id);
        sendForm(form,'.responseUdpateCard',function(data){
        	$('.responseUdpateCard').html(data);
        	$("#filtering_form").submit();
        });
      },function(error){
        $(".responseUdpateCard").addClass('alert alert-danger').html('<p>Error al cargar información de pago</p><p>MSJ: '+error.message_to_purchaser+'</p>');
      });
    }

	$(document).ready(function(){      
      $("#update-card input[name='tarjeta_fecha']").mask('00/0000',{placeholder: "Vencimiento mm/aaaa"})
  });
</script>