<!-- Home section -->
    <div class="section-fullscreen bg-image parallax" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-anuncia.jpg)">
        <div class="container">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <div class="bg-white contenedor-header-titulo2 text-center">
                  <h2 class="text-uppercase titulos-general font-montserrat"><b>No hemos podido procesar tu pago.</b></h2>
                  <a class="button button-md margin-top-30 text-center" id="btn-negro" href="<?= base_url() ?>mis-venues.html"><b>Ir a mis venues</b></a>
                </div>
              </div>
            </div><!-- end row -->
          </div>
        </div><!-- end container -->
    </div>
    <!-- end Home section -->

   
      </div><!-- end container -->
    </div>
    <!-- end About section -->