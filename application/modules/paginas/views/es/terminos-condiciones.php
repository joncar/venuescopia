<!-- Home section -->
    <div class="section-fullscreen bg-image parallax" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/header-terminos.jpg)">
        <div class="container">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <div class="bg-white contenedor-header-titulo2 text-center">
                  <h2 class="text-uppercase titulos-general font-montserrat"><b>Términos y condiciones</b></h2>
                  <a class="button button-md margin-top-30 text-center" id="btn-negro" href="#aviso" title="Leer más"><b>Leer más</b></a>
                </div>
              </div>
            </div><!-- end row -->
          </div>
        </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <!-- About section -->
    <div class="section" id="aviso">
      <div class="container">
        <div class="row contenedor-terminos-margin">
          <div class="col-12 col-lg-12">
            <?= $this->ajustes->terminos_y_condiciones; ?>
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end About section -->




    <!-- Scroll Text -->
    <script>
    // Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });
    </script>
