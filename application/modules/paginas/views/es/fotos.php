<!--
  DO NOT SIMPLY COPY THOSE LINES. Download the JS and CSS files from the
  latest release (https://github.com/enyo/dropzone/releases/latest), and
  host them yourself!
-->

<link rel="stylesheet" href="https://rawgit.com/enyo/dropzone/master/dist/dropzone.css">
<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/slim.cropper/slim.min.css" />

<style type="text/css">
	.dropzone.dz-started .dz-message{
		display: block !important;
	}
	.borde-dropzone{
	border: #F6F6F6 solid 2px !important;
	border-radius: 0px !important;
	background: white !important;
	margin-bottom:20px;
	font-family: 'Avenir LT Std 35 Light';
	letter-spacing: 0.05em;
	color:#000000;
	}

	.form-control {
	display: block;
	width: 100%;
	height: calc(2.25rem + 2px);
	padding: .375rem .75rem;
	font-size: 12px;
	line-height: 1.5;
	color:  black;
	background-color: #fff;
	background-clip: padding-box;
	border: 2px solid #000;
	border-radius: 0px;
	transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
	}

	.dropzone.dz-clickable img {
		cursor: default;
		width: 100%;
	}

	.modal-content {
		background-color:  transparent;
		background-clip: padding-box;
		border: 0px solid rgba(0,0,0,.2);
		border-radius: 0px;
		outline: 0;
	}

	.modal-header {
		display: block !important;
		padding: 0px;
		border-bottom: 1px solid  #e9ecef;
		text-align: center;
	}

	.btn {
		font-size: 1rem;
		line-height: 1.5;
		border-radius: 0px;
		font-size: 1em;
		margin: 0 .75em;
		padding: .75em 1.5em .875em;
	}

	.modal-footer {
		display: -ms-flexbox;
		display: flex;
		-ms-flex-align: center;
		align-items: center;
		-ms-flex-pack: end;
		justify-content: center;
	}

	.modal-title {
		margin-bottom: 0;
		line-height: 1.5;
		color:  white;
		font-weight: 900;
	}

	.close {
		text-shadow: 0px;
		color: white;
		opacity: 1;
	}

	.modal-header .close {
		padding: 0px;
		margin: -1rem -1rem -1rem auto;
	}

	.modal-open .modal {
		overflow-x: hidden;
		overflow-y: auto;
		background: rgba(0,0,0,.8);
	}

	.slim { margin: 0px;}
	.dropzone {padding: 0px;}

	.dropzone{
		/*display:flex;*/
		display: block;
		flex-wrap: nowrap;
		width: 100%;
		min-height: auto;
	}

	.dz-details{
	border: 2px solid #F6F6F6;
	position: relative;
	width: auto;
	padding: 14px;
	margin:0px;
	max-width: 100%;
	}

	.dz-details img, .dz-details input{
	display: block;
	}

	.dz-details input{
	margin: 14px 0;
	}

	.dz-filename, .dz-size{
	display: none;
	}

	.dz-remove {
	position: absolute;
	color: white;
	background: rgba(0,0,0,.7);
	padding: 5px;
	border-radius: 50%;
	width: 30px;
	height: 30px;
	}

	/*
	.dz-remove{
	position: absolute;
	top: 0;
	right: 7px;
	color:#000000;
	}*/

	.cropImage {
	position: absolute;
	color: white;
	background: rgba(0,0,0,.7);
	padding: 5px;
	border-radius: 50%;
	width: 30px;
	height: 30px;
	}

	/*
	.cropImage{
	position: absolute;
	top:0px;
	left:10px;
	color:#000000;
	}*/

	.dropzone .dz-message {
		text-align: center;
		margin: 2em 0;
		width: 100%;
	}

	.slim {
		background-color:  #ffffff;
		width: 100%;
	}

	.dropzone-empty {
		background-color: white;
		padding: 30px;
		display: block;
		white-space: normal;
		border: #000000 dotted 1px;
		font-size: 12px;
	}

	@media only screen and (min-width : 150px) {
	.dz-remove {
	top: 25%;
	left: 40%;
	}

	.cropImage {
	top: 25%;
	left: 55%;
	}

	}

	@media only screen and (min-width : 768px) {
	.dz-remove {
	top: 30%;
	left: 45%;
	}

	.cropImage {
	top: 30%;
	left: 55%;
	}

	}

	@media only screen and (min-width : 992px) {
	}
</style>

<?php 
	error_reporting(-1);
    ini_set('display_errors', 1);

    
    if(isset($_FILES['archivoDesarrolloHidrocalido'])){
		// En versiones de PHP anteriores a la 4.1.0, debería utilizarse $HTTP_POST_FILES en lugar
		// de $_FILES.
		$dir_subida = '../archivos/';
		$fichero_subido = $dir_subida . basename($_FILES['archivoDesarrolloHidrocalido']['name'][0]);
		echo '<pre>';
		/*if (move_uploaded_file($_FILES['archivoDesarrolloHidrocalido']['tmp_name'], $fichero_subido)) {
		    echo "El fichero es válido y se subió con éxito.\n";
		} else {
		    echo "¡Posible ataque de subida de ficheros!\n";
		}*/
		echo 'Más información de depuración:';
		print_r($_FILES);
		print "</pre>";
		die();
	}
?>

<div class="container" style="margin-top:150px;">
	<div class="row">
		<div class="col-12 col-lg-6">

			<div class="row section-title-interna">
				<h2 class="text-uppercase titulos-general2 font-montserrat"><b>4. Fotos de tu Venue</b></h2>
			</div>

			<div class="row margin-bottom-10">
				<div class="titulo text-general-p" style="margin-bottom:20px;">
					<h2 class="text-uppercase titulos-general2 font-montserrat" style="letter-spacing: 5px;font-size: 18px;">
						<span class="fotostag">Venue principal</span>
					</h2>
					<div class="otherAlert">Selecciona todas las fotos de tu venue principal.<br>Recuerda que la primera imagen será la fotografía de portada.</div>
				</div>

				<div class="panel panel-default" style="width: 100%;">
					<div class="panel-body text-center">
						<div id="formDropZone">
							<form id='dZUpload1' class='dropzone borde-dropzone' style='cursor: pointer;'>
		                		<div class='dz-default dz-message text-center'>
									<i class="fas fa-cloud-upload-alt" style="font-size: 2em; color: #E5E5E5;"></i><br>
		                			Arrastra tus archivos aquí<br>
		                			o selecciona
		                		</div>
		                	</form>
						</div>                
					</div>
				</div>
			</div>

			<div class="row margin-bottom-10">
				<div class="titulo text-general-p" style="margin-bottom:15px;">
					<h2 class="text-uppercase titulos-general2 font-montserrat" style="letter-spacing: 5px;font-size: 18px;">
						<span class="fotostag fotostag2">ESPACIO 1</span>
					</h2>
					<div class="otherAlert">
						Selecciona todas las fotos de tu espacio.<br>
						Puedes subir máximo 9 fotografías.
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-body text-center" >
					<div id="formDropZone">
						<form id='dZUpload2' class='dropzone borde-dropzone' style='cursor: pointer;'>
								<div class='dz-default dz-message text-center'>
									<i class="fas fa-cloud-upload-alt" style="font-size: 2em; color: #E5E5E5;"></i><br>
		                			Arrastra tus archivos aquí<br>
		                			o selecciona
		                		</div>
	                	</form>
					</div>                
				</div>
			</div>

			<div class="row margin-bottom-10">
				<div class="titulo text-general-p" style="margin-bottom:15px;">
					<h2 class="text-uppercase titulos-general2 font-montserrat" style="letter-spacing: 5px;font-size: 18px;">
						<span class="fotostag fotostag2">ESPACIO 2</span>
					</h2>
					<div class="otherAlert">
						Selecciona todas las fotos de tu espacio.<br>
						Puedes subir máximo 9 fotografías.
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-body text-center" >
					<div id="formDropZone">
						<form id='dZUpload3' class='dropzone borde-dropzone' style='cursor: pointer;'>
								<div class='dz-default dz-message text-center'>
									<i class="fas fa-cloud-upload-alt" style="font-size: 2em; color: #E5E5E5;"></i><br>
		                			Arrastra tus archivos aquí<br>
		                			o selecciona
		                		</div>
	                	</form>
					</div>                
				</div>
			</div>

			<div class="margin-bottom-50">
				<div class="col-12 col-sm-12">
					<a class="button button-md margin-top-10 text-center submit" id="btn-negro" onclick="showTask(3)" title="Conoce Venuescopia"><b>Regresar</b></a>
					<a class="button button-md margin-top-10 text-center submit" id="btn-negro" onclick="showTask(5)" title="Conoce Venuescopia"><b>Continuar</b></a>
				</div>
			</div>
		</div>

		<div class="col-12 col-lg-6" style="display: block; z-index: 2;">
			<div class="fondo-gris sticky">
				<!-- TIP -->
				<div class="fondo-tip-registro text-center tipContent tipContent4" style="line-height: 2;">
					<div style="font-size:14px; font-family:'Avenir LT Std 35 Light';" class="margin-bottom-50 nombre-tip"><b class="tip font-weoght-bold" style="font-family:'Avenir LT Std 85 Heavy';">TIP:</b>Espacio principal</div>
					<p>Entre más profesionales sean las fotos que agregues más lucirá tu publicación.<br>Recomendamos elegir la foto más atractiva como foto de portada.</p>
				</div>
				<!-- TIP -->
				<div class="fondo-tip-registro text-center tipContent tipContent4" style="line-height: 2;">
					<div style="font-size:14px; font-family:'Avenir LT Std 35 Light';" class="margin-bottom-50 nombre-tip"><b class="tip font-weoght-bold" style="font-family:'Avenir LT Std 85 Heavy';">TIP:</b>Tamaño Recomendado</div>
					<p>El tamaño recomendado para las fotos es de 2100 × 1400.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="dz-preview dz-file-preview" id="tpl" style="display: none;">
	<div class="dz-details">
		<div class="dz-filename"><span data-dz-name></span></div>
		<div class="dz-size" data-dz-size></div>
		<img data-dz-thumbnail />
		<input type="text" placeholder="Pie de foto" class="form-control">
		<a href="javascript:crop()" class="cropImage"><i class="far fa-edit"></i></a>
	</div>
	<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
	<div class="dz-success-mark"><span>✔</span></div>
	<div class="dz-error-mark"><span>✘</span></div>
	<div class="dz-error-message"><span data-dz-errormessage></span></div>		  
</div>

<!-- Modal -->
<div id="cropModal" class="modal fade">
	<div class="modal-dialog" style="width: 65%;">
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<h4 class="modal-title">Editar foto</h4>
		</div>
		<div class="modal-body">
			<div class="slim" id="slim-foto"
					data-service=""
					data-post="input, output, actions"
					data-size="2100,1406"
					data-instant-edit="true"
					data-push="true"
					data-did-upload="onUploadPhoto"
					data-download="true"
					data-will-save="addValueSlim2"
					data-label="Seleccionar imagen"
					data-meta-name="foto"
					data-force-size="2100,1406"
					data-max-file-size="20"
					data-min-size="1240,768"
					data-erase="false"
					data-remove="false"
					data-status-file-size="El archivo excede el tamaño de $0 MB que fue especificado."
					data-status-image-too-small="Inserte una imagen de mayor tamaño para obtener mejores resultados">
				<input type="file" id="slim-foto"/>
				<input type="hidden" data-val="" name="espacios[0][fotos][]" value="<?= base_url() ?>img/2100x1406.png" id="field-foto">
				<img src="<?= base_url() ?>img/2100x1406.png" alt="" style="width:100%; height:200px">
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
		</div>
	</div>
</div>

<script src="https://rawgit.com/enyo/dropzone/master/dist/dropzone.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/slim.croppic.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/slim.cropper.js"></script>

<script>
	var myDropzone, myDropzone2, myDropzone3;
	$(document).ready(function(){
		var myAwesomeDropzone = {
			url: "fotos.html",
			addRemoveLinks: true,
			paramName: "archivoDesarrolloHidrocalido",
			maxFilesize: 4, // MB
			dictRemoveFile: "<i class='far fa-trash-alt'></i>",
			uploadMultiple:true,
			params: {parametro1:'valor1',parametro2:'valor2'},
			previewTemplate:document.querySelector('#tpl').innerHTML,
			thumbnailWidth:476,
			success: function (file, response) {
				var imgName = response;
				file.previewElement.classList.add("dz-success");
				console.log("Successfully uploaded :" + imgName);
			},
			error: function (file, response) {
				file.previewElement.classList.add("dz-error");
			}
		} // FIN myAwesomeDropzone
		myDropzone = new Dropzone("#dZUpload1", myAwesomeDropzone); 
		myDropzone2 = new Dropzone("#dZUpload2", myAwesomeDropzone); 
		myDropzone3 = new Dropzone("#dZUpload3", myAwesomeDropzone);
		myDropzone.on("complete", function(file,response) {});
		myDropzone2.on("complete", function(file,response) {});
		myDropzone3.on("complete", function(file,response) {});
	});
		
	function getArchivos() {
		/*$.ajax({
			type: 'GET',
			url: 'php/getArchivos.php',
			success: function(data){
				$("#divMostrarArchivos").html("");
				$("#divMostrarArchivos").html("<br><p>Archivos:</p>"+data);
			}
		});*/
	}
	function crop(){
		$("#cropModal").modal('toggle');
	}
</script>

