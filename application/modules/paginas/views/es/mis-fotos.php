<?php if(empty($_SESSION['user'])) redirect('panel'); ?>
<?php
if(!empty($_GET['key']) && is_numeric(base64_decode($_GET['key']))):
$this->load->library('grocery_crud');
$this->load->library('ajax_grocery_crud');
$venue = $this->db->get_where('venues_espacios',array('id'=>base64_decode($_GET['key'])));
//Espacios
$fotos = new ajax_grocery_crud();
$fotos->set_table('venues_fotos')
   ->set_subject('fotos')
   ->set_theme('generic')
   ->columns('foto','pie_foto')
   ->where('venues_espacios_id',base64_decode($_GET['key']));
$fotos->set_url('entradas/backend/venues_fotos/'.base64_decode($_GET['key']).'/');
$fotos = $fotos->render(1,'application/modules/paginas/views/cruds/');
?>
<?php 
if(!empty($fotos->css_files)):
foreach($fotos->css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
<?php endforeach; ?>
<?php endif; ?>
<!-- About section -->
<div class="section margin-registro-top text-general-p">
  <!-- Product Tab content -->
  <div class="section no-padding margin-top-30">
    <div class="container">
      <div class="product-tab">
        <ul class="nav margin-bottom-20 text-uppercase">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('mis-venues.html') ?>"><h5>Mis venues</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= $venue->num_rows()>0?base_url('editar-venue.html?key='.base64_encode($venue->row()->venues_id)):'#' ?>"><h5>Editar Venue</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('editar-espacios.html') ?>?key=<?= $_GET['key'] ?>"><h5>Editar Espacio</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#tab-contactos"><h5>Fotos <span></span></h5></a>
          </li>
        </ul>
        <div class="tab-content">
          <!-- Description tab content -->
          <div class="tab-pane fade" id="tab-venues">
            
          </div>
          <!-- Reviews tab content -->
          <div class="tab-pane fade show active" id="tab-contactos">
            <?php 
              //View en application/modules/paginas/views/cruds/mis-mensajes/list_template.php
              //View en application/modules/paginas/views/cruds/mis-mensajes/list.php             
              echo $fotos->output;
            ?>
          </div>
        </div>
      </div>
      </div><!-- end container -->
    </div>
    <!-- end Product Tab content -->
  </div>
  <!-- end About section -->
  <?php 
    if(!empty($fotos->js_files)): ?>
    <?php foreach($fotos->js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>

    <script>

      function pauseVenue(id){        
          $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(0);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Pausar');
          $("#pausar-publicacion").modal('toggle');
      }

      function starVenue(id){
        $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(1);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Activar');
          $("#pausar-publicacion").modal('toggle');
      }

      function actualizar(form){
        insertar('entradas/backend/venues_espacios/update/<?= base64_decode($_GET['key']) ?>',form,'#report-success');
        return false;
      }
    </script>
    <?php endif ?>
<script>
  $(document).on('ready',function(){
    $(".chosen-select,.chosen-multiple-select").chosen({"search_contains": true, allow_single_deselect:true});  
  });
</script>

<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/tinymce.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/texteditor/tiny_mce/jquery.tinymce.js"></script>