<div id="page-wrap">
	<div class="container-fluid no-padding bg-image" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-about.jpg)" id="header-height">
		<div class="row align-items-center no-margin">
			<div class="col-12 col-lg-6 no-margin bg-white-interna padding-about">
				<div class="padding-50 margin-text-header">
					<h2 class="text-uppercase titulos-general font-montserrat text-responsive font-weight-bold">Todos los venues en un mismo lugar</h2>
					<div class="padding-same">
						<p class="text-responsive">Donde encuentras y te encuentran.</p>
					</div>
					<div class="text-responsive">
						<a class="button button-lg" id="btn-negro" href="#one" title="Leer más"><b>Leer más</b></a>
					</div>
				</div>
			</div>
			<div class="col-12 col-lg-6 no-margin no-padding"></div>
			</div><!-- end row -->
			</div><!-- end container-fluid -->
			<!-- About section -->
			<div class="section" id="one">
				<div class="container">
					<div class="row">
						<div class="section-title-interna margin-about">
							<div class="row">
								<div class="col-12">
									<h2 class="text-uppercase font-montserrat"><b>VENUESCOPIA</b></h2>
								</div>
							</div>
							</div><!-- end section-title -->
							<div class="row margin-bottom-20  margin-about align-items-center" id="#nosotros">
								<div class="col-12 col-lg-8">
									<p>
										Durante varios años y a lo largo de mi carrera, gran parte de mi trabajo ha sido el de seleccionar venues para todo tipo de eventos. 
										Conozco de primera mano la dedicación que se requiere para encontrar el lugar perfecto y recuerdo en muchas ocasiones encontrarme interrumpiendo conversaciones 
										ajenas preguntando “¿¡Cómo se llama ese lugar que comentaste!?”.<br><br>

										Es por esto que decidí crear Venuescopia. Decidí embarcarme en una nueva aventura y lanzar un nuevo sitio que cambia para siempre la manera en la que buscamos y encontramos nuevos espacios.
										Espacios para bodas, fiestas, eventos empresariales, shootings o cualquier otra ocasión que merezca ese lugar especial. <br><br>

										Venuescopia llega como el primer sitio en México en hacer de la búsqueda de venues algo fácil y divertido, creando la posibilidad de encontrar lugares 
										únicos que nunca antes pudiste haber imaginado estuvieran disponibles para ti. <br><br>

										Queremos que tengas todos estos lugares a tu alcance, que el lugar mágico con el que sueñas se convierta en 
										realidad y que el día más importante de tu vida suceda ahí. <br><br>

										Que si cuentas con un venue puedas ponerlo a la vista de todos y si nunca antes habías considerado rentar tu espacio, seamos 
										la plataforma que te anime a hacerlo. <br><br>

										En Venuescopia perseguimos todos los días una pasión: la de conectar a aquellos que sueñan con los que tienen el lugar soñado. <br><br>

										Bienvenidos.<br>
										<b style="font-family:'Avenir LT Std 85 Heavy';">Andrés Catán – CEO</b>
									</p>
								</div>
								<div class="col-12 col-lg-4 imagen-brujula">
									<img src="<?= base_url() ?>theme/assets/images/brujula.png" alt="Cónocenos - Venuescopia" class="mx-auto d-block">
								</div>
							</div>
							</div><!-- end row -->
							</div><!-- end container -->
						</div>
						<!-- end About section -->
					</div>




					<!-- Scroll Text -->
					<script>
					// Select all links with hashes
					$('a[href*="#"]')
					  // Remove links that don't actually link to anything
					  .not('[href="#"]')
					  .not('[href="#0"]')
					  .click(function(event) {
					    // On-page links
					    if (
					      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
					      &&
					      location.hostname == this.hostname
					    ) {
					      // Figure out element to scroll to
					      var target = $(this.hash);
					      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
					      // Does a scroll target exist?
					      if (target.length) {
					        // Only prevent default if animation is actually gonna happen
					        event.preventDefault();
					        $('html, body').animate({
					          scrollTop: target.offset().top
					        }, 1000, function() {
					          // Callback after animation
					          // Must change focus!
					          var $target = $(target);
					          $target.focus();
					          if ($target.is(":focus")) { // Checking if the target was focused
					            return false;
					          } else {
					            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
					            $target.focus(); // Set focus again
					          };
					        });
					      }
					    }
					  });
					</script>
