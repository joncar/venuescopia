
<!--
<div class="container-fluid no-padding bg-image parallax" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-anuncia.jpg)">
	<div class="row no-margin">
		<div class="col-12 col-lg-6 no-margin padding-about">

		</div>
		<div class="col-12 col-lg-6 no-margin bg-white-interna padding-about login-casa">
			<div class="margin-bienvenido">
				<h5 class="font-montserrat margin-bottom-30 text-uppercase" style="letter-spacing: 3px;">
					<b>Recuperar contraseña</b><br>				
				</h5>
				<form action="<?= base_url('registro/reset') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
				    <?= !empty($msj)?$msj:'' ?>
				    <input type="email" name="email" id="email" data-val="required" class="form-control" value="<?= $key ?>" readonly><br/>
				    <input type="password" class="form-control" name="pass" id="pass" placeholder="Nueva Contraseña"><br/>
				    <input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Contraseña"><br/>
				    <input type="hidden" name="key" value="<?= $key ?>">

					<button type="submit" class="button button-md margin-top-20 text-center" id="btn-negro" href="contacto-envio.php" title="Enviar mis comentarios" style="width: 100%;">
						<b>Recuperar Contraseña</b>
					</button>
				</form>
			</div>
		</div>
	</div>
</div><-->





<div class="container-fluid no-padding bg-image" style="background-image: url(https://bluepixel.mx/venuescopia/theme/assets/images/BG/background-anuncia.jpg)" id="header-contact2">
      <div class="row align-items-center no-margin">

        <div class="col-12 col-lg-6 bg-white-interna-contacto padding-about">
          <div class="padding-50 margin-text-header-contact">
            <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Recupera</b></h2>
            <div class="padding-same"><p class="text-responsive">Tu contraseña</p></div>
          </div>
        </div>

        <div class="col-12 col-lg-6 resolvimos-modal padding-contact">
          <div class="contact-form margin-contact">

		  <form action="<?= base_url('registro/reset') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
			<?= !empty($msj)?$msj:'' ?>
			<input type="email" name="email" id="email" data-val="required" class="form-control" value="<?= $key ?>" readonly><br/>
			<input type="password" class="form-control" name="pass" id="pass" placeholder="Nueva Contraseña" style="background: white;"><br/>
			<input type="password" class="form-control" name="pass2" id="pass2" placeholder="Repetir Contraseña" style="background: white;"><br/>
			<input type="hidden" name="key" value="<?= $key ?>" style="background: white;">

			<button type="submit" class="button button-md margin-top-20 text-center" id="btn-negro" href="contacto-envio.php" title="Enviar mis comentarios" style="width: 100%;">
				<b>Recuperar Contraseña</b>
			</button>
		</form>

          </div>
        </div>

  </div><!-- end row -->
</div>