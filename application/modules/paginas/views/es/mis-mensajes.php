<?php if(empty($_SESSION['user'])) redirect('panel'); ?>
<?php
if(is_numeric($_GET['key'])):
$this->load->library('grocery_crud');
$this->load->library('ajax_grocery_crud');
//Venues
/*$crud = new ajax_grocery_crud();
$crud->set_table('venues')
	 ->set_subject('venues')
	 ->set_theme('mis-venues');
$crud->set_url('entradas/backend/venues/');
$crud->where('user_id',$this->user->id);
$crud = $crud->render(1,'application/modules/paginas/views/cruds/');*/
//Mensajes
$mensajes = new ajax_grocery_crud();
$mensajes->set_table('mensajes')
	 	 ->set_subject('mensajes')
	 	 ->set_theme('mis-mensajes');
$mensajes->columns('nombre','correo','mensaje');          
$mensajes->set_url('entradas/backend/mensajes/'.$_GET['key'].'/');
$mensajes->where('user_id',$this->user->id)
		 ->where('venues_id',$_GET['key']);
$mensajes = $mensajes->render(1,'application/modules/paginas/views/cruds/');

$css_files = $mensajes->css_files;
$js_files = $mensajes->js_files;
?>
<?php 
if(!empty($css_files)):
foreach($css_files as $file): ?>
<!--<link type="text/css" rel="stylesheet" href="<?= $file ?>" />-->
<?php endforeach; ?>
<?php endif; ?>
<!-- About section -->
<div class="section margin-registro-top text-general-p">
	<!-- Product Tab content -->
	<div class="section no-padding margin-top-30">
		<div class="container">
			<div class="product-tab">
				<ul class="nav margin-bottom-20 text-uppercase">
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('mis-venues.html') ?>"><h5>Mis Venues</h5></a>
					</li>					
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('publicar.html') ?>"><h5>Agregar venue</h5></a>
					</li>
				</ul>
				<div class="tab-content">					
					<!-- Reviews tab content -->
					<div class="tab-pane fade show active" id="tab-contactos">
						<?php 
							//View en application/modules/paginas/views/cruds/mis-mensajes/list_template.php
							//View en application/modules/paginas/views/cruds/mis-mensajes/list.php							
							echo $mensajes->output;
						?>
					</div>
				</div>
			</div>
			</div><!-- end container -->
		</div>
		<!-- end Product Tab content -->
	</div>
	<!-- end About section -->
	<?php 
    if(!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>

    <script>

    	function pauseVenue(id){    	  
          $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(1);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Pausar');
          $("#pausar-publicacion").modal('toggle');
    	}

    	function starVenue(id){
    	  $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(0);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Activar');
          $("#pausar-publicacion").modal('toggle');
    	}
    </script>
<?php endif ?>