<?php if(empty($_SESSION['user'])) redirect('panel'); ?>
<?php
if(!empty($_GET['key']) && is_numeric(base64_decode($_GET['key']))):
$this->load->library('grocery_crud');
$this->load->library('ajax_grocery_crud');
//fotos
$foto = $this->db->get_where('venues_fotos',array('id'=>base64_decode($_GET['key'])));
$espacios = $this->db->get_where('venues_espacios',array('id'=>$foto->row()->venues_espacios_id));
$venue = $this->db->get_where('venues',array('id'=>$espacios->row()->venues_id));
$fotos = new ajax_grocery_crud();
$fotos->set_table('venues_fotos')
	 ->set_subject('fotos')
	 ->set_theme('mis-fotos')	 
	 ->field_type('venues_espacios_id','hidden')
	 ->unset_add()
	 ->field_type('foto','image',array('path'=>'img/venues','width'=>'2100','height'=>'1406'))   
	 ->set_primary_key_value(base64_decode($_GET['key']));
$fotos->disable_ftp();
$fotos->set_url('entradas/backend/venues_fotos/'.base64_decode($_GET['key']).'/');
$fotos = $fotos->render(3,'application/modules/paginas/views/cruds/');

$css_files = $fotos->css_files;
$js_files = $fotos->js_files;
?>
<?php 
if(!empty($css_files)):
foreach($css_files as $file): ?>
<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
<?php endforeach; ?>
<?php endif; ?>
<!-- About section -->
<div class="section margin-registro-top text-general-p">
	<!-- Product Tab content -->
	<div class="section no-padding margin-top-30">
		<div class="container">
			<div class="product-tab">
        <ul class="nav margin-bottom-20 text-uppercase">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url('mis-venues.html') ?>"><h5>Mis venues</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= $venue->num_rows()>0?base_url('editar-venue.html?key='.base64_encode($venue->row()->id)):'#' ?>"><h5>Editar Venue</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= $espacios->num_rows()>0?base_url('editar-espacios.html?key='.base64_encode($espacios->row()->id)):'#' ?>"><h5>Editar Espacio</h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= $espacios->num_rows()>0?base_url('mis-fotos.html?key='.base64_encode($espacios->row()->id)):'#' ?>"><h5>Fotos <span></span></h5></a>
          </li>
          <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#tab-contactos"><h5>Editar Fotos <span></span></h5></a>
          </li>
        </ul>
        <div class="tab-content">
          
          <div class="tab-pane fade show active" id="tab-contactos">
            <?php 
              //View en application/modules/paginas/views/cruds/mis-mensajes/list_template.php
              //View en application/modules/paginas/views/cruds/mis-mensajes/list.php             
              echo $fotos->output;
            ?>
          </div>
        </div>
      </div>
			</div><!-- end container -->
		</div>
		<!-- end Product Tab content -->
	</div>
	<!-- end About section -->
	<?php 
    if(!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
    <script src="<?= $file ?>"></script>
    <?php endforeach; ?>                
    <?php endif; ?>

    <script>

    	function pauseVenue(id){    	  
          $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(0);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Pausar');
          $("#pausar-publicacion").modal('toggle');
    	}

    	function starVenue(id){
    	  $("#pausar-publicacion input[name='id']").val(id);
          $("#pausar-publicacion input[name='pausado']").val(1);
          $("#pausar-publicacion #venues").html('');
          $("#pausar-publicacion .accion").html('Activar');
          $("#pausar-publicacion").modal('toggle');
    	}

    	function actualizar(form){
    		insertar('entradas/backend/venues_fotos/update/<?= base64_decode($_GET['key']) ?>',form,'#report-success');
    		return false;
    	}
    </script>
    <?php endif ?>