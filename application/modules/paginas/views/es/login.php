<div class="container-fluid no-padding bg-image parallax" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-anuncia.jpg)">
	<div class="row no-margin">
		<div class="col-12 col-lg-6 no-margin padding-about fondo-movil-login">
			<div class="padding-50 margin-top-50">
				<div class="col-12 col-sm-12 col-lg-12 bg-white login-casa">
					<h2 class="text-uppercase titulos-general-interna font-montserrat" style="letter-spacing: 3px;"><b>Crea tu cuenta</b></h2>
					<p>
						Al crear tu cuenta podrás guardar tus venues favoritos y recibir notificaciones sobre nuevos lugares.<br><br>
						<a href="#blanco-registro" class="d-block d-lg-none" style="text-decoration:underline">Ya tengo cuenta</a>
					</p>
				</div>
				<h5 class="font-montserrat margin-bottom-30 text-uppercase" style="letter-spacing: 3px;"><b>¿Eres nuevo? Bienvenido a bordo</b></h5>
				<form onsubmit="insertar('registro/index/insert/',this,'#respuestaregistro'); return false">
					<input type="text" placeholder="Nombre*" name="nombre" class="margin-bottom-10" style="border:0px;">
					<input type="text" placeholder="Apellidos*" name="apellido_paterno" class="margin-bottom-10" style="border:0px;">
					<input type="text" placeholder="Correo electrónico*" name="email" class="margin-bottom-10" style="border:0px;">
					<input type="password" placeholder="Contraseña*" name="password" class="margin-bottom-10" style="border:0px;">
					<input type="password" placeholder="Repetir contraseña*" name="password2" class="margin-bottom-10" style="border:0px;">
					<div id="respuestaregistro">

					</div>
					<div class="margin-top-10 text-general-p">
						<p class="text-left">
							<div style="padding: 10px;">
								<input type="checkbox" id="box-4" name="politicas" value="1">
								<label for="box-4">
									Al registrarte aceptas nuestros:<br>
									<a href="<?= base_url() ?>terminos-condiciones.html" target="_new">Términos y Condiciones</a> y
									<a href="<?= base_url() ?>aviso-privacidad.html" target="_new">Aviso de Privacidad</a>
								</label>
							</div>
						</p>
					</div>
					<button type="submit" class="button button-md margin-top-30 text-center" id="btn-negro" title="Crear cuenta"><b>Crear cuenta</b></button>
				</form>
			</div>
		</div>
		<div class="col-12 col-lg-6 no-margin bg-white-interna padding-about login-casa" id="blanco-registro">
			<div class="margin-bienvenido">
				<h5 class="font-montserrat margin-bottom-30 text-uppercase" style="letter-spacing: 3px;">
				<b>¿Ya eres miembro?</b><br>
				<b>Bienvenido a casa</b>
				</h5>
				<?php if (empty($_SESSION['user'])): ?><?php if (!empty($msj)) echo $msj ?><?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>
				<form role="form" class="login-form" action="<?= base_url('main/login') ?>" onsubmit="return validar(this)" method="post">
					<input type="email" placeholder="Usuario (correo electrónico)*" name="email" required class="margin-bottom-10" style="border: none;">
					<input type="password" placeholder="Contraseña*" name="pass" required class="margin-bottom-10" style="border: none;">
					<button class="button button-md margin-top-10 text-center" id="btn-negro" title="Conoce Venuescopia"><b>Log in</b></button>
				</form>
				<?php else: redirect('anuncia.html'); endif; ?>
				<div class="section-title section-home">
					<div class="row btn-olvide-login">
						<div class="col-12" style="margin-top: 0px; padding-left: 0px;">
							<button type="button" data-toggle="modal" data-target="#recuperar" class="link-todos">OLVIDÉ MI CONTRASEÑA</button>
						</div>
					</div>
				</div><!-- end section-title -->
			</div>
		</div>
	</div><!-- end row -->
</div><!-- end container-fluid -->
