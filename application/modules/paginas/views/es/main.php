<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <!--<div class="section-fullscreen bg-image parallax" data-jarallax-video="https://www.youtube.com/watch?v=n_hB8CPRxys">-->

    <!--
    <div class="section-fullscreen" id="animate-area">
        <div class="container text-center">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <h1 class="display-4 font-weight-thin line-height-130 titulo-header texto-negro margin-bottom-30">
                  <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia"><br>
                  Encuentra el Venue de tus sueños
                </h1>
                <form class="col-12 form-inline margin-top-50" autocomplete="off" action="<?= base_url() ?>venues.html">
                  <div class="autocomplete" id="buscador-header">
                    <input id="myInput" type="text" name="nombre" placeholder="Estado, ciudad, colonia o venue">
                    <ul style="display:none;padding: 0px;text-align: left;position: absolute; background: #fff;" id="listado-resultados"></ul>
                  </div><button class="btn btn-outline-success btn-buscar-header font-montserrat" type="submit">Buscar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>

    <style>
      @keyframes animatedBackground {
	  from {
	    background-position: 0 0;
	  }
	  to {
	    background-position: 100% 0;
	  }
	}
	#animate-area {
	  width: 100%;
	  height: 600px;
	  background-image: url(https://www.venuescopia.com/img/venues/home-bg.jpg);
	  background-position: 0px 0px;
	  background-repeat: repeat-x;
	  animation: animatedBackground 80s linear infinite alternate;
	}
    </style>-->


    <!--
    <div class="section-fullscreen bg-image animacion-header-home" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-login.jpg);" id="section-index">
        <div class="container text-center">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <h1 class="display-4 font-weight-thin line-height-130 titulo-header texto-negro margin-bottom-30">
                  <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia"><br>
                  Encuentra el Venue de tus sueños
                </h1>
                <form class="col-12 form-inline margin-top-50" autocomplete="off" action="<?= base_url() ?>venues.html">
                  <div class="autocomplete" id="buscador-header">
                    <input id="myInput" type="text" name="nombre" placeholder="Estado, ciudad, colonia o venue">
                    <ul style="display:none;padding: 0px;text-align: left;position: absolute; background: #fff;" id="listado-resultados"></ul>
                  </div><button class="btn btn-outline-success btn-buscar-header font-montserrat" type="submit">Buscar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>-->



    <div class="section-fullscreen bg-image" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-login.jpg);" id="section-index">
        <div class="container text-center">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <h1 class="display-4 font-weight-thin line-height-130 titulo-header texto-negro margin-bottom-30">
                  <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia"><br>
                  Encuentra el Venue de tus sueños
                </h1>
                <form class="col-12 form-inline margin-top-50" autocomplete="off" action="<?= base_url() ?>venues.html">
                  <div class="autocomplete" id="buscador-header">
                    <input id="myInput" type="text" name="nombre" placeholder="Estado, ciudad, colonia o venue">
                    <ul style="display:none;padding: 0px;text-align: left;position: absolute; background: #fff;" id="listado-resultados"></ul>
                  </div><button class="btn btn-outline-success btn-buscar-header font-montserrat" type="submit">Buscar</button>
                </form>
              </div>
            </div>
          </div>
        </div>
    </div>


    <!-- Blog section -->
    <div class="section catalogoMain">
      <!-- Destacadas -->
      <div class="container margin-bottom-50 padding-movil-home">

        <div class="section-title">
          <div class="row">
            <div class="col-12 col-md-6" style="z-index: -1;">
              <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Novedades</b></h2>
              <div class="subtitulos-general text-responsive">Nuestras propiedades más destacadas.</div>
            </div>
            <div class="col-12 col-md-6 link-todos text-responsive2 nuevo-ver-todas-propiedades">
              <a href="<?= base_url() ?>venues.html">Ver todas las propiedades</a>
            </div>
          </div>
        </div><!-- end section-title -->


        <!--<div><i id="icon" class="far fa-heart"></i></div>-->


        <!-- Propiedades destacadas -->
        <div class="row">
          <!-- Slider -->
          <div class="col-12">
              <!-- Testimonial section -->

              <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true">

                <?php
                  $this->db->limit(10);
                  $this->db->order_by('venues.fecha_aprobacion','DESC');
                  foreach($this->elements->venues(array('novedades'=>1))->result() as $v):
                ?>
                  <!-- Testimonial box 1 -->
                  <div class="border-radius bg-white">
                    <a href="<?= $v->link ?>" title="Ver Venue">
                      <div class="mx-auto d-block imagen-venues-home">
                        <img src="<?= $v->foto ?>" alt="<?= $v->nombre ?>">
                      </div>
                    </a>
                    <div class="favoritos-home">
                      <div class="wrap">

                        <button type="button" onclick="addFav('<?= $v->id ?>')">
                          <div class="notification-icon prueba animate-swing">
                            <i id="" class="icon<?= $v->id ?> far <?= $v->fav?'fas':'' ?> fa-heart"></i>
                          </div>
                        </button>


                      </div>
                    </div>
                    <div class="titulo-venue-home" id="inicio-carrousel">
                      <?php $this->load->view('es/includes/venues/precios-venues',array('d'=>$v),FALSE,'paginas');?>
                    </div>
                  </div>


              <?php endforeach ?>
             </div><!-- end owl-carousel -->

          </div>
        </div><!-- end row -->
      </div>
      <div class="container margin-bottom-50 padding-movil-home">

        <div class="section-title">
          <div class="row">
            <div class="col-12 col-md-6">
              <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Populares</b></h2>
              <div class="subtitulos-general text-responsive">Nuestras propiedades más populares.</div>
            </div>
            <div class="col-12 col-md-6 link-todos text-responsive2">
              <a href="<?= base_url() ?>venues/populares">Ver todas las propiedades</a>
            </div>
          </div>
        </div><!-- end section-title -->

        <!-- Propiedades destacadas -->
        <div class="row">
          <!-- Slider -->
          <div class="col-12">
              <!-- Testimonial section -->
              <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true" >
                    <?php
                      $this->db->limit(10);
                      $this->db->order_by('venues.fecha_aprobacion','DESC');
                      foreach($this->elements->venues(array('destacado'=>1))->result() as $v):
                    ?>
                      <!-- Testimonial box 1 -->
                      <div class="border-radius bg-white">
                        <a href="<?= $v->link ?>" title="Ver Venue">
                          <div class="mx-auto d-block imagen-venues-home">
                            <img src="<?= $v->foto ?>" alt="<?= $v->nombre ?>">
                          </div>
                        </a>
                        <div class="favoritos-home">
                          <div class="wrap">
                            <button type="button" onclick="addFav('<?= $v->id ?>')">
                              <div class="notification-icon prueba animate-swing">
                                <i id="" class="icon<?= $v->id ?> far <?= $v->fav?'fas':'' ?> fa-heart"></i>
                              </div>
                            </button>
                          </div>
                        </div>
                        <div class="titulo-venue-home" id="inicio-carrousel">
                          <?php $this->load->view('es/includes/venues/precios-venues',array('d'=>$v),FALSE,'paginas');?>
                        </div>
                      </div>
                  <?php endforeach ?>
              </div><!-- end owl-carousel -->
          </div>
        </div><!-- end row -->
      </div>
      <!-- Destacadas -->


      <?php 
        foreach($this->elements->categoria_venue()->result() as $c): 
          if($c->venues->num_rows()>0): ?>
          <!-- Destacadas -->
          <div class="container padding-movil-home">

            <div class="section-title">
              <div class="row">
                <div class="col-12 col-md-6">
                  <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b><?= $c->nombre ?></b></h2>
                  <div class="subtitulos-general text-responsive"><?= $c->descripcion ?></div>
                </div>
                <div class="col-12 col-md-6 link-todos text-responsive2">
                  <a href="<?= base_url() ?>venues/categorias/<?= $c->nombre ?>">Ver todas las propiedades</a>
                </div>
              </div>
            </div><!-- end section-title -->

            <!-- Propiedades destacadas -->
            <div class="row">
              <!-- Slider -->
              <div class="col-12">
                  <!-- Testimonial section -->
                  <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true" >

                    <?php foreach($c->venues->result() as $v): ?>
                       <!-- Testimonial box 1 -->
                      <div class="border-radius bg-white">
                        <a href="<?= $v->link ?>" title="Ver Venue">
                          <div class="mx-auto d-block imagen-venues-home">
                            <img src="<?= $v->foto ?>" alt="<?= $v->nombre ?>">
                          </div>
                        </a>
                        <div class="favoritos-home">
                          <div class="wrap">
                            <button type="button" onclick="addFav('<?= $v->id ?>')">
                              <div class="notification-icon prueba animate-swing">
                                <i id="icon<?= $v->id ?>" class="icon<?= $v->id ?> far <?= $v->fav?'fas':'' ?> fa-heart"></i>
                              </div>
                            </button>
                          </div>
                        </div>
                        <div class="titulo-venue-home" id="inicio-carrousel">
                          <?php $this->load->view('es/includes/venues/precios-venues',array('d'=>$v),FALSE,'paginas');?>
                        </div>
                      </div>
                    <?php endforeach ?>


                  </div><!-- end owl-carousel -->
              </div>
            </div><!-- end row -->
          </div>
          <!-- Destacadas -->
      <?php endif; endforeach ?>


</div>

<script>
  $(document).ready(function(){      
    if($(".autocomplete input").length>0){       
      autocomplete($(".autocomplete input"),'entradas/frontend/venues_search/',0);
    }
  });
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Organization",
  "url": "<?= base_url() ?>",
  "name": "Venuescopia",
  "contactPoint": {
    "@type": "ContactPoint",
    "email": "contacto@venuescopia.com",
    "contactType": "Centro de atención"
  }
}
</script>