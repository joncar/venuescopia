<div class="container-fluid no-padding bg-image" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/header-contacto.jpg)" id="header-contact2">
      <div class="row align-items-center no-margin">

        <div class="col-12 col-sm-12 col-md-6 col-lg-6 bg-white-interna-contacto padding-about">
          <div class="padding-50 margin-text-header-contact">
            <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Contacto</b></h2>
            <div class="padding-same"><p class="text-responsive">Estamos para servirte.</p></div>
            <div class="text-responsive">
              <a href="mailto:contacto@venuescopia.com" class="bold-mail font-montserrat">
              <img src="<?= base_url() ?>theme/assets/images/mail.png" alt="Email"><a href="mailto:contacto@venuescopia.com"><b>contacto@venuescopia.com</b></a>
            </div>
          </div>
        </div>

        <div class="col-12 col-sm-12 col-md-6 col-lg-6 resolvimos-modal padding-contact">
          <div class="contact-form margin-contact">
              <form method="post" action="paginas/frontend/contacto" onsubmit="sendForm(this,'.result'); return false;">
                <div class="form-row">
                  <div class="col-12 col-sm-6 margin-bottom-10">
                    <input type="text" id="name" name="nombre" placeholder="Nombre completo*" style="border: none;">
                  </div>
                  <div class="col-12 col-sm-6 margin-bottom-10">
                    <input type="email" id="email" name="email" placeholder="Correo electrónico*" style="border: none;">
                  </div>
                </div>
                <input type="text" id="subject" name="asunto" placeholder="Asunto*" class="margin-bottom-10" style="border: none;">
                <textarea name="message" id="message" placeholder="Mensaje*" style="border: none; height: 120px; resize: none; font-size: 13px;"></textarea>
                <div class="result"></div>
                <button type="submit" class="button button-md margin-top-20 text-center" id="btn-negro" href="contacto-envio.php" title="Enviar mis comentarios" style="width: 100%;"><b>Enviar</b></button>
              </form>

              <div class="submit-result">
                <span id="success">Thank you! Your Message has been sent.</span>
                <span id="error">Something went wrong, Please try again!</span>
              </div>
            </div>
        </div>

  </div><!-- end row -->
</div><!-- end container-fluid -->
