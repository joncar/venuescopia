<link type="text/css" rel="stylesheet" href="<?= base_url() ?>assets/grocery_crud/css/jquery_plugins/slim.cropper/slim.min.css" />
<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link href="<?= base_url() ?>theme/assets/css/main.css" rel="stylesheet">
<link href="<?= base_url() ?>theme/assets/css/responsive.css" rel="stylesheet">
<?php
if (!empty($_SESSION['publicacion'])) {
    $cache = $_SESSION['publicacion'];
}
?>

<style>
    a {color: #000000;}
    a:hover {color: #000000;text-decoration: none;}
</style>

<form id="formVenue" method="post" onsubmit="enviar(this);
        return false;">
    <div class="section margin-registro-top">
        <div class="container">


            <div class="row">
                <!-- Form -->
                <div class="col-12 col-lg-6">

                    <div class="row margin-bottom-10 margin-movil">
                        <div class="col-12">
                            <h2 class="text-uppercase titulos-general font-montserrat text-responsive" style="margin-bottom:0px;"><b>Publica tu Venue</b></h2>
                        </div>
                    </div>

                    <div class="row" id="myWizard">
                        <div class="col-12">
                            <div class="resultado"></div>
                        </div>
                        <div class="col-12" id="menu-top-safari">
                            <div class="navbar padding-registro-nav d-none d-lg-block" id="nav-progreso">
                                <div class="navbar-inner">
                                    <ul class="nav nav-pills nav-wizard">
                                        <li class="btn-registro nav-item pill-1" style="margin-right: 5px;margin-bottom: 5px;"><a class="nav-link active" href="#step1" data-id="1" data-toggle="tab"> 1. Comencemos</a></li>
                                        <li class="btn-registro nav-item pill-2" style="margin-right: 5px;margin-bottom: 5px;"><a class="nav-link" href="#step2" data-id="2" data-toggle="tab"> 2. Platícanos</a></li>
                                        <li class="btn-registro nav-item pill-3" style="margin-right: 5px;margin-bottom: 5px;"><a class="nav-link" href="#step3" data-id="3" data-toggle="tab"> 3. Detalles</a></li>
                                        <li class="btn-registro nav-item pill-4" style="margin-right: 5px;margin-bottom: 5px;"><a class="nav-link" href="#step4" data-id="4" data-toggle="tab"> 4. Fotos</a></li>
                                        <li class="btn-registro nav-item pill-5" style="margin-right: 5px;margin-bottom: 5px;"><a class="nav-link" href="#step5" data-id="5" data-toggle="tab"> 5. Visualiza</a></li>
                                        <li class="btn-registro nav-item pill-6" style="margin-right: 5px;margin-bottom: 5px;"><a class="nav-link" href="#step6" data-id="6" data-toggle="tab"> 6. Pagar</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <!-- Step 1 -->
                            <div class="tab-pane fade show active" id="step1" role="tabpanel">
                            <?php $this->load->view('includes/publicar/paso1'); ?>
                            </div>

                            <!-- Step 2 -->
                            <div class="tab-pane fade" id="step2" role="tabpanel">
                            <?php $this->load->view('includes/publicar/paso2'); ?>
                            </div>

                            <!-- Step 3 -->
                            <div class="tab-pane fade" id="step3" role="tabpanel">
                            <?php $this->load->view('includes/publicar/paso3'); ?>
                            </div>



                            <!-- Step 4 -->
                            <div class="tab-pane fade" id="step4" role="tabpanel">
                            <?php $this->load->view('includes/publicar/paso4'); ?>
                            </div>

                            <!-- Step 5 -->
                            <div class="tab-pane fade" id="step5" role="tabpanel">
                            <?php $this->load->view('includes/publicar/paso5'); ?>
                            </div>

                            <!-- Step 6 -->
                            <div class="tab-pane fade" id="step6" role="tabpanel">
                            <?php $this->load->view('includes/publicar/paso6'); ?>
                            </div>
                        </div>

                    </div>
                    <div id="push"></div>
                </div>

                <!-- Tips -->
                <div class="col-12 col-lg-6" style="display: block; z-index: 2;">
                    <div class="fondo-gris sticky">
<?php foreach ($this->elements->tips()->result() as
        $t): ?>
                            <!-- TIP -->
                            <div class="fondo-tip-registro text-center tipContent tipContent<?= $t->paso ?>" style="line-height: 2; <?= $t->paso != 1 ? 'display: none' : '' ?>">
                                <div style="font-size:14px; font-family:'Avenir LT Std 35 Light';" class="margin-bottom-50 nombre-tip"><b class="tip font-weoght-bold" style="font-family:'Avenir LT Std 85 Heavy';">TIP:</b><?= $t->nombre ?></div>
                            <?= $t->descripcion ?>
                            </div>
                            <!-- TIP -->
<?php endforeach ?>
                    </div>
                </div>
            </div><!-- end row -->

        </div><!-- end container -->
    </div>
</form>

<div id="cacheTag" style="position: fixed;width:100%;bottom:0px;left:0px;z-index:10000;display: none">
    Guardando...
</div>

<form id="pagarPaypal" action="https://sandbox.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick-subscriptions">
    <input type="hidden" name="business" value="venuescopia@gmail.com">
    <input type="hidden" name="currency_code" value="MXN">
    <input type="hidden" name="no_shipping" value="1">
    <input type="hidden" name="a3" value="333">
    <input type="hidden" name="p3" value="1">
    <input type="hidden" name="t3" value="M">
    <input type="hidden" name="src" value="1">
    <input type="hidden" name="sra" value="1">
</form>

<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/slim.croppic.min.js"></script>
<script src="<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/slim.cropper.js"></script>
<script src="<?= base_url('js/conekta.js') ?>"></script>

<script>
    var autocompleteGraph = autocomplete;
    var completarCiudad,completarEstado,completarColonia;
    var conektaPublic = '<?php $this->load->config('conekta'); echo $this->config->item('conekta_public'); ?>';
    $(document).ready(function () {
        $('#telefono_contacto').mask('00 0000-0000', {placeholder: "__ ____-____"});
        $("input[name='tarjeta_fecha']").mask('00/00', {placeholder: "MM/AA"});
        
        //Cachear amenidades en el modal
        <?php if(!empty($cache['espacios'][0]['amenidades']) && is_array($cache['espacios'][0]['amenidades'])): foreach(explode(',',$cache['espacios'][0]['amenidades'][0]) as $a): if(!empty($a)): ?>
                $("#box-<?= $a ?>").prop('checked',true);
        <?php endif; endforeach; endif ?>
        
        completarEstado = new autocompleteGraph($("input.estado"),'entradas/frontend/getDataEstado',1,document.getElementById('formVenue'));
        completarCiudad = new autocompleteGraph($("input.ciudad"),'entradas/frontend/getDataCiudad',1,document.getElementById('formVenue'));
        completarColonia = new autocompleteGraph($("input.colonia"),'entradas/frontend/getDataColonia',1,document.getElementById('formVenue'));
        $(".amenidadestag").html($("#amenidades input[type='checkbox']:checked").length+' amenidades');
        <?php if(empty($_SESSION['publicacion'])): ?>
            if(typeof(localStorage.datos)!='undefined'){
                cachearDatosCacheado();                
            }
        <?php endif ?>        
    });
    
    function selEstado(nombre,id){
        $("input.estado").val(nombre);
        $("input.hestado").val(id);
        completarCiudad._search();
        $("input.ciudad").val('');
        $("input.hciudad").val('');
        $("input.colonia").val('');
        $("input.hcolonia").val('');
        cachearDatos();
    }
    
    function selCiudad(nombre,id){
        $("input.ciudad").val(nombre);
        $("input.hciudad").val(id);
        completarColonia._search();
        $("input.colonia").val('');
        $("input.hcolonia").val('');
        cachearDatos();
    }
    
    function selColonia(nombre,id){
        $("input.colonia").val(nombre);
        $("input.hcolonia").val(id);
        cachearDatos();
    }

    function getData() {
        var data = new FormData(document.getElementById('formVenue'));
        data = new URLSearchParams(data);
        return data;
    }

    function cachearDatos(clear) {
        $("#cacheTag").show();
        if(!clear){
            var datos = getData();
        }else{
            var datos = 'clear=1';
        }
        var formData = new FormData(document.getElementById('formVenue'));
        var object = {};
        formData.forEach((value, key) => {            
            if(typeof(object[key]) == 'undefined' && (key.search('fotos')>-1 || key.search('pies_fotos')>-1)){
                object[key] = [];
            }
            if(key.search('fotos') > -1 || key.search('pies_fotos')>-1){
                if(value!=''){
                    object[key].push(value);
                }
            }else{
                object[key] = value;
            }            
        });
        localStorage.data = JSON.stringify(object);
        var current = window;        
        $.post('<?= base_url('entradas/frontend/cachearPublicacion') ?>?' + datos, {}, function () {
            if(!clear){
                localStorage.datos = datos;            
            }
            $("#cacheTag").hide();
        });        
    }

    function cachearDatosCacheado(){        
        var datos = localStorage.datos;
        var current = window;
        $.post('<?= base_url('entradas/frontend/cachearPublicacion') ?>?' + datos, {}, function () {
            localStorage.datos = datos;            
            document.location.reload();
        });        
    }
</script>

<script>
    var espacios_detalles = $("#espaciosDatos .row");    
    var autocomplete = '', map, marker;
    var totalTag = 0;
    var subtotalTag = 0;
    var descuentoTag = 0;
    var diasRenovacion = 0;
    var descuentoInfo = {};
    var fotos = [];
    var espacios = [];
    function ValidarCupon() {
        descuentoInfo = {};
        var cupon = $("input[name='codigo_promocion']").val();
        if (cupon != '') {
            cupon = btoa(cupon);
            $.post(URL + 'entradas/frontend/validarCupon', {cupon: cupon}, function (data) {
                data = JSON.parse(data);
                console.log(data);
                if (data.success) {
                    descuentoInfo = data;
                    $(".resultado").removeClass('alert alert-danger').html('');
                } else {
                    $(".resultado").addClass('alert alert-danger').html(data.msj);
                    $("input[name='codigo_promocion']").val('');
                }
                refreshTags();
            });
        }
    }

    function refreshTags() {
        totalTag = subtotalTag;
        descuentoTag = 0;
        //Validamos cupon
        if (descuentoInfo.success) { //Primer mes gratis
            if (descuentoInfo.type1 == '1') {
                if(parseInt(diasRenovacion) == 30){
                    descuentoTag = totalTag;
                    totalTag -= totalTag;
                }else{
                     $(".resultado").addClass('alert alert-danger').html('Este cupón sólo es válido para una membresia mensual');
                }
            } else {
                if (parseInt(descuentoInfo.type2) > 0) {//Porcentaje
                    descuentoTag += (parseFloat(descuentoInfo.type2) * totalTag) / 100;
                }

                if (parseInt(descuentoInfo.type3) > 0) {//Porcentaje
                    descuentoTag += parseFloat(descuentoInfo.type3);
                }

                totalTag -= descuentoTag;
                totalTag = totalTag>=0?totalTag:0;
            }
        }



        $("#subtotalTag").html('$' + format(subtotalTag));
        $("#descuentoTag").html('-$' + format(descuentoTag.toFixed(2)));
        $("#totalTag").html('$' + format(totalTag.toFixed(2)));
    }

    $("input[name='membresias_id']").on('click', function () {
        subtotalTag = parseInt($(this).data('val'));
        diasRenovacion = parseInt($(this).data('dias'));
        refreshTags();
    });

    function activar(obj) {
        $(obj).find('input').prop('checked', true);
        subtotalTag = parseInt($(obj).find('input').data('val'));
        diasRenovacion = parseInt($(obj).find('input').data('dias'));
        refreshTags();
    }



    function amenidades(el) {
        $('#amenidades').modal('toggle');
        el.find('input.amenidades').val('');
        var inputs = $("#amenidades input");
        inputs.unbind('click');
        inputs.on('click', function () {
            el.find('.amenidadestag').html($("#amenidades input:checked").length + ' amenidades');
            var x = 0;
            var str = '';
            $("#amenidades input").each(function () {
                str += $(this).prop('checked') ? $(this).val() + ',' : '';
                x++;
                if (x == $("#amenidades input").length) {
                    el.find('input.amenidades').val(str);
                }
            });
        });
    }

    function selPrecio(el, id) {
        $("#membresias_id").val(id);
        $(".membresias > div").removeClass('active');
        el.addClass('active');
        cachearDatos();
    }

    function addFotoAlEspacio(espacio,name){
        espacio.fotos = new FotoElement();
        espacio.fotos.nombre = typeof(name)=='undefined'?'Venue principal':name;
        espacio.fotos.maxFotos = typeof(name)=='undefined'?10:5;
        espacio.fotos.id = espacio.id;
        espacio.fotos.render();
        return espacio;
    }

    function setEspacios(){
        var espaciosHtml = $("#espaciosDatosExtras > .row");
        espacios = [];
        var espacio = $("#espaciosDatos > .row")[0];        
        espacio.id = $(espacio).data('ident');
        espacio.nombre = $(espacio).find('input.nombreEspacio').val();
        espacio.precio = $(espacio).find('input.precio').val();
        espacio.amenidades = $(espacio).find('input.amenidades').val();
        espacio.capacidad = $(espacio).find('input.capacidad').val();
        espacio = addFotoAlEspacio(espacio);        
        espacios.push(espacio);
        for(var i = 0;i<espaciosHtml.length;i++){
            espacio = $("#espaciosDatosExtras > .row")[i];            
            espacio.id = $(espacio).data('ident');
            espacio.nombre = $(espacio).find('input.nombreEspacio').val();
            espacio.nombre = espacio.nombre==''?'Espacio '+espacio.id:espacio.nombre;            
            espacio.precio = $(espacio).find('input.precio').val();
            espacio.amenidades = $(espacio).find('input.amenidades').val();
            espacio.capacidad = $(espacio).find('input.capacidad').val(); 
            espacio = addFotoAlEspacio(espacio,espacio.nombre);           
            espacios.push(espacio);            
        }

        if(typeof localStorage.data != 'undefined'){
            //Setear imagenes
            var datos = JSON.parse(localStorage.data);
            for(var i in espacios){                
                if(typeof(datos['espacios['+i+'][fotos][]'])!='undefined'){
                    espacios[i].fotos.fotos = [];
                    for(var k in datos['espacios['+i+'][fotos][]']){
                        espacios[i].fotos.fotos.push(datos['espacios['+i+'][fotos][]'][k]);
                        espacios[i].fotos.pies.push(datos['espacios['+i+'][pies_fotos][]'][k]);
                    }
                }
                espacios[i].fotos.redraw();
            }            
        }

        $(".cropImage").show(); 
    }

    $(document).on('ready', function () {
        $(document).on('change','.nombreEspacio',function(){
            var eid = $(this).parents('.row').data('ident');
            espacios[eid].nombre = $(this).val();
            espacios[eid].fotos.setNombre($(this).val());
        });
        $(document).on('change', "#numero_espacios", function () {
            var cant = parseInt($(this).val());
            var abiertos = parseInt($("#espaciosDatosExtras > .row").length);                    
            if(cant > abiertos){
                for (var i = abiertos; i < cant; i++) {
                    var descr = espacios_detalles.clone(true).off();                    
                    descr.find('.amenidadesContent').hide();
                    descr.find('input,textarea').val('');
                    descr.find('input,textarea').show();
                    descr.find('input,textarea').removeAttr('readonly');
                   
                    idEspacio = i + 1;
                    descr.attr('data-ident', idEspacio);
                    if (i > 0) {
                        descr.find('.espaciostag').html('Espacio ' + idEspacio);                        
                        descr.find('.amenidadesContent').remove();
                    } else {                        
                        descr.find('.espaciostag').html('Espacio ' + idEspacio);                        
                    }

                    descr.find('.preciotag').html('Precio');
                    descr.find('.capacidadtag').html('Capacidad');
                    descr.find('.amenidadestag').html('Amenidades');

                    //Cambiar id de variable
                    descr.find('input[type="text"],input[type="hidden"],textarea').each(function () {
                        var name = $(this).attr('name');
                        name = name.replace('0', idEspacio);
                        $(this).attr('name', name);
                    });
                    descr.attr('data-indice', idEspacio);                    
                    $("#espaciosDatosExtras").append(descr);
                    var espacio = descr;                    
                    espacio = addFotoAlEspacio(espacio,'Espacio '+idEspacio);
                    espacios.push(espacio);
                }
            }
            else if(cant < abiertos){
                for (var i = abiertos;i>cant;i--){
                    var eid = espacios.length-1;
                    espacios[eid].fotos.remove();
                    espacios.splice(eid,1);
                    console.log(espacios);
                    $("#espaciosDatosExtras > .row:last-child").remove();
                }
            }            
        });
        setEspacios();

        //$("#numero_espacios").trigger('change');
        var coords = $("input[name='mapa']").val();
        coords = coords.replace('(','');
        coords = coords.replace(')','');
        coords = coords.split(',');
        var c = document.getElementById('mapa');
        var opt = {
            center: new google.maps.LatLng(coords[0],coords[1]),
            zoom: 16
        };
        map = new google.maps.Map(c, opt);
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(coords[0],coords[1]),
            map: map
        });

        google.maps.event.addListener(marker, 'dragend', function () {
            $('#ubicacion').val(marker.getPosition());
        });

        var input = document.getElementById('direccion');
        autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode', 'establishment']});
        autocomplete.addListener('place_changed', fillInAddress);


        $("a[data-toggle='tab']").on('shown.bs.tab', function () {
            var id = $(this).data('id');
            console.log($(this));
            $(".tipContent").hide();
            $(".tipContent" + id).show();
        });        

        $(document).on('change', 'input[name="requiere_factura"]', function () {
            if ($(this).prop('checked')) {
                $("#requiere_factura").collapse('show');
            } else {
                $("#requiere_factura").collapse('hide');
            }
        });

        $(document).on('keypress', 'input', function (e) {
            if (e.which == 13) {
                e.preventDefault();
                return false;
            }
        });

        $(document).on('change', 'input[name="nombre"]', function (e) {
            $('input[name="espacios[0][nombre]"]').val($(this).val());
        });

        $(document).on('change', 'textarea[name="descripcion"]', function (e) {
            $('textarea[name="espacios[0][descripcion]"').val($(this).val());
        });

        //Evento para cachear datos
        $(document).on('change', 'input,textarea,select', function () {
            cachearDatos();
        });

        $(document).on('change','#direccion',function(){
            if($(this).val()!=''){
                searchDireccion($(this).val(),true);                
            }
        });

    });




    function fillInAddress() {
        var place = autocomplete.getPlace().geometry.location;        
        marker.setPosition(new google.maps.LatLng(place.lat(), place.lng()));
        map.panTo(new google.maps.LatLng(place.lat(), place.lng()));
        //searchDireccion($("#direccion").val());
        $("#ubicacion").val(marker.getPosition());
    }

    function searchDireccion(direccion,moveMark) {
        var geocoder = new google.maps.Geocoder();
        var address = direccion;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var lugares = results[0].address_components;
                cachearDatos();
                if(typeof moveMark!=='undefined'){
                    marker.setPosition(results[0].geometry.location);
                    $("#ubicacion").val(results[0].geometry.location);
                }
                //$('input[name="colonia"]').val(lugares[2].long_name);
                //$('input[name="ciudad"]').val(lugares[3].long_name);                
            } else {
                alert('No se ha podido encontrar la ubicación indicada: ' + status);
                window.history.back();
            }
        });
    }

    function ready() {
        $("button[type='submit']").hide();
        $(".ready").show();
    }

    function pagarConConketa(form) {
        $(".resultado").removeClass('alert alert-danger alert-info alert-success').html('');
        var fecha = $("input[name='tarjeta_fecha']").val();
        fecha = fecha.split('/');
        if (fecha.length == 2) {
            var params = {
                "card": {
                    "number": $("input[name='tarjeta_numero']").val(),
                    "name": $("input[name='tarjeta_nombre']").val(),
                    "exp_year": fecha[1],
                    "exp_month": fecha[0],
                    "cvc": $("input[name='tarjeta_cvc']").val(),
                }
            };
        } else {
            params = {};
        }
        conektaLib.tokenizar(params,function(response) {
            $("input[name='token']").val(response.id);
            insertar('entradas/frontend/venues/insert', form, '.resultado', function (data) {                
                if (data.success) {
                    localStorage.removeItem('datos');
                    localStorage.removeItem('data');
                    document.location.href = "<?= base_url('pagook.html') ?>";
                }else{                        
                    $(document).find('.resultado').addClass('alert alert-danger').html(data.error_message);
                }
                $(document).find('button[type="submit"]').attr('disabled',false);
            },function(){
                $(document).find('button[type="submit"]').attr('disabled',false);
            });
        }, function (error) {
            $(document).find('button[type="submit"]').attr('disabled',false);
            console.log(error);
            error = error.message_to_purchaser.replace('The card could not be processed, please try again later','Debe completar los datos de la tarjeta para poder realizar el cobro');
            $(".resultado").addClass('alert alert-danger').html('<p>Error al cargar información de pago</p><p>MSJ: ' + error + '</p>');
        });
    }

    function pagarConPaypal(form) {
        form = new FormData(form);
        form.append('token', 'Paypal Connect');
        insertar('entradas/frontend/venues/insert', form, '.resultado', function (data) {
            if (data.success) {
                $(document).find('button[type="submit"]').attr('disabled',false);
                localStorage.removeItem('datos');
                //Poner datos en form y enviar
                document.location.href = "<?= base_url() ?>entradas/frontend/pagarPaypal/" + data.insert_primary_key;
                localStorage.removeItem('data');
            }
        },function(){$(document).find('button[type="submit"]').attr('disabled',false);});
    }

    function pagarCon(id) {
        $(".collapse.multi-collapse").collapse('hide');
        $("button.TDC,button.PAYPAL").removeClass('active');
        switch (id) {
            case '1': //Conekta
                $("#conekta").collapse('toggle');
                $("input[name='forma_pago']").val(id);
                $("button.TDC").addClass('active');
                break;
            case '2': //Paypal
                $("#paypal").collapse('toggle');
                $("input[name='forma_pago']").val(id);
                $("button.PAYPAL").addClass('active');
                //Poner datos en form paypal
                break;
        }
    }
    function enviar(form) {
        $(document).find('button[type="submit"]').attr('disabled',true);
        if ($("input[name='forma_pago']").val() == 1) {
            pagarConConketa(form);
        } else {
            //alert('Tipo de pago en desarrollo.');
            pagarConPaypal(form);
        }
    }

    function showTask(id) {
        $("a[href='#step" + id + "']").tab('show');
        console.log(id);
        $("document,body,html").animate({scrollTop: $('#formVenue').offset().top}, 600);
    }

    function noespacios(){
        $('#hasEspacios').collapse('hide');
        $('.membresias button').removeClass('active');
        $(this).addClass('active');
        $("#espaciosDatosExtras").html('');        
        $("#numero_espacios").val('');
        $("#hasEspacios .select-styled").html('Selecciona la cantidad');
        for(var i = espacios.length-1;i>0;i--){
            espacios[i].fotos.remove();
        }
        espacios = [espacios[0]];        
        cachearDatos();
    }

    

    function preview() {
        var data = getData();
        var current = window;
        var win = window.open(URL + 'preview.html', "_blank", "modal=yes");
        win.blur();
        current.focus();
    }

    function refreshCache(){
        localStorage.removeItem('datos');
        localStorage.removeItem('data');
        cachearDatos(true);
    }
</script>
