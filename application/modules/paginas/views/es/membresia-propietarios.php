<script>
  var conektaPublic = '<?php $this->load->config('conekta'); echo $this->config->item('conekta_public'); ?>';
</script>
<script src="<?= base_url('js/conekta.js') ?>"></script>
<?php
  $venue = $this->elements->venues(array('venues.id'=>$id),FALSE)->row();
  $membresias = $this->elements->membresias(array('id'=>$venue->membresias_id))->row();
  $this->db->order_by('id','DESC');
  $transacciones = $this->elements->transacciones(array('venues_id'=>$id));
  $this->db->order_by('fecha','DESC');
  $ultima = $this->elements->transacciones(array('venues_id'=>$id));
?>
    <div class="section">
      <div class="container">
        <?php if($venue->status==1): ?>
        <div class="margin-bottom-50 col-md-10 offset-md-1">

          <div class="col-12 col-sm-12 margin-bottom-20">
              <div class="margin-bottom-20">
                <h3 class="font-weight-bold">Próximo Pago</h3>
              </div>
              <div class="table-responsive">
                  <table class="product-table">
                    <tbody>

                      <?php
                        if($ultima->num_rows()>0){
                          $ultima = $ultima->row();
                          $next =  @strtotime($ultima->vencimiento);
                        }else{
                          $next = strtotime($venue->fecha_publicacion);
                        }
                      ?>
                      <tr>
                        <td><span>Pago correspondiente a renovación de publicación</span></td>
                        <td><span><b><?= date("d/m/Y",$next) ?></b></span></td>
                        <td>
                          <?php if(!empty($ultima->pdf)): ?>
                            <span><a href="<?= $ultima->pdf ?>"><i class="fas fa-file-download"></i> Descargar recibo</a></span>
                          <?php endif ?>
                        </td>
                      </tr>

                      <tr>
                        <td><span>Subtotal</span></td>
                        <td><span>$<?= number_format($membresias->precio,2,'.',',') ?> MXN</span></td>
                        <td></td>
                      </tr>

                      <tr>
                        <td><span>Total</span></td>
                        <td><span>$<?= number_format($membresias->precio,2,'.',',') ?> MXN</span></td>
                        <td></td>
                      </tr>

                    </tbody>
                  </table>
              </div>
          </div>

          <div class="col-12 col-sm-12 text-right">
            <a class="button button-lg cancel" id="btn-blanco" href="<?= base_url('entradas/backend/cancel/'.$venue->id) ?>" title="Cancelar esta membresia"><b>Cancelar membresía</b></a>
            <a class="button button-lg change" id="btn-blanco" href="<?= base_url('entradas/backend/change/'.$venue->id) ?>" title="Cambiar esta membresia"><b>Cambiar membresía</b></a>
          </div>

        </div><!-- end row -->


        <?php else: ?>
          <div class="margin-bottom-50 col-md-10 offset-md-1">
            <div class="col-12 col-sm-12 margin-bottom-20">
              <div class="margin-bottom-20">
                <h3 class="font-weight-bold">Membresia cancelada</h3>
                Su venue será anunciado hasta el <?= @date("d/m/Y",strtotime($ultima->row()->vencimiento)) ?>
              </div>
            </div>


          </div>

          <div class="col-12 col-md-10 offset-md-1 text-right margin-bottom-50">
            <a class="button button-sm lock" id="btn-blanco" href="<?= base_url('entradas/backend/iniciarpausar/'.$venue->id) ?>" title="Cancelar esta membresia"><b><?= $venue->pausado==1?'Volver a anunciar':'Dejar de anunciar' ?></b></a>
            <a class="button button-sm change" id="btn-blanco" href="<?= base_url('entradas/backend/change/'.$venue->id) ?>" title="Cambiar esta membresia"><b>Renovar membresía</b></a>
          </div>
        <?php endif ?>


        <div class="margin-bottom-50 col-md-10 offset-md-1">
          <div class="col-12 col-sm-12 margin-bottom-20">

              <div class="margin-bottom-20">
                <h3 class="font-weight-bold">Historial de pagos</h3>
              </div>

              <div class="table-responsive">
                  <table class="product-table">
                    <tbody>
                       <?php $estados = array('-2'=>'Reembolsado','-1'=>'Pago rechazado','1'=>'En validación','2'=>'Procesado'); ?>
                       <?php foreach($transacciones->result() as $t): ?>
                        <tr>
                          <td><span>Pago correspondiente por <?= mb_strtolower($t->descripcion) ?></span></td>
                          <td><span>$<?= $t->importe ?> MXN</span></td>
                          <td><span><?= date("d/m/Y",strtotime($t->fecha)) ?></span></td>
                          <td><span><?= $estados[$t->estado_pago] ?></span></td>
                          <td>
                            <?php if(!empty($t->pdf)): ?>
                              <span><a href="<?= $t->pdf ?>"><i class="fas fa-file-download"></i> Descargar recibo</a></span>
                            <?php endif ?>
                          </td>
                        </tr>
                      <?php endforeach ?>

                    </tbody>
                  </table>
              </div>
          </div>

          <div class="col-12 col-sm-12 text-right">
            <a class="button button-lg" id="btn-blanco" href="<?= base_url() ?>mis-venues.html" title="Leer más"><b>Regresar a mis venues</b></a>
            <a class="button button-lg" id="btn-blanco" href="<?= base_url() ?>membresias.html" title="Leer más"><b>Regresar a mis membresías</b></a>
          </div>

        </div><!-- end row -->



      </div><!-- end container -->
    </div>
    <!-- end About section -->
<script>
  $(document).on('click',".cancel",function(e){
    e.preventDefault();
    $(".cancelarSubscripcionBtn").attr('onclick','document.location.href="'+$(this).attr('href')+'";');
    $("#cancelar-suscripcion").modal('toggle');
  });

  $(document).on('click',".change",function(e){
    e.preventDefault();
    var id = $(this).attr('href');
    id = id.split('/');
    id = id[id.length-1];
    subs = '<?= $venue->status==1?$venue->membresias_id:'' ?>';
    $("#update-suscripcion .buttonSubscription"+subs).attr('type','button').attr('disabled',true).html('Subscripción elegida');

    $("#update-suscripcion input[name='venues_id']").val(id);
    $("span#pagoActual").html('<?= $venue->forma_pago==1?'Tarjeta de crédito':'Paypal' ?>');

    $("#update-suscripcion").modal('toggle');
  });

  $(document).on('click',".lock",function(e){
    e.preventDefault();
    var id = $(this).attr('href');
    id = id.split('/');
    id = id[id.length-1];
    var modal = '<?= $venue->pausado==1?'#iniciarpausarModal2':'#iniciarpausarModal' ?>';
    $(modal+" input[name='id']").val(id);
    $(modal).modal('toggle');
  });
  $(document).on('ready',function(){
    $("#update-suscripcion,#iniciarpausarModal2,#iniciarpausarModal,#cancelar-suscripcion").on('hidden.bs.modal',function(){
      document.location.reload();
    });
  })
</script>









<!-- Update suscripción -->
<?php $membresias = $this->elements->membresias(); ?>
<div class="modal fade bd-example-modal-md" id="update-suscripcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <!--<img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">-->
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">

          <div class="row">
          <div class="col-12 col-lg-12">

              <div class="product-tab">
              <ul class="nav" style="font-size:12px;">
                <?php foreach($membresias->result() as $n=>$m): ?>
                  <li class="nav-item">
                    <a class="nav-link <?= $n==0?'active':'' ?>" data-toggle="tab" href="#opcion-suscripcion<?= $m->id ?>">
                      <h5 class="font-weight-light" style="font-size: 11px;"><?= $m->nombre ?></h5>
                    </a>
                  </li>
                <?php endforeach ?>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#opcion-pago">
                      <h5 class="font-weight-light" style="font-size: 11px;">Método de pago</h5>
                    </a>
                </li>
              </ul>

              <div class="tab-content">


                <?php foreach($membresias->result() as $n=>$m): ?>
                    <!-- Reviews tab content -->
                        <div class="tab-pane fade <?= $n==0?'active show':'' ?>" id="opcion-suscripcion<?= $m->id ?>">
                            <div class="row">

                                <div class="col-12 col-md-12">
                                  <div class="contact-form">
                                    <!--<form method="post" id="contactform">
                                      <input type="text" id="subject" name="subject" placeholder="Código de promoción" required="" class="margin-bottom-30">
                                      <a href="contacto-envio.php"><button class="button button-xl button-dark" type="submit">Aplicar código</button></a>
                                    </form>-->
                                    <!-- Submit result -->
                                    <div class="submit-result">
                                      <span id="success">Thank you! Your Message has been sent.</span>
                                      <span id="error">Something went wrong, Please try again!</span>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-12 col-md-12 margin-bottom-20">
                                    <h2 class="text-uppercase titulos-general-modal font-montserrat margin-bottom-20"><b>Detalles de la suscripción</b></h2>
                                    <?= $m->descripcion ?>
                                </div>

                                <div class="col-12 col-md-12 margin-bottom-20">
                                    <h2 class="text-uppercase titulos-general-modal font-montserrat margin-bottom-20"><b>Detalles de la compra</b></h2>
                                    Nombre: <?= @$this->user->nombre ?><br>
                                    Precio: $<?= number_format($m->precio,0,'.',',') ?> MXN<br>
                                    Renovación: cada <?= $m->dias_renovacion ?> dias
                                </div>

                                <form action="entradas/backend/change" onsubmit="sendForm(this,'#update-suscripcion .resultado'); return false;" class="mx-auto d-block" style="width: 100%;">


                                  <!--
                                  <div class="row membresias margin-bottom-10 alerta" style="display: none">
                                    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
                                        <p>¿Seguro que desea cambiar su membresia?</p>
                                        <button type="submit" style="width:61%;padding: 14px;white-space: nowrap;" class="button button-md margin-top-10 text-center" id="btn-blanco">Elegir membresía1</button>
                                    </div>
                                  </div>
                                  
                                  <div class="col-12 col-md-12 margin-bottom-20">                                  
                                      <div id="updateSubscripcion<?= $m->id ?>"></div>
                                      <input type="hidden" name="membresias_id" value="<?= $m->id ?>">
                                      <input type="hidden" name="venues_id" value="0">
                                      <button type="button" class="<?= 'buttonSubscription'.$m->id ?> button button-md text-center montserrat font-weight-bold" id="btn-blanco" style="width: 100%;" onclick="$(this).parents('form').find('.alerta').fadeIn(600)">Elegir membresia2</button>
                                  </div>-->



                                  <div class="row membresias margin-bottom-10">
                                    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
                                        <button type="button" style="width:100%;" class="button button-md text-center active <?= 'buttonSubscription'.$m->id ?>" data-toggle="collapse" data-target="#hasEspacios" onclick="
                                        $('#noHasEspacios').collapse('hide');
                                        $('.membresias button').removeClass('active');
                                        $(this).addClass('active');
                                        $('.alerta').hide();
                                        $(this).parents('form').find('.alerta').fadeIn(600)" checked="" autocomplete="off" id="btn-blanco" aria-expanded="true">Cambiar membresía</button>
                                    </div>

                                    <div class="col-12 col-sm-12 alerta" style="margin-bottom: 0px; display: none">
                                        <div class="multi-collapse in collapse show" id="" style="">
                                            <div class="card card-body">
                                              <p class="text-center">¿Seguro que desea cambiar su membresia?</p>
                                              <button type="submit" style="width:100%;padding: 14px;white-space: nowrap;" class="button button-md margin-top-10 text-center font-weight-bold" id="btn-blanco">Sí, deseo cambiarla</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
                                        <div class="multi-collapse collapse" id="noHasEspacios" style=""></div>
                                    </div>
                                  </div>

                                  <div id="updateSubscripcion<?= $m->id ?>"></div>
                                  <input type="hidden" name="membresias_id" value="<?= $m->id ?>">
                                  <input type="hidden" name="venues_id" value="0">
                                  <div class="resultado col-12 col-md-12 margin-bottom-20"></div>
                                </form>
                                <!--<div class="col-12 col-md-12 margin-bottom-20 text-center">
                                    <a href="#"><i class="far fa-credit-card"></i> Editar forma de pago</a>
                                </div>-->

                            </div><!-- end row -->
                        </div>

                <?php endforeach ?>
                <div class="tab-pane fade" id="opcion-pago">
                  <div class="row margin-top-30">

                    <div class="col-12 col-md-12 margin-bottom-20">
                        <h2 class="text-uppercase titulos-general-modal font-montserrat margin-bottom-20"><b>Método de pago</b></h2>
                        Método de pago actual: <span id="pagoActual">Tarjeta de crédito</span>
                    </div>

                    <div class="col-12 col-sm-12" style="margin-bottom: 0px;padding: 0px;" id="btns-pagos-anuncia">
                      <div class="col-12 col-sm-6" style="margin-bottom: 0px;"><button class="button button-md margin-top-10 text-center" type="button" id="btn-blanco" onclick="pagarConChange('1')" style="width: 100%; font-size: 10px;">Tarjeta</button></div>
                      <div class="col-12 col-sm-6" style="margin-bottom: 0px;"><button class="button button-md margin-top-10 text-center" type="button" id="btn-blanco" onclick="pagarConChange('2')" style="width: 100%; font-size: 10px;">Paypal</button></div>
                    </div>


                    <form action="entradas/backend/actualizar_forma_pago" method="post" onsubmit="enviarChange(this,'.responsePaymentChange'); return false;">
                      <div class="col-12 col-sm-12" style="margin-bottom: 30px;">

                        <div class="collapse multi-collapse" id="conektaChange">
                          <div class="card card-body">

                            <div class="col-12 col-lg-12">
                              <input type="text" id="name" value="" name="tarjeta_nombre" placeholder="Nombre en la tarjeta">
                            </div>

                            <div class="row margin-bottom-30">
                              <div class="col-12 col-lg-12">
                                <input type="text" id="name" value="" name="tarjeta_numero" maxlength="16" placeholder="Número de la Tarjeta">
                              </div>
                              <div class="col-12 col-lg-6" style="margin-bottom:0px;">
                                <input type="text" id="name" value="" name="tarjeta_fecha" placeholder="MM/AA">
                              </div>
                              <div class="col-12 col-lg-6" style="margin-bottom:0px;">
                                <input type="text" id="name" value="" name="tarjeta_cvc" maxlength="3" placeholder="CVC">
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-12 col-lg-12">
                                <button id="btn-negro" class="button button-md text-center montserrat font-weight-bold" type="submit" style="width:100%;">Cambiar</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="collapse multi-collapse" id="paypalChange">
                        <div class="card card-body">
                          <div class="row">
                            <div class="col-12 col-lg-12">
                              <button id="btn-negro" class="button button-md text-center montserrat font-weight-bold" type="submit" style="width:100%;">Elegir</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="col-12 col-lg-12 responsePaymentChange"></div>
                      <input type="hidden" name="venues_id" value="">
                      <input type="hidden" name="forma" value="1">
                      <input type="hidden" name="token" value="">
                    </form>


                  </div>
                </div>

              </div>
            </div>

          </div>
        </div>

      </div>

      <div class="modal-footer text-center">
        <div class="col-12">
          <button type="button" class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro" style="width: 100%;">Regresar</button>
        </div>
      </div>

    </div>
  </div>

  <script>
    $(document).ready(function () {        
        $("input[name='tarjeta_fecha']").mask('00/00', {placeholder: "MM/AA"});
    });
    function pagarConChange(id){
      $(".collapse.multi-collapse").collapse('hide');
      switch(id){
        case '1': //Conekta
          $("#conektaChange").collapse('toggle');
          $("input[name='forma']").val(id);
        break;
        case '2': //Paypal
          $("#paypalChange").collapse('toggle');
          $("input[name='forma']").val(id);
          //Poner datos en form paypal
        break;
      }
    }

    function pagarConConketaChange(form){
      $(".responsePaymentChange").removeClass('alert alert-danger alert-info alert-success').html('');
      var fecha = $("#opcion-pago input[name='tarjeta_fecha']").val();
      fecha = fecha.split('/');
      console.log(fecha);
      if(fecha.length==2){
        var params = {
          "card": {
            "number": $("#opcion-pago input[name='tarjeta_numero']").val(),
            "name": $("#opcion-pago input[name='tarjeta_nombre']").val(),
            "exp_year": fecha[1],
            "exp_month": fecha[0],
            "cvc": $("#opcion-pago input[name='tarjeta_cvc']").val(),
          }
        };
      }else{
        params = {};
      }
      console.log(params);
      conektaLib.tokenizar(params,function(response){
        $("input[name='token']").val(response.id);
        sendForm(form,'.responsePaymentChange');
      },function(error){
        $(".responsePaymentChange").addClass('alert alert-danger').html('<p>Error al cargar información de pago</p><p>MSJ: '+error.message_to_purchaser+'</p>');
      });
    }

    function pagarConPaypalChange(form){
        sendForm(form,'.responsePaymentChange');
    }

    function enviarChange(form){
      if($("input[name='forma']").val()==1){
        pagarConConketaChange(form);
      }else{
        //alert('Tipo de pago en desarrollo.');
        pagarConPaypalChange(form);
      }
    }
  </script>

</div>
