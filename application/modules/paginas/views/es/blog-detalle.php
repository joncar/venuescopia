[menu]
<!--Page Title-->
    <section class="page-title" style="background-image:url(<?= base_url() ?>theme/theme/images/background/6.jpg);">
        <div class="auto-container">
            <h1>[titulo]</h1>
            <ul class="bread-crumb clearfix">
                <li><a href="[base_url]">Inicio </a></li>
                <li>Noticias</li>
            </ul>
        </div>
    </section>
    <!--End Page Title-->

    <!-- Sidebar Page Container -->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Content Side-->
                <div class="content-side col-md-8 col-sm-12 col-xs-12">
                    <div class="blog-detail">

                        <!-- News Block -->
                        <div class="news-block-two">
                            <div class="inner-box">
                                <div class="image-box">
                                    <img src="[foto]" alt="">
                                </div>
                                <div class="content-box">
                                    <h3><a href="#">[titulo]</a></h3>
                                    <ul class="info">
                                        <li>Per <a href="#">[user]</a></li>
                                        <li><a href="#">[fecha]</a></li>
                                    </ul>
                                    [texto]
                                </div>
                            </div>
                        </div>

                    </div>

                </div><!-- Blog List -->

                <!--Sidebar Side-->
                <div class="sidebar-side col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar">                        
                        <!-- Latest News -->
                        <div class="sidebar-widget popular-posts">
                            <div class="sidebar-title"><h2>Entradas recientes</h2></div>
                            <div class="widget-content">
                                [foreach:recientes]
                                <article class="post">
                                    <div class="post-thumb"><a href="[link]"><img src="[foto]" alt=""></a></div>
                                    <h3><a href="blog-single.html">[titulo]</a></h3>
                                    <span class="date">[fecha]</span>
                                </article>
                                [/foreach]
                                
                            </div>
                        </div>                        
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <!-- End Sidebar Container -->

    <!-- Call To Action -->
    <section class="call-to-action" style="background-image: url(<?= base_url() ?>theme/theme/images/background/1.jpg);">
        <div class="auto-container">
            <div class="inner-container clearfix">
                <div class="title-box">
                    <span class="icon flaticon-medical-2"></span>
                    <h2>Health Care Center</h2>
                    <p>if you have any Emerangcy by health problem contact this <span>No. 035 687 9514</span> or contact form</p>
                </div>
                <div class="btn-box">
                    <a href="[base_url]contacte.html" class="theme-btn btn-style-two"><i>+</i> Contacte</a>
                </div>
            </div>
        </div>
    </section>

[footer]