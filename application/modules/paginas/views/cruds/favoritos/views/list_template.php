<?php
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
if (!$this->is_IE7()) {
$this->set_js_lib('assets/grocery_crud/js/common/list.js');
}
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');
$this->set_js('js/listados-favoritos.js?v1.0');
?>
<script type='text/javascript'>
var base_url = '<?php echo base_url(); ?>';
var subject = '<?php echo $subject ?>';
var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
var ajax_list = '<?php echo $ajax_list_url; ?>';
var unique_hash = '<?php echo $unique_hash; ?>';
var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
var crud_pagin = 1;
var fragmentos = 1;
var total_results = <?= $total_results ?>;
var order_field = <?= empty($order_field)?'undefined':"'".$order_field."'" ?>;
</script>
<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>>
    <?php if ($success_message !== null) { ?>
    <p><?php echo $success_message; ?></p>
    <?php }
    ?>
</div>
<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" data-orderUrl="'.$order_url.'" onsubmit="return filterSearchClick(this)" style="max-width:100%"'); ?>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    
    
<style>
  #hidden-dots .owl-carousel .owl-dots .owl-dot {display: none;}
</style>

  <!-- Sidebar Desktop -->
          <div class="shop-sidebar sidebar-filtros d-none d-sm-block" style="z-index:1000">
            <div class="shop-sidebar-box">
              <h5 class="text-uppercase title-filtrar">Filtrar por</h5>
              <ul class="shop-sidebar-category">
                <li><button class="button margin-bottom-10 text-center preciotag" data-notype="true" type="button" onclick="precio($(this).parents('form'))" id="btn-blanco-small" style="width: 100%;" data-label="Precio"><b>Precio</b></button></li>
                <li><button class="button margin-bottom-10 text-center capacidadtag" type="button" onclick="capacidad($(this).parents('form'))" id="btn-blanco-small" style="width: 100%;" data-label="Capacidad"><b>Capacidad</b></button></li>
                <li class="listadoDropableSearch margin-bottom-10">
                    <input type="text" id="estado" class="estado form-control" placeholder="Estado" value="<?= @$_GET['estado'] ?>">
                    <ul></ul>
                </li>
                <li class="listadoDropableSearch margin-bottom-10">
                    <input type="text" id="ciudad" class="ciudad form-control" placeholder="Ciudad / Municipio" value="<?= @$_GET['ciudad'] ?>">
                    <ul></ul>
                </li>
                <li class="listadoDropableSearch margin-bottom-10">
                    <input type="text" id="colonia" class="colonia form-control" placeholder="Colonia" value="<?= @$_GET['colonia'] ?>">
                    <ul></ul>
                </li>
                <li>
                  <?= form_dropdown_from_query('tipos_venue_id','tipos_venue','id','nombre',@$_GET['tipo'],'',FALSE,'tipoVenue','Tipo'); ?>
                </li>
                <li>
                  <button onclick="cleanFilters()" class="button margin-bottom-10 text-center" type="button" id="btn-negro-small" style="width: 100%;">
                    <b>Limpiar</b>
                  </button>
                </li>
              </ul>
            </div>
          </div>


            <!-- Products -->
          <div class="col-12 col-lg-12 container-resultas margin-top-20">
            <div class="row align-items-center no-padding margin-buscador-top">

              <div class="col-12 col-lg-9" style="margin-top: 8px;">


                  <!-- 
                  <div class="autocomplete" id="buscador-header" style="border: #000000 solid 1px; margin-right: -5px;">
                    <input id="myInput" type="text" name="nombre" placeholder="Ciudad, colonia o venue" value="<?= @$_GET['nombre'] ?>">
                    <ul style="display:none;padding: 0 20px;text-align: left;position: absolute;width: 100%;background: #fff;"></ul>
                  </div>
                  <button class="btn btn-outline-success btn-buscar-header font-montserrat" type="submit">Buscar</button>-->

                  
                  <div class="input-group mb-3 autocomplete" id="btn-buscar-nuevo">
                    <input type="text" name="nombre" class="form-control" placeholder="Estado, ciudad, colonia o venue" aria-label="Ciudad, colonia o venue" aria-describedby="basic-addon2" value="<?= @$_GET['nombre'] ?>">
                    <div class="input-group-append">
                      <button class="btn btn-outline-secondary font-montserrat btn-buscar-header-nuevo" type="submit" id="btn-buscar-nuevo">Buscar</button>
                    </div>
                    <ul></ul>
                  </div>

                  <style>
                    #btn-buscar-nuevo .form-control {
                        background: transparent;
                        border: #000000 1px solid;
                        border-radius: 0;
                        border-top-right-radius: 0px;
                        border-bottom-right-radius: 0px;
                        padding: 10px 10px;
                        margin: 0;
                        margin-bottom: 0px;
                        font-size: 14px;
                        font-weight: 300;
                        font-family: 'Avenir LT Std 35 Light';
                        color: #A0A09F;
                    }
                    #btn-buscar-nuevo .btn-outline-secondary {margin-bottom: 0px; letter-spacing: 3px; font-size: 12px;}
                    .btn-buscar-header-nuevo {background-color: #000000; color: #ffffff; border: #000000 solid 2px;}
                    .btn-buscar-header-nuevo:hover {background-color: #ffffff; color: #000000; border: #000000 solid 2px;}
                  </style>


              </div>

              <div class="col-12 col-lg-3 d-none d-sm-block" style="padding-right: 0px; border: 0;">
                  <select name="order_by[0]" class="custom-select">
                    <option value="">Ordenar por</option>
                    <option value="precio_minimo" data-type="asc">Precio menor/mayor</option>
                    <option value="precio_maximo" data-type="desc">Precio mayor/menor</option>
                  </select>
                  <input type="hidden" name="order_by[1]" value="desc">
              </div>
            </div>

            <!-- Sidebar Movil -->
            <div class="d-block d-sm-none filtros-movil">
              <div class="shop-sidebar-box d-none d-sm-block">
                <h5 class="margin-bottom-10 text-uppercase">Filtrar por:</h5>
              </div>
              <div class="row">
                <div class="col-6">
                    <button class="button margin-bottom-20 text-center preciotag" type="button" onclick="precio($(this).parents('form'))" id="btn-blanco-small" style="width: 100%;" data-label="Precio"><b>Precio</b></button>
                </div>
                <div class="col-6">
                    <button class="button margin-bottom-20 text-center capacidadtag" type="button" onclick="capacidad($(this).parents('form'))" id="btn-blanco-small" style="width: 100%;" data-label="Capacidad"><b>Capacidad</b></button>
                </div>
                  <div class="col-6 listadoDropableSearch">
                    <input type="text" id="estado" class="estado form-control" placeholder="Estado" value="<?= @$_GET['estado'] ?>">
                    <ul></ul>
                </div>
                  <div class="col-6 listadoDropableSearch">
                    <input type="text" id="ciudad" class="ciudad form-control" placeholder="Ciudad / Municipio" value="<?= @$_GET['ciudad'] ?>">
                    <ul></ul>
                </div>
                  <div class="col-6 listadoDropableSearch">
                    <input type="text" id="colonia" class="colonia form-control" placeholder="Colonia" value="<?= @$_GET['colonia'] ?>">
                    <ul></ul>
                </div>
                  
                <div class="col-6">
                    <?= form_dropdown_from_query('tipos_venue_id2','tipos_venue','id','nombre',0,'',FALSE,'tipoVenue','Tipo'); ?>
                </div>
              </div>


              <div class="row margin-top-30">
                  <div class="col-12">
                    <button onclick="cleanFilters()" class="button margin-bottom-10 text-center" type="button" id="btn-negro-small" style="width: 100%;">
                      <b>Limpiar</b>
                    </button>
                  </div>
              </div>

              
            </div>
            <!-- Sidebar Movil -->

            <div class="row">
              <div class="col-12 title-resultas text-responsive text-uppercase" style="padding-left: 5px;" id="margin-movil-titulo-resultas">
                Lista de favoritos <?php
                  if(!empty($_GET['nombre'])){
                    echo 'para '.$_GET['nombre'];
                  }elseif(!empty($_GET['tipo'])){
                    echo 'para '.$_GET['tipo'];
                  }
                ?>
              </div>
              <div class="row ajax_list" style="width:100%;">
                        <div ></div>
                        Consultando resultados...
                </div>
            </div>

            <!-- Pagination -->
            <nav style="margin-top: 70px;">
              <ul class="pagination justify-content-center margin-top-50 margin-bottom-50">
              </ul>
            </nav>
          </div>

          



<!-- Colonia y ciudad --->
<input type="hidden" name="d_estado" value="<?= @$_GET['estado'] ?>">
<input type="hidden" name="d_colonia" value="<?= @$_GET['colonia'] ?>">
<input type="hidden" name="d_ciudad" value="<?= @$_GET['ciudad'] ?>">
                 
<input type="hidden" class="precio_min" id="precio_min" name="precio_min" value="">
<input type="hidden" class="precio_max" id="precio_max" name="precio_max" value="">
<input type="hidden" class="capacidad_min" id="capacidad_min" name="capacidad_min" value="">
<input type="hidden" class="capacidad_max" id="capacidad_max" name="capacidad_max" value="">


<input type='hidden' class='page'  value='1'/>
<input type='hidden' name="per_page" class='per_page'  value='12'/>
<input type='hidden' class='total_results'  value='<?= $total_results ?>'/>




</div>

<?php echo form_close() ?>