<?php foreach ($list as $num_row => $row):
 $row2 = $row;
 $this->load = get_instance()->load;
 $this->elements = get_instance()->elements;
 $row = $this->elements->venues(array('venues.id'=>$row->id),FALSE);
 if($row->num_rows()>0 && $row->row()->espacios->num_rows()>0):
  $row = $row->row();
  $row->delete_url = base_url('entradas/backend/favoritos/delete/'.$row2->favid);
 ?>

      <style>
    #hidden-dots .owl-carousel .owl-dots .owl-dot {display: none;}
    #hidden-dots-interes .owl-carousel .owl-dots .owl-dot {display: none;}
  </style>

      <!-- Product box -->
      <div class="col-12 col-sm-4 col-lg-4 product-single margin-bottom-50" id="product-single">
        <div class="product-single-img" id="hidden-dots"> 
          <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
            <div class="owl-stage-outer owl-height">
              <div class="owl-stage">
                
                <?php 
                  $x = 0; 
                  foreach($row->espacios->result() as $e):
                    foreach($e->fotos->result() as $f): 
                    //if($x<3):
                ?>
                  <div class="owl-item <?= $x==0?'active':'' ?>">
                    <div data-hash="first<?= $x ?>"><img src="<?= $f->foto ?>" alt="Propiedades destacadas Venuescopia"></div>
                  </div>
              <?php /*endif;*/ $x++; endforeach; endforeach ?>

              </div>
            </div>

            <?php if($e->fotos->num_rows()>1): ?>
              <div class="owl-nav">
                <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
              </div>
            <?php endif ?>
          </div>
        </div>
        <!-- end product-img -->

        <div class="favoritos-home2" style="text-align: right;">
            <a href="<?= $row->delete_url ?>" class="delete-row" type="button">
              <div class="notification-icon prueba animate-swing"> 
                <i class="icon<?= $row->id ?> far <?= $row->fav?'fas':'' ?> fa-heart"></i>
              </div>
            </a>
        </div>

        <div class="titulo-venue-home">
          <?php $this->load->view('es/includes/venues/precios-venues.php',array('d'=>$row),FALSE,'paginas');?>
        </div>      
    </div>

  <?php endif ?>

<?php endforeach ?>
<?php if(count($list)==1): ?>
  <div class="col-12 col-lg-4 product-single margin-bottom-50" id="product-single" style="visibility: hidden">
        <div class="product-single-img" id="hidden-dots"> 
          
        </div>
        <!-- end product-img -->

        <div class="favoritos-home2" style="text-align: right;">
            <button type="button" onclick="addFav('12')">
              <div class="notification-icon prueba animate-swing"> 
                <i class="icon12 far  fa-heart"></i>
              </div>
            </button>
        </div>
        
        <div class="titulo-venue-home">
          <a href="<?= base_url() ?>venue/12-jardin-para-bodas">
            <h5 class="text-uppercase font-montserrat"><b>Jardin para bodas</b></h5>
            <div class="text-general-p text-resultas-movil">Jardín Botánico de Tizatlán, Santa María Ixtulco, Tlaxcala de Xicohténcatl, Tlax., México</div>

          </a>
        </div>      
    </div>
<?php endif ?>
<?php if(count($list)==0): ?>

    <div class="row" style="height: 50vh;">
      <div class="col-12 col-sm-12">
        <img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Cónocenos - Venuescopia" class="mx-auto" style="width:30%;"><br>
        Aún no tienes venues favoritos.
      </div>
    </div>

<?php endif ?>
