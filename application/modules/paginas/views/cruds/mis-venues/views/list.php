<?php
    foreach ($list as $num_row => $row):
    $row = get_instance()->elements->venues(array('venues.id'=>$row->id),FALSE)->row();
?>
    <tr style="line-height: 1;">

        <td class="wishlist-product-thumbnail">
            <a href="<?= $row->link ?>" title="Ver Venue">
                <img src="<?= $row->foto ?>" alt="<?= $row->nombre ?>">
            </a>
            <h6><?= strlen($row->nombre)>25?substr($row->nombre,0,25).'...':$row->nombre ?></h6>
        </td>

        <td><span><a href="<?= base_url() ?>mis-mensajes.html?key=<?= $row->id ?>"><i class="fas fa-envelope-open-text"></i> <?= $row->mensajes->num_rows() ?></a></span></td>
        <td><span><?= $row->precio ?></span></td>
        <td><p class="text-dark"><?= $row->capacidad ?></p></td>
        <td>
            <p class="text-dark">
                <?php if($row->bloqueado=='0'): ?>
                    <?php if($row->pausado=='0'): ?>
                    <button type="button" style="width: 40px;" class="btn" id="btn-negro" onclick="pauseVenue('<?= $row->id ?>')" title="Pausar Publicación"><i class="fas fa-pause-circle"></i></button>
                    <?php else: ?>
                    <button type="button" style="width: 40px;" class="btn" id="btn-negro" onclick="starVenue('<?= $row->id ?>')" title="Restaurar Publicación"><i class="fas fa-play-circle"></i></button>
                    <?php endif ?>
                <?php else: ?>
                    <button type="button" style="width: 40px;" disabled="true" class="btn" id="btn-negro" title="Publicación pausada por el administrador"><i class="fas fa-times-circle"></i></button>
                <?php endif ?>

                    <a href="<?= base_url() ?>editar-venue.html?key=<?= base64_encode($row->id) ?>">
                        <button type="button" style="width: 40px;" class="btn" id="btn-negro" title="Editar Publicación"><i class="fas fa-pen-square"></i></button>
                    </a>
                    <!--<a href="<?= base_url() ?>editar-venue.html?key=<?= base64_encode($row->id) ?>" class="btn" id="btn-negro" title="Editar Publicación" style="width: 40px;"><i class="fas fa-pen-square"></i></a>-->
            </p>
        </td>
        <td>
            <p class="text-dark">
                <a href="<?= $row->link ?>">
                    <i class="fas fa-eye"></i> <?= $row->vistas ?>
                </a>
            </p>
        </td>
        <td>
            <p class="text-dark" style="line-height: 1.2; text-decoration: underline;">
                <a title="Ver más" href="<?= base_url('venue-plan/'.$row->id) ?>">
                    Ver más
                </a>
            </p>
        </td>
    </tr>
<?php endforeach ?>
<?php if(count($list)==0): ?>
    <tr><td>No se encuentran datos a mostrar en este momento</td></tr>
<?php endif ?>
