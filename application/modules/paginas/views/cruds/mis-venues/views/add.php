<?php echo form_open( $insert_url, 'method="post" autocomplete="off" enctype="multipart/form-data" onsubmit="return actualizar(this)"'); ?>
<?php
$counter = 0;
        foreach($fields as $field)
        {
                $even_odd = $counter % 2 == 0 ? 'odd' : 'even';
                $counter++;
?>
<div class='form-group' id="<?php echo $field->field_name; ?>_field_box">
        <label for='field-<?= $field->field_name ?>' id="<?php echo $field->field_name; ?>_display_as_box"  style="width:100%">
                <?php echo $input_fields[$field->field_name]->display_as; ?><?php echo ($input_fields[$field->field_name]->required)? "<span class='required'>*</span> " : ""; ?> :
        </label>
        <?php echo $input_fields[$field->field_name]->input ?>
</div>


<?php }?>
<!-- Start of hidden inputs -->
<?php
        foreach($hidden_fields as $hidden_field){
                echo $hidden_field->input;
}
?>


<div class="row" style="margin-bottom: 20px;">
        <div class="btn-group margin-bottom-10">         
        <button type="submit" class="button button-md margin-top-10 text-center" id="btn-negro"><b>Añadir</b></button>
        </div>     
</div>


<div id='report-success'></div>



<?php echo form_close(); ?>
<script>
	var validation_url = '<?php echo $validation_url?>';
	var list_url = '<?php echo $list_url?>';
	var message_alert_edit_form = "<?php echo $this->l('alert_edit_form')?>";
	var message_update_error = "<?php echo $this->l('update_error')?>";        
</script>