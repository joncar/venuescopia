<?php
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/jquery.noty.js');
$this->set_js_lib('assets/grocery_crud/js/jquery_plugins/config/jquery.noty.config.js');
if (!$this->is_IE7()) {
$this->set_js_lib('assets/grocery_crud/js/common/list.js');
}
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/cookies.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.form.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/jquery.printElement.min.js');
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/pagination.js');
$this->set_css('assets/grocery_crud/css/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js');
$this->set_js('js/listados.js');
/** Jquery UI */
$this->load_js_jqueryui();
if(!empty($order_field)){
$this->set_js('assets/grocery_crud/themes/bootstrap2/js/order.js');
}
?>
<script type='text/javascript'>
var base_url = '<?php echo base_url(); ?>';
var subject = '<?php echo $subject ?>';
var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
var ajax_list = '<?php echo $ajax_list_url; ?>';
var unique_hash = '<?php echo $unique_hash; ?>';
var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
var crud_pagin = 1;
var fragmentos = 1;
var total_results = <?= $total_results ?>;
var order_field = <?= empty($order_field)?'undefined':"'".$order_field."'" ?>;
</script>
<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>>
    <?php if ($success_message !== null) { ?>
    <p><?php echo $success_message; ?></p>
    <?php }
    ?>
</div>
<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" data-orderUrl="'.$order_url.'" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
    
    <div class="margin-bottom-20">
    <div class="row align-items-end">
        <div class="col-12">
            <h2 class="text-uppercase titulos font-montserrat"><b><?= $subject ?></b></h2>
        </div>
        <?php if(!$unset_add): ?>
            <div class="col-12">
                <a href="<?= $add_url ?>" class="button button-md margin-top-10 text-center" id="btn-negro"><b>Añadir</b></a>
            </div>
        <?php endif ?>
    </div>
    </div>
    <div class="table-responsive">
        <table class="product-table">
            <thead>
                <tr>
                    <?php foreach ($columns as $column): ?>   
                        <th scope="col"><h6 class="heading-uppercase no-margin"><?= $column->display_as ?></h6></th>
                    <?php endforeach ?>                    
                    <th scope="col"><h6 class="heading-uppercase no-margin" style="text-align: right">Pagos</h6></th>
                </tr>
            </thead>
            <tbody class="ajax_list">
                <tr><td>Consultando datos.</td></tr>
            </tbody>
        </table>
    </div>
    <!-- Pagination -->
    <nav>
    <ul class="pagination justify-content-center margin-top-30">
    </ul>
    </nav>

    <input type='hidden' class='page'  value='1'/>
    <input type='hidden' name="per_page" class='per_page'  value='10'/>
    <input type='hidden' class='total_results'  value='<?= $total_results ?>'/>
</div>
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php echo form_close() ?>