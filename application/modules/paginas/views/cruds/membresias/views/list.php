<?php
foreach ($list as $num_row => $row): ?>        
    <tr>
    <?php foreach ($columns as $column): ?>
            <td class='<?php if (isset($order_by[0]) && $column->field_name == $order_by[0]) { ?>sorted<?php } ?>'>
                <div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;'; ?></div>
            </td>
    <?php endforeach ?>
    <?php if (!$unset_delete || !$unset_edit || !$unset_read || !$unset_clone || !empty($actions)) { ?>
            <td align="right">
                <div class="hidden-sm hidden-xs btn-group">
                    <?php if(!$unset_read):?>
                        <!--<a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="btn btn-xs btn-success">
                            <i class="ace-icon fa fa-arrow-right bigger-120"></i>
                        </a>-->
                    <?php endif ?>
                    <?php if(!$unset_edit):?>
                    <a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="btn btn-xs btn-info">
                        <i class="fa fa-pencil-alt"></i>
                    </a>
                    <?php endif ?>         
                    <?php if(!$unset_clone):?>
                    <a href='<?php echo $row->clone_url?>' title='Clonar <?php echo $subject?>' class="btn btn-xs btn-warning">
                        <i class="fa fa-clone"></i>
                    </a>
                    <?php endif ?>
                    <?php if(!$unset_delete):?>
                    <a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="btn btn-xs btn-danger delete-row">
                        <i class="fa fa-trash-alt"></i>
                    </a>
                    <?php endif ?>
                    <?php if(!empty($row->action_urls)):?>
                    
                    <?php                        
                        foreach($row->action_urls as $action_unique_id => $action_url):
                                        $action = $actions[$action_unique_id];                        
                    ?>
                        <a href="<?php echo $action_url; ?>" class="tooltip-info <?php echo $action->css_class; ?> crud-action" style="margin-right:5px">
                            <?= $action->label ?>
                        </a> 
                    <?php endforeach ?>
                    
                    <?php endif ?>
                </div>
            </td>
    <?php } ?>
    </tr>
<?php endforeach ?>
