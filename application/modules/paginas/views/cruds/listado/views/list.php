<?php foreach ($list as $num_row => $row):
 $this->load = get_instance()->load; 
 $this->elements = get_instance()->elements; 
 $row = $this->elements->venues(array('venues.id'=>$row->id),FALSE); 
 if($row->num_rows()>0 && $row->row()->espacios->num_rows()>0):
  $row = $row->row();
 ?>            

  <style>
    #hidden-dots .owl-carousel .owl-dots .owl-dot {display: none;}
    #hidden-dots-interes .owl-carousel .owl-dots .owl-dot {display: none;}
  </style>

      <!-- Product box -->
      <div class="col-12 col-lg-4 product-single margin-bottom-50" id="product-single" itemscope itemtype="http://schema.org/Place">
        <div class="product-single-img" id="hidden-dots"> 
          <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
            <div class="owl-stage-outer owl-height">
              <div class="owl-stage">
                
              	<?php 
              		$x = 0; 
              		foreach($row->espacios->result() as $e):
              			foreach($e->fotos->result() as $f): 
              			//if($x<3):
              	?>
	                <div class="owl-item <?= $x==0?'active':'' ?>">
	                  <div data-hash="first<?= $x ?>"><img src="<?= $f->foto ?>" alt="Propiedades destacadas Venuescopia"></div>
	                </div>
            	<?php /*endif;*/ $x++; endforeach; endforeach ?>

              </div>
            </div>

            <?php if($e->fotos->num_rows()>1): ?>
              <div class="owl-nav">
                <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
              </div>
            <?php endif ?>
          </div>
        </div>
        <!-- end product-img -->

        <div class="favoritos-home2" style="text-align: right;">
            <button type="button" onclick="addFav('<?= $row->id ?>')">
              <div class="notification-icon prueba animate-swing"> 
                <i class="icon<?= $row->id ?> far <?= $row->fav?'fas':'' ?> fa-heart"></i>
              </div>
            </button>
        </div>
        
        <div class="titulo-venue-home">
          <?php $this->load->view('es/includes/venues/precios-venues.php',array('d'=>$row),FALSE,'paginas');?>
        </div>      
    </div>
  <?php endif ?>
<?php endforeach ?>
<?php 
  if(count($list)==0): 
  $rows = get_instance()->elements->venues(array()); 
?>
  
    <div class="emptyResults col-12 text-responsive" style="padding-left: 10px;" id="margin-movil-titulo-resultas">
        <img src="https://bluepixel.mx/venuescopia/theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" id="logo-resultados"><br>
        <div style="font-size: 14px; letter-spacing: 0.05em;">
        No se encontraron venues con ese criterio de búsqueda.<br>
        Próximamente nuevos lugares estarán disponibles.
        </div>
    </div>

    <?php if(count($list)==0): ?>
    <div class="col-12 title-resultas text-responsive text-uppercase" style="padding-left: 10px;" id="margin-movil-titulo-resultas">
        Quizás pueda interesarte
    </div>

    <?php foreach ($rows->result() as $num_row => $row): ?> 
    <?php if($row->espacios->num_rows()>0): ?>   
      <div class="col-12 col-lg-4 product-single margin-bottom-50" id="product-single">
        <div class="product-single-img" id="hidden-dots-interes">
          <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
            <div class="owl-stage-outer owl-height">
              <div class="owl-stage">
                <?php 
                  $x = 0; 
                  foreach($row->espacios->result() as $e):
                    foreach($e->fotos->result() as $f): 
                    if($x<3):
                ?>
                  <div class="owl-item <?= $x==0?'active':'' ?>">
                    <div data-hash="first<?= $x ?>"><img src="<?= $f->foto ?>" alt="Propiedades destacadas Venuescopia"></div>
                  </div>
                <?php endif; $x++; endforeach; endforeach ?>
              </div>              
            </div>
            <div class="owl-nav">
              <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
              <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
            </div>
          </div>
        </div>

        <div class="favoritos-home2" style="text-align: right;">
            <button type="button" onclick="addFav('<?= $row->id ?>')">
              <div class="notification-icon prueba animate-swing"> 
                <i id="icon<?= $row->id ?>" class="far <?= $row->fav?'fas':'' ?> fa-heart"></i>
              </div>
            </button>
        </div>

        <div class="titulo-venue-home">
          <?php get_instance()->load->view('es/includes/venues/precios-venues.php',array('d'=>$row),FALSE,'paginas');?>
        </div>   
      </div>
      <?php endif ?>
    <?php endforeach ?>
    <?php else: ?>
      <div style="min-height: 100vh;"></div>
    <?php endif ?>    
    
<?php endif ?>