<?php 
    foreach ($list as $num_row => $row): 
    $delete_url = $row->delete_url;
    $venue = get_instance()->elements->venues(array('venues.id'=>$row->venues_id),false)->row();
    $row = get_instance()->elements->venues_espacios(array('id'=>$row->id))->row();
?>        
    <tr>
        <td class="wishlist-product-thumbnail">
            <a href="<?= $venue->link ?>">
                <img src="<?= $row->foto ?>" alt="">
            </a>
            <h6><?= $row->nombre ?></h6>
            
        </td>
        <td><span> <?= $row->capacidad ?></span></td>
        <td><p class="text-dark"><?= $row->precio ?></p></td>
        <td>
            <a href="<?= base_url() ?>editar-espacios.html?key=<?= base64_encode($row->id) ?>" class="btn" id="btn-negro" title="Editar Publicación" style="width: 40px;"><i class="fas fa-pen-square"></i></a>
            <a href="<?= $delete_url ?>" class="btn delete-row" id="btn-negro" title="Eliminar publicación" style="width: 40px;"><i class="fas fa-times"></i></a>
        </td>

    </tr>
<?php endforeach ?>
<?php if(count($list)==0): ?>
    <tr><td>No se encuentran datos a mostrar en este momento</td></tr>
<?php endif ?>