<?php
    foreach ($list as $num_row => $row):
    $venue = get_instance()->elements->venues(array('venues.id'=>$row->venues_id),FALSE)->row();
?>
    <tr>
        <td class="wishlist-product-thumbnail">
            <img src="<?= $venue->foto ?>" alt="">
        </td>
        <td>
            <h6 style="font-family:'Avenir LT Std 35 Light'; font-size:12px; letter-spacing: 1.5px;"><?= $venue->nombre ?></h6>
        </td>
        <td>
            <a href="mailto:<?= $row->correo ?>" style="font-family:'Avenir LT Std 35 Light'; font-size:12px; letter-spacing: 1.5px;">
                <i class="fas fa-envelope-open-text"></i> <?= $row->correo ?>
            </a>
        </td>
        <td>
            <p class="text-dark texto-mensaje-contacto" style="font-family:'Avenir LT Std 35 Light'; font-size:12px; letter-spacing: 1.5px; line-height:2;">
                <?= $row->mensaje ?>
            </p>
        </td>
    </tr>
<?php endforeach ?>
<?php if(count($list)==0): ?>
    <tr><td>No se encuentran datos a mostrar en este momento</td></tr>
<?php endif ?>
