<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Reportes extends Panel{
        function __construct() {
            parent::__construct();
        } 
        
        function loadView($view = ''){
            if(is_string($view)){
                $output = $this->load->view($view,array(),TRUE);
                $view = array('view'=>'panel','crud'=>'user','output'=>$output);
            }
            parent::loadView($view);
        }
        
        function report_venues(){
        	$crud = $this->crud_function('','');
        	$crud->set_primary_key('id');
        	$crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print();
        	if(!empty($_POST['filtro'])){
        		$_GET['filtro'] = $_POST['filtro'];
        	}
        	if(!empty($_GET['filtro'])){
        		foreach($_GET['filtro'] as $n=>$v){
        			if($n=='fecha_desde'){
        				if(!empty(trim($v))){
        					$crud->where('DATE(fecha_registro) >=',trim($v));
        				}
        			}
        			elseif($n=='fecha_hasta'){
        				if(!empty(trim($v))){
        					$crud->where('DATE(fecha_registro) <=',trim($v));
        				}
        			}	
        			else{
        				if(!empty(trim($v))){
        					$crud->where($n,trim($v));
        				}
        			}
        		}
        	}
            $crud->display_as('numero_espacios','Número de espacios')
                 ->display_as('capacidad_minimo','Capacidad Mínima')
                 ->display_as('capacidad_maximo','Capacidad Máxima')
                 ->display_as('precio_minimo','Precio Mínimo')
                 ->display_as('precio_maximo','Precio Máximo')
                 ->display_as('usuario','Nombre y apellido del usuario')
                 ->display_as('correo_contacto','Correo')
                 ->display_as('membresia','Membresía')
                 ->display_as('veces_contactado','Número de veces contactado')
                 ->display_as('ver_telefono','Número de veces ver teléfono')
                 ->display_as('fecha_registro','Fecha de registro')
                 ->display_as('tipo_venue','Tipo de venue')
                 ->display_as('d_estado','Estado')
                 ->display_as('d_ciudad','Ciudad')
                 ->display_as('d_colonia','Colonia')
                 ->unset_columns('estado','ciudad','colonia');
        	$crud = $crud->render();
        	$crud->output = $this->load->view('venues',array('output'=>$crud->output),TRUE);
        	$this->loadView($crud);
        }

        
        function report_membresias(){
            $crud = $this->crud_function('','');
            $crud->set_primary_key('id');
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print();
            if(!empty($_POST['filtro'])){
                $_GET['filtro'] = $_POST['filtro'];
            }
            if(!empty($_GET['filtro'])){
                foreach($_GET['filtro'] as $n=>$v){
                    if($n=='fecha_desde'){
                        if(!empty(trim($v))){
                            $crud->where('DATE(fecha_de_pago) >=',trim($v));
                        }
                    }
                    elseif($n=='fecha_hasta'){
                        if(!empty(trim($v))){
                            $crud->where('DATE(fecha_de_pago) <=',trim($v));
                        }
                    }   
                    else{
                        if(!empty(trim($v))){
                            $crud->where($n,trim($v));
                        }
                    }
                }
            }
            $crud->display_as('d_estado','Estado')
                 ->display_as('d_ciudad','Ciudad')
                 ->display_as('d_colonia','Colonia')
                 ->unset_columns('estado','ciudad','colonia');
            $crud = $crud->render();
            $crud->output = $this->load->view('membresias',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }

        function report_venues_usuarios(){
            $crud = $this->crud_function('','');
            $crud->set_primary_key('id');
            $crud->unset_add()->unset_edit()->unset_delete()->unset_read()->unset_print();
            if(!empty($_POST['filtro'])){
                $_GET['filtro'] = $_POST['filtro'];
            }
            if(!empty($_GET['filtro'])){
                foreach($_GET['filtro'] as $n=>$v){
                    if($n=='fecha_desde'){
                        if(!empty(trim($v))){
                            $crud->where('DATE(fecha_registro) >=',trim($v));
                        }
                    }
                    elseif($n=='fecha_hasta'){
                        if(!empty(trim($v))){
                            $crud->where('DATE(fecha_registro) <=',trim($v));
                        }
                    }   
                    else{
                        if(!empty(trim($v))){
                            $crud->where($n,trim($v));
                        }
                    }
                }
            }
            $crud = $crud->render();
            $crud->output = $this->load->view('venues_usuarios',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }

        
    }
?>
