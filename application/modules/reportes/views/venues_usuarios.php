<div>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link <?= empty($_GET)?'active':'' ?>" href="#home" aria-controls="home" role="tab" data-toggle="tab">Filtros</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= !empty($_GET)?'active':'' ?>" href="#profile" aria-controls="home" role="tab" data-toggle="tab">Reporte</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?= empty($_GET)?'active':'' ?>" id="home">
      <div class="card card-default">
      	<div class="card-body">
      		<h1 class="card-title" style="margin-bottom: 20px">Elige el filtro de busqueda</h1>
      		<form action="" method="get">
      		  <div class="row">
      		  	<div class="col-12"><b>Filtrar por fecha</b></div>
      		  </div>
			  <div class="row">
			    <div class="col">
		    	  <label for="inputEmail4">Desde</label>
			      <input type="date" class="form-control" name="filtro[fecha_desde]" value="<?= @$_GET['filtro']['fecha_desde'] ?>" placeholder="Desde la fecha">
			    </div>
			    <div class="col">
		    	  <label for="inputEmail4">Hasta</label>
			      <input type="date" class="form-control" name="filtro[fecha_hasta]" value="<?= @$_GET['filtro']['fecha_hasta'] ?>" placeholder="Hasta la fecha">
			    </div>
			  </div>

			  

			  <button type="submit" class="btn btn-primary">Consultar</button>
			</form>
      	</div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane <?= !empty($_GET)?'active':'' ?>" id="profile">
      <?= $output ?>
    </div>
  </div>

</div>