<div>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link <?= empty($_GET)?'active':'' ?>" href="#home" aria-controls="home" role="tab" data-toggle="tab">Filtros</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?= !empty($_GET)?'active':'' ?>" href="#profile" aria-controls="home" role="tab" data-toggle="tab">Reporte</a>
    </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?= empty($_GET)?'active':'' ?>" id="home">
      <div class="card card-default">
      	<div class="card-body">
      		<h1 class="card-title" style="margin-bottom: 20px">Elige el filtro de busqueda</h1>
      		<form action="" method="get">
      		  <div class="row">
      		  	<div class="col-12"><b>Filtrar por fecha</b></div>
      		  </div>
			  <div class="row">
			    <div class="col">
		    	  <label for="inputEmail4">Desde</label>
			      <input type="date" class="form-control" name="filtro[fecha_desde]" value="<?= @$_GET['filtro']['fecha_desde'] ?>" placeholder="Desde la fecha">
			    </div>
			    <div class="col">
		    	  <label for="inputEmail4">Hasta</label>
			      <input type="date" class="form-control" name="filtro[fecha_hasta]" value="<?= @$_GET['filtro']['fecha_hasta'] ?>" placeholder="Hasta la fecha">
			    </div>
			  </div>

			  <div class="row">
				
		    	  <?php foreach($this->db->get('membresias')->result() as $m): ?>
		    	  <div class="col">
		    	  	<div class="form-group">
					    <div class="form-check">
					      <input class="form-check-input" name="filtro[id_membresia]" <?= @$_GET['filtro']['membresias_id']==$m->id?'checked':'' ?> type="radio" id="gridCheck<?= $m->id ?>" value="<?= $m->id ?>" style="display: none">
					      <label class="form-check-label" for="gridCheck<?= $m->id ?>">
					        <?= $m->nombre ?>
					      </label>
					    </div>
					</div>
				   </div>
		    	  <?php endforeach ?>			    
			  </div>

			  <div class="row">
			    <div class="col">
		    	  <label for="inputEmail4">Estado</label>
			      <?= form_dropdown_from_query('filtro[estado]','estados','id','nombre',@$_GET['filtro']['estado'],'id="estado"') ?>
			    </div>
			    <div class="col">
		    	  <label for="inputEmail4">Ciudad</label>
			      <?php $this->db->limit(10); echo form_dropdown_from_query('filtro[ciudad]','ciudades','id','nombre',@$_GET['filtro']['ciudad'],'id="ciudad"') ?>
			    </div>
			    <div class="col">
		    	  <label for="inputEmail4">Colonia</label>
			      <?php $this->db->limit(10); echo form_dropdown_from_query('filtro[colonia]','colonias','id','nombre',@$_GET['filtro']['colonia'],'id="colonia"') ?>
			    </div>
			  </div>

			  <button type="submit" class="btn btn-primary">Consultar</button>
			</form>
      	</div>
      </div>
    </div>
    <div role="tabpanel" class="tab-pane <?= !empty($_GET)?'active':'' ?>" id="profile">
      <?= $output ?>
    </div>
  </div>

</div>

<script>
	$("#estado").on('change',function(){
		$.post('<?= base_url() ?>entradas/admin/ciudades/json_list',{
			'search_field[]':'estados_id',
			'search_text[]':$(this).val(),
			'operator':'where',
			'per_page':'10000'
		},function(data){
			data = JSON.parse(data);
			var opt = '<option value="">Seleccione una ciudad</option>';
			for(var i in data){
				opt+= '<option value="'+data[i].id+'">'+data[i].ciudades_nombre+'</option>';
			}
			$("#ciudad").html(opt);
		});
	});

	$("#ciudad").on('change',function(){
		$.post('<?= base_url() ?>entradas/admin/colonias/json_list',{
			'search_field[]':'ciudades_id',
			'search_text[]':$(this).val(),
			'operator':'where',
			'per_page':'10000'
		},function(data){
			data = JSON.parse(data);
			var opt = '<option value="">Seleccione una colonia</option>';
			for(var i in data){
				opt+= '<option value="'+data[i].id+'">'+data[i].colonias_nombre+'</option>';
			}
			$("#colonia").html(opt);
		});
	});
</script>