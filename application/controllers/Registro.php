<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Main.php');
class Registro extends Main {
        const IDROLUSER = 2;
        public function __construct()
        {
            parent::__construct();         
            $this->load->library('grocery_crud');
            $this->load->library('ajax_grocery_crud');
        }
        
        public function loadView($param = array('view'=>'main'))
        {
                if(!empty($param->output)){
                    $panel = 'panel';
                    $param->view = empty($param->view)?$panel:$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                if(is_string($param)){
                    $param = array('view'=>$param);
                }
                $template = 'templateadmin';
                $this->load->view($template,$param);
        }

        public function loadViewTheme($param = array('view'=>'main'))
        {
                if(!empty($param->output)){
                    $panel = 'panel';
                    $param->view = empty($param->view)?$panel:$param->view;
                    $param->crud = empty($param->crud)?'user':$param->crud;
                    $param->title = empty($param->title)?ucfirst($this->router->fetch_method()):$param->title;
                }
                if(is_string($param)){
                    $param = array('view'=>$param);
                }
                $template = 'template';
                $this->load->view($template,$param);
        }

        public function index($url = 'main',$page = 0)
        {
            if($this->router->fetch_class()=='registro'){
                $crud = new ajax_grocery_CRUD();
                $crud->set_theme('bootstrap2');
                $crud->set_table('user');
                $crud->set_subject('<span style="font-size:18px">Nuevo usuario</span>');
                $crud->unset_back_to_list()
                     ->unset_delete()
                     ->unset_read()
                     ->unset_edit()
                     ->unset_list()
                     ->unset_export()
                     ->unset_print();
                $crud->callback_before_insert(array($this,'binsertion'));
                $crud->callback_after_insert(array($this,'ainsertion'));
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
                $crud->required_fields('password','email','nombre','apellido_paterno');
                $crud->display_as('apellido_paterno','Apellidos');
                if(!empty($_POST) && $crud->getParameters(FALSE)=='insert_validation'){
                    $crud->fields('password','email','nombre','apellido_paterno','password2','politicas');
                    $crud->set_rules('password','Contraseña','required|min_length[8]');
                    $crud->set_rules('password2','Repetir contraseña','required|matches[password]');
                    $crud->set_rules('politicas','Politícas de privacidad','required');
                }
                if(empty($_POST['ajax'])){
                    $crud->set_lang_string('insert_success_message','Se ha registrado correctamente su cuenta <script>document.location.href="'.base_url().'registro-realizado.html";</script>');
                }else{
                    $crud->set_lang_string('insert_success_message','Se ha registrado correctamente su cuenta <script>window.onlogin();</script>');
                    unset($_POST['ajax']);
                }
                $output = $crud->render();
                $output->view = 'registro';
                $output->crud = 'user';
                $output->title = 'REGISTRAR';
                $output->output = $output->output;
                $output->scripts = get_header_crud($output->css_files,$output->js_files,TRUE);
                $this->loadView($output);   
            }
        }   

        function validateEmail(){
            if(!empty($_POST['email'])){
                $email = $this->db->get_where('user',array('email'=>$_POST['email']));
                if($email->num_rows()>0){
                    $email = $email->row();
                    if($email->status==0){                                                
                        $data = $_POST;
                        $data['password'] = md5($data['password']);
                        unset($data['password2']);
                        unset($data['politicas']);
                        $this->db->update('user',$data,array('id'=>$email->id));

                        $_POST['link'] = base_url('registro/validar/'.base64_encode($email->id));
                        $_POST['contrasena'] = $_POST['password'];
                        get_instance()->enviarcorreo((object)$_POST,11);

                        $this->form_validation->set_message('validateEmail','Se ha registrado correctamente su cuenta <script>conectar();</script>');
                        return false;
                    }else{
                        $this->form_validation->set_message('validateEmail','Este email ya se encuentra registrado, intenté nuevamente con otro valor.');
                        return false;
                    }
                }else{
                    return true;
                }
            }
        }           
        
        function conectar()
        {
            $this->loadView('predesign/login');
        }
        /* Callbacks */
        function binsertion($post)
        {            
            $post['status'] = 1;
            $post['admin'] = 0;
            $post['fecha_registro'] = date("Y-m-d H:i:s");
            get_instance()->contrasena = $post['password'];
            $post['password'] = md5($post['password']);
            return $post;
        }
        
        function ainsertion($post,$primary)
        {              
            //Asignar rol
            get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::IDROLUSER));            
            get_instance()->user->login_short($primary);   
            $post['link'] = base_url('registro/validar/'.base64_encode($primary));
            $post['contrasena'] = get_instance()->contrasena;
            get_instance()->enviarcorreo((object)$post,11);
            return true;
        }

        function validar($primary)
        {              
            $primary = base64_decode($primary);
            if(is_numeric($primary) && $this->db->get_where('user',array('id'=>$primary))->num_rows()>0){
                $this->db->update('user',array('status'=>1),array('id'=>$primary));
                $this->user->login_short($primary);   
                redirect(base_url()); 
            }
        }
        
       
        function reset(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('pass','Contraseña','required|min_length[8]');
            $this->form_validation->set_rules('pass2','Contraseña','required|min_length[8]|matches[pass]');            
            if($this->form_validation->run())
            {
                $this->db->update('user',array('password'=>md5($this->input->post('pass'))),array('email'=>$this->input->post('email')));
                session_unset();
                $_SESSION['msj'] = $this->success('Se ha restablecido su contraseña');  
                $_SESSION['key'] = 1;
                //header("Location:".base_url('login.html'));
                $this->user->login_short($this->db->get_where('user',array('email'=>$this->input->post('email')))->row()->id);
                $this->loadViewTheme(array('view'=>'recover','msj'=>$this->success('Se ha restablecido su contraseña <script>setTimeout(function(){document.location.href="'.base_url().'";},3000);</script>'),'key'=>$this->input->post('email')));
            }
            else{
                $key = empty($_POST['email'])?'':$_POST['email'];
                $this->loadViewTheme(array('view'=>'recover','key'=>$key,'msj'=>$this->error($this->form_validation->error_string())));
            }
        }
       
        function forget($key = '',$ajax = '')
        {
            if(empty($_POST) && empty($key)){
                $this->loadViewTheme(array('view'=>'forget'));
            }
            else
            {                                
                if(empty($key)){
                    $this->form_validation->set_rules('email','Email','required|valid_email');
                    if($this->form_validation->run())
                    {
                        $user = $this->db->get_where('user',array('email'=>$this->input->post('email')));
                        if($user->num_rows()>0){
                            $_SESSION['key'] = md5($this->input->post('email'));
                            $_SESSION['email'] = $this->input->post('email');
                            $user->row()->link = base_url('registro/forget/'.base64_encode($_POST['email']));
                            $this->enviarcorreo($user->row(),12);
                            echo $this->success('Se han enviado los pasos de restauración a su correo');
                            
                        }
                        else{
                            echo $this->error('El correo utilizado no esta registrado');
                        }
                    }
                    else{
                        echo $this->error($this->form_validation->error_string());
                    }
                }
                else
                {
                    $key = base64_decode($key);
                    $this->loadViewTheme(array('view'=>'recover','key'=>$key));                    
                }
            }
        }     

        function cancelarCuenta(){
            $msj = $this->error('Usuario no registrado');
            if($this->user->log){
                $user = $this->db->get_where('user',array('id'=>$this->user->id));
                $venues = $this->db->get_where('venues',array('user_id'=>$this->user->id,'pausado'=>0,'bloqueado'=>0));
                if($venues->num_rows()>0){
                    $msj = $this->error('Antes de eliminar tu cuenta debes cancelar los anuncios activos. <a href="'.base_url('mis-venues.html').'">Ir a mis venues.</a>');                    
                }else{
                    //Eliminar venues
                    /*foreach($this->db->get_where('venues',array('user_id'=>$this->user->id))->result() as $v){
                        foreach($this->db->get_where('venues_espacios',array('venues_id'=>$v->id))->result() as $e){
                            $this->db->delete('venues_fotos',array('venues_espacios_id'=>$e->id));
                        }
                        $this->db->delete('venues_espacios',array('venues_id'=>$v->id));
                        $this->db->delete('mensajes',array('venues_id'=>$v->id));
                    }*/
                    //Eliminar mensajes
                    $this->db->update('user',array('status'=>0),array('id'=>$this->user->id));
                    session_unset();
                    $msj = $this->success('Usuario eliminado con éxito <script>setTimeout(function(){document.location.href="'.base_url().'";},1400);</script>');
                }
            }

            echo $msj;
        }
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
