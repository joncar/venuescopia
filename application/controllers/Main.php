<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
session_save_path('/tmp');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {
    //public $theme = 'themes/bristol/';
    public $theme = '';
    public $min_width_image = '1024';
    public $min_height_image = '768';
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->model('querys');
        $this->load->model('elements');
        $this->load->library('redsysAPI');
        $this->load->library('traduccion');
        $this->load->model('carrito');
        date_default_timezone_set('America/Mexico_City');
        setlocale(LC_ALL, 'es_ES', 'Spanish_Spain', 'Spanish');    
        if(empty($_SESSION['user']) && empty($_SESSION['tmpsesion'])){
            $_SESSION['tmpsesion'] = '1'.date("his");
        }

        if(empty($_SESSION['lang'])){
            $_SESSION['lang'] = 'es';            
        }
        $this->theme = 'es/';
        $this->ajustes = $this->db->get('ajustes')->row();
    }
    
    
    
    function get_entries(){
        $blog = new Bdsource();
        $blog->limit = array('3','0');
        $blog->order_by = array('fecha','DESC');        
        $blog->init('blog');
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
        }
        if($this->blog->num_rows()>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
    }

    public function index() {
        $this->get_entries();
        $this->loadView(array('view'=>'main', 'blog'=>$this->blog));
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember'])){
                            $_SESSION['remember'] = 1;
                        }
                        if (empty($_POST['redirect'])){
                            $redirect = $this->user->admin==1?'panel':'';
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url($redirect) . '"</script>');
                        }
                        else{
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                        }
                    } else{
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                    }
                } else{
                    $_SESSION['msj'] = $this->error('Usuario o contraseña incorrecta, intente de nuevo.');
                }
            } else{
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');
            }

            if (!empty($_SESSION['msj'])){
                header("Location:" . base_url('login.html'));
            }
        } else{
            header("Location:" . base_url('login.html'));
        }
    }

    public function loginShort() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        echo $this->success('Usuario logueado correctamente <script>window.onlogin();</script>');
                    } else{
                        echo $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                    }
                } else{
                    echo $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
                }
            } else{
                echo $this->error('Debe completar todos los campos antes de continuar');
            }            
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    function getHead($page){
        $ajustes = $this->db->get('ajustes')->row();
        //$stocookie = '<link href="'.base_url('js/stocookie/stoCookie.css').'">';        
        $stocookie = '';
        $page = str_replace('</head>',$stocookie.'</head>',$page);
        return $page;
    }

    function getBody($page){
        $ajustes = $this->db->get('ajustes')->row();        
        $stocookie = '';
        //$stocookie = '<script src="'.base_url('js/stocookie/stoCookie.min.js').'"></script>';
        //$stocookie.= html_entity_decode($ajustes->cookies);
        $page = str_replace('</body>',$stocookie.'</body>',$page);
        $page= str_replace('</body>',$ajustes->analytics.'</body>',$page);
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param)){
            $param = array('view' => $param);
        }
        $ajustes = $this->db->get('ajustes')->row();
        if(empty($param['title'])){
            $param['title'] = $ajustes->titulo;
        }
        $param['module'] = empty($param['module'])?'':$param['module'];
        $param['favicon'] = $ajustes->favicon;
        $param['keywords'] = $ajustes->keywords;
        $param['descr'] = empty($param['descr'])?$ajustes->description:$param['descr'];
        $page = $this->load->view('template', $param,true);
        $page = $this->getHead($page);
        $page = $this->getBody($page);
        $page = $this->traduccion->traducir($page,$_SESSION['lang']);

        $page = str_replace('data-lang="'.$_SESSION['lang'].'"','class="active"',$page);
        echo $page;
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/404'));
    }

    public function traduccion($idioma = 'ca'){        
        $_SESSION['lang'] = $idioma;        
        if(empty($_GET['url'])){
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            redirect($_GET['url']);
        }
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            if(is_numeric($idnotificacion)){
                $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            }else{
                $mensaje = (object)$idnotificacion;
            }
            $mensaje->texto = str_replace('usuarios.','',$mensaje->texto);
            $mensaje->titulo = str_replace('usuarios.','',$mensaje->titulo);
            foreach($usuario as $n=>$v){ 
             if(is_string($v)){            
                 $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
                 $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
        }

        function testMail(){
            correo('joncar.c@gmail.com','TEST','TEST');
        }

        /*function deleteIMGWhatNotExistsInBd(){
            $files = scandir('img/venues');
            foreach($files as $n=>$v){
                if($v!='.' && $v!='..' && (strpos($v,'.png') || strpos($v,'.jpg') || strpos($v,'.jpeg') || strpos($v,'.JPG')) && $this->db->get_where('venues_fotos',array('foto'=>$v))->num_rows()==0){
                    unlink('img/venues/'.$v);
                }
            }
        }*/

}
