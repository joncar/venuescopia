DROP VIEW IF EXISTS view_misplanes;
CREATE VIEW view_misplanes AS 
SELECT 
venues.id, 
venues.nombre,
venues.fecha_publicacion as fecha,
transacciones.vencimiento as vencimiento,
transacciones.estado_pago,
transacciones.id as nro_transaccion,
membresias.nombre as Plan,
transacciones.importe as Importe,
venues.user_id
FROM `venues`
INNER JOIN membresias ON membresias.id = venues.membresias_id
LEFT JOIN transacciones ON transacciones.id = last_pay(venues.id)
GROUP BY venues.id