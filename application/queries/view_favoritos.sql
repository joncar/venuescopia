DROP VIEW IF EXISTS view_favoritos;
CREATE VIEW view_favoritos AS 
SELECT 
venues.id,
venues.nombre,
venues.foto,
venues.direccion,
venues.destacado,
venues.tipos_venue_id,
estados.nombre as d_estado,
ciudades.nombre as d_ciudad,
colonias.nombre as d_colonia,
venues_espacios.precio_minimo,
venues_espacios.precio_maximo,
venues_espacios.capacidad_minimo,
venues_espacios.capacidad_maximo,
venues.bloqueado,
venues.pausado,
favoritos.user_id,
favoritos.id as favid
FROM `favoritos`
INNER JOIN venues ON venues.id = favoritos.venues_id
INNER JOIN (
	SELECT 
	venues_id,
	MIN(precio(0,precio)) as precio_minimo,
	MAX(precio(1,precio)) as precio_maximo,
	MIN(precio(0,capacidad)) as capacidad_minimo,
	MAX(precio(1,capacidad)) as capacidad_maximo
	FROM 
	venues_espacios
	GROUP BY venues_id
) as venues_espacios ON venues_espacios.venues_id = venues.id
INNER JOIN estados ON estados.id = venues.estado
INNER JOIN ciudades ON ciudades.id = venues.ciudad
LEFT JOIN colonias ON colonias.id = venues.colonia

