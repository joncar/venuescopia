DROP VIEW IF EXISTS view_populares;
CREATE VIEW view_populares AS
SELECT 
venues.id,
venues.nombre,
venues.descripcion,
venues.tipos_venue_id,
venues.telefono_contacto,
venues.correo_contacto,
venues.direccion,
venues.ciudad,
venues.estado,
venues.colonia,
venues.foto,
venues.membresias_id,
venues.categoria_venue_id,
venues.user_id,
venues.vistas,
venues.pausado,
venues.bloqueado,
estados.nombre as d_estado,
ciudades.nombre as d_ciudad,
colonias.nombre as d_colonia,
'1' as 'destacado',
precio(0,precio) as precio_minimo,
precio(1,precio) as precio_maximo,
precio(0,capacidad) as capacidad_minimo,
precio(1,capacidad) as capacidad_maximo
FROM venues
INNER JOIN (SELECT MIN(id) as id,precio,capacidad,venues_id FROM venues_espacios GROUP BY venues_id ) as venues_espacios ON venues_espacios.venues_id = venues.id
INNER JOIN estados ON estados.id = venues.estado
INNER JOIN ciudades ON ciudades.id = venues.ciudad
INNER JOIN colonias ON colonias.id = venues.colonia
ORDER BY venues.vistas DESC
LIMIT 10