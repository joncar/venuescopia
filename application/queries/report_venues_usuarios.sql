DROP VIEW IF EXISTS report_venues_usuarios;
CREATE VIEW report_venues_usuarios AS SELECT 
user.id,
CONCAT(user.nombre,' ',user.apellido_paterno) as nombre_completo,
venues.fecha_publicacion as fecha_registro,
COUNT(venues.id) as venues_registrados,
COALESCE(SUM(transacciones.importe),0) AS pagado_hasta_la_fecha
FROM user
LEFT JOIN venues ON venues.user_id = user.id
LEFT JOIN transacciones ON transacciones.venues_id = venues.id AND transacciones.estado_pago = 2
GROUP BY user.id