DROP VIEW IF EXISTS report_venues;
CREATE VIEW report_venues AS SELECT 
view_venues.id,
view_venues.nombre,
get_espacios(view_venues.id) AS espacios,
count_espacios(view_venues.id) as numero_espacios,
view_venues.capacidad_minimo,
view_venues.capacidad_maximo,
view_venues.precio_minimo,
view_venues.precio_maximo,
CONCAT(user.nombre,' ',user.apellido_paterno) as usuario,
view_venues.correo_contacto,
membresias.nombre as membresia,
view_venues.vistas,
COUNT(mensajes.id) as veces_contactado,
view_venues.solicitud_telefono as ver_telefono,
view_venues.fecha_publicacion as fecha_registro,
tipos_venue.nombre as tipo_venue,
view_venues.pausado,
view_venues.bloqueado,
MAX(transacciones.vencimiento) as vencimiento,
IF(DATE(NOW())>=MAX(transacciones.vencimiento),'SI','NO') as vencido,
view_venues.d_estado,
view_venues.d_ciudad,
view_venues.d_colonia,
view_venues.estado,
view_venues.ciudad,
view_venues.colonia
FROM view_venues
INNER JOIN user ON user.id = view_venues.user_id
INNER JOIN membresias ON membresias.id = view_venues.membresias_id
INNER JOIN venues_espacios ON venues_espacios.venues_id = view_venues.id
INNER JOIN tipos_venue ON tipos_venue.id = view_venues.tipos_venue_id
LEFT JOIN transacciones ON transacciones.venues_id = view_venues.id
LEFT JOIN mensajes ON mensajes.venues_id = view_venues.id
GROUP BY view_venues.id, venues_espacios.venues_id