##PRiorioridad

## estado, ciudad, colonia, tipo, venue, direccion
DROP VIEW IF EXISTS _view_buscador;
CREATE VIEW _view_buscador AS
SELECT '1' AS prioridad,id, d_estado as nombre, CONCAT('venues.html?estado=',d_estado,'&tag=',d_estado) as url FROM view_venues WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
UNION 
SELECT '2' AS prioridad,id, CONCAT(d_ciudad,', ',d_estado), CONCAT('venues.html?estado=',d_estado,'&ciudad=',d_ciudad,'&tag=',CONCAT(d_ciudad,', ',d_estado)) FROM view_venues WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
UNION 
SELECT '3' AS prioridad,id, CONCAT(d_colonia,', ',d_ciudad,', ',d_estado), CONCAT('venues.html?estado=',d_estado,'&ciudad=',d_ciudad,'&colonia=',d_colonia,'&tag=',CONCAT(d_colonia,', ',d_ciudad,', ',d_estado)) FROM view_venues WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
UNION 
SELECT '4' AS prioridad,id,tipos_venue.nombre, CONCAT('venues.html?tipo=',tipos_venue.id,'&tipoName=',tipos_venue.nombre,'&tag=',tipos_venue.nombre) FROM tipos_venue
UNION
SELECT 
'5' AS prioridad,
id,
nombre,
CONCAT('venue/',id,'-') as url
FROM venues
WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
UNION
SELECT 
'6' AS prioridad,
id,
direccion,
CONCAT('venue/',id,'-') as url
FROM venues
WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0);

DROP VIEW IF EXISTS view_buscador;
CREATE VIEW view_buscador AS SELECT * FROM _view_buscador GROUP BY nombre;






#OBSOLETE
DROP VIEW IF EXISTS _view_buscador;
CREATE VIEW _view_buscador AS 
#Nombre
SELECT 
id,
nombre,
CONCAT('venue/',id,'-') as url
FROM venues
WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
#Tipo
UNION 
SELECT id,tipos_venue.nombre, CONCAT('venues.html?tipo=',tipos_venue.id,'&tipoName=',tipos_venue.nombre,'&tag=',tipos_venue.nombre) FROM tipos_venue
#direccion
UNION
SELECT 
id,
direccion,
CONCAT('venue/',id,'-') as url
FROM venues
WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
#estado
UNION 
SELECT id, d_estado, CONCAT('venues.html?estado=',d_estado,'&tag=',d_estado) FROM view_venues WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
#ciudad
UNION 
SELECT id, CONCAT(d_ciudad,', ',d_estado), CONCAT('venues.html?estado=',d_estado,'&ciudad=',d_ciudad,'&tag=',CONCAT(d_ciudad,', ',d_estado)) FROM view_venues WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
#colonia
UNION 
SELECT id, CONCAT(d_colonia,', ',d_ciudad,', ',d_estado), CONCAT('venues.html?estado=',d_estado,'&ciudad=',d_ciudad,'&colonia=',d_colonia,'&tag=',CONCAT(d_colonia,', ',d_ciudad,', ',d_estado)) FROM view_venues WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0);



DROP VIEW IF EXISTS view_buscador;
CREATE VIEW view_buscador AS SELECT * FROM _view_buscador GROUP BY nombre;












#OBSOLETE
DROP VIEW IF EXISTS _view_buscador;
CREATE VIEW _view_buscador AS SELECT 
id,
nombre,
CONCAT('venue/',id,'-') as url
FROM venues
WHERE (pausado is null or pausado = 0) AND (bloqueado is null or bloqueado = 0)
UNION 
SELECT 
id,
CONCAT('venues en la colonia de ',colonia,' de la ciudad de ',ciudad,' (',getTotalVenues('colonia',colonia),')'),
CONCAT('venues.html?colonia=',colonia,'&nombre=',CONCAT('venues en ',colonia,' (',getTotalVenues('colonia',colonia),')')) as url
FROM venues
GROUP BY venues.colonia
UNION 
SELECT 
id,
CONCAT('venues en la ciudad de ',ciudad,' (',getTotalVenues('ciudad',ciudad),')'),
CONCAT('venues.html?ciudad=',ciudad,'&nombre=',CONCAT('venues en ',ciudad,' (',getTotalVenues('ciudad',ciudad),')')) as url
FROM venues
GROUP BY venues.ciudad


DROP VIEW IF EXISTS view_buscador;
CREATE VIEW view_buscador AS SELECT 
*
FROM _view_buscador
GROUP BY _view_buscador.url












# Obsolette #
DROP VIEW IF EXISTS view_buscador;
CREATE VIEW view_buscador AS SELECT  
id, 
nombre as criterial,
CONCAT(nombre,' en ',venues.colonia,', ',venues.ciudad) as nombre
FROM venues
WHERE (venues.pausado IS NULL OR venues.pausado = 0) AND (venues.pausado IS NULL OR venues.bloqueado = 0)
UNION 
SELECT 
id, 
venues.ciudad as criterial,
CONCAT(nombre,' en ',venues.colonia,', ',venues.ciudad)
FROM venues
WHERE (venues.pausado IS NULL OR venues.pausado = 0) AND (venues.pausado IS NULL OR venues.bloqueado = 0)
UNION 
SELECT 
id, 
venues.colonia as criterial,
CONCAT(nombre,' en ',venues.colonia,', ',venues.ciudad)
FROM venues
WHERE (venues.pausado IS NULL OR venues.pausado = 0) AND (venues.pausado IS NULL OR venues.bloqueado = 0)




