DROP VIEW IF EXISTS report_membresias;
CREATE VIEW report_membresias AS SELECT 
view_venues.id,
view_venues.nombre as Venue,
membresias.id as id_membresia,
membresias.nombre as Membresia,
DATE(MAX(transacciones.fecha)) AS fecha_de_pago,
DATE(MAX(transacciones.vencimiento)) AS fecha_de_renovacion,
view_venues.codigo_promocion AS cupon_aplicado,
transacciones.importe as monto_total,
view_venues.requiere_factura as factura,
view_venues.razon_social,
view_venues.rfc,
IF(transacciones.forma_pago=1,'CONEKTA','PAYPAL') as tipo_pago,
user.id AS ID_Usuario,
CONCAT(user.nombre,' ',user.apellido_paterno) as usuario,
user.email as correo,
view_venues.d_estado,
view_venues.d_ciudad,
view_venues.d_colonia,
view_venues.estado,
view_venues.ciudad,
view_venues.colonia
FROM view_venues
INNER JOIN user ON user.id = view_venues.user_id
INNER JOIN membresias ON membresias.id = view_venues.membresias_id
LEFT JOIN transacciones ON transacciones.venues_id = view_venues.id
GROUP BY view_venues.id