DROP VIEW IF EXISTS view_venues;
CREATE VIEW view_venues AS
SELECT 
venues.*,
venues_id,
estados.nombre as d_estado,
ciudades.nombre as d_ciudad,
colonias.nombre as d_colonia,
tipos_venue.nombre as tipoVenue,
MIN(precio(0,precio)) as precio_minimo,
MAX(precio(1,precio)) as precio_maximo,
MIN(precio(0,capacidad)) as capacidad_minimo,
MAX(precio(1,capacidad)) as capacidad_maximo
FROM 
venues_espacios
INNER JOIN venues ON venues.id = venues_id
INNER JOIN estados ON estados.id = venues.estado
INNER JOIN ciudades ON ciudades.id = venues.ciudad
LEFT JOIN colonias ON colonias.id = venues.colonia
LEFT JOIN tipos_venue ON tipos_venue.id = venues.tipos_venue_id
GROUP BY venues_id


##OBSOLETE 

DROP VIEW IF EXISTS view_venues;
CREATE VIEW view_venues AS
SELECT 
venues.*,
estados.nombre as d_estado,
ciudades.nombre as d_ciudad,
colonias.nombre as d_colonia,
precio(0,precio) as precio_minimo,
precio(1,precio) as precio_maximo,
precio(0,capacidad) as capacidad_minimo,
precio(1,capacidad) as capacidad_maximo
FROM venues
INNER JOIN (SELECT MIN(id) as id,precio,capacidad,venues_id FROM venues_espacios GROUP BY venues_id ) as venues_espacios ON venues_espacios.venues_id = venues.id
INNER JOIN estados ON estados.id = venues.estado
INNER JOIN ciudades ON ciudades.id = venues.ciudad
LEFT JOIN colonias ON colonias.id = venues.colonia