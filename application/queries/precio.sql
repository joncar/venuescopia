##PARAMS _tipo BOOLEAN 0 = Min, max = 0
##PARAMS _precio VARCHAR FORMAT: min-max
BEGIN

DECLARE Lat DECIMAL(11,2);
DECLARE Lng DECIMAL(11,2);

SELECT SUBSTRING_INDEX(_precio,'-',1) INTO Lat;
SELECT SUBSTRING_INDEX(_precio,'-',-1) INTO Lng;

return IF(_tipo=0,Lat,Lng);
END