    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php $this->load->view('includes/menu-top'); ?>
        </header>

        <aside class="left-sidebar">
            <?php $this->load->view('includes/menu-lateral'); ?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor"><?= empty($title) ? 'Escritorio' : $title ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?= base_url('panel') ?>">Inicio</a></li>
                            <li class="breadcrumb-item active"><?= empty($title) ? 'Escritorio' : $title ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <!-- .left-right-aside-column-->
                            <div class="contact-page-aside">
                                <?= empty($crud)?$this->load->view('includes/dashboard'):$this->load->view('cruds/'.$crud); ?>
                            </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php $this->load->view('includes/footer'); ?>

        </div>
    </div>