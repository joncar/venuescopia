<!doctype html>
<html lang="es">
	<title><?= empty($title) ? 'WEB' : $title ?></title>
  	<meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
	<meta name="description" content="<?= empty($descr) ?'': strip_tags($descr) ?>" /> 	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	
	<?php if(!empty($foto)): ?>
		<meta property="og:url"                content="<?= $url ?>" />
		<meta property="og:type"               content="article" />
		<meta property="og:title"              content="<?= $title ?>" />
		<meta property="og:description"        content="¡Mira este Venue que encontré en Venuescopia!" />
		<meta property="og:image"              content="<?= $foto ?>" />
	<?php endif ?>

	<link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
	<script>window.wURL = window.URL;</script>
	<link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>	
	<script>var URL = '<?= base_url() ?>';</script>

	<!-- Theme CSS --->
	<link href="<?= base_url() ?>theme/assets/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/app.css" rel="stylesheet">
	<!-- Main -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/css/ion.rangeSlider.min.css"/>
	<link href="<?= base_url() ?>theme/assets/css/main.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/css/responsive.css" rel="stylesheet">
	<!-- Font -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>theme/assets/font/avenir/style.css"/>
	<!-- Fonts/Icons -->
	<link href="<?= base_url() ?>theme/assets/plugins/font-awesome/css/all.css" rel="stylesheet">
	<link href="<?= base_url() ?>theme/assets/plugins/themify/themify-icons.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900,900i" rel="stylesheet"> 
	<script src="<?= base_url() ?>theme/assets/plugins/jquery.min.js"></script>
	<script src="<?= base_url() ?>js/frame.js?v1.11"></script>
	<link href="<?= base_url('assets/grocery_crud/css/jquery_plugins/chosen/chosen.css') ?>" rel="stylesheet" type="text/css">	
</head>

<body data-preloader="2">		
	<?php $this->load->view('es/includes/menu',array(),FALSE,'paginas'); ?>
	<?php $module = empty($module)?'':$module; $this->load->view($view,array(),FALSE,$module);  ?>

	<!-- Footer -->
    <?php $this->load->view('es/includes/footer.php',array(),FALSE,'paginas');?>
    <?php $this->load->view('es/includes/modales.php',array(),FALSE,'paginas');?>
    <!-- Librerias -->
    <?php $this->load->view('es/includes/librerias.php',array(),FALSE,'paginas');?>

    <!-- Modal Inicio -->
    <script>
    $(window).load(function(){
       setTimeout(function(){
           $('#myModal').modal('show');
       }, 7000);
    });
    </script>
    <!-- Modal Inicio -->

    <script>
      $('.notification-icon').click(function(e) {
		  var that = $(this);
		  that.toggleClass("active "+ that.attr("data-active-icon") + " " + that.attr("data-icon"));
		});
    </script>
    <script src="<?= base_url('js/jquery.mask.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.0/js/ion.rangeSlider.min.js"></script>
</body>
</html>