<!Doctype html>
<html lang="es">
	<head>
            <title><?= empty($title)?'Administrador':$title.'' ?></title>
            <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">            
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
            <meta name="description" content="<?= empty($keywords) ?'': strip_tags($description) ?>" />    
            <script>window.wURL = window.URL;</script>
            <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
            <link href="<?= base_url() ?>assets/consola/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <link href="<?= base_url() ?>assets/consola/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
            <link href="<?= base_url() ?>assets/consola/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
            <script src="<?= base_url() ?>theme/assets/plugins/jquery.min.js"></script>
            <script src="https://cdn.jsdelivr.net/jquery.migrate/1.4.1/jquery-migrate.min.js"></script>
            <!-- Gropcery crud --->
            <?php 
            if(!empty($css_files)):
            foreach($css_files as $file): ?>
            <link type="text/css" rel="stylesheet" href="<?= $file ?>" />
            <?php endforeach; ?>
            <?php endif; ?>

            <?php 
            if(!empty($js_files)): ?>
            <?php foreach($js_files as $file): ?>
            <script src="<?= $file ?>"></script>
            <?php endforeach; ?>                
            <?php endif; ?>
            
            <link href="<?= base_url() ?>assets/consola/assets/plugins/c3-master/c3.min.css" rel="stylesheet">
            <!-- Custom CSS -->
            <link href="<?= base_url() ?>assets/consola/css/style.css" rel="stylesheet">
            <!-- You can change the theme colors from here -->
            <link href="<?= base_url() ?>assets/consola/css/colors/blue.css" id="theme" rel="stylesheet">
            <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
            <!--<script src='//maps.googleapis.com/maps/api/js?key=AIzaSyA0s642F0yJ7s6dAjT0u0f4HD7xMs2TetU&libraries=places'></script>
            <script src='<?= base_url() ?>assets/grocery_crud/js/jquery_plugins/config/map.js'></script>-->
    </head>  
    <body>
     <?php $this->load->view($view); ?> 
     
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url() ?>assets/consola/assets/plugins/popper/popper.min.js"></script>
    <script src="<?= base_url() ?>assets/consola/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url() ?>assets/consola/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?= base_url() ?>assets/consola/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url() ?>assets/consola/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= base_url() ?>assets/consola/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?= base_url() ?>assets/consola/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url() ?>assets/consola/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!-- chartist chart -->
    <!--<script src="<?= base_url() ?>assets/consola/assets/plugins/chartist-js/dist/chartist.min.js"></script> Da error de js en la consola 
    <script src="<?= base_url() ?>assets/consola/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>--->
    <!--c3 JavaScript -->
    <script src="<?= base_url() ?>assets/consola/assets/plugins/d3/d3.min.js"></script>
    <script src="<?= base_url() ?>assets/consola/assets/plugins/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <script src="<?= base_url() ?>assets/consola/js/dashboard1.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?= base_url() ?>assets/consola/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
    
    </body>             
</html>
