<div class="section-fullscreen bg-image parallax" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-login.jpg)">
    <div class="row align-items-center position-middle text-center">
      <div class="col-12 col-sm-12 text-center">
        <div class="margin-bottom-50"><img src="<?= base_url() ?>theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" style="width: 20%;"></div>
        <h2 class="text-uppercase titulos-general font-montserrat text-responsive" style="line-height: 1.2; text-align:center">
          ¡UPS!<br>Lo sentimos, pero esta página no está disponible.
        </h2>
        <a class="button button-md margin-top-30" href="http://www.bluepixel.mx/venuescopia" id="btn-top-login2">Volver a venuescopia.com.mx</a>
      </div>
    </div><!-- end row -->
</div>