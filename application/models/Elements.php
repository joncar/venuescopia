<?php 
class Elements extends CI_Model{
	function __construct(){
		parent::__construct();
    $this->ajustes = $this->db->get('ajustes')->row();
	}

	function membresias($where = array()){
		$data = $this->db->get_where('membresias',$where);		
		return $data;
	}

	function amenidades($where = array()){
		$data = $this->db->get_where('amenidades',$where);
		foreach($data->result() as $n=>$d){
			$data->row($n)->icono = base_url('img/amenidades/'.$d->icono);
		}
		return $data;
	}

	function venues_fotos($where = array()){
      $data = $this->db->get_where('venues_fotos',$where);
      foreach($data->result() as $n=>$v){
            if(!empty($v->foto)){
              $data->row($n)->foto = file_exists('img/venues/'.$v->foto)?base_url('img/venues/'.$v->foto):base_url('img/2100x1406.png');
            }else{
              $data->row($n)->foto = base_url('img/2100x1406.png');
            }
      }      
      return $data;
	}

	function venues_espacios($where = array()){
      $data = $this->db->get_where('venues_espacios',$where);
      foreach($data->result() as $n=>$v){
              $data->row($n)->fotos = $this->venues_fotos(array('venues_espacios_id'=>$v->id));
              $precio = explode('-',$v->precio);
              $data->row($n)->precio = '$'.@number_format($precio[0],0,'.',',').'-$'.@number_format($precio[1],0,'.',',');
              $capacidad = explode('-',$v->capacidad);
              $data->row($n)->capacidad = @number_format($capacidad[0],0,'.',',').'-'.@number_format($capacidad[1],0,'.',',');
              if($data->row($n)->fotos->num_rows()>0){
                  $data->row($n)->foto = $data->row($n)->fotos->row()->foto;
              }
      }
      return $data;
	}

	function venues($where = array(),$filtrar = true){	

		if($filtrar){
			$where['pausado'] = 0;
			$where['bloqueado'] = 0;
		} 
    $this->db->select('venues.*,
      venues_id,
      MIN(precio(0,precio)) as precio_minimo,
      MAX(precio(1,precio)) as precio_maximo,
      MIN(precio(0,capacidad)) as capacidad_minimo,
      MAX(precio(1,capacidad)) as capacidad_maximo',FALSE);
      $this->db->join('venues','venues.id = venues_espacios.venues_id','INNER');
      $this->db->group_by('venues_id');
    $data = $this->db->get_where('venues_espacios',$where);
		foreach($data->result() as $n=>$v){
      $v->precio_maximo = $v->precio_maximo==$this->ajustes->precio_maximo?number_format($v->precio_maximo,0,'.',',').'+':number_format($v->precio_maximo,0,'.',',');
      $v->capacidad_maximo = $v->capacidad_maximo==$this->ajustes->capacidad_maxima?number_format($v->capacidad_maximo,0,'.',',').'+':number_format($v->capacidad_maximo,0,'.',',');
			$data->row($n)->espacios = $this->venues_espacios(array('venues_id'=>$v->id));
			$data->row($n)->precio = '$'.number_format($v->precio_minimo,0,'.',',').' - $'.$v->precio_maximo;
			$data->row($n)->capacidad = number_format($v->capacidad_minimo,0,'.',',').' - '.$v->capacidad_maximo;
			$data->row($n)->amenidades = @explode(',',$data->row($n)->espacios->row()->amenidades);
			$data->row($n)->foto = @$data->row($n)->espacios->num_rows()>0?@$data->row($n)->espacios->row()->fotos->row()->foto:'';
			$data->row($n)->link = base_url().'venue/'.toUrl($v->id.'-'.$v->nombre);
			$data->row($n)->mensajes = $this->db->get_where('mensajes',array('venues_id'=>$v->id));
			if($this->user->log){
				$data->row($n)->fav = $this->db->get_where('favoritos',array('user_id'=>$this->user->id,'venues_id'=>$v->id))->num_rows()==0?false:true;
			}else{
				$data->row($n)->fav = false;
			}

      if($data->row($n)->foto == base_url('img/venues/')){
        $data->row($n)->foto = base_url('img/2100x1406.png');
      }
		}
		return $data;
	}

	function categoria_venue($where = array()){

		$data = $this->db->get_where('categoria_venue',$where);
		foreach($data->result() as $n=>$v){
      $this->db->order_by('venues.fecha_aprobacion','DESC');
			$data->row($n)->venues = $this->venues(array('categoria_venue_id'=>$v->id));			
		}
		return $data;
	}

	function tips($where = array()){

		$data = $this->db->get_where('tips',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->venues = $this->venues(array('categoria_venue_id'=>$v->id));
		}
		return $data;
	}

	function transacciones($where = array()){

		$data = $this->db->get_where('transacciones',$where);
		foreach($data->result() as $n=>$v){
			$data->row($n)->pdf = !empty($v->pdf)?base_url('facturas/'.$v->pdf):'';
		}
		return $data;
	}

  function updateCard($cliente,$token = ''){
    $cliente = $this->db->get_where('user',array('id'=>$cliente))->row();    
    require_once(APPPATH.'libraries/conekta/Conekta.php');
    \Conekta\Conekta::setLocale('es');
    try {
        //Creamos cliente
        \Conekta\Conekta::setApiKey("key_fmrhZD369Uzt7WdxmwyxdA");
        if(empty($cliente->token_conekta)){
            $customer = \Conekta\Customer::create(
              array(
                'name'  => $cliente->nombre,
                'email' => $cliente->email,                
                'payment_sources' => array(array(
                    'token_id' => $token,
                    'type' => "card"
                ))
              )
            );                
            $this->db->update('user',array('token_conekta'=>$customer->id),array('id'=>$cliente->id));
        }else{
            $customer = $cliente->token_conekta;
            $customer = \Conekta\Customer::find($customer);                                
            if(!empty($token)){
              if(count($customer->payment_sources)>0){
                  $customer->payment_sources[0]->delete();
              }                                
              $customer->createPaymentSource(array('token_id' => $token,'type' => "card"));                                
          }
        }                              
      return (object)array('success'=>true,'msj'=>get_instance()->success('Pago realizado con éxito'));
    }catch (\Conekta\Handler $error) {
      //Normal object methods            
      return (object)array('success'=>false,'msj'=>get_instance()->error('Ocurrio un error con su tarjeta MSJ:'.$error->getMessage()));  
    }   
  }

	function retryPay($transaccion,$cliente,$token = '',$charge = true){
		$transaccion = $this->db->get_where('transacciones',array('id'=>$transaccion));
		$cliente = $this->db->get_where('user',array('id'=>$cliente))->row();
        if($transaccion->num_rows()>0){
            $venue = $this->elements->venues(array('venues.id'=>$transaccion->row()->venues_id),FALSE);
            if($venue->num_rows()>0){
                $transaccion = $transaccion->row();                    
                if($transaccion->estado_pago==2){
                    return (object)array('success'=>false,'msj'=>get_instance()->error('La transacción ya fue aprobada'));
                }
                $venue = $venue->row();
                $primary = $venue->id;
                $membresia = $this->db->get_where('membresias',array('id'=>$venue->membresias_id))->row();                
                $pago = $transaccion->id;
                $post = $_POST;
                if($membresia->precio==0){
                	return (object)array('success'=>true,'msj'=>get_instance()->success('Pago realizado con éxito pero no se ha cobrado ya que el costo posee un valor 0'));
                }
                require_once(APPPATH.'libraries/conekta/Conekta.php');
                try {
                    //Creamos cliente
                    $this->load->config('conekta');
                    \Conekta\Conekta::setApiKey($this->config->item('conekta_private'));                
                    if(empty($cliente->token_conekta)){
                        $customer = \Conekta\Customer::create(
                          array(
                            'name'  => $cliente->nombre,
                            'email' => $cliente->email,                
                            'payment_sources' => array(array(
                                'token_id' => $token,
                                'type' => "card"
                            ))
                          )
                        );                
                        $this->db->update('user',array('token_conekta'=>$customer->id),array('id'=>$cliente->id));
                    }else{
                        $customer = $cliente->token_conekta;
                        $customer = \Conekta\Customer::find($customer);                                
                        if(!empty($token)){
	                        if(count($customer->payment_sources)>0){
	                            $customer->payment_sources[0]->delete();
	                        }                                
	                        $customer->createPaymentSource(array('token_id' => $token,'type' => "card"));                                
                    	}
                    }

                    //Crear primer cargo
                    if($charge){
                      \Conekta\Order::create(array(
                        'currency' => 'MXN',
                        'customer_info' => array(
                          'customer_id' => $customer->id
                        ),
                        'line_items' => array(
                          array(
                            'name' => 'Cargo por publicación o renovación de venue',
                            'unit_price' => $membresia->precio*100,
                            'quantity' => 1
                          )
                        ),
                        'charges' => array(
                          array(
                            'payment_method' => array(
                              'type' => 'default'
                            )
                          )
                        )
                      ));
                    }
                    $this->db->update('venues',array('bloqueado'=>0),array('id'=>$primary));
                    $this->db->update('transacciones',array('estado_pago'=>2),array('id'=>$pago));                    
                    $transaccion = $this->db->get_where('transacciones',array('id'=>$pago))->row();
                    $post = array();
                    $post['cliente'] = $cliente->nombre.' '.$cliente->apellido_paterno;
                    $post['importe'] = '$'.$transaccion->importe.'MXN';
                    $post['fecha'] = date("d/m/Y");
                    $post['renovacion'] = date("d/m/Y",strtotime('+'.$membresia->dias_renovacion.' days'));
                    if($venue->requiere_factura==1){ 	                    
                    	$post['razon_social'] = $venue->razon_social;
                    	$post['rfc'] = $venue->rfc;
	                }
	                else{
	                    $post['razon_social'] = '';
	                    $post['rfc'] = '';
	                }
	                $post['promo'] = '';
                  $post['medio_pago'] = 'CONEKTA';
                  $post['requiere_factura'] = $venue->requiere_factura==1?'SI':'NO';                
                  if($charge){
                     get_instance()->enviarcorreo((object)$post,21,$this->ajustes->email_facturacion);                                   
	                   get_instance()->enviarcorreo((object)$post,18,$cliente->email);              
                  }
                  return (object)array('success'=>true,'msj'=>get_instance()->success('Pago realizado con éxito'));
                }catch (\Conekta\Handler $error) {
                  //Normal object methods
                  $this->db->update('transacciones',array('estado_pago'=>-1),array('id'=>$pago));
                  get_instance()->enviarcorreo((object)array('mensajeconekta'=>$error->getMessage()),17,$this->user->email);
                  return (object)array('success'=>false,'msj'=>get_instance()->error('Ocurrio un error con su tarjeta MSJ:'.$error->getMessage()));  
                }        
            }else{
                return (object)array('success'=>false,'msj'=>get_instance()->error('Venue no encontrado'));
            }
        }else{
            return (object)array('success'=>false,'msj'=>get_instance()->error('Transacción no permitida'));
        }
	}

	function cancelPaypal($id){
		$api_request = 'USER=' . urlencode( 'venuescopia-facilitator_api1.gmail.com' )
                 .  '&PWD=' . urlencode( 'DMH8ZCA7CGZFX5A2' )
                 .  '&SIGNATURE=' . urlencode( 'AP4N5UUZZ-qWEbS6G8I3PN3ffOnEAmCObwDoFy9ONh3gVI6Xauf3pwcD' )
                 .  '&VERSION=76.0'
                 .  '&METHOD=ManageRecurringPaymentsProfileStatus'
                 .  '&PROFILEID=' . urlencode($id)
                 .  '&ACTION=' . urlencode( 'Cancel' )
                 .  '&NOTE=' . urlencode('Actualización de pago');

     $ch = curl_init();
     curl_setopt( $ch, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp' ); // For live transactions, change to 'https://api-3t.paypal.com/nvp'
     curl_setopt( $ch, CURLOPT_VERBOSE, 1 );

     // Uncomment these to turn off server and peer verification
     // curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
     // curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
     curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
     curl_setopt( $ch, CURLOPT_POST, 1 );

     // Set the API parameters for this transaction
     curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_request );

     // Request response from PayPal
     $response = curl_exec( $ch );

     // If no response was received from PayPal there is no point parsing the response
     if( ! $response )
         die( 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')' );

     curl_close( $ch );

     // An associative array is more usable than a parameter string
     parse_str( $response, $parsed_response );

     return $parsed_response;
	}

  function getPayPalData($venue){
    $post = $this->venues(array('venues.id'=>$venue),FALSE);
    $post = (array)$post->row();
    $membresia = $this->db->get_where('membresias',array('id'=>$post['membresias_id']))->row();
    $promomsj = '';

    switch($membresia->dias_renovacion){
        case '30':
            $interval = 'M';
            $frec = 1;
        break;
        case '90':
            $interval = 'M';
            $frec = 3;
        break;
        case '365':
            $interval = 'Y';
            $frec = 1;
        break;
    }

    return array('id'=>$venue,'post'=>$post,'interval'=>$interval,'frec'=>$frec,'precio'=>$membresia->precio);

  }

  function lastTransaccion($venue){
    return $this->db->get_where('transacciones',array('venues_id'=>$venue,'estado_pago'=>2));
  }

  function cancelPaypalSubscription($token){
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
        'USER' => 'andres_api1.venuescopia.com',  //Your API User
        'PWD' => 'SH9D7PP3BLVHD5QL',  //Your API Password
        'SIGNATURE' => 'ARXTGAav9zDU.Ftj-wpS9pSkKiBaABTfvV6eaod4shliwIrMEld6eoo1',   //Your API Signature
        'VERSION' => '108',
        'METHOD' => 'ManageRecurringPaymentsProfileStatus',
        'PROFILEID' => $token,         //here add your profile id                      
        'ACTION'    => 'Cancel' //this can be selected in these default paypal variables (Suspend, Cancel, Reactivate)
    )));
    $response =    curl_exec($curl);
    curl_close($curl);
    $nvp = array();
    if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
        foreach ($matches['name'] as $offset => $name) {
            $nvp[$name] = urldecode($matches['value'][$offset]);
        }
    }
    //printf("<pre>%s</pre>",print_r($nvp, true));
    return $nvp['ACK']=='Success'?true:false;
  }

	
}