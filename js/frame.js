function emergente(data,xs,ys,boton,header){


	var x = (xs==undefined)?(window.innerWidth/2)-325:xs;
	var y = (ys==undefined)?(window.innerHeight/2):ys;
	var b = (boton==undefined || boton)?true:false;
	var h = (header==undefined)?'Mensaje':header;
	if($(".modal").html()==undefined){
	$('body,html').animate({scrollTop: 0}, 800);
	var str = '';
            var str = '<!-- Modal -->'+
            '<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog">'+
            '<div class="modal-content">'+
            '<div class="modal-body">'+
            data+
            '</div>'+
            '<div class="modal-footer">'+
            '<button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>'+
            '</div>'+
            '</div><!-- /.modal-content -->'+
            '</div><!-- /.modal-dialog -->'+
            '</div><!-- /.modal -->';
            $("body").append(str);
            $("#myModal").modal("toggle");
	}
	else
	{
		$(".modal-body").html(data);
                    if($("#myModal").css('display')=='none')
                        $("#myModal").modal("toggle");
	}
}

function autocomplete(inp,url,minLength,form) {  
  this.inp = inp;
  this.inp.attr('autocomplete','off');
  this.url = url;
  this.ajax = undefined;
  this.val = '';  
  this.list = inp.parent().find('ul');
  this.form = typeof form ==='undefined'?null:form;
  this.minLength = typeof minLength ==='undefined'?0:minLength;
  this.results = [];
  this.selected = -1;
  this.events = function(){
    var l = this;
    this.inp.on('keypress',function(e){           
      switch(e.which){
        case 13: //Enter
          l.onEnter(e);
        break;
      }
    });
    this.inp.on('keyup',function(e){         
      switch(e.which){        
        case 38: //UP
          e.preventDefault();
          if(l.results.length==0){
            l.search(e);
          }
          else{          
            l.selected = l.selected>0?l.selected-1:0;
            l.selected = typeof(l.results[l.selected])=='undefined'?0:l.selected;            
            var elegido = l.results[l.selected];
            l.inp.val(l.results[l.selected].text);
            l.list.find('li').removeClass('active');
            $(l.list.find('li')[l.selected]).addClass('active');  
          }        
        break;
        case 40: //Down
          e.preventDefault();
          if(l.results.length==0){
            l.search();
          }
          else{          
            l.selected = l.selected<l.results.length?l.selected+1:0;
            l.selected = typeof(l.results[l.selected])=='undefined'?0:l.selected;            
            var elegido = l.results[l.selected];
            l.inp.val(l.results[l.selected].text);
            l.list.find('li').removeClass('active');
            $(l.list.find('li')[l.selected]).addClass('active');  
          }        
        break;
        case 13: //ENTER          
          l.inp.blur();
        break;
        default:
          if($(this).val()!='' && $(this).val().length>=l.minLength){
            l.list.show();
            l.val = $(this).val();
            l.search();            
          }else{
            l.list.hide();
            l.val = '';
          }
        break;
      }         
    });

    this.inp.on('focus',function(e){      
      if($(this).val()!='' && $(this).val().length>=l.minLength){
        l.list.show();
        l.val = $(this).val();
        $(this).select();
      }
    });

    $(document).on('click',function(e){        
      if(e.target!=l.inp[0]){
          l.list.hide();
          l.val = '';
      }
    });
    this.inp.on('blur',function(e){        
      setTimeout(function(){l.list.hide();},600);
    });
  }

  this.onEnter = function(e){    
    if(this.selected!=-1){
      e.preventDefault();      
      this.inp.blur();
      this.list.hide();
      var lin = this.results[this.selected].link;      
      if(lin.search('javascript')==-1){        
        document.location.href=lin;
      }else{        
        $(document).find('a[href="'+lin+'"]')[0].click();
      }
      return;
    }                    
    if(this.list.length==1 && typeof this.results[0].link != 'undefined'){
      e.preventDefault();
      this.inp.blur();
      this.list.hide();
      document.location.href=this.results[0].link;
      return;
    }
  }


  this.validateSelected = function(){
    if(this.results.length>0){
      var escrito = this.inp.val();
      escrito = escrito.toLowerCase();
      for(var i in this.results){
        var r = this.results[i].text;
        r = r.toLowerCase();          
        if(r==escrito){
          this.selected = parseInt(i);
        }
      }
    }
  }
  
  this._search = function(){
      var l = this;
      l.results = [];
      l.selected = -1;
       if(typeof(this.ajax)!='undefined'){
              this.ajax.abort();
              this.ajax = undefined;
            }

            if(!this.form){
                    this.ajax = $.post(URL+this.url,{
                      'q':this.val
                    },function(data){
                      data = JSON.parse(data);
                      l.results = data;
                      l.validateSelected();
                      if(data.length>0){
                        var str = '';
                        for(var i in data){
                          str+= '<li><a href="'+data[i].link+'">'+data[i].text+'</a></li>';
                        }
                        l.list.html(str);
                      }else{
                        l.list.html('<li><a href="#">No se encontraron resultados para mostrar</a></li>');
                      }
                    });
            }else{
                var form = new FormData(this.form);
                form.append('q',this.val);
                this.ajax = remoteConnection(this.url,form,function(data){
                      data = JSON.parse(data);
                      l.results = data;
                      l.validateSelected();
                      if(data.length>0){
                        var str = '';
                        for(var i in data){
                          str+= '<li><a href="'+data[i].link+'">'+data[i].text+'</a></li>';
                        }
                        l.list.html(str);
                      }else{
                        l.list.html('<li><a href="#">No se encontraron resultados para mostrar</a></li>');
                      }
                });
            }
  }

  this.search = function(){
    var l = this;
    this.list.html('<li><a href="#">Buscando...</a></li>');
    if(this.val!=''){
       this._search();
    }else{
      this.list.hide();
    }
  }

  this.events();
}

function remoteConnection(url,data,callback){        
    return $.ajax({
        url: URL+url,
        data: data,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:callback
    });
};

var onajax = false;
function insertar(url,datos,resultId,callback,callbackError){    
    if(!onajax){
      onajax = true;
      $(document).find(resultId).removeClass('alert alert-danger').html('');
      //$(document).find(resultId).addClass('alert alert-info').html('Cargando, por favor espere');
      var submit = $(datos).find('button[type="submit"]');
      var beforeTag = submit.html();
      submit.html('<b>Enviando...</b>');
      $("button[type=submit]").attr('disabled',true);
      var uri = url.replace('insert','insert_validation');
      uri = uri.replace('update','update_validation');          
      if(!(datos instanceof FormData) && (datos instanceof HTMLFormElement)){
          datos = new FormData(datos);
      }else if(!(datos instanceof FormData)){
          var d = new FormData();
          for(var i in datos){
              d.append(i,datos[i]);
          }
          datos = d;
          console.log(datos);
      }


      remoteConnection(uri,datos,function(data){
          $("button[type=submit]").attr('disabled',false);
          onajax = false;
          data = $(data).text();
          data = JSON.parse(data);          
          if(data.success){
            remoteConnection(url,datos,function(data){
              submit.html(beforeTag);
              data = $(data).text();              
              data = JSON.parse(data);
              if(typeof(callback)=='function'){
                  callback(data);
              }else{
                  $(document).find(resultId).removeClass('alert alert-info');
                  $(document).find(resultId).addClass('alert alert-success').html(data.success_message);
              }
            });
          }else{
            submit.html(beforeTag);
            $(document).find(resultId).removeClass('alert alert-info');
            $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
            if(typeof(callbackError)!=='undefined'){
              callbackError(data);
            }else{
              $(document).find(resultId).removeClass('alert alert-info');
              $(document).find(resultId).addClass('alert alert-danger').html(data.error_message);
            }
          }
      });
    }
}

function sendForm(form,divResponseId,callback){
    divResponseId = typeof(divResponseId)==='undefined'?'#response':divResponseId;
    var url = $(form).attr('action');
    var response = $(document).find(divResponseId);
    var data = new FormData(form);
    //response.html('<div class="alert alert-info">Comunicandose con el servidor por favor espere</div>');
    var submit = $(form).find('button[type="submit"]');
    var beforeTag = submit.html();
    submit.html('<b>Enviando...</b>');
    remoteConnection(url,data,function(data){
        submit.html(beforeTag);
        if(typeof(callback)==='undefined'){
            if(response!==undefined){
                response.html(data);                
            }else{
                console.log(response);
            }
        }else{
            callback(data);
        }
    });
    return false;
}