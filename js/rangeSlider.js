var rangePrecio = undefined,
    rangeCapacidad = undefined,
    _rangePrecio = undefined,
    _rangeCapacidad = undefined,
    el = undefined;
var rangePrecioArray = [];
for(var i = 0;i<=1500000;i+=5000){
  rangePrecioArray.push(i);
}

var rangeCapacidadArray = [];
for(var i = 0;i<=10000;i+=10){
  rangeCapacidadArray.push(i);
}
$(document).on('ready',function(){
   _rangePrecio = $(".js-range-slider").ionRangeSlider({
      onChange: function (data) {          
          eventChangePrecio(data);
      },
      onUpdate:function(data){
          eventChangePrecio(data);
      }
   });

   rangePrecio = $(".js-range-slider").data("ionRangeSlider");

   _rangeCapacidad = $(".js-range-sliderCapacidad").ionRangeSlider({
      onChange: function (data) {          
          eventChangeCapacidad(data);
      },
      onUpdate: function (data) {
          eventChangeCapacidad(data);
      }
   });

   rangeCapacidad = $(".js-range-sliderCapacidad").data("ionRangeSlider");

   $(document).on('change','.lowDollarAmountCapacidad',function(){
       rangeCapacidad.update({
         from:parseInt($(this).val())
       });
   });
   $(document).on('change','.highDollarAmountCapacidad',function(){
      rangeCapacidad.update({
         to:parseInt($(this).val())
       });
   });

   $(document).on('change','.lowDollarAmountPrecio',function(){
     rangePrecio.update({
         from:parseInt($(this).val())
       });
   });
   $(document).on('change','.highDollarAmountPrecio',function(){
     rangePrecio.update({
         to:parseInt($(this).val())
       });
   });

   $(document).on('focus','.lowDollarAmountCapacidad',function(){
       $(this).val(revertFormat($(this).val()));
       $(this).select();
   });
   $(document).on('focus','.highDollarAmountCapacidad',function(){
       $(this).val(revertFormat($(this).val()));
       $(this).select();
   });

   $(document).on('focus','.lowDollarAmountPrecio',function(){
       $(this).val(revertFormat($(this).val()));
       $(this).select();
   });
   $(document).on('focus','.highDollarAmountPrecio',function(){
       $(this).val(revertFormat($(this).val()));
       $(this).select();
   });

});

function eventChangePrecio(data){  
    var hasta = data.to;
    var desde = data.from;
    hasta = format(hasta);
    desde = format(desde);
    if(data.to == data.max){
      hasta = hasta+'+';
    }         
    $(data.input).parent().find('.lowDollarAmountPrecio').val(desde);
    $(data.input).parent().find('.highDollarAmountPrecio').val(hasta);          
    el.find('input.precio').val(data.from+'-'+data.to);
    type = el.find('.preciotag').data('notype')?'':'';
    el.find('.preciotag').html('$'+desde+'-$'+hasta+type);
    el.find('.preciotag').val('$'+desde+'-$'+hasta+type);
    //console.log(el.find('.preciotag.preciotag'));
    if(el.find(".precio_min").length>0){
      el.find(".precio_min").val(data.from);
      el.find(".precio_max").val(data.to);
    }
}

function eventChangeCapacidad(data){
  var hasta = data.to;
  var desde = data.from;
  hasta = format(hasta);
  desde = format(desde);
  if(data.to == data.max){
    hasta = hasta+'+';
  }        
  $(data.input).parent().find('.lowDollarAmountCapacidad').val(desde);
  $(data.input).parent().find('.highDollarAmountCapacidad').val(hasta);          
  el.find('input.capacidad').val(data.from+'-'+data.to);
  el.find('.capacidadtag').html(desde+'-'+hasta+'');
  el.find('.capacidadtag').val(desde+'-'+hasta+'');
  if(el.find(".precio_min").length>0){
    el.find(".capacidad_min").val(data.from);
    el.find(".capacidad_max").val(data.to);
  }
}

function precio(ele){  
  el = ele;
  var tag = ele.find(".preciotag"),from,to;
  from = parseInt($('.js-range-slider').data('from'));
    to = parseInt($('.js-range-slider').data('to'));
  if(tag.length>0 && tag.val()!=''){
    tag = tag.val();
    tag = tag.split('-');
    if(tag.length==2){
      from = revertFormat(tag[0]);
      to = revertFormat(tag[1]);
    }
    from = from.replace('$','');
    to = to.replace('$','');
  }else if(tag.length>0 && tag.html()!=''){
    tag = tag.html();
    tag = tag.split('-');
    if(tag.length==2){
      from = revertFormat(tag[0]);
      to = revertFormat(tag[1]);
      from = from.replace('$','');
      to = to.replace('$','');
    }
    
  }
  
  rangePrecio.update({
         from:from,
         to:to
  });
  $('#filtro-precio').modal('toggle');
  $('#filtro-precio').on('shown.bs.modal',function(){
    $('.lowDollarAmountPrecio').focus();
  });  
}

function capacidad(ele){  
  el = ele;
  var tag = ele.find(".capacidadtag"),from,to;
  from = parseInt($('.js-range-slider').data('from'));
    to = parseInt($('.js-range-slider').data('to'));
  if(tag.length>0 && tag.val()!=''){
    tag = tag.val();
    tag = tag.split('-');
    if(tag.length==2){
      from = revertFormat(tag[0]);
      to = revertFormat(tag[1]);
    }
  }else if(tag.length>0 && tag.html()!=''){
    tag = tag.html();
    tag = tag.split('-');
    if(tag.length==2){
      from = revertFormat(tag[0]);
      to = revertFormat(tag[1]);
    }
  }

  rangeCapacidad.update({
         from:from,
         to:to
  });
  $('#filtro-capacidad').modal('toggle');  
  $('#filtro-capacidad').on('shown.bs.modal',function(){  
    $('.lowDollarAmountCapacidad').focus();
  });
}

function format(input)
{
    var f = numeral(input).format('0,0');
    return f;
}

function cleanRanges(){
  rangePrecio.reset();
  rangeCapacidad.reset();
  $(".preciotag").html('<b>Precio</b>');
  $(".capacidadtag").html('<b>Capacidad</b>');
}

function revertFormat(number){
  number = number.replace(/,/g,'');
  return number;
}