<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu-interno.php');?>



    <!-- Products section -->
      <div class="container margin-registro-top">
        <div class="row">

          <!-- Products -->
          <div class="col-12 col-lg-12 container-resultas margin-top-20">
            <div class="row align-items-center no-padding margin-buscador-top">

              <div class="col-12 col-lg-9">
                <form class="form-inline" autocomplete="off" action="/action_page.php">
                  <div class="autocomplete" id="buscador-header" style="border: #000000 solid 1px;">
                    <input id="myInput" type="text" name="myCountry" placeholder="Ciudad, colonia o venue">
                  </div>
                  <button class="btn btn-outline-success btn-buscar-header font-montserrat" type="submit">Buscar</button>
                </form>
              </div>

              <div class="col-12 col-lg-3 d-none d-sm-block" style="margin-top: 15px; padding-right: 0px; border: 0;">
                  <select class="custom-select">
                    <option value="1">Destacadas</option>
                    <option value="1">Populares</option>
                    <option value="2">Recientes</option>
                    <option value="2">Mis Favoritos</option>
                    <option value="2">Mayor Precio</option>
                    <option value="2">Menor Precio</option>
                  </select>
              </div>
            </div>

            <!-- Sidebar Movil -->
            <div class="d-block d-sm-none filtros-movil">
              <div class="shop-sidebar-box">
                <h5 class="margin-bottom-10 text-uppercase">Filtrar por:</h5>
              </div>
              <div class="row">
                <div class="col-4">
                    <button class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Precio</b></button>
                </div>
                <div class="col-4">
                    <button class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Capacidad</b></button>
                </div>
                <div class="col-4">
                    <select class="custom-select" style="width: 100%;" id="propiedad-lateral">
                      <option value="1" style="font-size: 8px;">Jardín</option>
                      <option value="1">Hotel</option>
                      <option value="2">Restaurante</option>
                      <option value="2">Otro</option>
                    </select>
                </div>
              </div>
            </div>
            <!-- Sidebar Movil -->

            <div class="row">

              <div class="col-12 title-resultas text-responsive text-uppercase" style="padding-left: 5px;" id="margin-movil-titulo-resultas">Condesa, ciudad de méxico</div>

              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>

              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>

              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>

              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>

              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>

              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>


              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>

              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>


              <!-- Product box -->
              <div class="col-6 col-lg-4 product-single margin-bottom-50" id="product-single">
                <div class="product-single-img">
                  <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                    <div class="owl-stage-outer owl-height" style="height: 158px;">
                      <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                        <div class="owl-item active">
                          <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                        <div class="owl-item">
                          <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                        </div>
                      </div>
                    </div>

                    <div class="owl-nav">
                      <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                      <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                    </div>
                  </div>
                </div>
                <!-- end product-img -->

                <div class="favoritos-home2" style="text-align: right;">
                    <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                </div>

                <div class="titulo-venue-home">
                  <?php include('includes/venues/precios-venues.php');?>
                </div>
              </div>


            </div>

            <!-- Pagination -->
            <nav>
              <ul class="pagination justify-content-center margin-top-50 margin-bottom-50">
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
              </ul>
            </nav>
          </div>

          <!-- Sidebar Desktop -->
          <div class="shop-sidebar sidebar-filtros d-none d-sm-block">
            <div class="shop-sidebar-box">
              <h5 class="text-uppercase title-filtrar">Filtrar por</h5>
              <ul class="shop-sidebar-category">
                <button class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Precio</b></button>
                <button class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Capacidad</b></button>

                <li>
                  <select class="custom-select" style="width: 100%;" id="propiedad-lateral">
                    <option selected>Tipo</option>
                    <option value="1">Jardín</option>
                    <option value="1">Hotel</option>
                    <option value="2">Restaurante</option>
                    <option value="2">Otro</option>
                  </select>
                </li>
              </ul>
            </div>
          </div>

        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end Products section -->

    
    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

    <!-- Estilos Selects -->
    <script>
    /*
    Reference: http://jsfiddle.net/BB3JK/47/
    */

    $('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

    });
    </script>

  </body>
</html>
