<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <!-- Home section -->
    <div class="owl-carousel owl-nav-overlay owl-dots-overlay" data-owl-nav="true" data-owl-dots="true" data-owl-items="1" id="slide-venue">
      <!-- Slider box 1 -->
      <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/venues-interna/venue-interna-01.jpg);">
        <div class="section-xl">
          <div class="container">
            <div class="row share-header">
                <a class="button button-lg text-center" href="pago.php" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px; margin-right: 20px;"><b>Pagar para finalizar</b></a>
                <a class="button button-lg text-center" href="pago.php" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px;"><b>Seguir editando</b></a>
            </div>
          </div><!-- end container -->
        </div>
      </div>
      <!-- Slider box 2 -->
      <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/venues-interna/venue-interna-02.jpg);">
        <div class="section-xl">
          <div class="container">
            <div class="row share-header">
                <a class="button button-lg text-center" href="pago.php" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px; margin-right: 20px;"><b>Pagar para finalizar</b></a>
                <a class="button button-lg text-center" href="pago.php" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px;"><b>Seguir editando</b></a>
            </div>
          </div><!-- end container -->
        </div>
      </div>
      <!-- Slider box 3 -->
      <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/venues-interna/venue-interna-03.jpg);">
        <div class="section-xl">
          <div class="container">
            <div class="row share-header">
                <a class="button button-lg text-center" href="pago.php" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px; margin-right: 20px;"><b>Pagar para finalizar</b></a>
                <a class="button button-lg text-center" href="pago.php" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px;"><b>Seguir editando</b></a>
            </div>
          </div><!-- end container -->
        </div>
      </div>
    </div><!-- end owl-carousel -->
    <!-- end Home section -->

    <div class="section">
      <div class="container">

          <div class="row col-spacing-50">

              <div class="col-12 col-lg-8">
                  <div class="section-title-interna3">
                    <div class="row">
                      <div class="col-12">
                        <h2 class="text-uppercase font-montserrat"><b>ST. Regis Hotel</b></h2>
                        <div class="subtitulos-general">Reforma, Ciudad de México.</div>
                      </div>
                    </div>
                  </div>

                  <div class="row personas-venue text-left font-montserrat">
                      <div class="col-12 col-lg-4"><h4><b><img src="assets/images/iconos/personas.png" alt="Amenidades del Venue"> 50-250 personas</b></h4></div>
                      <div class="col-12 col-lg-4"><h4><b><img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue"> $100,000 - $200,000</b></h4></div>
                  </div>

                  <div class="row texto-descripcion margin-bottom-30">
                    El hotel más importante de México. Contamos con 3 salones distintos para eventos privados. Somos un venue de calidad, tradición y vanguardia.<br>
                    Ubicado en la avenida más importante de la capital del país, tener un evento en nuestras instalaciones es estar en el corazón de lo más bello de esta ciudad.
                    Ofrecemos distintos precios, paquetes y opciones para que celebres con nosotros los eventos más importantes de tu vida.<br>
                    Con más de 50 años de experiencia somos el primer destino del turismo de primer nivel y nuestros servicios son de la más alta calidad.
                  </div>

                  <div class="section-title-interna3">
                    <div class="row">
                      <div class="col-12">
                        <h2 class="text-uppercase font-montserrat"><b>Amenidades</b></h2>
                      </div>
                    </div>
                  </div>


                  <!-- Amenidades Desktop -->
                  <div class="row personas-venue text-left font-montserrat d-none d-sm-block">

                    <table class="table" id="table-amenidades">
                      <tbody>
                        <tr>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-catering.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Catering</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-audio.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Sonido/Audio/Video</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-sanitarios.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Sanitarios</span>
                          </td>
                        </tr>

                        <tr>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-bar.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Bar</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-mobiliario.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Mobiliario</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-wifi.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Internet (WiFi)</span>
                          </td>
                        </tr>

                        <tr>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-seguridad.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Seguridad</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-encarpado.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Encarpado</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-luz.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Planta de Luz</span>
                          </td>
                        </tr>

                        <tr>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-aire.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Aire Acondicionado</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-actividades.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Actividades</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-discapacitados.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Acceso Discapacitados</span>
                          </td>
                        </tr>

                        <tr>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-estacionamiento.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Estacionamiento</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-areas.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Áreas Verdes</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-alojamiento.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Alojamiento</span>
                          </td>
                        </tr>

                        <tr>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-limpieza.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Limpieza</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-capilla.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Capilla</span>
                          </td>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-ceremonia.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Ceremonia Civil / Religiosa</span>
                          </td>
                        </tr>

                        <tr>
                          <td>
                              <img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;">
                              <span style="font-size:10px;">Cocktail Bienvenida</span>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

              </div>


              <!-- Amenidades Movil -->
              <div class="row margin-bottom-30 d-sm-none list-amenidades">
                <div class="col-6">
                  <ul>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                  </ul>
                </div>

                <div class="col-6">
                  <ul>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                    <li class="amenidades-movil"><img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;"> Capilla</li>
                  </ul>
                </div>
              </div>


              <div class="col-12 col-lg-4" style="display: block; z-index: 2;" id="margin-form-movil">
                  <div id="myHeader">
                    <div class="row">
                      <div class="btn-solicitar">
                        <a class="button button-md text-center" id="btn-telefono" data-toggle="modal" data-target="#telefono" title="Publica tu Venue" style="padding:  10px 30px;"><b>Ver teléfono</b></a>
                      </div>
                      <!-- Contact Form -->
                      <div class="contact-form">
                        <form method="post" id="contactform">
                          <div class="form-row">
                            <div class="col-12 col-sm-12">
                              <input type="text" id="name" name="name" placeholder="Nombre" required>
                            </div>
                            <div class="col-12 col-sm-12">
                              <input type="email" id="email" name="email" placeholder="Correo electrónico" required>
                            </div>
                            <div class="col-12 col-sm-12">
                              <textarea name="message" id="message" placeholder="¡Hola! Me interesa este venue. Gracias por contactarme."></textarea>
                            </div>
                          </div>
                          <div class="btn-solicitar">
                            <button class="button button-lg btn-buscar-header btn-contacto-movil" type="submit" data-toggle="modal" data-target="#contacto-venue">Contactar</button>
                          </div>
                        </form>
                        <!-- Submit result -->
                        <div class="submit-result">
                          <span id="success" style="color: #000000;">Gracias, mensaje enviado al venue!</span>
                          <span id="error" style="color: #000000;">Algo no estuvo bien, por favor vuelve a intentarlo</span>
                        </div>
                      </div><!-- end contact-form -->
                    </div><!-- end inner row -->
                  </div>
              </div>

          </div>

          <div class="row margin-bottom-20">
              <?php include('includes/venues/espacios.php');?>
          </div>

          <div class="row margin-bottom-20">
              <div class="section-title-interna3">
                <div class="row">
                  <div class="col-12">
                    <h2 class="text-uppercase font-montserrat"><b>Mapa</b></h2>
                  </div>
                </div>
              </div>

              <div class="col-12 col-lg-12">
                  <div class="section-title-interna3">
                    <div class="row">
                      <div class="col-12">
                        <div class="subtitulos-general">Reforma 439, Colonia Centro, Ciudad de México.</div>
                      </div>

                      <div class="col-12">
                          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15050.342250269225!2d-99.16839093237355!3d19.430304625878488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff4be2f41ead%3A0xe7179355336f6a97!2sSt+Regis!5e0!3m2!1ses-419!2smx!4v1540947814818" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                      </div>
                    </div>
                  </div><!-- end section-title -->
              </div>
          </div>

          <div class="row">
              <div class="col-12 col-sm-12">
                  <a class="button button-md margin-top-10 text-center btn-preview" id="btn-negro" href="registro.php" title="Conoce Venuescopia"><b>Seguir editando</b></a>
                  <a class="button button-md margin-top-10 text-center btn-preview" id="btn-negro" href="membresia-propietarios.php" title="Conoce Venuescopia"><b>Pagar para finalizar</b></a>
              </div>
          </div>

      </div><!-- end container -->
    </div>

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

    <!-- Sticky contact venue -->
    <script>
    window.onscroll = function() {myFunction()};
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
    </script>

  </body>
</html>
