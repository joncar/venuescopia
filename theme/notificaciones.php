<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu-interno.php');?>

    <!-- Blog section  -->
    <div class="section margin-registro-top">
      <div class="container">
        <!-- Blog Posts -->
        <div class="blog-small-thumbnail">
          <!-- Blog Post box 1 -->
          <div class="col-md-8 offset-md-2">
              <div class="blog-post-box">
                <div class="blog-post-content">
                  <div class="blog-post-title margin-bottom-10">
                    <ul class="list-horizontal-dash font-small margin-bottom-10"><li>Oct 31, 2018 / María de Buen / maria@correo.com</a></li></ul>
                    <h5 class="font-montserrat text-uppercase">Mensaje</h5>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
              </div>
              <hr>
          </div>

          <!-- Blog Post box 1 -->
          <div class="col-md-8 offset-md-2">
              <div class="blog-post-box">
                <div class="blog-post-content">
                  <div class="blog-post-title margin-bottom-10">
                    <ul class="list-horizontal-dash font-small margin-bottom-10"><li>Oct 31, 2018 / María de Buen / maria@correo.com</a></li></ul>
                    <h5 class="font-montserrat text-uppercase">Mensaje</h5>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
              </div>
              <hr>
          </div>

          <!-- Blog Post box 1 -->
          <div class="col-md-8 offset-md-2">
              <div class="blog-post-box">
                <div class="blog-post-content">
                  <div class="blog-post-title margin-bottom-10">
                    <ul class="list-horizontal-dash font-small margin-bottom-10"><li>Oct 31, 2018 / María de Buen / maria@correo.com</a></li></ul>
                    <h5 class="font-montserrat text-uppercase">Mensaje</h5>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
              </div>
              <hr>
          </div>

          <!-- Blog Post box 1 -->
          <div class="col-md-8 offset-md-2">
              <div class="blog-post-box">
                <div class="blog-post-content">
                  <div class="blog-post-title margin-bottom-10">
                    <ul class="list-horizontal-dash font-small margin-bottom-10"><li>Oct 31, 2018 / María de Buen / maria@correo.com</a></li></ul>
                    <h5 class="font-montserrat text-uppercase">Mensaje</h5>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
              </div>
              <hr>
          </div>

          <!-- Blog Post box 1 -->
          <div class="col-md-8 offset-md-2">
              <div class="blog-post-box">
                <div class="blog-post-content">
                  <div class="blog-post-title margin-bottom-10">
                    <ul class="list-horizontal-dash font-small margin-bottom-10"><li>Oct 31, 2018 / María de Buen / maria@correo.com</a></li></ul>
                    <h5 class="font-montserrat text-uppercase">Mensaje</h5>
                  </div>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
                </div>
              </div>
              <hr>
          </div>
          <!-- Pagination -->
          <nav>
            <ul class="pagination justify-content-center margin-top-70">
              <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item"><a class="page-link" href="#">3</a></li>
              <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
            </ul>
          </nav>
        </div><!-- end blog-small-thumbnail -->
      </div><!-- end container -->
    </div>
    <!-- end Blog section -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
