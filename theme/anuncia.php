<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <!-- Home section -->
    <div class="section-fullscreen bg-image" style="background-image: url(assets/images/BG/background-anuncia.jpg)" id="header-height"
        <div class="container">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3">
                <div class="bg-white contenedor-header-titulo2 text-center">
                  <h2 class="text-uppercase titulos-general font-montserrat margin-bottom-10 text-responsive"><b>Tu venue a la vista de todos</b></h2>
                  <p>Porque el futuro está aquí y en Venuescopia, todos te<br>encuentran con un sólo click.</p>
                  <a class="button button-lg margin-top-30 text-center" id="btn-negro" href="registro.php" title="Publica tu Venue"><b>Anuncia tu Venue</b></a>
                </div>
              </div>
            </div><!-- end row -->
          </div>
        </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <div class="section">
      <div class="container">

        <div class="section-title-interna margin-bottom-50">
          <div class="row align-items-end">
            <div class="col-12">
              <h2 class="text-uppercase titulos-general2 font-montserrat"><b>Nuestros Planes</b></h2>
              <div class="subtitulos-general2">
                <p>
                  Encuentra el plan ideal para tí y tu negocio y comienza a anunciar tu Venue en nuestra plataforma ¡Hoy!
                </p>
              </div>
            </div>
          </div>
        </div><!-- end section-title -->

        <div class="row margin-bottom-10">
          <!-- Text Venue -->
          <!-- Prices box 2 -->
          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-precios">
              <div class="price-features">
              <h3 class="font-montserrat margin-bottom-20">Costo por Venue</h3>
              <div class="price"><h1 class="font-montserrat">380<span>/MENSUAL</span></h1></div>
              </div>
              <p id="precios">Puedes cancelar en cualquier momento</p>
              <div class="ahorro-anuncia">Renovación automática</div>
            </div>
          </div>

          <!-- Prices box 3 -->
          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-precios">
              <div class="price-features">
              <h3 class="font-montserrat margin-bottom-20">Costo por Venue</h3>
              <div class="price"><h1 class="font-montserrat">333<span>/TRIMESTRAL</span></h1></div>
              </div>
              <p id="precios">Equivalente a pagar $999 al trimestre</p>
              <div class="ahorro-anuncia">Ahorras $140 vs el plan mensual</div>
            </div>
          </div>

          <!-- Prices box 3 -->
          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-precios">
              <div class="price-features">
              <h3 class="font-montserrat margin-bottom-20">Costo por Venue</h3>
              <div class="price"><h1 class="font-montserrat">300<span>/ANUAL</span></h1></div>
              </div>
              <p id="precios">Equivalente a pagar $3,600 al año y<br>ahorras $960 vs el plan mensual</p>
              <div class="ahorro-anuncia">Mejor opción</div>
            </div>
          </div>
        </div><!-- end row -->

        <div class="section-title-interna margin-bottom-50">
          <div class="row align-items-end">
            <div class="col-12">
              <div class="subtitulos-general2">
                <p>
                  Qué obtienes al contratar un plan de anuncio con nosotros:
                </p>
              </div>
            </div>
          </div>
        </div><!-- end section-title -->

        <div class="row">
          <div class="col-12 col-lg-6">
            <ul>
              <li>Anuncio privado para tu Venue.</li>
              <li>Descripción y listado de amenidades del Venue.</li>
              <li>Subir hasta 10 imágenes por anuncio.</li>
              <li>Upload en el anuncio des espacios para renta separados dentro del mismo Venue.</li>
              <li>Ubicación y mapa live en el anuncio.</li>
            </ul>
          </div>
          <div class="col-12 col-lg-6">
            <ul>
              <li>Contacto de interesados directamente desde el sitio.</li>
              <li>Registro de visitas al anuncio desde tu Venue.</li>
              <li>Editable y visible 24/7 a todo el mundo.</li>
              <li>Tráfico de personas interesadas en ranta de espacios.</li>
              <li>Mayor oportunidad de capitalizar tu negocio.</li>
            </ul>
          </div>
        </div><!-- end row -->

        <div class="row">
          <div class="col-12 col-sm-12 text-center">
              <a class="button button-lg margin-top-50 text-center" id="btn-negro" href="registro.php" title="Publica tu Venue"><b>Anuncia tu Venue</b></a>
          </div>
        </div><!-- end row -->

      </div><!-- end container -->
    </div>


    <div class="section">
      <div class="container">

        <div class="section-title-interna margin-bottom-50">
          <div class="row align-items-end">
            <div class="col-12">
              <h2 class="text-uppercase titulos-general2 font-montserrat"><b>Nuevo, fácil, único.</b></h2>
              <div class="subtitulos-general2">Porque somos los primeros y llegamos para quedarnos.</div>
            </div>
          </div>
        </div><!-- end section-title -->

        <div class="row">
          <!-- Text Venue -->
          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-height">
              <img src="assets/images/iconos/about-01.png" alt="Beneficios de anunciar tu Venue con Venuescopia" class="img-beneficios-anuncia">
              <h3 class="font-montserrat margin-bottom-20">ONLINE</h3>
              <div class="price-features">
                <p>No estar online es no estar en ningún lugar, con nosotros estás en todos lados y en los dispositivos de todos.</p>
              </div>
            </div>
          </div>
          <!-- Prices box 2 -->
          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-height">
              <img src="assets/images/iconos/about-02.png" alt="Beneficios de anunciar tu Venue con Venuescopia" class="img-beneficios-anuncia">
              <h3 class="font-montserrat margin-bottom-20">todos los ojos en ti</h3>
              <div class="price-features">
                <p>Tu venue al alcance de todos a cualquier hora, en cualquier lugar.</p>
              </div>
            </div>
          </div>
          <!-- Prices box 3 -->
          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-height">
              <img src="assets/images/iconos/about-03.png" alt="Beneficios de anunciar tu Venue con Venuescopia" class="img-beneficios-anuncia">
              <h3 class="font-montserrat margin-bottom-20">inteligente y económico</h3>
              <div class="price-features">
                <p>Anunciarte con nosotros es increíblemente económico, tu inversión aquí es inteligente.</p>
              </div>
            </div>
          </div>
        </div><!-- end row -->

      </div><!-- end container -->
    </div>

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
