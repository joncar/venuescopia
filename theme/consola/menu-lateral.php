<!-- Sidebar scroll-->
<div class="scroll-sidebar">
    <!-- User profile -->
    <div class="user-profile" style="background: url(assets/images/background/user-info.jpg) no-repeat;">
        <!-- User profile image -->
        <div class="profile-img"> <img src="assets/images/users/profile.png" alt="user" /> </div>
    </div>
    <!-- End User profile text-->
    <!-- Sidebar navigation-->
    <nav class="sidebar-nav">
        <ul id="sidebarnav">
            <li> 
                <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                <ul aria-expanded="false" class="collapse">
                    <li><a href="index.php">Usuarios del sistema</a></li>
                    <li><a href="usuarios.php">Usuarios</a></li>
                    <li><a href="transacciones.php">Transacciones</a></li>
                    <li><a href="facturacion.php">Facturación</a></li>
                    <li><a href="catalogos.php">Catálogos</a></li>
                    <li><a href="notificaciones.php">Notificaciones</a></li>
                    <li><a href="reportes.php">Reportes</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- End Sidebar navigation -->
</div>
<!-- End Sidebar scroll-->
<!-- Bottom points-->
<div class="sidebar-footer">
    <!-- item--><a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
    <!-- item--><a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
    <!-- item--><a href="login.php" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a> </div>
<!-- End Bottom points-->