<!DOCTYPE html>
<html lang="en">
<head>
<?php include('head.php');?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Reportes</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Transacciones</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>LAST MONTH</small></h6>
                                    <h4 class="m-t-0 text-primary">$48,356</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div>
                            <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <div class="col-12">
                        <div class="dt-buttons">
                            <a class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example23" href="transacciones.php"><span>Regresar</span></a>
                        </div>

                        <div class="card">
                            <div class="card-body">

                              <div class="col-12 col-lg-12">
                                  <h2 class="font-weight-norma text-uppercase titulos"><b>Transacción para ST. Regis Hotel</b></h2>
                                  <div class="margin-bottom-30">
                                    <p>Reforma, Ciudad de México.</p>
                                    <h4 class="iconos-venue"><b><img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:2%;">50-250 personas</b></h4>
                                    <h4 class="iconos-venue"><b><img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:2%;">$100,000 - $200,000</b></h4>
                                  </div><br>
                                  
                                <h2 class="font-weight-norma text-uppercase titulos"><b>Nombre del Cliente</b></h2>
                                <h6 class="heading-uppercase no-margin"><b>Descripción</b></h6>
                                El hotel más importante de México. Contamos con 3 salones distintos para eventos privados. Somos un venue de calidad, tradición y vanguardia.<br>
                                Ubicado en la avenida más importante de la capital del país, tener un evento en nuestras instalaciones es estar en el corazón de lo más bello de esta ciudad. 
                                Ofrecemos distintos precios, paquetes y opciones para que celebres con nosotros los eventos más importantes de tu vida.<br>
                                Con más de 50 años de experiencia somos el primer destino del turismo de primer nivel y nuestros servicios son de la más alta calidad.
                              </div>

                          </div>

                        </div>
                    </div>
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>
    <?php include('librerias.php');?>
</body>
</html>