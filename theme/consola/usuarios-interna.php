<!DOCTYPE html>
<html lang="en">
<head>
<?php include('head.php');?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <header class="topbar">
            <?php include('menu-top.php');?>
        </header>

        <aside class="left-sidebar">
            <?php include('menu-lateral.php');?>
        </aside>

        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-5 col-8 align-self-center">
                        <h3 class="text-themecolor">Usuarios</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                            <li class="breadcrumb-item active">Usuarios detalle</li>
                        </ol>
                    </div>
                    <div class="col-md-7 col-4 align-self-center">
                        <div class="d-flex m-t-10 justify-content-end">
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>THIS MONTH</small></h6>
                                    <h4 class="m-t-0 text-info">$58,356</h4></div>
                                <div class="spark-chart">
                                    <div id="monthchart"></div>
                                </div>
                            </div>
                            <div class="d-flex m-r-20 m-l-10 hidden-md-down">
                                <div class="chart-text m-r-10">
                                    <h6 class="m-b-0"><small>LAST MONTH</small></h6>
                                    <h4 class="m-t-0 text-primary">$48,356</h4></div>
                                <div class="spark-chart">
                                    <div id="lastmonthchart"></div>
                                </div>
                            </div>
                            <div class="">
                                <button class="right-side-toggle waves-effect waves-light btn-success btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card"> <img class="card-img" src="assets/images/background/user-info.jpg" alt="Card image">
                            <div class="card-img-overlay card-inverse social-profile d-flex ">
                                <div class="align-self-center"> <img src="assets/images/users/profile.png" class="img-circle" width="100">
                                    <h4 class="card-title">Nombre del usuario</h4>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-body"> <small class="text-muted">Correo</small>
                                <h6>correo@gmail.com</h6> <small class="text-muted db">Teléfono</small>
                                <h6>+91 654 784 547</h6> <small class="text-muted db">Dirección</small>
                                <h6>Dirección completa</h6>
                                <div class="map-box">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div> <small class="text-muted db">Redes Sociales</small>
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-facebook"></i></button>
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-twitter"></i></button>
                                <button class="btn btn-circle btn-secondary"><i class="fab fa-youtube"></i></button>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#historial" role="tab">Historial de compras</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#propiedades" role="tab">Propiedades</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#perfil" role="tab">Perfil</a> </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="historial" role="tabpanel">
                                    <div class="card-body">
                                        
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body">

                                                        <div class="dt-buttons">
                                                            <a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example23" href="#"><span>Excel</span></a>
                                                            <a class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example23" href="usuarios.php"><span>Regresar al listado</span></a>
                                                        </div>

                                                        <div id="example23_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="example23"></label></div>

                                                        <div class="table-responsive">
                                                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Venue de interés</th>
                                                                        <th>Fecha de compra</th>
                                                                        <th>Estatus</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>01</th>
                                                                        <th>Hotel ST. Regis</th>
                                                                        <th>00/00/000000 - 00/00/0000</th>
                                                                        <th>Pagado</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>02</th>
                                                                        <th>Hotel ST. Regis</th>
                                                                        <th>00/00/000000 - 00/00/0000</th>
                                                                        <th>Pagado</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>03</th>
                                                                        <th>Hotel ST. Regis</th>
                                                                        <th>00/00/000000 - 00/00/0000</th>
                                                                        <th>Pagado</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>04</th>
                                                                        <th>Hotel ST. Regis</th>
                                                                        <th>00/00/000000 - 00/00/0000</th>
                                                                        <th>Pagado</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>05</th>
                                                                        <th>Hotel ST. Regis</th>
                                                                        <th>00/00/000000 - 00/00/0000</th>
                                                                        <th>Pagado</th>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!--second tab-->
                                <div class="tab-pane" id="propiedades" role="tabpanel">
                                    <div class="card-body">

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="card">
                                                    <div class="card-body">

                                                        <div class="dt-buttons">
                                                            <a class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="example23" href="#"><span>Excel</span></a>
                                                            <a class="dt-button buttons-pdf buttons-html5" tabindex="0" aria-controls="example23" href="usuarios.php"><span>Regresar al listado</span></a>
                                                        </div>

                                                        <div id="example23_filter" class="dataTables_filter"><label>Search:<input type="search" class="" placeholder="" aria-controls="example23"></label></div>

                                                        <div class="table-responsive">
                                                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>ID</th>
                                                                        <th>Venue</th>
                                                                        <th>Estatus</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <th>01</th>
                                                                        <th><a href="propiedad-interna.php" title="Ver detalle">Hotel ST. Regis</a></th>
                                                                        <th>Activo</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>02</th>
                                                                        <th><a href="propiedad-interna.php" title="Ver detalle">Hotel ST. Regis</a></th>
                                                                        <th>Activo</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>03</th>
                                                                        <th><a href="propiedad-interna.php" title="Ver detalle">Hotel ST. Regis</a></th>
                                                                        <th>Activo</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>04</th>
                                                                        <th><a href="propiedad-interna.php" title="Ver detalle">Hotel ST. Regis</a></th>
                                                                        <th>Activo</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>05</th>
                                                                        <th><a href="propiedad-interna.php" title="Ver detalle">Hotel ST. Regis</a></th>
                                                                        <th>Activo</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>06</th>
                                                                        <th><a href="propiedad-interna.php" title="Ver detalle">Hotel ST. Regis</a></th>
                                                                        <th>Activo</th>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="tab-pane" id="perfil" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row margin-bottom-20">
                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Estatus</strong>
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="radio3" name="radio" type="radio" class="custom-control-input">
                                                                <span class="custom-control-label">Activo</span>
                                                            </label>
                                                            <label class="custom-control custom-radio">
                                                                <input id="radio4" name="radio" type="radio" class="custom-control-input">
                                                                <span class="custom-control-label">Inactivo</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-xs-6 b-r"> <strong>Bloquear/Reactivar</strong>
                                                <div class="form-group row">
                                                    <div class="col-md-9">
                                                        <div class="m-b-10">
                                                            <label class="custom-control custom-radio">
                                                                <input id="radio3" name="radio" type="radio" class="custom-control-input">
                                                                <span class="custom-control-label">Bloquear</span>
                                                            </label>
                                                            <label class="custom-control custom-radio">
                                                                <input id="radio4" name="radio" type="radio" class="custom-control-input">
                                                                <span class="custom-control-label">Activar</span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form class="form-horizontal form-material">
                                            <div class="form-group">
                                                <label class="col-md-12">Nombre</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="..." class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Email</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="correo@gmail.com" class="form-control form-control-line" name="example-email" id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-12">Password</label>
                                                <div class="col-md-12">
                                                    <input type="password" value="password" class="form-control form-control-line">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="example-email" class="col-md-12">Teléfono</label>
                                                <div class="col-md-12">
                                                    <input type="email" placeholder="..." class="form-control form-control-line" name="example-email" id="example-email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-12">Selecionar País</label>
                                                <div class="col-sm-12">
                                                    <select class="form-control form-control-line">
                                                        <option>London</option>
                                                        <option>India</option>
                                                        <option>Usa</option>
                                                        <option>Canada</option>
                                                        <option>Thailand</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <button class="btn btn-success">Guardar cambios</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
            </div>

            <?php include('footer.php');?>

        </div>
    </div>
    <?php include('librerias.php');?>
</body>
</html>