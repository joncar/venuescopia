<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <div class="container-fluid no-padding bg-image" style="background-image: url(assets/images/BG/background-anuncia.jpg)" id="header-contact">
      <div class="row align-items-center no-margin">

        <div class="col-12 col-lg-6 no-margin bg-white-interna-contacto padding-about">
          <div class="padding-50 margin-text-header-contact">
            <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Contacto</b></h2>
            <div class="padding-same">
              <p class="text-responsive">Estamos para servirte.</p>
            </div>
            <div class="text-responsive">
              <a href="mailto=hola@venuescopia.com" class="bold-mail font-montserrat"><img src="assets/images/mail.png" alt="Email"><b>hola@venuescopia.com</b></a>
            </div>
          </div>
        </div>

        <div class="col-12 col-lg-6 resolvimos-modal padding-contact">
          <div class="contact-form margin-contact">
              <form method="post" id="contactform">
                <div class="form-row">
                  <div class="col-12 col-sm-6 margin-bottom-10">
                    <input type="text" id="name" name="name" placeholder="Nombre*" required style="border: none;">
                  </div>
                  <div class="col-12 col-sm-6 margin-bottom-10">
                    <input type="email" id="email" name="email" placeholder="Correo Electrónico*" required style="border: none;">
                  </div>
                </div>
                <input type="text" id="subject" name="subject" placeholder="Asunto*" required class="margin-bottom-10" style="border: none;">
                <textarea name="message" id="message" placeholder="Mensaje*" style="border: none; height: 120px; resize: none;"></textarea>
                <a class="button button-md margin-top-20 text-center" id="btn-negro" href="contacto-envio.php" title="Enviar mis comentarios" style="width: 100%;"><b>Enviar</b></a>
              </form>

              <div class="submit-result">
                <span id="success">Thank you! Your Message has been sent.</span>
                <span id="error">Something went wrong, Please try again!</span>
              </div>
            </div>
        </div>

      </div><!-- end row -->
    </div><!-- end container-fluid -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
