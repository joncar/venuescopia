<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <style>
    .blue {
      box-shadow: 0px 0px 0px 24px #00a8ff inset !important;
    }
    .red {
      box-shadow: 0px 0px 0px 24px #ff605f inset !important;
    }
    .green {
      box-shadow: 0px 0px 0px 24px #96d100 inset !important;
    }
    .purple {
      box-shadow: 0px 0px 0px 24px #d066fa inset !important;
    }
    .check {
      height: 100%;
      -webkit-transform: scale(0);
              transform: scale(0);
      width: 100%;
      transition-delay: 0.2s;
      background: url("http://frolovoleg.ru/images/checkmark.svg") center no-repeat;
    }
  </style>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>


    <!-- Product Tab content -->
    <div class="section margin-registro-top">
      <div class="container">

          <div class="row">

              <div class="col-12 col-lg-6">

                <div class="row">
                  <div class="col-12 col-md-12">
                    <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Publica tu Venue</b></h2>
                  </div>
                </div>

                <div class="row" id="myWizard">

                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="6" style="width: 100%;">Paso 1 de 6</div>
                    </div>

                    <div class="navbar padding-registro-nav">
                        <div class="navbar-inner">
                            <ul class="nav nav-pills nav-wizard">
                                <li class="active btn-registro">
                                    <div class="nav-wedge"></div><a href="#step1" data-toggle="tab" data-step="1"> 1. Comencemos</a>
                                </li>
                                <li class="disabled btn-registro">
                                    <div class="nav-wedge"></div><a href="#step2" data-toggle="tab" data-step="2"> 2. Platícanos</a>
                                </li>
                                <li class="disabled btn-registro">
                                    <div class="nav-wedge"></div><a href="#step3" data-toggle="tab" data-step="3"> 3. Espacios</a>
                                </li>
                                <li class="disabled btn-registro">
                                    <div class="nav-wedge"></div><a href="#step4" data-toggle="tab" data-step="4"> 4. Fotos</a>
                                </li>
                                <li class="disabled btn-registro">
                                    <div class="nav-wedge"></div><a href="#step5" data-toggle="tab" data-step="5"> 5. Visualiza</a>
                                </li>
                                <li class="disabled btn-registro">
                                    <div class="nav-wedge"></div><a href="#step6" data-toggle="tab" data-step="6"> 6. Pagar</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="tab-content">
                        <!-- Step 1 -->
                        <div class="tab-pane fade in active" id="step1">

                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>1. Registra tu Venue</b></h2></div>

                            <div class="row margin-bottom-20">
                                <p style="font-size:13px;">En Venuescopia estamos contentos de que te sumes a nuestra plataforma.<br>
                                En los próximos minutos te estaremos guiando a través de unos simples pasos para que logres registrar con éxito tu Venue.<br>
                                ¡Bienvenido!</p>
                            </div>

                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>¿Cuál es el nombre de tu Venue?</b></h2></div>

                            <div class="row margin-bottom-20">
                                <input type="text" id="name" name="name" placeholder="Nombre" required="" class="margin-bottom-20">
                                <textarea rows="10" name="message" id="message" placeholder="Descripción de tu Venue"></textarea>
                            </div>

                            <div class="row margin-bottom-20">
                                <p style="font-size:13px;">
                                  <b>¡Atención!</b><br>
                                  ¿Tu Venue cuenta con varios espacios que puedan ser rentados al mismo tiempo para diferentes eventos?<br>
                                  Si se renta todo el Venue al mismo tiempo, elige la opción 1.<br>
                                  En caso de tener varios espacios, elige a contonuación cuántos espacios distintos tiene el Venue.
                                </p>
                            </div>

                            <div class="row margin-bottom-20">
                                <label>¿Cuántos espacios para renta tiene tu Venue?</label>
                                <select class="custom-select">
                                  <option value="1">01</option>
                                  <option value="1">02</option>
                                  <option value="1">03</option>
                                  <option value="1">04</option>
                                  <option value="1">05</option>
                                  <option value="1">06</option>
                                  <option value="1">07</option>
                                  <option value="1">08</option>
                                </select>
                                <div>
                                  <a class="button button-md margin-top-30 text-center" id="btn-negro" href="about.php#nosotros" title="Conoce Venuescopia"><b>Continuar</b></a>
                                  <span class="guardar-salir"><a href="#">Guardar y salir</a></span>
                                </div>
                            </div>
                        </div>

                        <!-- Step 2 -->
                        <div class="tab-pane fade" id="step2">

                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>2. Platícanos un poco más sobre tu Venue</b></h2></div>

                            <div class="row margin-bottom-20">
                                <label>Tipo de Venue</label>
                                <select class="custom-select">
                                  <option value="1">Jardín</option>
                                  <option value="1">Hotel</option>
                                  <option value="1">Restaurante</option>
                                  <option value="2">Otros</option>
                                </select>
                            </div>

                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>Datos de contacto</b></h2></div>

                            <div class="row margin-bottom-20">
                                <div class="col-12 col-lg-6" style="padding-left: 0px;">
                                  <input type="text" id="name" name="name" placeholder="Teléfono" required>
                                </div>

                                <div class="col-12 col-lg-6" style="padding-right:0px;">
                                  <input type="text" id="name" name="name" placeholder="Correo" required>
                                </div>
                            </div>

                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>Enséñanos la ubicación de tu venue</b></h2></div>

                            <div class="row margin-bottom-20">
                                <div class="contact-form">
                                    <form method="post" id="contactform">
                                      <div class="form-row">
                                          <div class="col-12 col-sm-12 margin-bottom-20">
                                            <input type="text" id="name" name="name" placeholder="Ingresa la dirección" required>
                                          </div>

                                          <!-- Autocomplete o con selects -->
                                          <div class="col-12 col-sm-12 margin-bottom-20">
                                              <label>Ciudad</label>
                                              <select class="custom-select">
                                                <option value="1">01</option>
                                                <option value="1">02</option>
                                              </select>
                                          </div>

                                          <div class="col-12 col-sm-12 margin-bottom-20">
                                              <label>Estado</label>
                                              <select class="custom-select">
                                                <option value="1">01</option>
                                                <option value="1">02</option>
                                              </select>
                                          </div>
                                          <!-- Autocomplete o con selects -->

                                          <div class="col-12 col-sm-12 margin-bottom-20">
                                            <input type="text" id="name" name="name" placeholder="Colonia" required>
                                          </div>

                                          <div class="col-12 col-sm-12">
                                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15050.342250269225!2d-99.16839093237355!3d19.430304625878488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff4be2f41ead%3A0xe7179355336f6a97!2sSt+Regis!5e0!3m2!1ses-419!2smx!4v1540947814818" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                                          </div>
                                      </div>
                                    </form>
                                  </div>
                             </div><!-- end row -->

                             <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Regresar</b></a>
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Continuar</b></a>
                                <span class="guardar-salir"><a href="#">Guardar y salir</a></span>
                             </div>
                        </div>

                        <!-- Step 3 -->
                        <div class="tab-pane fade" id="step3">
                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>3. Agreguemos tus espacios del Venue</b></h2></div>

                            <div class="row">
                              <div class="margin-bottom-20">
                                  <div class="col-12 col-sm-12 no-padding" style="margin-bottom: 0px;">
                                    <div class="text-general-p font-weight-bold text-uppercase">Venue Principal</div>
                                    <div class="contact-form no-padding">
                                      <form method="post" id="contactform">
                                        <div class="form-row">
                                          <div class="col-12 col-sm-12 margin-bottom-20">
                                            <input type="text" id="name" name="name" placeholder="Nombre del Venue" required>
                                          </div>
                                          <div class="col-12 col-sm-4  margin-bottom-20">
                                            <div class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Capacidad</b></div>
                                            <!-- Preview - esto se muestra una vez que escogiste opciones del modal -->
                                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                                <img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:6%;"> 50-250 personas
                                            </div>
                                          </div>
                                          <div class="col-12 col-sm-4  margin-bottom-20">
                                            <div class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Precio</b></div>
                                            <!-- Preview -->
                                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                                <img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:6%;"> $100,000 - $200,000
                                            </div>
                                          </div>
                                          <div class="col-12 col-sm-4  margin-bottom-20">
                                            <div class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#amenidades" id="btn-blanco-small" style="width: 100%;"><b>Amenidades</b></div>
                                            <!-- Preview -->
                                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                                <a class="margin-bottom-20 text-center amenidades-registro" type="button" data-toggle="modal" data-target="#amenidades-preview" style="width: 100%;">20 amenidades <i class="fas fa-eye"></i></a>
                                            </div>
                                          </div>
                                          <div class="col-12 col-sm-12  margin-bottom-20">
                                            <textarea rows="10" name="message" id="message" placeholder="Descripción del Venue"></textarea>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                              </div>
                            </div><!-- end row -->

                            <!-- Si selecciono más de un espacio aqui se muestran todos -->
                            <div class="col-12 col-sm-12" style="padding-left: 0px;">
                              <div class="margin-bottom-20">
                                  <div class="col-12 col-sm-12 no-padding" style="margin-bottom: 0px;">
                                    <div class="text-general-p font-weight-bold text-uppercase">Espacio 1</div>
                                    <div class="contact-form no-padding">
                                      <form method="post" id="contactform">
                                        <div class="form-row">
                                          <div class="col-12 col-sm-12 margin-bottom-20">
                                            <input type="text" id="name" name="name" placeholder="Nombre del Espacio" required>
                                          </div>
                                          <div class="col-12 col-sm-4  margin-bottom-20">
                                            <div class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Capacidad</b></div>
                                            <!-- Preview - esto se muestra una vez que escogiste opciones del modal -->
                                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                                <img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:6%;"> 50-250 personas
                                            </div>
                                          </div>
                                          <div class="col-12 col-sm-4  margin-bottom-20">
                                            <div class="button margin-bottom-20 text-center" type="button" data-toggle="modal" data-target="#filtro-precio" id="btn-blanco-small" style="width: 100%;"><b>Precio</b></div>
                                            <!-- Preview -->
                                            <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block text-center">
                                                <img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:6%;"> $100,000 - $200,000
                                            </div>
                                          </div>
                                          <div class="col-12 col-sm-12  margin-bottom-20">
                                            <textarea rows="10" name="message" id="message" placeholder="Descripción del espacio"></textarea>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                              </div>
                            </div><!-- end row -->

                            <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Regresar</b></a>
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Continuar</b></a>
                                <span class="guardar-salir"><a href="#">Guardar y salir</a></span>
                             </div>
                        </div>

                        <!-- Step 4 -->
                        <div class="tab-pane fade" id="step4">

                            <div class="row section-title-interna">
                                <h2 class="text-uppercase titulos-general2 font-montserrat"><b>4. Fotos de tu Venue</b></h2>
                            </div>

                            <div class="col-12 col-sm-12 no-padding ">
                              <div class="margin-bottom-20">
                                  <form>
                                    <div class="text-general-p"><b>Fotos de mi Venue principal "Nombre del Venue".</b><br>Selecciona una foto desde tu ordenador.<br>Debes subir una foto de portada y máximo 10 imágenes más.</div>
                                    <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="validatedCustomFile" required="">
                                      <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                      <div class="invalid-feedback">Example invalid custom file feedback</div>
                                    </div>
                                  </form>
                                  <!-- Va en la parte inferior de la imagen del detalle del Venue -->
                                  <div>
                                    <textarea name="message" id="message" placeholder="Pie de foto"></textarea>
                                  </div>
                                  <!-- Ver miniaturas -->
                                  <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                      <img src="https://picsum.photos/50/50">
                                      <img src="https://picsum.photos/50/50">
                                      <img src="https://picsum.photos/50/50">
                                      <img src="https://picsum.photos/50/50">
                                  </div>
                                  <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                      <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Agregar foto</b></a>
                                  </div>
                              </div>
                            </div><!-- end row -->

                            <div class="col-12 col-sm-12 no-padding ">
                              <div class="margin-bottom-20">
                                  <form>
                                    <div class="text-general-p"><b>Fotos de mi Espacio 1 "Nombre del Espacio"</b><br>Selecciona una foto desde tu ordenador.<br>Debes subir una foto de portada y máximo 6 imágenes más.</div>
                                    <div class="custom-file">
                                      <input type="file" class="custom-file-input" id="validatedCustomFile" required="">
                                      <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                      <div class="invalid-feedback">Example invalid custom file feedback</div>
                                    </div>
                                  </form>
                                  <div><textarea name="message" id="message" placeholder="Pie de foto"></textarea></div>
                                  <!-- Ver miniaturas -->
                                  <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                      <img src="https://picsum.photos/50/50">
                                      <img src="https://picsum.photos/50/50">
                                      <img src="https://picsum.photos/50/50">
                                      <img src="https://picsum.photos/50/50">
                                  </div>
                                  <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                      <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Agregar foto</b></a>
                                  </div>
                              </div>
                            </div><!-- end row -->

                            <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Regresar</b></a>
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="preview-venue.php" title="Conoce Venuescopia"><b>Continuar</b></a>
                                <span class="guardar-salir"><a href="#">Guardar y salir</a></span>
                            </div>
                        </div>

                        <!-- Step 5 -->
                        <div class="tab-pane fade" id="step5">
                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>5.- Visualiza tu anuncio</b></h2></div>

                            <div class="row margin-bottom-20">
                                <p style="font-size:13px;">
                                  ¡Estas muy cerca de publicar tu anuncia en Venuescopia para que todo el mundo lo pueda ver y rentar!
                                </p>
                            </div>

                            <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Regresar</b></a>
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="preview-venue.php" title="Conoce Venuescopia"><b>Ver anuncio</b></a>
                                <span class="guardar-salir"><a href="#">Guardar y salir</a></span>
                             </div>
                        </div>

                        <!-- Step 6 -->
                        <div class="tab-pane fade" id="step6">
                            <div class="row section-title-interna" style="padding-left: 0px;"><h2 class="text-uppercase titulos-general2 font-montserrat"><b>6.- Pagar para finalizar</b></h2></div>

                            <div class="row margin-bottom-20">
                                <div class="col-12 col-sm-12 margin-bottom-20">
                                  <p style="font-size:13px;">
                                    ¡Listo! Al hacer tu pago quedará listo el anuncio de tu venue para que todo el mundo te encuentre.<br><br>
                                    <b>Qué obtienes al contratar un plan de anuncio con nosotros:</b><br>
                                    <ul style="font-size:13px;">
                                      <li>Contacto de interesados directamente desde el sitio.</li>
                                      <li>Registro de visitas al anuncio desde tu Venue.</li>
                                      <li>Editable y visible 24/7 a todo el mundo.</li>
                                      <li>Tráfico de personas interesadas en ranta de espacios.</li>
                                      <li>Mayor oportunidad de capitalizar tu negocio</li>
                                    </ul>
                                  </p>
                                </div>

                                <!-- Autocomplete o con selects -->
                                <div class="col-12 col-sm-12 margin-bottom-20">

                                  <div class="col-12 col-lg-4">
                                    <div class="prices-box bg-white beneficios-anunciar anuncia-precios">
                                      <div class="price-features">
                                      <h3 class="font-montserrat margin-bottom-20">Costo por Venue</h3>
                                      <div class="price"><h1 class="font-montserrat">380<span>/MENSUAL</span></h1></div>
                                      </div>
                                      <p id="precios">Puedes cancelar en cualquier momento</p>
                                      <div class="ahorro-anuncia">Renovación automática</div>
                                    </div>
                                    <div class="button one"><div class="check"></div></div>
                                  </div>

                                  <div class="col-12 col-lg-4">
                                    <div class="prices-box bg-white beneficios-anunciar anuncia-precios">
                                      <div class="price-features">
                                      <h3 class="font-montserrat margin-bottom-20">Costo por Venue</h3>
                                      <div class="price"><h1 class="font-montserrat">333<span>/TRIMESTRAL</span></h1></div>
                                      </div>
                                      <p id="precios">Equivalente a pagar $999 al trimestre</p>
                                      <div class="ahorro-anuncia">Ahorras $140 vs el plan mensual</div>
                                    </div>
                                    <div class="button two"><div class="check"></div></div>
                                  </div>

                                  <div class="col-12 col-lg-4">
                                    <div class="prices-box bg-white beneficios-anunciar anuncia-precios">
                                      <div class="price-features">
                                      <h3 class="font-montserrat margin-bottom-20">Costo por Venue</h3>
                                      <div class="price"><h1 class="font-montserrat">300<span>/ANUAL</span></h1></div>
                                      </div>
                                      <p id="precios">Equivalente a pagar $3,600 al año y<br>ahorras $960 vs el plan mensual</p>
                                      <div class="ahorro-anuncia">Mejor opción</div>
                                    </div>
                                    <div class="button three"><div class="check"></div></div>
                                  </div>

                                </div>

                                <div class="col-12 col-sm-12 margin-bottom-20">
                                  <p style="font-size:13px;">
                                    Crea tu cuenta
                                  </p>
                                </div>

                                <div class="row margin-bottom-10">
                                    <div class="col-12 col-lg-12">
                                      <input type="text" id="name" name="name" placeholder="Nombre" required>
                                    </div>
                                    <div class="col-12 col-lg-12">
                                      <input type="text" id="name" name="name" placeholder="Correo" required>
                                    </div>
                                    <div class="col-12 col-lg-12">
                                      <input type="text" id="name" name="name" placeholder="Contraseña" required>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 margin-bottom-20">
                                  <p style="font-size:13px;">
                                    Ingresa tu información de pago
                                  </p>
                                </div>

                                <div class="row margin-bottom-10">
                                    <div class="col-12 col-lg-4">
                                      <input type="text" id="name" name="name" placeholder="Nombre en la tarjeta" required>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                      <input type="text" id="name" name="name" placeholder="Número de la Tarjeta" required>
                                    </div>
                                    <div class="col-12 col-lg-4">
                                      <input type="text" id="name" name="name" placeholder="Fecha de vencimiento" required>
                                    </div>
                                </div>

                                <div class="row margin-bottom-10">
                                    <div class="col-12 col-lg-12" style="padding-right:0px;">
                                      <a class="button text-center" id="btn-negro" data-toggle="modal" data-target="#login" style="font-size: 12px;"><b>Agregar tarjeta</b></a>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-12 margin-bottom-20">
                                  <p style="font-size:13px;">
                                    Ya tengo cuenta <a data-toggle="modal" data-target="#login" style="text-decoration:underline;">Iniciar sesión</a>
                                  </p>
                                </div>

                                <div class="contact-form">
                                  ¿Tienes un código de descuento? Ingresalo aquí
                                  <form method="post" id="contactform">
                                    <div class="form-row">
                                        <div class="col-12 col-sm-12 margin-bottom-20">
                                          <input type="text" id="name" name="name" placeholder="XXXXXX" required>
                                        </div>
                                    </div>
                                  </form>
                                </div>
                            </div>

                            <div class="col-12 col-sm-12" style="padding-left: 0px;">
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Regresar</b></a>
                                <a class="button button-md margin-top-10 text-center" id="btn-negro" data-toggle="modal" data-target="#login"><b>Pagar</b></a>
                                <span class="guardar-salir"><a href="#">Guardar y salir</a></span>
                             </div>
                        </div>

                    </div>

                </div>
                <div id="push"></div>
              </div>

              <div class="col-12 col-lg-6 no-padding" style="display: block; z-index: 2;">
                 <div id="myHeader">
                      <div class="fondo-gris">
                          <!-- TIP -->
                          <div class="fondo-tip-registro text-center margin-bottom-30">
                            <p>
                              <div style="font-size:14px;" class="margin-bottom-50 nombre-tip"><b class="font-montserrat tip">TIP</b> NOMBRE DEL VENUE</div>
                              <span style="font-size:13px;">Recuerda indicar el nombre oficial de tu venue. Ejemplo:</span><br>
                              <span><b class="text-uppercase font-montserrat" style="font-size: 13px;">hacienda san gabriel</b></span>
                            </p>
                          </div>
                          <!-- TIP -->
                          <div class="fondo-tip-registro text-center margin-bottom-30">
                            <p>
                              <div style="font-size:14px;" class="margin-bottom-50 nombre-tip"><b class="font-montserrat tip">TIP</b> NOMBRE DEL VENUE</div>
                              <span style="font-size:13px;">Recuerda indicar el nombre oficial de tu venue. Ejemplo:</span><br>
                              <span><b class="text-uppercase font-montserrat" style="font-size: 13px;">hacienda san gabriel</b></span>
                            </p>
                          </div>
                      </div>
                  </div>
              </div>

          </div>

      </div><!-- end container -->
    </div>
    <!-- end Product Tab content -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>







    <!-- Estilos Selects -->
        <script>
        $('select').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();
        });

        $listItems.click(function(e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            //console.log($this.val());
        });

        $(document).click(function() {
            $styledSelect.removeClass('active');
            $list.hide();
        });

        });
        </script>








    <script>
    var slider = document.getElementById("myRange");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
    slider.oninput = function() {
      output.innerHTML = this.value;
    }
    </script>

    <!-- Checkbox Paso 6 -->
    <script>
      (function () {
      $(document).ready(function () {
        var changeButton, resetButton;
        resetButton = function () {
          $(".check").css("transform", "scale(0)");
          $(".button").removeClass("blue");
          $(".button").removeClass("red");
          $(".button").removeClass("green");
          return $(".button").removeClass("purple");
        };
        changeButton = function (className, colorName) {
          return $(className).click(function () {
            resetButton();
            $(this).addClass(colorName);
            return $(this).children().css("transform", "scale(1)");
          });
        };
        changeButton(".one", "blue");
        changeButton(".two", "red");
        changeButton(".three", "green");
        return changeButton(".four", "purple");
      });

    }).call(this);

    //# sourceURL=coffeescript
    </script>

    <!-- ADD Div Subir Venue -->
    <script>
      $(function()
    {
    $(document).ready(function(){
        var buttonadd = '<a class="button button-md margin-top-10 text-center margin-bottom-30" id="btn-negro" href="#" title="Conoce Venuescopia"><b>Agregar espacio</b></a>';
        var fvrhtmlclone = '<div class="fvrclonned">'+$(".fvrduplicate").html()+buttonadd+'</div>';
        $( ".fvrduplicate" ).html(fvrhtmlclone);
        $( ".fvrduplicate" ).after('<div class="fvrclone"></div>');

        $(document).on('click', '.btn-add', function(e)
        {
            e.preventDefault();

            $( ".fvrclone" ).append(fvrhtmlclone);
                  $(this).removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('Eliminar campo');

        }).on('click', '.btn-remove', function(e)
        {
            $(this).parents('.fvrclonned').remove();

        e.preventDefault();
        return false;
      });

    });

    // FUNÇÂO DE TESTE
    function showValues() {
    var fields = $( ":input" ).serializeArray();
    $( "#results" ).empty();
    jQuery.each( fields, function( i, field ) {
      $( "#results" ).append( field.value + " " );
    });
    }

    $( "form" ).submit(function( event ) {
      showValues();
      console.log( $( this ).serializeArray() );
      event.preventDefault();
    });

    });
    </script>

    <script>
      $('.next').click(function(){
      var nextId = $(this).parents('.tab-pane').next().attr("id");
      $('[href=#'+nextId+']').tab('show');
      return false;

    })

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      //update progress
      var step = $(e.target).data('step');
      var percent = (parseInt(step) / 6) * 100;

      $('.progress-bar').css({width: percent + '%'});
      $('.progress-bar').text("Paso " + step + " de 6");

      //e.relatedTarget // previous tab

    })

    $('.first').click(function(){

      $('#myWizard a:first').tab('show')

    })
    </script>

    <!-- Sticky contact venue -->
    <script>
    window.onscroll = function() {myFunction()};
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
    </script>

  </body>
</html>
