<!-- Home section -->
    <div class="section-fullscreen bg-image" style="background-image: url(<?= base_url() ?>theme/assets/images/BG/background-anuncia.jpg)" id="header-height">
        <div class="container">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-10 offset-md-1 col-lg-6 offset-lg-3">
                <div class="bg-white contenedor-header-titulo2 text-center">
                  <h2 class="text-uppercase titulos-general font-montserrat margin-bottom-20 text-responsive"><b>Tu venue a la vista de todos</b></h2>
                  <p>Porque el futuro está aquí y en Venuescopia, todos pueden encontrarte con un solo click.</p>
                  <a class="button button-lg margin-top-30 text-center" id="btn-negro" href="<?= base_url().'publicar.html' ?>" title="Publica tu Venue"><b>Anuncia tu Venue</b></a>
                </div>
              </div>
            </div><!-- end row -->
          </div>
        </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <div class="section">
      <div class="container">

        <div class="row align-items-end section-title-interna margin-bottom-50">
          <div class="col-12">
            <h2 class="text-uppercase titulos-general2 font-montserrat"><b>Nuestros Planes</b></h2>
            <div class="subtitulos-general2">
              <p>Elige el plan ideal para ti y anuncia tu venue en nuestra plataforma.</p>
            </div>
          </div>
        </div>

        <div class="row margin-bottom-10">
          <?php foreach($this->elements->membresias()->result() as $m): ?>
	          <div class="col-12 col-lg-4">
	            <div class="prices-box bg-white beneficios-anunciar anuncia-precios">
	              <div class="price-features">
  	              <h3 class="font-montserrat margin-bottom-20">Costo por Venue</h3>
  	              <div class="price"><h1 class="font-montserrat"><?= $m->precio ?><span>/<?= $m->nombre ?></span></h1></div>
	              </div>
	              <p><?= $m->descripcion ?></p>
	              <div class="ahorro-anuncia"><?= $m->accion ?></div>
	            </div>
	          </div>
      	  <?php endforeach ?>
        </div>

        <div class="row align-items-end section-title-interna">
          <div class="col-12" style="margin-bottom: 0px;">
            <div class="subtitulos-general2">
              <p>Queremos que el lugar de tus sueños deje de ser un sueño y se convierta en realidad. Queremos que el espacio mágico que tienes sea descubierto.</p>
            </div>
          </div>
        </div>

        <div class="row margin-bottom-20">
          <div class="col-12 col-sm-12 text-center">
              <a class="button button-lg text-center" id="btn-negro" href="<?= base_url() ?>publicar.html" title="Publica tu Venue"><b>Anuncia tu Venue</b></a>
          </div>
        </div><!-- end row -->

        <div class="row align-items-end section-title-interna">
          <div class="col-12">
            <div class="subtitulos-general2">
              <p>Qué obtienes al contratar una publicación con nosotros:</p>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-lg-6">
            <ul>
              <li>Contacto directo de clientes interesados.</li>
              <li>Visibilidad 24/7.</li>
              <li>Publicación editable en cualquier momento y desde cualquier lugar.</li>
              <li>Conteo de número de visitas a tu publicación.</li>
            </ul>
          </div>
          <div class="col-12 col-lg-6">
            <ul>
              <li>Descripción del espacio y listado de amenidades.</li>
              <li>Hasta 10 imágenes por publicación.</li>
              <li>Opción de carga de distintos espacios dentro de un mismo venue.</li>
              <li>Tráfico de personas interesadas en ranta de espacios.</li>
              <li>Ubicación en Google Maps dentro de la publicación.</li>
            </ul>
          </div>
        </div><!-- end row -->

      </div><!-- end container -->
    </div>


    <div class="section">
      <div class="container">

        <div class="row align-items-end section-title-interna margin-bottom-50">
          <div class="col-12">
            <h2 class="text-uppercase titulos-general2 font-montserrat"><b>Nuevo, fácil, único.</b></h2>
            <div class="subtitulos-general2">Porque somos los primeros y llegamos para quedarnos.</div>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-height">
              <img src="<?= base_url() ?>theme/assets/images/iconos/about-02.png" alt="Beneficios de anunciar tu Venue con Venuescopia" class="img-beneficios-anuncia">
              <h3 class="font-montserrat margin-bottom-20">Todos los ojos en ti</h3>
              <div class="price-features"><p>Tu venue al alcance de todos los momentos, en cualquier lugar.</p></div>
            </div>
          </div>

          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-height">
              <img src="<?= base_url() ?>theme/assets/images/iconos/about-01.png" alt="Beneficios de anunciar tu Venue con Venuescopia" class="img-beneficios-anuncia">
              <h3 class="font-montserrat margin-bottom-20">Tic-Toc</h3>
              <div class="price-features"><p>Crea una publicación en cuestión de minutos</p></div>
            </div>
          </div>

          <div class="col-12 col-lg-4">
            <div class="prices-box bg-white beneficios-anunciar anuncia-height">
              <img src="<?= base_url() ?>theme/assets/images/iconos/about-03.png" alt="Beneficios de anunciar tu Venue con Venuescopia" class="img-beneficios-anuncia">
              <h3 class="font-montserrat margin-bottom-20">Inteligente y económico</h3>
              <div class="price-features"><p>Anunciarte con nosotros es una inersión inteligente y económica.</p></div>
            </div>
          </div>
        </div><!-- end row -->

      </div><!-- end container -->
    </div>
