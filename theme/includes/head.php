<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="keywords" content="">
<title>Venuescopia</title>
<!-- Favicon -->
<link href="../assets/images/favicon.png" rel="shortcut icon">
<!-- CSS -->
<link href="assets/plugins/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
<link href="assets/plugins/owl-carousel/owl.theme.default.min.css" rel="stylesheet">
<link href="assets/plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
<link href="assets/css/app.css" rel="stylesheet">
<!-- Main -->
<link href="assets/css/main.css" rel="stylesheet">
<link href="assets/css/responsive.css" rel="stylesheet">
<!-- Font -->
<link rel="stylesheet" type="text/css" href="assets/font/avenir/style.css"/>
<!-- Fonts/Icons -->
<link href="assets/plugins/font-awesome/css/all.css" rel="stylesheet">
<link href="assets/plugins/themify/themify-icons.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,800,900,900i" rel="stylesheet"> 

<!--[if lt IE 9]>
<script src="/js/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<![endif]-->

<!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!--[if lt IE 9]>
 <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->






<!--[if lt IE 9]>
    <script src="bower_components/html5shiv/dist/html5shiv.js"></script>
<![endif]-->