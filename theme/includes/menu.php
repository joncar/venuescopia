<header>
  <!-- navbar
  <nav class="navbar navbar-absolute navbar-fixed">
    <div class="container">
      <a class="navbar-brand" href="index.php">
        <img src="assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia">
      </a>
      <ul class="nav dropdown-dark">
        <li class="nav-item"><a class="nav-link" href="index.php"><b>INICIO</b></a></li>
        <li class="nav-item"><a class="nav-link" id="btn-top-login" href="anuncia.php">ANUNCIA TU VENUE</a></li>
        <li class="nav-item"><a class="nav-link" href="about.php">ABOUT</a></li>
        <li class="nav-item"><a class="nav-link" href="faq.php">FAQ</a></li>
        <li class="nav-item"><a class="nav-link" href="membresias.php">MEMBRESIAS</a></li>
        <li class="nav-item"><a class="nav-link" href="contacto.php">CONTACTO</a></li>
        <li class="nav-item"><a class="nav-link" id="btn-top-login2" href="login.php">INICIAR SESIÓN</a></li>
        <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-facebook-square"></i></a></li>
        <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-instagram"></i></a></li>
      </ul>

      <button class="nav-toggle-btn">
          <span class="lines"></span>
      </button>
    </div>
  </nav>-->

  <!-- Menu Desktop -->
  <nav class="navbar navbar-absolute navbar-fixed d-none d-md-block">
      <div class="container">

          <a class="navbar-brand" href="index.php"><img src="assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia"></a>

          <ul class="nav dropdown-dark" id="nav-main">
            <li class="nav-item" id="bold-menu"><a class="nav-link font-montserrat" href="index.php">HOME</a></li>
            <li class="nav-item"><a class="nav-link" id="btn-top-login" href="anuncia.php" style="font-family: 'Avenir LT Std 35 Light';">ANUNCIA TU VENUE</a></li>
            <li class="nav-item"><a class="nav-link" href="about.php" style="font-family: 'Avenir LT Std 35 Light';">ABOUT</a></li>
            <li class="nav-item"><a class="nav-link" href="faq.php" style="font-family: 'Avenir LT Std 35 Light';">FAQ</a></li>
            <li class="nav-item"><a class="nav-link" href="contacto.php" style="font-family: 'Avenir LT Std 35 Light';">CONTACTO</a></li>
            <li class="nav-item"><a class="nav-link" id="btn-top-login2" href="login.php" style="font-family: 'Avenir LT Std 35 Light';">LOGIN / SIGN UP</a></li>
          </ul>

          <ul class="list-horizontal-unstyled margin-nav" id="redes-top">
            <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-facebook-square"></i></a></li>
            <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-instagram"></i></a></li>
          </ul>

      </div>
  </nav>

  <!-- Menu Movil -->
  <nav class="navbar navbar-absolute navbar-fixed d-block d-sm-none" id="menu-movil">
    <div class="container">
      <a class="navbar-brand" href="index.php">
        <img src="assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia">
      </a>
      <ul class="nav dropdown-dark">
        <li class="nav-item"><a class="nav-link" href="index.php"><b>HOME</b></a></li>
        <li class="nav-item"><a class="nav-link" id="btn-top-login" href="anuncia.php">ANUNCIA TU VENUE</a></li>
        <li class="nav-item"><a class="nav-link" href="about.php">ABOUT</a></li>
        <li class="nav-item"><a class="nav-link" href="faq.php">FAQ</a></li>
        <li class="nav-item"><a class="nav-link" href="contacto.php">CONTACTO</a></li>
        <li class="nav-item"><a class="nav-link" id="btn-top-login2" href="login.php">LOGIN / SIGN UP</a></li>
        <li class="nav-item">
          <a href="#" target="blank" class="redes-menu-movil"><i class="fab fa-facebook-square"></i></a>
          <a href="#" target="blank" class="redes-menu-movil"><i class="fab fa-instagram"></i></a>
        </li>
      </ul>

      <button class="nav-toggle-btn">
          <span class="lines"></span>
      </button>
    </div>
  </nav>

</header>

<!-- Scroll to top button -->
<div class="scrolltotop">
  <a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="fas fa-chevron-up"></i></a>
</div>
<!-- end Scroll to top button -->
