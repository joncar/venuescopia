<a href="venues-interna.php">
  <h5 class="text-uppercase font-montserrat"><b>ST. REGIS hotel</b></h5>
  <div class="text-general-p text-resultas-movil text-responsive">Reforma, Ciudad de México.</div>
  <!-- Desktop -->
  <div class="text-general-p font-montserrat font-weight-bold precio-resultados d-none d-sm-block">
      <img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:5%;"> 50-250 personas &nbsp
      <img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:5%;"> $100,000 - $200,000
  </div>
  <!-- Movil -->
  <div class="text-general-p font-montserrat font-weight-bold precio-resultados text-center d-block d-sm-none">
      <img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:7%;"> 50-250 personas<br>
      <img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:7%;"> $100,000 - $200,000
  </div>
</a>
