<!-- Destacadas -->
<div class="container">

  <div class="section-title section-home">
    <div class="row">
      <div class="col-12 col-md-6">
        <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Recomendaciones</b></h2>
        <div class="subtitulos-general text-responsive">¡Más venues para tí!</div>
      </div>
      <div class="col-12 col-md-6 link-todos text-responsive2">
      </div>
    </div>
  </div><!-- end section-title -->

  <!-- Propiedades destacadas -->
  <div class="row">
    <!-- Slider -->
    <div class="col-12">
        <!-- Testimonial section -->
        <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true" >

          <!-- Testimonial box 1 -->
          <div class="border-radius bg-white">
            <a href="venues-interna.php" title="Ver Venue">
              <div class="mx-auto d-bloc imagen-venues-home">
                <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
              </div>
            </a>
            <div class="favoritos-home" style="text-align: right;">
                <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
            </div>
            <div class="titulo-venue-home">
              <?php include('includes/venues/precios-venues.php');?>
            </div>
          </div>

          <!-- Testimonial box 1 -->
          <div class="border-radius bg-white">
            <a href="venues-interna.php" title="Ver Venue">
              <div class="mx-auto d-bloc imagen-venues-home">
                <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
              </div>
            </a>
            <div class="favoritos-home" style="text-align: right;">
                <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
            </div>
            <div class="titulo-venue-home">
              <?php include('includes/venues/precios-venues.php');?>
            </div>
          </div>

          <!-- Testimonial box 1 -->
          <div class="border-radius bg-white">
            <a href="venues-interna.php" title="Ver Venue">
              <div class="mx-auto d-bloc imagen-venues-home">
                <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
              </div>
            </a>
            <div class="favoritos-home" style="text-align: right;">
                <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
            </div>
            <div class="titulo-venue-home">
              <?php include('includes/venues/precios-venues.php');?>
            </div>
          </div>

          <!-- Testimonial box 1 -->
          <div class="border-radius bg-white">
            <a href="venues-interna.php" title="Ver Venue">
              <div class="mx-auto d-bloc imagen-venues-home">
                <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
              </div>
            </a>
            <div class="favoritos-home" style="text-align: right;">
                <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
            </div>
            <div class="titulo-venue-home">
              <?php include('includes/venues/precios-venues.php');?>
            </div>
          </div>

        </div><!-- end owl-carousel -->
    </div>
  </div><!-- end row -->

</div>
<!-- Destacadas -->
