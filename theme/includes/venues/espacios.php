<div class="section-title-interna3 margin-top-30 margin-bottom-30">
  <div class="row">
    <div class="col-12">
      <h2 class="text-uppercase font-montserrat"><b>Espacio 1</b></h2>
    </div>
  </div>
</div>


<div class="row margin-bottom-30">

  <!-- Product box -->
  <div class="col-12 col-lg-4 product-single margin-bottom-50" id="product-single">
    <div class="product-single-img">
      <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
        <div class="owl-stage-outer owl-height" style="height: 158px;">
          <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
            <div class="owl-item active">
              <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
            </div>
            <div class="owl-item">
              <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
            </div>
            <div class="owl-item">
              <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
            </div>
          </div>
        </div>

        <div class="owl-nav">
          <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
          <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
        </div>
      </div>
    </div>
    <!-- end product-img -->

    <!--<div class="favoritos-home2" style="text-align: right;">
        <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
    </div>-->

    <div class="titulo-venue-home">
      <?php include('includes/venues/precios-venues.php');?>
    </div>
  </div>


  <div class="col-12 col-lg-8 margin-bottom-50">
      <p>
        El hotel más importante de México. Contamos con 3 salones distintos para eventos privados. Somos un venue de calidad, tradición y vanguardia.<br>
        Ubicado en la avenida más importante de la capital del país, tener un evento en nuestras instalaciones es estar en el corazón de lo más bello de esta ciudad.
        Ofrecemos distintos precios, paquetes y opciones para que celebres con nosotros los eventos más importantes de tu vida.<br>
        Con más de 50 años de experiencia somos el primer destino del turismo de primer nivel y nuestros servicios son de la más alta calidad.
      </p>
  </div>
</div>
