<!--
<header>

  <nav class="navbar navbar-absolute navbar-fixed">
    <div class="container">
      <a class="navbar-brand" href="index.php">
        <img src="assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia">
      </a>
      <ul class="nav dropdown-dark">
        <li class="nav-item" id="bold-menu"><a class="nav-link font-montserrat" href="index.php">HOME</a></li>
        <li class="nav-item"><a class="nav-link" id="btn-top-login" href="anuncia.php" style="font-family: 'Avenir LT Std 35 Light';">ANUNCIA TU VENUE</a></li>
        <li class="nav-item"><a class="nav-link" href="about.php" style="font-family: 'Avenir LT Std 35 Light';">ABOUT</a></li>
        <li class="nav-item"><a class="nav-link" href="faq.php" style="font-family: 'Avenir LT Std 35 Light';">FAQ</a></li>
        <li class="nav-item"><a class="nav-link" href="contacto.php" style="font-family: 'Avenir LT Std 35 Light';">CONTACTO</a></li>
        <li class="nav-item"><a class="nav-link" href="membresias.php" style="font-family: 'Avenir LT Std 35 Light';">PLANES</a></li>


        <li class="nav-item nav-dropdown">
          <a class="nav-link redes-menu" href="#"><i class="fas fa-user-circle"></i></a>
          <ul class="dropdown-menu" style="margin-top: -15px;">
            <div class="texto-blanco font-montserrat nombre-menu"><b>Hola, María</b></div>
            <li><a href="perfil-propietarios.php">Mi Perfil</a></li>
            <li><a href="editar-perfil.php">Editar Perfil</a></li>
            <li><a href="mis-venues.php">Mis Venues</a></li>
            <li><a href="index.php">Salir <i class="fas fa-sign-in-alt"></i></a></li>
          </ul>
        </li>

        <li class="nav-item nav-dropdown">
          <a class="nav-link redes-menu" href="#"><i class="far fa-bell" style="font-size: 18px;"></i></a>
          <ul class="dropdown-menu" style="margin-top: -15px;">
            <li><a href="notificaciones.php">Notificación</a></li>
            <li><a href="notificaciones.php">Notificación</a></li>
            <li><a href="notificaciones.php">Notificación</a></li>
            <li><a href="notificaciones.php">Notificación</a></li>
            <li><a href="notificaciones.php">Ver todas las notificaciones</a></li>
          </ul>
        </li>

        <li class="nav-item"><a class="nav-link redes-menu" href="favoritos.php"><i class="far fa-heart" style="font-size: 18px;"></i></a></li>


        <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-facebook-square"></i></a></li>
        <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-instagram"></i></a></li>
      </ul>

      <button class="nav-toggle-btn">
              <span class="lines"></span>
          </button>
    </div>
  </nav>
</header> --->

<header>

  <!-- Menu Desktop -->
  <nav class="navbar navbar-absolute navbar-fixed hd-none d-sm-block">
      <div class="container">
          <a class="navbar-brand" href="index.php"><img src="assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia"></a>
          <ul class="nav dropdown-dark" id="nav-main">
            <li class="nav-item" id="bold-menu"><a class="nav-link font-montserrat" href="index.php">HOME</a></li>
            <li class="nav-item"><a class="nav-link" id="btn-top-login" href="anuncia.php" style="font-family: 'Avenir LT Std 35 Light';">ANUNCIA TU VENUE</a></li>
            <li class="nav-item"><a class="nav-link" href="about.php" style="font-family: 'Avenir LT Std 35 Light';">ABOUT</a></li>
            <li class="nav-item"><a class="nav-link" href="faq.php" style="font-family: 'Avenir LT Std 35 Light';">FAQ</a></li>
            <li class="nav-item"><a class="nav-link" href="contacto.php" style="font-family: 'Avenir LT Std 35 Light';">CONTACTO</a></li>
            <li class="nav-item nav-dropdown">
              <a class="nav-link" href="#" style="font-family: 'Avenir LT Std 35 Light';">Hola María</a>
              <ul class="dropdown-menu" style="margin-top: -15px;">
                <li><a href="membresia-propietarios.php" style="padding:10px;" class="font-weight-normal">Membresía</a></li>
                <li><a href="mis-venues.php" style="padding:10px;" class="font-weight-normal">Mis Venues</a></li>
                <li><a href="favoritos.php" style="padding:10px;" class="font-weight-normal">Favoritos</a></li>
                <li><a href="index.php" style="padding:10px;" class="font-weight-normal">Salir <i class="fas fa-sign-in-alt"></i></a></li>
              </ul>
            </li>
          </ul>

          <ul class="list-horizontal-unstyled margin-nav" id="redes-top">
            <li class="nav-item"><a class="nav-link redes-menu" href="notificaciones.php"><i class="far fa-envelope" style="font-size: 22px;"></i> <span class="badge badge-dark">4</span></a></li>
            <li class="nav-item"><a class="nav-link redes-menu" href="favoritos.php"><i class="far fa-heart" style="font-size: 22px;"></i></a></li>
            <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-facebook-square"></i></a></li>
            <li class="nav-item"><a class="nav-link redes-menu" href="#" target="blank"><i class="fab fa-instagram"></i></a></li>
          </ul>
      </div>
  </nav>

  <!-- Menu Movil -->
  <nav class="navbar navbar-absolute navbar-fixed d-block d-sm-none">
    <div class="container">
      <a class="navbar-brand" href="index.php">
        <img src="assets/images/icon-venuescopia-menu.png" class="mx-auto d-block" alt="Logo Venuescopia">
      </a>
      <ul class="nav dropdown-dark">
        <li class="nav-item"><a class="nav-link"><b>Hola María</b></a></li>
        <li class="nav-item"><a class="nav-link" href="membresia-propietarios.php"><b>MEMBRESÍA</b></a></li>
        <li class="nav-item"><a class="nav-link" href="mis-venues.php"><b>VENUES</b></a></li>
        <li class="nav-item"><a class="nav-link" href="favoritos.php"><b>FAVORITOS</b></a></li>
        <li class="nav-item"><a class="nav-link" href="index.php"><b>HOME</b></a></li>
        <li class="nav-item"><a class="nav-link" id="btn-top-login" href="anuncia.php">ANUNCIA TU VENUE</a></li>
        <li class="nav-item"><a class="nav-link" href="about.php">ABOUT</a></li>
        <li class="nav-item"><a class="nav-link" href="faq.php">FAQ</a></li>
        <li class="nav-item"><a class="nav-link" href="contacto.php">CONTACTO</a></li>
        <li class="nav-item">
          <a href="notificaciones.php" target="blank" class="redes-menu-movil2"><i class="far fa-envelope"></i> <span class="badge badge-dark">4</span></a>
          <a href="favoritos.php" target="blank" class="redes-menu-movil2"><i class="far fa-heart"></i></a>
          <a href="#" target="blank" class="redes-menu-movil2"><i class="fab fa-facebook-square"></i></a>
          <a href="#" target="blank" class="redes-menu-movil2"><i class="fab fa-instagram"></i></a>
          <a href="index.php" target="blank" class="redes-menu-movil2"><i class="fas fa-sign-in-alt"></i></a>
        </li>
      </ul>

      <button class="nav-toggle-btn">
          <span class="lines"></span>
      </button>
    </div>
  </nav>


</header>

<!-- Scroll to top button -->
<div class="scrolltotop">
  <a class="button-circle button-circle-sm button-circle-dark" href="#"><i class="fas fa-chevron-up"></i></a>
</div>
<!-- end Scroll to top button -->
