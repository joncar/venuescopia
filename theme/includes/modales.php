<!-- Modal Inicio
<div class="modal fade" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
      </div>
      <div class="modal-body text-center">
          <div class="font-montserrat modal-inicio">NEWSLETTER</div>
          <div class="font-avenir subtitulo-modal-inicio">NUEVOS VENUES Y NADA MÁS.</div>

          <form class="col-12 form-inline margin-top-50 no-padding">
            <div class="form-row margin-bottom-50">
              <div class="col-12 col-sm-11" style="padding-right: 0px;">
                <input type="text" id="name" name="name" placeholder="Correo electrónico" required="" style="border: black solid 1px; padding: 9px 10px;">
              </div>
              <div class="col-12 col-sm-1" style="padding-left: 0px;">
                <a class="button button-sm text-center" id="btn-negro" href="#" style="padding: 7px 20px;"><b>Enviar</b></a>
              </div>
            </div>
          </form>

      </div>
    </div>
  </div>
</div>
Modal Inicio -->

<!-- Login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-12 col-sm-12">
            <ul class="nav justify-content-center margin-bottom-30">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tab-login"><h6 class="heading-uppercase">Login</h6></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tab-register"><h6 class="heading-uppercase">Regístrate</h6></a>
              </li>
            </ul>
            <div class="tab-content">
              <!-- Login Tab content -->
              <div class="tab-pane fade show active" id="tab-login">

                <h2 class="text-uppercase titulos-general-modal font-montserrat text-center margin-bottom-20"><b>BIENVENIDO A CASA</b></h2>

                <form>
                  <input type="text" placeholder="Usuario (correo o contraseña)" name="name" required class="margin-bottom-20">
                  <input type="password" placeholder="Contraseña" name="pw" required class="margin-bottom-20">
                </form>

                <div class="col-12 text-center" style="margin-bottom: 0px;">
                  <a class="button button-lg text-center margin-bottom-10" id="btn-negro" href="index.php"><b>Login</b></a>
                </div>

                <div class="text-center" id="link-olvide-contrasena">
                    <a data-toggle="modal" data-target="#recuperar">OLVIDÉ MI CONTRASEÑA</a>
                </div>

              </div>
              <!-- Register Tab content -->
              <div class="tab-pane fade" id="tab-register">
                <h2 class="text-uppercase titulos-general-modal font-montserrat text-center margin-bottom-20">
                  <b>¿Eres nuevo?</b><br>
                  <b>BIENVENIDO A BORDO</b>
                </h2>

                <form>
                  <input type="text" placeholder="Nombre" name="name" required class="margin-bottom-30">
                  <input type="text" placeholder="Apellidos" name="name" required class="margin-bottom-30">
                  <input type="text" placeholder="Correo electrónico" name="email" required class="margin-bottom-30">
                  <input type="password" placeholder="Contraseña" name="pw" required class="margin-bottom-30">
                  <input type="password" placeholder="Confirmar contraseña" name="pw2" required class="margin-bottom-20">
                </form>

                <div class="text-center">
                    <a href="index.php">Crear cuenta</a><br>
                    <a href="terminos-condiciones.php">Consulta nuestros Términos y Condiciones</a>
                </div>

              </div>
            </div>
          </div>
        </div><!-- end row -->

      </div>
    </div>
  </div>
</div>

<!-- Recuperar contraseña -->
<div class="modal fade" id="recuperar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
        <div class="margin-bottom-30">
          <p class="text-center">Ingresa tu correo electrónico y recibirás instrucciones para recuperar tu contraseña.</p>
        </div>

        <form>
          <input type="text" placeholder="Correo" name="name" required="" class="margin-bottom-10">
        </form>

        <div class="col-12 text-center">
          <a class="button button-md text-center montserrat font-weight-bold" id="btn-negro" href="perfil-propietarios.php" title="Conoce Venuescopia">Recuperar contraseña</a>
        </div>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>-->
    </div>
  </div>
</div>

<!-- Telefono venue -->
<div class="modal fade" id="telefono" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body text-center">
          <span class="font-montserrat" style="font-size:14px; letter-spacing: 3px;"><b>ST. REGIS HOTEL</b></span><br>
          <div class="telefono-modal margin-top-20"><i class="fas fa-phone"></i> 55 54 67 88</div>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button type="button" class="btn" id="btn-negro">Aceptar</button>
      </div>-->
    </div>
  </div>
</div>

<!-- Share venue -->
<div class="modal fade" id="share" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body text-center">
          <div class="col-12">
              DESCUBRÍ ESTE LUGAR EN VENUESCOPIA:<br>
              <span class="font-montserrat"><b>ST. REGIS HOTEL</b></span><br>
              Reforma, Ciudad de México.
          </div>

          <div class="col-12 margin-top-20">
              <ul class="list-horizontal-unstyled">
                <li><a href="#"><i class="fas fa-envelope"></i></a></li>
                <li><a href="#"><i class="fab fa-whatsapp"></i></a></li>
                <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fas fa-share-alt"></i></a></li>
              </ul>
          </div>
      </div>
      <!--
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button type="button" class="btn" id="btn-negro">Aceptar</button>
      </div>-->
    </div>
  </div>
</div>

<!-- FAQ -->
<div class="modal fade" id="faq" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body text-center">

        <div class="contact-form">
          <!--<p class="text-center margin-bottom-20">¿Cómo podemos ayudarte?</p>-->

          <h2 class="text-uppercase titulos-general-modal font-montserrat margin-bottom-20"><b>¿Cómo podemos ayudarte?</b></h2>

          <form method="post" id="contactform">
            <div class="form-row">
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="text" id="name" name="name" placeholder="Nombre*" required style="border: #0000000 solid 2px;">
              </div>
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="email" id="email" name="email" placeholder="Correo Electrónico*" required style="border: #0000000 solid 2px;">
              </div>
            </div>
            <textarea name="message" id="message" placeholder="Mensaje*" style="border: #0000000 solid 2px; height: 150px; resize: none;"></textarea>
          </form>

          <div class="col-12 text-center">
            <a class="button button-md text-center montserrat font-weight-bold" id="btn-negro" href="contacto-envio.php" title="Enviar mis comentarios" style="width: 100%;"><b>Enviar</b></a>
          </div>

          <!-- Submit result -->
          <div class="submit-result">
            <span id="success">Thank you! Your Message has been sent.</span>
            <span id="error">Something went wrong, Please try again!</span>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Eliminar favoritos -->
<div class="modal fade" id="eliminar-favoritos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">¿Eliminar de la lista de tus favoritos?</p>
      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<!-- Pausar Publicación -->
<div class="modal fade" id="pausar-publicacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Deseas pausar esta publicación?<br>
            Puedes eliminar este Venue y reactivarlo en cuanto lo prefieras. - Descripción de la pausa
          </p>
      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<!-- Eliminar Publicación -->
<div class="modal fade" id="eliminar-publicacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Eliminar este Venue?<br>
            Puedes eliminar este Venue y reactivarlo en cuanto lo prefieras.<br>
            No hay reembolso en caso de que quieras cancelar tu suscripción. - Descripción de la eliminación
          </p>
      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<!-- Editar Publicación -->
<div class="modal fade" id="editar-publicacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
        </div>
      </div>
      <div class="modal-body">

          <p class="text-center">
            Editar Venue
          </p>

          <form method="post" id="contactform">
            <div class="form-row">
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="text" id="name" name="name" placeholder="Nombre*" required style="border: #0000000 solid 2px;">
              </div>
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="email" id="email" name="email" placeholder="Precio*" required style="border: #0000000 solid 2px;">
              </div>
              <div class="col-12 col-sm-12 margin-bottom-20">
                <input type="email" id="email" name="email" placeholder="Personas*" required style="border: #0000000 solid 2px;">
              </div>
              <div class="col-12 col-sm-12 margin-bottom-20">
                  <label>Cambiar forma de Pago</label>
                  <select class="custom-select">
                    <option value="1">Anual</option>
                    <option value="1">Mensual</option>
                    <option value="1">Trimestral</option>
                  </select>
              </div>
            </div>
          </form>

      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<!-- Ver Solicitud -->
<div class="modal fade" id="ver-solicitud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            <b>Nombre:</b> Nombre del Solicitante<br>
            <b>Rango de Precio:</b> No. <br>
            <b>Rango de Personas:</b> No.
          </p>
      </div>
      <div class="modal-footer">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<!-- Update credit card -->
<div class="modal fade" id="update-card" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">

          <div class="col-12 col-sm-12">
              <div class="contact-form">
                <form method="post" id="contactform">
                  <div class="form-row">
                    <div class="col-12 col-sm-12 margin-bottom-30">
                      <input type="text" id="name" name="name" placeholder="Número de tarjeta" required="">
                    </div>
                    <div class="col-12 col-sm-6 margin-bottom-30">
                      <input type="text" id="name" name="name" placeholder="MM" required="">
                    </div>
                    <div class="col-12 col-sm-6 margin-bottom-30">
                      <input type="text" id="name" name="name" placeholder="YY" required="">
                    </div>
                  </div>
                </form>
              </div>
          </div>

          <div class="col-12 col-sm-12" style="margin-bottom: 0px;">
              <div class="contact-form">
                <form method="post" id="contactform">
                  <div class="form-row">
                    <div class="col-12 col-sm-12 margin-bottom-30">
                      <input type="text" id="name" name="name" placeholder="Nombre de la tarjeta" required="">
                    </div>
                    <div class="col-12 col-sm-6">
                      <input type="text" id="name" name="name" placeholder="CVC" required="">
                    </div>
                    <div class="col-12 col-sm-6">
                      <input type="text" id="name" name="name" placeholder="ZIP" required="">
                    </div>
                  </div>
                </form>
              </div>
          </div>

      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>

<!-- Cancelar suscripción -->
<div class="modal fade" id="cancelar-suscripcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">
          <p class="text-center">
            ¿Seguro deseas cancelar tu suscripción?<br>
            La cancelación comienza desde que termine tu corte de pago
          </p>
      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>


<!-- Update suscripción -->
<div class="modal fade" id="update-suscripcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">

          <div class="row">
          <div class="col-12 col-lg-12">

              <div class="product-tab">
              <ul class="nav margin-bottom-20">
                <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#opcion-suscripcion1"><h5 class="font-weight-light">Por año</h5></a></li>
                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#opcion-suscripcion2"><h5 class="font-weight-light">Por Mes</h5></a></li>
              </ul>

              <div class="tab-content">

                <!-- Reviews tab content -->
                <div class="tab-pane fade active show" id="opcion-suscripcion1">
                    <div class="row">

                        <div class="col-12 col-md-12">
                          <div class="contact-form">
                            <form method="post" id="contactform">
                              <input type="text" id="subject" name="subject" placeholder="Código de promoción" required="" class="margin-bottom-30">
                              <a href="contacto-envio.php"><button class="button button-xl button-dark" type="submit">Aplicar código</button></a>
                            </form>
                            <!-- Submit result -->
                            <div class="submit-result">
                              <span id="success">Thank you! Your Message has been sent.</span>
                              <span id="error">Something went wrong, Please try again!</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-12 col-md-12 margin-bottom-20">
                            <h6 class="heading-uppercase no-margin"><b>Detalles de la suscripción</b></h6>
                            Período Contratado: 00/00/0000<br>
                            Costo de la suscripción: $111.00/ por año
                        </div>

                        <div class="col-12 col-md-12 margin-bottom-20">
                            <h6 class="heading-uppercase no-margin"><b>Detalles de la compra</b></h6>
                            Nombre: Nombre del Propietario<br>
                            Número de Tarjeta: XXXX XXXX XXXXX<br>
                            Tipo de tarjeta: XXXXXXXXXXXX
                        </div>

                        <div class="col-12 col-md-12 margin-bottom-20 text-center">
                            <a href="#"><i class="far fa-credit-card"></i> Editar forma de pago</a>
                        </div>

                    </div><!-- end row -->
                </div>

                <!-- Reviews tab content -->
                <div class="tab-pane fade" id="opcion-suscripcion2">
                    <div class="row">

                        <div class="col-12 col-md-12">
                          <div class="contact-form">
                            <form method="post" id="contactform">
                              <input type="text" id="subject" name="subject" placeholder="Código de promoción" required="" class="margin-bottom-30">
                            </form>

                            <div class="col-12 text-center">
                              <a href="contacto-envio.php"><button class="button button-xl button-dark" type="submit">Aplicar código</button></a>
                            </div>

                            <!-- Submit result -->
                            <div class="submit-result">
                              <span id="success">Thank you! Your Message has been sent.</span>
                              <span id="error">Something went wrong, Please try again!</span>
                            </div>
                          </div>
                        </div>

                        <div class="col-12 col-md-12 margin-bottom-20">
                            <h6 class="heading-uppercase no-margin"><b>Detalles de la suscripción</b></h6>
                            Período Contratado: 00/00/0000<br>
                            Costo de la suscripción: $111.00/ por mes
                        </div>

                        <div class="col-12 col-md-12 margin-bottom-20">
                            <h6 class="heading-uppercase no-margin"><b>Detalles de la compra</b></h6>
                            Nombre: Nombre del Propietario<br>
                            Número de Tarjeta: XXXX XXXX XXXXX<br>
                            Tipo de tarjeta: XXXXXXXXXXXX
                        </div>

                        <div class="col-12 col-md-12 margin-bottom-20 text-center">
                            <a href="#"><i class="far fa-credit-card"></i> Editar forma de pago</a>
                        </div>

                    </div><!-- end row -->
                </div>

              </div>
            </div>

          </div>
        </div>

      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cancelar</button>
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Aceptar</button>
      </div>

    </div>
  </div>
</div>


<!-- Cancelar suscripción -->
<div class="modal fade" id="venues-individuales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>
      <div class="modal-body">

        <div class="row margin-bottom-20">
          <img src="assets/images/venues-interna/venue-interna-05.jpg" alt="Propiedades Venuescopia"><br>
          <span class="font-montserrat text-uppercase"><b>Hotel ST. Regis - Centro de convenciones</b></span>
        </div>

        <div class="row margin-bottom-20">
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent purus risus, condimentum ut magna id, fringilla mattis nisi. Praesent non orci risus. Pellentesque habitant
            morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>
        </div>

      </div>

      <div class="modal-footer">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cerrar</button>
      </div>

    </div>
  </div>
</div>


<!-- Filtros de precio -->
<div class="modal fade" id="filtro-precio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title" id="exampleModalLabel">Precio y Capacidad</h5>
        </div>
      </div>

      <div class="modal-body">

        <!--
        <div class="row">
          <div class="col-12 col-lg-5 precio-modal font-montserrat"><b>$100 MXN</b></div>
          <div class="col-12 col-lg-2 text-center precio-minus"><i class="fas fa-minus"></i></div>
          <div class="col-12 col-lg-5 precio-modal font-montserrat"><b>$300 MXN</b></div>
        </div>

        <div class="row">
          <div class="range-slider">
            <input class="range-slider__range" type="range" value="100" min="0" max="500">
            <span class="range-slider__value">0</span>
          </div>

          <div class="range-slider">
            <input class="range-slider__range" type="range" value="250" min="0" max="500" step="50">
            <span class="range-slider__value">0</span>
          </div>
        </div>-->

        <div class="row">
            <div class="col-12 col-lg-5 no-padding">
                <div class="range-slider">
                  <input class="range-slider__range" type="range" value="100" min="0" max="100">
                  <div class="precio-modal font-montserrat">$<span class="range-slider__value">0</span>MXN</div>
                </div>
            </div>

            <div class="col-12 col-lg-2 text-center precio-minus no-padding"><i class="fas fa-minus"></i></div>

            <div class="col-12 col-lg-5 no-padding">
                <div class="range-slider">
                  <input class="range-slider__range" type="range" value="100" min="0" max="100">
                  <div class="precio-modal font-montserrat">$<span class="range-slider__value">0</span>MXN</div>
                </div>
            </div>
        </div>

      </div>

      <div class="modal-footer col-12 text-center">
        <button class="button button-md text-center montserrat font-weight-bold" id="btn-negro">Escoger</button>
      </div>

    </div>
  </div>
</div>

<!-- Amenidades Publicación -->
<div class="modal fade" id="amenidades" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="amenidades-modal">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>

      <div class="modal-body">
          <div class="row margin-bottom-20">Agrega todas las amenidades del Venue</div>

          <div class="row">
              <table class="table">
                <tbody>
                  <tr>
                    <td>
                        <input type="checkbox" id="box-01">
                        <label for="box-01" style="font-size:10px;">Catering / Servicio de Comida</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-02">
                        <label for="box-02" style="font-size:10px;">Sonido/Audio/Video</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-03">
                        <label for="box-03" style="font-size:10px;">Sanitarios</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-04">
                        <label for="box-04" style="font-size:10px;">Bar</label>
                    </td>
                  </tr>

                  <tr>
                    <td>
                        <input type="checkbox" id="box-05">
                        <label for="box-05" style="font-size:10px;">Mobiliario</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-06">
                        <label for="box-06" style="font-size:10px;">Internet (WiFi)</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-07">
                        <label for="box-07" style="font-size:10px;">Seguridad</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-08">
                        <label for="box-08" style="font-size:10px;">Encarpado</label>
                    </td>
                  </tr>

                  <tr>
                    <td>
                        <input type="checkbox" id="box-09">
                        <label for="box-09" style="font-size:10px;">Planta de Luz</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-10">
                        <label for="box-10" style="font-size:10px;">Aire Acondicionado</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-11">
                        <label for="box-11" style="font-size:10px;">Actividades</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-12">
                        <label for="box-12" style="font-size:10px;">Acceso Discapacitados</label>
                    </td>
                  </tr>

                  <tr>
                    <td>
                        <input type="checkbox" id="box-13">
                        <label for="box-13" style="font-size:10px;">Estacionamiento</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-14">
                        <label for="box-14" style="font-size:10px;">Áreas Verdes</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-15">
                        <label for="box-15" style="font-size:10px;">Alojamiento</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-16">
                        <label for="box-16" style="font-size:10px;">Limpieza</label>
                    </td>
                  </tr>

                  <tr>
                    <td>
                        <input type="checkbox" id="box-17">
                        <label for="box-17" style="font-size:10px;">Capilla</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-18">
                        <label for="box-18" style="font-size:10px;">Espacio para Ceremonia Civil / Religiosa</label>
                    </td>
                    <td>
                        <input type="checkbox" id="box-19">
                        <label for="box-19" style="font-size:10px;">Espacio para Cocktail Bienvenida</label>
                    </td>
                  </tr>
                </tbody>
              </table>
          </div><!-- Row -->
      </div>

      <div class="modal-footer text-responsive">
        <button class="button button-md montserrat font-weight-bold" id="btn-negro">Agregar amenidades</button>
      </div>

    </div>
  </div>
</div>

<!-- Amenidades Publicación -->
<div class="modal fade" id="amenidades-preview" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="amenidades-modal">
      <div class="modal-header">
        <div class="col-12">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" class="mx-auto d-block">
            <!--<h5 class="modal-title" id="exampleModalLabel">Reestablecer contraseña</h5>-->
        </div>
      </div>

      <div class="modal-body">

        <div class="row">

          <table class="table">
            <tbody>
              <tr>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-catering.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Catering</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-audio.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Sonido/Audio/Video</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-sanitarios.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Sanitarios</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-bar.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Bar</span>
                </td>
              </tr>

              <tr>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-mobiliario.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Mobiliario</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-wifi.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Internet (WiFi)</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-seguridad.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Seguridad</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-encarpado.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Encarpado</span>
                </td>
              </tr>

              <tr>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-luz.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Planta de Luz</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-aire.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Aire Acondicionado</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-actividades.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Actividades</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-discapacitados.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Acceso Discapacitados</span>
                </td>
              </tr>

              <tr>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-estacionamiento.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Estacionamiento</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-areas.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Áreas Verdes</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-alojamiento.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Alojamiento</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-limpieza.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Limpieza</span>
                </td>
              </tr>

              <tr>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-capilla.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Capilla</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-ceremonia.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Ceremonia Civil / Religiosa</span>
                </td>
                <td>
                    <img src="assets/images/iconos/amenidades/icono-bienvenida.png" alt="Amenidades Venues" style="width:10%;">
                    <span style="font-size:10px;">Cocktail Bienvenida</span>
                </td>
              </tr>
            </tbody>
          </table>

        </div><!-- end row -->

      </div>

      <div class="modal-footer text-center">
        <button class="button button-md text-center montserrat font-weight-bold" data-dismiss="modal" id="btn-negro">Cerrar</button>
      </div>

    </div>
  </div>
</div>


<!-- Funciones Slider Range Precio -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script type="text/javascript">
  var rangeSlider = function(){
  var slider = $('.range-slider'),
      range = $('.range-slider__range'),
      value = $('.range-slider__value');

  slider.each(function(){

    value.each(function(){
      var value = $(this).prev().attr('value');
      $(this).html(value);
    });

    range.on('input', function(){
      $(this).next(value).html(this.value);
    });
  });
};

rangeSlider();
</script>
<!-- Funciones Slider Range Precio -->








<!-- Estilos Selects -->
    <script>
    $('select').each(function(){
    var $this = $(this), numberOfOptions = $(this).children('option').length;

    $this.addClass('select-hidden');
    $this.wrap('<div class="select"></div>');
    $this.after('<div class="select-styled"></div>');

    var $styledSelect = $this.next('div.select-styled');
    $styledSelect.text($this.children('option').eq(0).text());

    var $list = $('<ul />', {
        'class': 'select-options'
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
        $('<li />', {
            text: $this.children('option').eq(i).text(),
            rel: $this.children('option').eq(i).val()
        }).appendTo($list);
    }

    var $listItems = $list.children('li');

    $styledSelect.click(function(e) {
        e.stopPropagation();
        $('div.select-styled.active').not(this).each(function(){
            $(this).removeClass('active').next('ul.select-options').hide();
        });
        $(this).toggleClass('active').next('ul.select-options').toggle();
    });

    $listItems.click(function(e) {
        e.stopPropagation();
        $styledSelect.text($(this).text()).removeClass('active');
        $this.val($(this).attr('rel'));
        $list.hide();
        //console.log($this.val());
    });

    $(document).click(function() {
        $styledSelect.removeClass('active');
        $list.hide();
    });

    });
    </script>
