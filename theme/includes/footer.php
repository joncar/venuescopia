<!-- Desktop  -->
<footer class="d-none d-sm-block">
  <div class="footer bg-dark-lighter text-uppercase">
    <div class="container" style="padding-left: 0px;padding-right: 0px;">
      <div class="row">
        <div class="col-12 col-sm-3 col-md-3 col-lg-3 logo-footer" style="padding-left: 0px;padding-right: 0px;">
          <a href="index.php"><img src="assets/images/icon-venuescopia-footer.png" alt="Logo Venuescopia"></a>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-2" style="padding-left: 0px;padding-right: 0px;" id="margin-footer-01">
          <ul class="list-icon ">
            <li><a href="#">Anuncia tu Venue</a></li>
            <li><a href="#">About</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-2 text-center" style="padding-left: 0px;padding-right: 0px;" id="margin-footer-02">
          <ul class="list-icon ">
            <li><a href="#">FAQ</a></li>
            <li><a href="#">Contacto</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-2 col-md-2 col-lg-2 text-right" style="padding-left: 0px;padding-right: 0px;" id="margin-footer-03">
          <ul class="list-icon ">
            <li><a href="#">Login / Sign up</a></li>
            <li><a href="#">Terminos y Condiciones</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-3 col-md-3 col-lg-3 text-right" id="margin-footer-redes">
          <ul class="list-horizontal-unstyled">
            <li><a href="#"><i class="fab fa-facebook-square" style="font-size:22px;"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram" style="font-size:22px;"></i></a></li>
          </ul>
        </div>
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end footer -->

  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 text-responsive" style="font-size: 10px;">© 2018 by Venuescopia</div>
      </div><!-- end row -->
    </div><!-- end container -->
  </div>
</footer>


<!-- Movil -->
<footer class="d-block d-sm-none">
  <div class="footer bg-dark-lighter text-uppercase">
    <div class="container">
      <div class="row">
        <div class="col-12 col-md-6 col-lg-3 logo-footer" style="padding-left: 0px;padding-right: 0px;">
          <a href="index.php"><img src="assets/images/logo-venuescopia-blanco.png" alt="Logo Venuescopia" class="mx-auto d-block"></a>
        </div>
        <div class="col-12 col-md-6 col-lg-3 text-center margin-bottom-30">
          <ul class="list-horizontal-unstyled">
            <li><a href="#"><i class="fab fa-facebook-square" style="font-size:22px;"></i></a></li>
            <li><a href="#"><i class="fab fa-instagram" style="font-size:22px;"></i></a></li>
          </ul>
        </div>
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end footer -->

  <div class="footer-bottom">
    <div class="container">
        <div class="col-12 text-responsive" style="font-size: 10px;">© 2018 by Venuescopia</div>
    </div><!-- end container -->
  </div>
</footer>
