<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2" style="overflow: hidden;">

    <div class="section-fullscreen bg-image parallax" style="background-image: url(http://www.bluepixel.mx/venuescopia/theme/assets/images/BG/background-login.jpg)">
        <div class="row align-items-center position-middle text-center">
          <div class="col-12 col-sm-12 text-center">
            <div class="margin-bottom-50"><img src="http://www.bluepixel.mx/venuescopia/theme/assets/images/logo-venuescopia.png" alt="Logo Venuescopia" style="width: 20%;"></div>
            <h1 class="text-uppercase titulos-general font-montserrat text-responsive" style="line-height: 0.2;">
              ¡UPS!<br><br>Error 404.<br>Lo sentimos, pero esta página no está disponible.
            </h1>
            <a class="button button-md margin-top-30" href="http://www.bluepixel.mx/venuescopia" id="btn-top-login2">Volver a venuescopia.com.mx</a>
          </div>
        </div><!-- end row -->
    </div>

    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

  </body>
</html>

