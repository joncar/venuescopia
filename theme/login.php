<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <div class="container-fluid no-padding bg-image parallax" style="background-image: url(assets/images/BG/background-anuncia.jpg)">
			<div class="row no-margin">
				<div class="col-12 col-lg-6 no-margin padding-about">
					<div class="padding-50 margin-top-50">

            <div class="col-12 col-sm-12 col-lg-12 bg-white login-casa">
                <h2 class="text-uppercase titulos-general-interna font-montserrat" style="letter-spacing: 3px;"><b>Crea tu cuenta</b></h2>
                 <p>Al crear tu cuenta guarda tus venues y recibe notificaciones sobre lugares nuevos.</p>
            </div>

            <h5 class="font-montserrat margin-bottom-30 text-uppercase" style="letter-spacing: 3px;"><b>¿Eres nuevo? Bienvenido a bordo</b></h5>
            <form>
              <input type="text" placeholder="Nombre*" name="name" required class="margin-bottom-10" style="border:0px;">
              <input type="text" placeholder="Apellidos*" name="name" required class="margin-bottom-10" style="border:0px;">
              <input type="text" placeholder="Correo electrónico*" name="email" required class="margin-bottom-10" style="border:0px;">
              <input type="password" placeholder="Contraseña*" name="pw" required class="margin-bottom-10" style="border:0px;">
              <input type="password" placeholder="Repetir contraseña*" name="pw2" required class="margin-bottom-10" style="border:0px;">

              <div class="margin-top-10 text-general-p">
                <p class="text-left">
                  <div style="padding: 10px;">
                    <input type="checkbox" id="box-4">
                    <label for="box-4">
                      Al registrarte aceptas nuestros:<br>
                      <a href="terminos-condiciones.php">Términos y Condiciones</a> y
                      <a href="aviso-privacidad.php">Aviso de Privacidad</a>
                    </label>
                  </div>
                </p>
              </div>

              <a href="perfil-general.php"><a class="button button-md margin-top-30 text-center" id="btn-negro" href="perfil-general.php" title="Crear cuenta"><b>Crear cuenta</b></a>
            </form>

					</div>
				</div>

        <div class="col-12 col-lg-6 no-margin bg-white-interna padding-about login-casa">
              <div class="margin-bienvenido">
                  <h5 class="font-montserrat margin-bottom-30 text-uppercase" style="letter-spacing: 3px;">
                    <b>¿Ya eres miembro?</b><br>
                    <b>Bienvenido a casa</b>
                  </h5>
                  <form>
                    <input type="text" placeholder="Usuario (correo electrónico)*" name="name" required class="margin-bottom-10" style="border: none;">
                    <input type="password" placeholder="Contraseña*" name="pw" required class="margin-bottom-10" style="border: none;">
                    <a class="button button-md margin-top-10 text-center" id="btn-negro" a href="membresia-propietarios.php" title="Conoce Venuescopia"><b>Log in</b></a>
                  </form>

                  <div class="section-title section-home">
                    <div class="row">
                      <div class="text-left link-todos" style="margin-top: 0px;">
                        <button type="button" data-toggle="modal" data-target="#recuperar" class="link-todos">OLVIDÉ MI CONTRASEÑA</button>
                      </div>
                    </div>
                  </div><!-- end section-title -->

              </div>
				</div>

			</div><!-- end row -->
		</div><!-- end container-fluid -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
