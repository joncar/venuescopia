<!DOCTYPE html>
<html lang="en">
<head>
<?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <!-- Home section -->
    <!--<div class="section-fullscreen bg-image parallax" data-jarallax-video="https://www.youtube.com/watch?v=n_hB8CPRxys">-->
    <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/BG/background-login.jpg)" id="section-index">
        <div class="container text-center">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <h1 class="display-4 font-weight-thin line-height-130 titulo-header texto-negro margin-bottom-30">
                  <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia"><br>
                  Encuentra el Venue de tus sueños
                </h1>

                <form class="col-12 form-inline margin-top-50" autocomplete="off" action="/action_page.php">
                  <div class="autocomplete" id="buscador-header">
                    <input id="myInput" type="text" name="myCountry" placeholder="Ciudad, colonia o venue">
                  </div>
                  <button class="btn btn-outline-success btn-buscar-header font-montserrat" type="submit">Buscar</button>
                </form>

              </div>
            </div><!-- end row -->
          </div>
        </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <!-- Blog section -->
    <div class="section">
      <!-- Destacadas -->
      <div class="container margin-bottom-50 padding-movil-home">

        <div class="section-title">
          <div class="row">
            <div class="col-12 col-md-6">
              <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Novedades</b></h2>
              <div class="subtitulos-general text-responsive">Nuestras propiedades más destacadas.</div>
            </div>
            <div class="col-12 col-md-6 link-todos text-responsive2">
              <a href="venues.php">Ver todas las Propiedades</a>
            </div>
          </div>
        </div><!-- end section-title -->

        <!-- Propiedades destacadas -->
        <div class="row">
          <!-- Slider -->
          <div class="col-12">
              <!-- Testimonial section -->
              <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true" >

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>
                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>
                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>
                  <div class="favoritos-home" style="text-align: right;">
                      <button type="button" data-toggle="modal" data-target="#login"><i class="far fa-heart"></i></button>
                  </div>
                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

              </div><!-- end owl-carousel -->
          </div>
        </div><!-- end row -->
      </div>
      <!-- Destacadas -->

      <!-- Destacadas -->
      <div class="container margin-bottom-50 padding-movil-home">

        <div class="section-title">
          <div class="row">
            <div class="col-12 col-md-6">
              <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Populares</b></h2>
              <div class="subtitulos-general text-responsive">Nuestras propiedades más populares.</div>
            </div>
            <div class="col-12 col-md-6 link-todos text-responsive2">
              <a href="venues.php">Ver todas las Propiedades</a>
            </div>
          </div>
        </div><!-- end section-title -->

        <!-- Propiedades destacadas -->
        <div class="row">
          <!-- Slider -->
          <div class="col-12">
              <!-- Testimonial section -->
              <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true" >

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

              </div><!-- end owl-carousel -->
          </div>
        </div><!-- end row -->
      </div>
      <!-- Destacadas -->

      <!-- Destacadas -->
      <div class="container padding-movil-home">

        <div class="section-title">
          <div class="row">
            <div class="col-12 col-md-6">
              <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Bodas</b></h2>
              <div class="subtitulos-general text-responsive">Nuestros mejores venues para tu boda.</div>
            </div>
            <div class="col-12 col-md-6 link-todos text-responsive2">
              <a href="venues.php">Ver todas las Propiedades</a>
            </div>
          </div>
        </div><!-- end section-title -->

        <!-- Propiedades destacadas -->
        <div class="row">
          <!-- Slider -->
          <div class="col-12">
              <!-- Testimonial section -->
              <div class="owl-carousel" data-owl-margin="30" data-owl-xs="1" data-owl-sm="1" data-owl-md="1" data-owl-lg="1" data-owl-xl="3" data-owl-nav="true" data-owl-dots="true" >

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>

                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

                <!-- Testimonial box 1 -->
                <div class="border-radius bg-white">
                  <a href="venues-interna.php" title="Ver Venue">
                    <div class="mx-auto d-block imagen-venues-home">
                      <img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia">
                    </div>
                  </a>

                  <div class="favoritos-home">
                    <div class="wrap">
                      <button type="button" data-toggle="modal" data-target="#login">
                        <i class="notification-icon animate-swing far fa-heart" data-icon="far fa-heart" data-active-icon="fas fa-heart"></i>
                      </button>
                    </div>   
                  </div>
                  
                  <div class="titulo-venue-home">
                    <?php include('includes/venues/precios-venues.php');?>
                  </div>
                </div>

              </div><!-- end owl-carousel -->
          </div>
        </div><!-- end row -->
      </div>
      <!-- Destacadas -->
    </div>

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

    <!-- Modal Inicio -->
    <script>
    $(window).load(function(){
       setTimeout(function(){
           $('#myModal').modal('show');
       }, 7000);
    });
    </script>
    <!-- Modal Inicio -->

    <script>
      $('.notification-icon').click(function(e) {
  var that = $(this);
  that.toggleClass("active "+ that.attr("data-active-icon") + " " + that.attr("data-icon"));
});

    </script>

  </body>
</html>
