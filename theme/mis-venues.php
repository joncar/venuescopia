<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu-interno.php');?>

    <!-- About section -->
    <div class="section margin-registro-top text-general-p">


      <!-- Product Tab content -->
  		<div class="section no-padding margin-top-30">
  			<div class="container">
  				<div class="product-tab">
  					<ul class="nav margin-bottom-20 text-uppercase">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tab-venues"><h5>Mis Venues</h5></a>
              </li>
  						<li class="nav-item">
  							<a class="nav-link" data-toggle="tab" href="#tab-contactos"><h5>Mensajes <span>(3)</span></h5></a>
  						</li>
  					</ul>
  					<div class="tab-content">
              <!-- Description tab content -->
              <div class="tab-pane fade show active" id="tab-venues">

                  <div class="margin-bottom-20">
                    <div class="row align-items-end">
                      <div class="col-12">
                        <h2 class="text-uppercase titulos font-montserrat"><b>Mis Venues</b></h2>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive">
                    <table class="product-table">
                      <thead>
                        <tr>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Venue</h6></th>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Precio</h6></th>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Personas</h6></th>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Acciones</h6></th>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Vistas</h6></th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <a href="#"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></a>
                            <h6>Nombre del Venue</h6>
                          </td>
                          <td><span>$29 - $50</span></td>
                          <td><p class="text-dark"><i class="fas fa-check"></i> 1000 - 5000</p></td>
                          <td>
                            <p class="text-dark">
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#pausar-publicacion" title="Pausar Publicación"><i class="fas fa-pause-circle"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#editar-publicacion" title="Editar Publicación"><i class="fas fa-pen-square"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#eliminar-publicacion" title="Eliminar Publicación"><i class="fas fa-trash-alt"></i></button>
                            </p>
                          </td>
                          <td><p class="text-dark"><i class="fas fa-eye"></i> 5000</p></td>
                        </tr>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <a href="#"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></a>
                            <h6>Nombre del Venue</h6>
                          </td>
                          <td><span>$29 - $50</span></td>
                          <td><p class="text-dark"><i class="fas fa-check"></i> 1000 - 5000</p></td>
                          <td>
                            <p class="text-dark">
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#pausar-publicacion" title="Pausar Publicación"><i class="fas fa-pause-circle"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#editar-publicacion" title="Editar Publicación"><i class="fas fa-pen-square"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#eliminar-publicacion" title="Eliminar Publicación"><i class="fas fa-trash-alt"></i></button>
                            </p>
                          </td>
                          <td><p class="text-dark"><i class="fas fa-eye"></i> 5000</p></td>
                        </tr>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <a href="#"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></a>
                            <h6>Nombre del Venue</h6>
                          </td>
                          <td><span>$29 - $50</span></td>
                          <td><p class="text-dark"><i class="fas fa-check"></i> 1000 - 5000</p></td>
                          <td>
                            <p class="text-dark">
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#pausar-publicacion" title="Pausar Publicación"><i class="fas fa-pause-circle"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#editar-publicacion" title="Editar Publicación"><i class="fas fa-pen-square"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#eliminar-publicacion" title="Eliminar Publicación"><i class="fas fa-trash-alt"></i></button>
                            </p>
                          </td>
                          <td><p class="text-dark"><i class="fas fa-eye"></i> 5000</p></td>
                        </tr>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <a href="#"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></a>
                            <h6>Nombre del Venue</h6>
                          </td>
                          <td><span>$29 - $50</span></td>
                          <td><p class="text-dark"><i class="fas fa-check"></i> 1000 - 5000</p></td>
                          <td>
                            <p class="text-dark">
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#pausar-publicacion" title="Pausar Publicación"><i class="fas fa-pause-circle"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#editar-publicacion" title="Editar Publicación"><i class="fas fa-pen-square"></i></button>
                              <button type="button" class="btn" id="btn-negro" data-toggle="modal" data-target="#eliminar-publicacion" title="Eliminar Publicación"><i class="fas fa-trash-alt"></i></button>
                            </p>
                          </td>
                          <td><p class="text-dark"><i class="fas fa-eye"></i> 5000</p></td>
                        </tr>

                      </tbody>
                    </table>
                  </div>

                  <!-- Pagination -->
                  <nav>
                    <ul class="pagination justify-content-center margin-top-70">
                      <li class="page-item"><a class="page-link" href="#">«</a></li>
                      <li class="page-item active"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item"><a class="page-link" href="#">»</a></li>
                    </ul>
                  </nav>

              </div>

  						<!-- Reviews tab content -->
  						<div class="tab-pane fade" id="tab-contactos">
                  <div class="margin-bottom-20">
                    <div class="row align-items-end">
                      <div class="col-12">
                        <h2 class="text-uppercase titulos font-montserrat"><b>Mensajes</b></h2>
                      </div>
                    </div>
                  </div>

                  <div class="table-responsive">
                    <table class="product-table">
                      <thead>
                        <tr>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Nombre</h6></th>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Correo</h6></th>
                          <th scope="col"><h6 class="heading-uppercase no-margin">Mensaje</h6></th>
                        </tr>
                      </thead>
                      <tbody>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <h6>Nombre del Contacto</h6>
                          </td>
                          <td><a href="#">correo@dominio.com.mx</a></td>
                          <td>
                            <p class="text-dark texto-mensaje-contacto">
                              <b>Mensaje:</b><br>
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit orci ac sem viverra, hendrerit vulputate libero pellentesque. Nunc commodo eleifend magna eu luctus. Suspendisse eu nunc leo. Ut vitae imperdiet risus. Donec viverra, lacus ut molestie ornare, felis nibh commodo lorem, non facilisis nunc est a sem.
                            </p>
                          </td>
                        </tr>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <h6>Nombre del Contacto</h6>
                          </td>
                          <td><a href="#">correo@dominio.com.mx</a></td>
                          <td>
                            <p class="text-dark texto-mensaje-contacto">
                              <b>Mensaje:</b><br>
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit orci ac sem viverra, hendrerit vulputate libero pellentesque. Nunc commodo eleifend magna eu luctus. Suspendisse eu nunc leo. Ut vitae imperdiet risus. Donec viverra, lacus ut molestie ornare, felis nibh commodo lorem, non facilisis nunc est a sem.
                            </p>
                          </td>
                        </tr>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <h6>Nombre del Contacto</h6>
                          </td>
                          <td><a href="#">correo@dominio.com.mx</a></td>
                          <td>
                            <p class="text-dark texto-mensaje-contacto">
                              <b>Mensaje:</b><br>
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit orci ac sem viverra, hendrerit vulputate libero pellentesque. Nunc commodo eleifend magna eu luctus. Suspendisse eu nunc leo. Ut vitae imperdiet risus. Donec viverra, lacus ut molestie ornare, felis nibh commodo lorem, non facilisis nunc est a sem.
                            </p>
                          </td>
                        </tr>

                        <tr>
                          <td class="wishlist-product-thumbnail">
                            <h6>Nombre del Contacto</h6>
                          </td>
                          <td><a href="#">correo@dominio.com.mx</a></td>
                          <td>
                            <p class="text-dark texto-mensaje-contacto">
                              <b>Mensaje:</b><br>
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus hendrerit orci ac sem viverra, hendrerit vulputate libero pellentesque. Nunc commodo eleifend magna eu luctus. Suspendisse eu nunc leo. Ut vitae imperdiet risus. Donec viverra, lacus ut molestie ornare, felis nibh commodo lorem, non facilisis nunc est a sem.
                            </p>
                          </td>
                        </tr>

                      </tbody>
                    </table>
                  </div>

                  <!-- Pagination -->
                  <nav>
                    <ul class="pagination justify-content-center margin-top-70">
                      <li class="page-item"><a class="page-link" href="#">«</a></li>
                      <li class="page-item active"><a class="page-link" href="#">1</a></li>
                      <li class="page-item"><a class="page-link" href="#">2</a></li>
                      <li class="page-item"><a class="page-link" href="#">3</a></li>
                      <li class="page-item"><a class="page-link" href="#">»</a></li>
                    </ul>
                  </nav>
  						</div>
  					</div>
  				</div>
  			</div><!-- end container -->
  		</div>
  		<!-- end Product Tab content -->

    </div>
    <!-- end About section -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
