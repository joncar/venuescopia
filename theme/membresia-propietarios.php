<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu-interno.php');?>

    <!-- About section -->
    <div class="section margin-registro-top">
      <div class="container">

        <div class="margin-bottom-30">
          <div class="row">
            <div class="col-md-8 offset-md-2" style="padding-left: 20px;">
              <img src="assets/images/logo-venuescopia.png" alt="Logo Venuescopia" style="width:20%; margin-bottom:10px;">
              <h2 class="text-uppercase titulos-general font-montserrat text-responsive"><b>Gracias</b></h2>
              <div class="subtitulos-general text-responsive">Por tu pago! Revisaremos tu anuncio y será publicado en poco tiempo.</div>
            </div>
          </div>
        </div><!-- end section-title -->

        <div class="margin-bottom-30 col-12 col-md-8 offset-md-2">
          <div class="col-12">
            <h2 class="font-weight-norma text-uppercase titulos font-montserrat"><b>Mis Datos</b></h2>
            <form>
              <label>Nombre:</label>
              <input type="text" placeholder="María" name="name" required="" class="margin-bottom-10">
              <label>Apellidos:</label>
              <input type="text" placeholder="De Buen" name="name" required="" class="margin-bottom-10">
              <label>Correo:</label>
              <input type="text" placeholder="maria@gmail.com" name="email" required="" class="margin-bottom-10">
              <label>Password:</label>
              <input type="password" placeholder="XXXXXXX" name="pw" required="" class="margin-bottom-30">
            </form>
            <a class="button button-md text-center" id="btn-negro" href="#" title="Guardar cambios" style="width: 100%;"><b>Guardar cambios</b></a>
          </div>
        </div>

        <div class="text-left margin-bottom-50 col-md-8 offset-md-2">
            <div class="margin-bottom-30">
              <h3 class="font-montserrat">Plan contratado</h3>
              <p>Gracias por confiar en Venuescopia, este es el plan que tienes contratado</p>
            </div>

            <div class="table-responsive">
                <table class="product-table">
                  <tbody>
                    <tr>
                      <td><span>Nombre del Plan</span></td>
                      <td><span class="text-uppercase"><b class="text-uppercase font-montserrat">Trimestral</b></span></td>
                    </tr>

                    <tr>
                      <td><span>Estado de la suscripción</span></td>
                      <td><b><span class="text-success text-uppercase"><b class="text-uppercase font-montserrat" style="color:green;">Activo</span></b></td>
                    </tr>

                    <tr>
                      <td><span>Período de Inicio</span></td>
                      <td><span>October 10th 2018, 12:35:59 pm</span></td>
                    </tr>

                    <tr>
                      <td><span>Período de Término</span></td>
                      <td><span>October 10th 2019, 12:35:59 pm</span></td>
                    </tr>
                  </tbody>
                </table>
            </div>
        </div>

        <div class="text-left margin-bottom-20 col-md-8 offset-md-2">
          <div class="prices-box">
              <div class="col-12 col-lg-12">
                <h3 class="font-montserrat text-responsive">Forma de pago</h3>
                <p>Puedes agregar una forma de pago para cada tipo de venue</p>
              </div>

              <div class="col-12 col-lg-6">
                <p>
                  <b class="text-uppercase font-montserrat">Nombre del Propietario:</b><br>María de Buen<br>
                  <b class="text-uppercase font-montserrat">Forma de pago:</b><br><i class="fab fa-cc-paypal" style="font-size: 38px;"></i><br>
                  <b class="text-uppercase font-montserrat">Vence en:</b><br>00/00/0000</b><br>
                </p>
              </div>
              <div class="col-12 col-lg-6 text-responsive">
                  <a class="button button-md margin-top-30" id="btn-negro" type="submit" data-toggle="modal" data-target="#update-card"><b>Actualizar</b></a>
                  <a class="button button-md margin-top-30" id="btn-blanco" type="submit" data-toggle="modal" data-target="#update-card"><b>Agregar forma de pago</b></a>
              </div>
          </div>
        </div><!-- end row -->

        <div class="margin-bottom-50 col-md-8 offset-md-2">
          <div class="col-12 col-sm-12 margin-top-20">
             <!--<button class="button button-xl button-dark" type="submit" data-toggle="modal" data-target="#update-suscripcion">Update</button>-->
             <button class="button button-xl" type="submit" data-toggle="modal" data-target="#cancelar-suscripcion">Cancelar mi suscripción</button>
          </div>
        </div><!-- end row -->


        <div class="margin-bottom-50 col-md-8 offset-md-2">
          <div class="col-12 col-sm-12">

              <div class="margin-bottom-30">
                <h3 class="font-montserrat">Próxima factura</h3>
                <p>Te recordamos la próxima factura</p>
              </div>

              <div class="table-responsive">
                  <table class="product-table">
                    <tbody>
                      <tr>
                        <td><span>Factura correspondiente a</span></td>
                        <td><span><b>November 10th 2018 - 1541871359</b></span></td>
                        <td><span><a href="#"><i class="fas fa-file-download"></i> Descargar recibo</a></span></td>
                      </tr>

                      <tr>
                        <td><span>Subtotal</span></td>
                        <td><span>$100</span></td>
                        <td></td>
                      </tr>

                      <tr>
                        <td><span>Total</span></td>
                        <td><span>$100</span></td>
                        <td></td>
                      </tr>

                      <tr>
                        <td><span>Próxima fecha de corte</span></td>
                        <td><span>En 2 días - 9 de Noviembre 2018</span></td>
                        <td></td>
                      </tr>

                    </tbody>
                  </table>
              </div>
          </div>
        </div><!-- end row -->

        <div class="margin-bottom-50 col-md-8 offset-md-2">
          <div class="col-12 col-sm-12">

              <div class="margin-bottom-30">
                <h3 class="font-montserrat">Historial de facturas</h3>
              </div>

              <div class="table-responsive">
                  <table class="product-table">
                    <tbody>
                      <tr>
                        <td><span>Factura correspondiente a</span></td>
                        <td><span>$100</span></td>
                        <td><span>November 10th 2018 - 1541871359</span></td>
                        <td><span><a href="#"><i class="fas fa-file-download"></i> Descargar recibo</a></span></td>
                      </tr>

                      <tr>
                        <td><span>Factura correspondiente a</span></td>
                        <td><span>$100</span></td>
                        <td><span>November 10th 2018 - 1541871359</span></td>
                        <td><span><a href="#"><i class="fas fa-file-download"></i> Descargar recibo</a></span></td>
                      </tr>

                      <tr>
                        <td><span>Factura correspondiente a</span></td>
                        <td><span>$100</span></td>
                        <td><span>November 10th 2018 - 1541871359</span></td>
                        <td><span><a href="#"><i class="fas fa-file-download"></i> Descargar recibo</a></span></td>
                      </tr>

                      <tr>
                        <td><span>Factura correspondiente a</span></td>
                        <td><span>$100</span></td>
                        <td><span>November 10th 2018 - 1541871359</span></td>
                        <td><span><a href="#"><i class="fas fa-file-download"></i> Descargar recibo</a></span></td>
                      </tr>

                      <tr>
                        <td><span>Factura correspondiente a</span></td>
                        <td><span>$100</span></td>
                        <td><span>November 10th 2018 - 1541871359</span></td>
                        <td><span><a href="#"><i class="fas fa-file-download"></i> Descargar recibo</a></span></td>
                      </tr>

                    </tbody>
                  </table>
              </div>
          </div>
        </div><!-- end row -->



      </div><!-- end container -->
    </div>
    <!-- end About section -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <!-- Modales -->
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
