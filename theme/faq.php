<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <!-- Home section -->
    <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/BG/background-anuncia.jpg)">
        <div class="container">
          <div class="position-middle">



            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 bg-white contenedor-header-titulo3">

                  <h2 class="text-uppercase titulos-general font-montserrat"><b>PREGUNTAS FRECUENTES FAQ</b></h2>
                  <ul class="accordion accordion-oneopen">
                    <!-- 1 -->
                    <li>
                      <div class="accordion-title">
                        <h6>¿Quién puede usar Venuescopia?</h6>
                      </div>
                      <div class="accordion-content">
                        <p>Nuestros usuarios son todos aquellos que busquen un espacio nuevo, único de manera fácil y sencilla para un evento, boda, convención, lanzamientos, fiestas y más.</p>
                      </div>
                    </li>
                    <!-- 2 -->
                    <li>
                      <div class="accordion-title">
                        <h6>¿Cómo puedo contactar a un espacio o venue que me interesa?</h6>
                      </div>
                      <div class="accordion-content">
                        <p>Es muy sencillo, a través de la página del venue que deseas contactar, encontrarás un formulario al costado derecho en donde podrás escribir directamente al venue o bien obtener
                          su número  de teléfono directo para atender tu solicitud.</p>
                      </div>
                    </li>
                    <!-- 3 -->
                    <li>
                      <div class="accordion-title">
                        <h6>¿Qué obtengo al contratar un plan para anunciar mi venue?</h6>
                      </div>
                      <div class="accordion-content">
                        <p>Podrás crear y publicar de manera muy sencilla un anuncio personalizado para tu venue con amenidades, fotos del venue, ubicación, descripción y datos para que te contacten
                          interesados en rentar tu venue.</p>
                      </div>
                    </li>
                    <!-- 4 -->
                    <li>
                      <div class="accordion-title">
                        <h6>¿Puedo reservar un venue a través de Venuescopia?</h6>
                      </div>
                      <div class="accordion-content">
                        <p>Para reservar o conocer aún más detalles para tu evento, te ofrecemos los datos de contacto de cada venue con un solo click para que puedan atender todos tus deseos de
                          manera personalizada.</p>
                      </div>
                    </li>
                    <!-- 5 -->
                    <li>
                      <div class="accordion-title">
                        <h6>¿Cómo puedo compartir una publicación?</h6>
                      </div>
                      <div class="accordion-content">
                        <p>Puedes copiar el link de la página del venue directamente desde el buscador o bien dar click en el botón de Share en la esquina superior derecha del anuncio para mandarlo
                          por Whatsapp, Facebook, mail o copiar el link.</p>
                      </div>
                    </li>
                    <!-- 6 -->
                    <li>
                      <div class="accordion-title">
                        <h6>¿Puedo subir varios venues en un mismo anuncio?</h6>
                      </div>
                      <div class="accordion-content">
                        <p>Cada anuncio es único para cada venue. Crea nuevos anuncios detallados para tus distintos espacios en renta para que todos te encuentren fácil y rápido.</p>
                      </div>
                    </li>
                  </ul>

                  <div class="col-12 text-center margin-top-10" style="margin-bottom:0px;">
                    <button type="button" data-toggle="modal" data-target="#faq" class="link-todos resolvimos-modal">¿No resolvimos tus dudas?</button>
                  </div>

              </div>
            </div><!-- end row -->



          </div>
        </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>
  </body>
</html>
