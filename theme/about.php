 <!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

<!-- Menu -->
<?php include('includes/menu.php');?>

<div id="page-wrap">

    <div class="container-fluid no-padding bg-image" style="background-image: url(assets/images/BG/background-about.jpg)" id="header-height">
			<div class="row align-items-center no-margin">

				<div class="col-12 col-lg-6 no-margin bg-white-interna padding-about">
					<div class="padding-50 margin-text-header">
            <h2 class="text-uppercase titulos-general font-montserrat text-responsive font-weight-bold">Todos los venues en un mismo lugar</h2>
            <div class="padding-same">
              <p class="text-responsive">Donde encuentras y te encuentran.</p>
            </div>
            <div class="text-responsive">
              <a class="button button-lg" id="btn-negro" href="#one" title="Conoce Venuescopia"><b>Leer más</b></a>
            </div>
					</div>
				</div>

        <div class="col-12 col-lg-6 no-margin no-padding"></div>
			</div><!-- end row -->
		</div><!-- end container-fluid -->

    <!-- About section -->
    <div class="section" id="one">
      <div class="container">
        <div class="row">

          <div class="section-title-interna margin-about">
            <div class="row">
              <div class="col-12">
                <h2 class="text-uppercase font-montserrat"><b>Conoce venuescopia</b></h2>
              </div>
            </div>
          </div><!-- end section-title -->

          <div class="row margin-bottom-20  margin-about" id="#nosotros">
              <div class="col-12 col-lg-6">
                <p>Venuescopia es la primera plataforma en México que permite a sus usuarios descubrir los venues de sus sueños.<br>
                Nuestra misión es unir a nuestros usuarios visitantes con nuestros anunciantes para así ser el sitio más importante de búsqueda de espacios para eventos a nivel nacional.<br><br>
                Hemos planeado nuestro sitio pensando en tus intereses y en tu mayor comodidad. Con nosotros puedes crear perfiles tanto de anunciante como de visitante y ambos tipos de perfiles están diseñados para brindarte las herramientas adecuadas para obtener los mayores beneficios de nuestros servicios.<br><br>
                Es muy fácil y estamos para servirte.</p>
              </div>
              <div class="col-12 col-lg-6 imagen-brujula">
                  <img src="assets/images/brujula.png" alt="Cónocenos - Venuescopia" class="mx-auto d-block">
              </div>
          </div>

        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end About section -->

</div>


  <!-- Footer -->
  <?php include('includes/footer.php');?>
  <!-- Librerias -->
  <?php include('includes/librerias.php');?>
  </body>
</html>

<!-- Scroll Text -->
<script>
// Select all links with hashes
$('a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
      &&
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });
</script>
