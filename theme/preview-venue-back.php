<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <!-- Home section -->
    <div class="owl-carousel owl-nav-overlay owl-dots-overlay" data-owl-nav="true" data-owl-dots="true" data-owl-items="1" id="slide-venue">
      <!-- Slider box 1 -->
      <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/venues-interna/venue-interna-01.jpg);">
        <div class="section-xl">
          <div class="container text-center">
            <div class="row share-header">
                <div class="col-12 col-lg-8"></div>
                <div class="col-12 col-lg-4">
                    <a class="button button-md text-center" href="pago.php" id="btn-blanco" title="Publica tu Venue" style="padding:  10px 30px;"><b>Pagar para finalizar</b></a>
                </div>
            </div>
          </div><!-- end container -->
        </div>
      </div>
      <!-- Slider box 2 -->
      <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/venues-interna/venue-interna-02.jpg);">
        <div class="section-xl">
          <div class="container text-center">
            <div class="row">
            </div><!-- end row -->
          </div><!-- end container -->
        </div>
      </div>
      <!-- Slider box 3 -->
      <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/venues-interna/venue-interna-03.jpg);">
        <div class="section-xl">
          <div class="container text-center">
            <div class="row">
            </div><!-- end row -->
          </div><!-- end container -->
        </div>
      </div>
    </div><!-- end owl-carousel -->
    <!-- end Home section -->

    <div class="section">
      <div class="container">

        <div class="row col-spacing-50">
            <div class="col-12 col-lg-8">

                <div class="section-title-interna3">
                  <div class="row">
                    <div class="col-12">
                      <h2 class="text-uppercase font-montserrat"><b>ST. Regis Hotel</b></h2>
                      <div class="subtitulos-general">Reforma, Ciudad de México.</div>
                    </div>
                  </div>
                </div><!-- end section-title -->

                <div class="row personas-venue text-left font-montserrat">
                    <div class="col-12 col-lg-4">
                        <h4><b><img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;"> 50-250 personas</b></h4>
                    </div>

                    <div class="col-12 col-lg-8">
                        <h4><b><img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:5%;"> $100,000 - $200,000</b></h4>
                    </div>
                </div>

                <div class="row texto-descripcion margin-bottom-10">
                  El hotel más importante de México. Contamos con 3 salones distintos para eventos privados. Somos un venue de calidad, tradición y vanguardia.<br>
                  Ubicado en la avenida más importante de la capital del país, tener un evento en nuestras instalaciones es estar en el corazón de lo más bello de esta ciudad.
                  Ofrecemos distintos precios, paquetes y opciones para que celebres con nosotros los eventos más importantes de tu vida.<br>
                  Con más de 50 años de experiencia somos el primer destino del turismo de primer nivel y nuestros servicios son de la más alta calidad.
                </div>

                <div class="personas-venue">
                    <div class="row iconos-venue margin-bottom-20">
                      <div class="col-12 col-md-6 col-lg-4">
                        <div class="float-left margin-right-20 icon-2xl" style="width:10%;">
                          <img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue">
                        </div>
                        <h5 class="heading-uppercase font-montserrat position-middle"><b>Invitados</b></h5>
                      </div>

                      <div class="col-12 col-md-6 col-lg-4">
                        <div class="float-left margin-right-20 icon-2xl" style="width:10%;">
                          <img src="assets/images/iconos/capilla.png" alt="Amenidades del Venue" class="iconos-venue">
                        </div>
                        <h5 class="heading-uppercase font-montserrat position-middle"><b>Capilla</b></h5>
                      </div>

                      <div class="col-12 col-md-6 col-lg-4">
                        <div class="float-left margin-right-20 icon-2xl" style="width:10%;">
                          <img src="assets/images/iconos/catering.png" alt="Amenidades del Venue" class="iconos-venue">
                        </div>
                        <h5 class="heading-uppercase font-montserrat position-middle"><b>Catering</b></h5>
                      </div>
                    </div><!-- end row -->

                    <div class="row iconos-venue">
                      <div class="col-12 col-md-6 col-lg-4">
                        <div class="float-left margin-right-20 icon-2xl" style="width:10%;">
                          <img src="assets/images/iconos/estacionamiento.png" alt="Amenidades del Venue" class="iconos-venue">
                        </div>
                        <h5 class="heading-uppercase font-montserrat position-middle"><b>Seguridad</b></h5>
                      </div>

                      <div class="col-12 col-md-6 col-lg-4">
                        <div class="float-left margin-right-20 icon-2xl" style="width:10%;">
                          <img src="assets/images/iconos/bar.png" alt="Amenidades del Venue" class="iconos-venue">
                        </div>
                        <h5 class="heading-uppercase font-montserrat position-middle"><b>Bar</b></h5>
                      </div>

                      <div class="col-12 col-md-6 col-lg-4">
                        <div class="float-left margin-right-20 icon-2xl" style="width:10%;">
                          <img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue">
                        </div>
                        <h5 class="heading-uppercase font-montserrat position-middle"><b>Estacionamiento</b></h5>
                      </div>
                    </div><!-- end row -->
                </div>
            </div>

            <div class="col-12 col-lg-4" style="display: block; z-index: 2;">
                <div id="myHeader">
                  <div class="row" >
                    <div class="btn-solicitar">
                      <a type="button" data-toggle="modal" data-target="#telefono"><button class="button button-lg btn-informes"><b>Ver teléfono</b></button></a>
                    </div>
                    <!-- Contact Form -->
                    <div class="contact-form">
                      <form method="post" id="contactform">
                        <div class="form-row">
                          <div class="col-12 col-sm-12">
                            <input type="text" id="name" name="name" placeholder="Nombre" required>
                          </div>
                          <div class="col-12 col-sm-12">
                            <input type="email" id="email" name="email" placeholder="Correo electrónico" required>
                          </div>
                          <div class="col-12 col-sm-12">
                            <textarea name="message" id="message" placeholder="¡Hola! Me interesa este venue. Gracias por contactarme."></textarea>
                          </div>
                        </div>
                        <div class="btn-solicitar">
                          <button class="button button-lg btn-buscar-header" type="submit" data-toggle="modal" data-target="#contacto-venue">Contactar</button>
                        </div>
                      </form>
                      <!-- Submit result -->
                      <div class="submit-result">
                        <span id="success">Gracias, mensaje enviado</span>
                        <span id="error">Algo no estuvo bien, por favor vuelve a intentarlo</span>
                      </div>
                    </div><!-- end contact-form -->
                  </div><!-- end inner row -->
                </div>
            </div>
        </div>

          <div class="row margin-bottom-20">
              <div class="section-title-interna3">
                <div class="row">
                  <div class="col-12">
                    <h2 class="text-uppercase font-montserrat"><b>Espacios</b></h2>
                  </div>
                </div>
              </div><!-- end section-title -->


              <div class="row">
                    <div class="col-12 col-lg-4">
                      <!-- Product box -->
                      <div class="col-12 col-lg-12 product-single">
                        <div class="product-single-img">
                          <div class="owl-carousel product-single-img-slider owl-loaded owl-drag" data-owl-items="1" data-owl-nav="true" id="favoritos-carrousel">
                            <div class="owl-stage-outer owl-height">
                              <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1012px;">
                                <div class="owl-item active">
                                  <div data-hash="first"><img src="assets/images/venues-home/venue-01.jpg" alt="Propiedades destacadas Venuescopia"></div>
                                </div>
                                <div class="owl-item">
                                  <div data-hash="second"><img src="assets/images/venues-home/venue-02.jpg" alt="Propiedades destacadas Venuescopia"></div>
                                </div>
                                <div class="owl-item">
                                  <div data-hash="third"><img src="assets/images/venues-home/venue-03.jpg" alt="Propiedades destacadas Venuescopia"></div>
                                </div>
                              </div>
                            </div>

                            <div class="owl-nav">
                              <button type="button" role="presentation" class="owl-prev"><i class="ti-angle-left"></i></button>
                              <button type="button" role="presentation" class="owl-next"><i class="ti-angle-right"></i></button>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-12 col-lg-8">
                        <div class="titulo-venue-home margin-bottom-20">
                          <a href="venues-interna.php">
                            <h5 class="text-uppercase font-montserrat"><b>ST. REGIS hotel</b></h5>
                            <span class="text-general-p">Reforma, Ciudad de México.</span>
                          </a>
                        </div>
                        <p>Ubicado en la avenida más importante de la capital del país, tener un evento en nuestras instalaciones es estar en el corazón de lo más bello de esta ciudad. Ofrecemos distintos precios, paquetes y opciones para que celebres con nosotros los eventos más importantes de tu vida.</p>

                        <div class="row personas-venue2 text-left font-montserrat">
                            <div class="col-12 col-lg-4 no-padding">
                                <h4><b><img src="assets/images/iconos/personas.png" alt="Amenidades del Venue" class="iconos-venue" style="width:10%;"> 50-250 personas</b></h4>
                            </div>

                            <div class="col-12 col-lg-8">
                                <h4><b><img src="assets/images/iconos/presupuesto.png" alt="Amenidades del Venue" class="iconos-venue" style="width:5%;"> $100,000 - $200,000</b></h4>
                            </div>
                        </div>

                    </div>
              </div>

          </div>

          <div class="col-12 col-lg-12 margin-bottom-20">
              <div class="section-title-interna">
                <div class="row">
                  <div class="col-12">
                    <h2 class="text-uppercase titulos-general font-montserrat"><b>Mapa</b></h2>
                    <div class="subtitulos-general">Reforma 439, Colonia Centro, Ciudad de México.</div>
                  </div>

                  <div class="col-12">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15050.342250269225!2d-99.16839093237355!3d19.430304625878488!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1ff4be2f41ead%3A0xe7179355336f6a97!2sSt+Regis!5e0!3m2!1ses-419!2smx!4v1540947814818" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                  </div>
                </div>
              </div><!-- end section-title -->
          </div>

          <div class="section-title-interna3">
            <div class="row">
              <div class="col-12">
                <h2 class="text-uppercase font-montserrat"><b>Preview de tu anuncio</b></h2>
              </div>
            </div>
          </div><!-- end section-title -->

          <div class="col-12 col-sm-12">
              <a class="button button-md margin-top-10 text-center" id="btn-negro" href="registro.php" title="Conoce Venuescopia"><b>Seguir editando</b></a>
              <a class="button button-md margin-top-10 text-center" id="btn-negro" href="pago.php" title="Conoce Venuescopia"><b>Pagar para finalizar</b></a>
          </div>

        </div><!-- end row -->
      </div><!-- end container -->
    </div>

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <?php include('includes/modales.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

    <!-- Sticky contact venue -->
    <script>
    window.onscroll = function() {myFunction()};
    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
    </script>

  </body>
</html>
