<!DOCTYPE html>
<html lang="en">
<head>
  <?php include('includes/head.php');?>
</head>
<body data-preloader="2">

    <!-- Menu -->
    <?php include('includes/menu.php');?>

    <!-- Home section -->
    <div class="section-fullscreen bg-image parallax" style="background-image: url(assets/images/BG/background-anuncia.jpg)">
        <div class="container">
          <div class="position-middle">
            <div class="row">
              <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2">
                <div class="bg-white contenedor-header-titulo2 text-center">
                  <h2 class="text-uppercase titulos-general font-montserrat"><b>Aviso de Privacidad</b></h2>
                  <a class="button button-md margin-top-30 text-center" id="btn-negro" href="#one" title="Publica tu Venue"><b>Leer más</b></a>
                </div>
              </div>
            </div><!-- end row -->
          </div>
        </div><!-- end container -->
    </div>
    <!-- end Home section -->

    <!-- About section -->
    <div class="section" id="one">
      <div class="container">
        <div class="row align-items-center col-spacing-50">
          <div class="col-12 col-lg-6">
            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque imperdiet bibendum enim accumsan scelerisque. Proin blandit ultrices libero a bibendum. Nullam pellentesque turpis massa, quis tincidunt nisi egestas ut. Etiam elementum, massa sit amet interdum porta, magna orci imperdiet quam, vel vestibulum lectus turpis at tellus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris lacus dui, finibus nec erat quis, euismod venenatis odio. Donec aliquam enim in aliquam laoreet. Mauris quis turpis iaculis, lobortis mauris nec, pellentesque mi. Vivamus eu vestibulum leo. Pellentesque consectetur ligula facilisis, aliquet lectus eu, iaculis nunc. Suspendisse potenti. Aenean porttitor justo vel arcu blandit, sit amet sagittis purus pretium.<br>

            Sed maximus ornare dictum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Praesent ante lectus, fringilla ut fermentum nec, consectetur nec sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nam finibus magna sed accumsan lacinia. Aliquam erat volutpat. Nulla tincidunt elit leo. Maecenas tristique, enim sed sollicitudin pharetra, massa ex viverra lacus, a fringilla est orci sed enim. Suspendisse mattis arcu eget feugiat fermentum. Vivamus nec consequat turpis, in lobortis orci. Ut tempor fermentum enim, at aliquet purus pharetra sed. Mauris non tortor sed ipsum sagittis feugiat non in diam. Mauris non volutpat arcu, quis fringilla turpis. Sed viverra, lectus a congue laoreet, elit leo mattis orci, sit amet cursus massa purus eleifend diam. Pellentesque eleifend tortor sapien. </p>
          </div>

          <div class="col-12 col-lg-6 imagen-brujula">
              <img src="assets/images/brujula.png" alt="Cónocenos - Venuescopia" class="mx-auto d-block">
          </div>
        </div><!-- end row -->
      </div><!-- end container -->
    </div>
    <!-- end About section -->

    <!-- Footer -->
    <?php include('includes/footer.php');?>
    <!-- Librerias -->
    <?php include('includes/librerias.php');?>

    <!-- Scroll Text -->
    <script>
    // Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });
    </script>

  </body>
</html>
