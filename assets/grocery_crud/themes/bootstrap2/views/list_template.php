<?php

$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/jquery.noty.js');
$this->set_js_lib($this->default_javascript_path . '/jquery_plugins/config/jquery.noty.config.js');
if (!$this->is_IE7()) {
    $this->set_js_lib($this->default_javascript_path . '/common/list.js');
}
$this->set_js($this->default_theme_path . '/bootstrap2/js/cookies.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/flexigrid.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/jquery.form.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.numeric.min.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/jquery.printElement.min.js');
$this->set_js($this->default_theme_path . '/bootstrap2/js/pagination.js');
$this->set_css($this->default_css_path . '/jquery_plugins/fancybox/jquery.fancybox.css');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.fancybox-1.3.4.js');
$this->set_js($this->default_javascript_path . '/jquery_plugins/jquery.easing-1.3.pack.js');
/** Jquery UI */

if(!empty($order_field)){
    $this->set_js($this->default_theme_path . '/bootstrap2/js/order.js');
}
?>
<script type='text/javascript'>
    var base_url = '<?php echo base_url(); ?>';
    var subject = '<?php echo $subject ?>';
    var ajax_list_info_url = '<?php echo $ajax_list_info_url; ?>';
    var ajax_list = '<?php echo $ajax_list_url; ?>';
    var unique_hash = '<?php echo $unique_hash; ?>';
    var message_alert_delete = "<?php echo $this->l('alert_delete'); ?>";
    var crud_pagin = 1;
    var fragmentos = 1;
    var total_results = <?= $total_results ?>;
    var order_field = <?= empty($order_field)?'undefined':"'".$order_field."'" ?>;
</script>


<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>>
    <?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?>
</div>

<div class="card">
<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" data-orderUrl="'.$order_url.'" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">
     <div class="left-aside">
        <ul class="list-style-none">
            <li class="box-label">
                <a href="javascript:void(0)" class="ajax_refresh_and_loading" tabindex="0" aria-controls="dynamic-table" data-original-title="" title="">Refrescar listado</a>
            </li>
            <?php if(!$unset_export && empty($_GET['filtro'])): ?>
            <li class="box-label">
                <a class="btn btn-primary" href="<?php echo $export_url; ?>" title="Exportar en excel" style="color: #fff;text-align: left;padding: 10px;width: 48%;">
                  Exportar <i class="fas fa-download"></i>
                </a>
            </li>
            <?php endif; ?>
            <?php
                if(!$unset_export && !empty($_GET['filtro'])):
                $urlget = '?';
                foreach($_GET['filtro'] as $n=>$v){
                    $urlget.= 'filtro['.$n.'] = '.$v.'&';
                }
            ?>
            <li class="box-label">
                <a class="btn btn-primary" href="<?php echo $export_url.$urlget; ?>" title="Exportar en excel" style="color: #fff;text-align: left;padding: 10px;width: 48%;">
                  Exportar <i class="fas fa-download"></i>
                </a>
            </li>
            <?php endif; ?>
            <?php if(!$unset_print): ?>
            <li class="box-label">
                <a href="<?php echo $print_url; ?>" tabindex="0" aria-controls="dynamic-table" data-original-title="Imprimir listado" title="">Imprimir</a>
            </li>
            <?php endif; ?>
            <li class="box-label">
                <select name="per_page" id='per_page'  aria-controls="dynamic-table" class="form-control input-sm per_page">
                    <?php foreach ($paging_options as $option) { ?>
                        <option value="<?php echo $option; ?>" <?php if ($option == $default_per_page) { ?>selected="selected"<?php } ?>><?php echo $option; ?>&nbsp;&nbsp;</option>
                    <?php } ?>
                </select> por pagina
            </li>
        </ul>
    </div>
    <!-- /.left-aside-column-->
    <div class="right-aside">
        <div class="right-page-header">
            <div class="d-flex">
                <div class="align-self-center">
                    <h4 class="card-title m-t-10"><?php echo $subject?> </h4>
                </div>
                <div class="ml-auto">
                    <select name="search_field" id="search_field" class="form-control">
                        <option value=""><?php echo $this->l('list_search_all');?></option>
                        <?php foreach($columns as $column){?>
                        <option value="<?php echo $column->field_name?>"><?php echo $column->display_as?>&nbsp;&nbsp;</option>
                        <?php }?>
                    </select>
                    <input type="text" id="demo-input-search2" placeholder="Buscar en la tabla" name="search_text" class="form-control">
                </div>
            </div>
        </div>
        <div class="table-responsive">


            <?php if (!empty($list)): ?>
            <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                <thead>
                    <tr>
                        <?php foreach ($columns as $column): ?>
                            <th class="field-sorting sorting" rel='<?php echo $column->field_name ?>'>
                                <?php echo $column->display_as ?>
                                <span id="th_<?= $column->field_name ?>"></span>
                                <?php if (isset($order_by[0]) && $column->field_name == $order_by[0]) { ?>
                                    <?php if ($order_by[1] == 'asc'): ?>
                                        <i class="fa fa-arrow-up"></i>
                                    <?php else: ?>
                                        <i class="fa fa-arrow-down"></i>
                                <?php endif; ?>
                            <?php } ?>
                            </th>
                        <?php endforeach ?>
                        <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)): ?>
                            <th align="right" abbr="tools" axis="col1" class="" width='20%'>
                                <div class="text-right">
                                    <?php echo $this->l('list_actions'); ?>
                                </div>
                            </th>
                        <?php endif ?>
                    </tr>
                </thead>
                <tbody class="ajax_list">
                    Consultando datos.
                </tbody>
            </table>
            <table class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                <tfoot>
                    <tr>
                        <td>
                            <a href="<?php echo $add_url?>" class="btn btn-info btn-rounded"><?php echo $this->l('list_add'); ?> <?php echo $subject?></a>
                        </td>
                        <td>
                            <div id="dynamic-table_paginate" class="dataTables_paginate paging_simple_numbers pageContent" style="text-align:right">
                                    <ul class="pagination" style="float:right">
                                    </ul>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
             <?php else: ?>
                <table id="demo-foo-addrow" class="table m-t-30 table-hover no-wrap contact-list" data-page-size="10">
                    <tbody>
                        <tr><td colspan="" rowspan="" headers="">Sin datos para mostrar</td></tr>
                    </tbody>
                    <tfoot>
                    <?php if(!$unset_add): ?>
                        <tr>
                            <td>
                                <a href="<?php echo $add_url?>" class="btn btn-info btn-rounded"><?php echo $this->l('list_add'); ?> <?php echo $subject?></a>
                            </td>
                        </tr>
                    <?php endif ?>
                </tfoot>
                </table>
                <div>

                </div>
            <?php endif; ?>


        </div>
        <!-- .left-aside-column-->
    </div>
    <!-- /.left-right-aside-column-->











</div>

<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>

<?php if(!empty($_GET['filtro'])): ?>
    <?php foreach($_GET['filtro'] as $n=>$v): ?>
        <input type="hidden" name="filtro[<?= $n ?>]" value="<?= $v ?>">
    <?php endforeach ?>
<?php endif ?>
<?php echo form_close() ?>
</div>
